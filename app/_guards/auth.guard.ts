﻿import { FichaPersonaDataService } from './../pages/fichaPersona/fichaPersonaData.service';
import { ApiBackEndService } from './../_services/apiBackEnd.service';
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private apiBackEndService: ApiBackEndService) { }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (localStorage.getItem('currentUser') && localStorage.getItem('CodUsuario')) {
            this.apiBackEndService.currentUser  = JSON.parse(localStorage.getItem("currentUser"));
            this.apiBackEndService.CodUsuario  = localStorage.getItem("CodUsuario");

            await this.apiBackEndService.getOrganizacionesByUsuario().then(() => {
                //console.log('Entre getOrganizacionesUsuario');
            })
            // logged in so return true
            return true;
        }

        // not logged in so redirect to login page with the return url
        console.log(state.url);
        //this.router.navigate(['/Login'], { queryParams: { returnUrl: state.url }, replaceUrl: true});
        this.router.navigate(['/Login', state.url]);
        //this.router.navigateByUrl('/#/Login/' + state.url);
        return false;
    }
}
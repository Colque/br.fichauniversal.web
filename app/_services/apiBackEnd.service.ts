import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, Subscriber } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch'; // don't forget this, or you'll get a runtime error
import { Router, ActivatedRoute } from '@angular/router';

import { Http, Headers, Response, RequestOptions } from '@angular/http';//este muestra error

import { HttpClientService } from './http-client.service';


@Injectable()
export class ApiBackEndService{
    public currentUser: any;//obtenemos los datos del token
    public CodUsuario: any;
    public timeOut: any = 20000;
    public enviroment: string;
    public organizacionesUsuario: any = [];
    public userSistemList: any = [];    

    public ApiFicha: string;
    private urlWebApi: string;
    private urlWebApiHabitat: string;
    private urlWebApiVivienda: string;
    private urlWebApiUniversal: string;
    
    private urlWebApiFusion: string;
    private urlWebApiCPI: string;
    private urlWebApiConin: string;
    private urlWebApiIA: string;

    private urlWebApiBase64:string;

    private HeadContentType = "application/json";
    private HeadAccept = "application/json";
    private HeadAuthorization = this.currentUser ? (this.currentUser.token_type.toString() + " " +  this.currentUser.access_token.toString()) : '';
    private NoCargarResultado: any;
    public NeedLogin: boolean;

    public apiKeyGoogleMap = 'AIzaSyAlOwvFc38yUX5KzDELWwOzenonIy8EKCE';

    public respuestaNeedLogin: Observable<any>;
    //persona
    public respuestaDatosPersonales: Observable<any>;
    public respuestaVivienda: Observable<any>;
    public respuestaFotos: Observable<any>;
    public respuestaVideos2D: Observable<any>;
    public respuestaVideos360: Observable<any>;
    public respuestaSalud: Observable<any>;
    public respuestaDiscapacidades: Observable<any>;
    public respuestaRiesgosSalud: Observable<any>;
    public respuestaEnfermedadesAgudas: Observable<any>;
    public respuestaEnfermedadesCronicas: Observable<any>;
    public respuestaVacunas: Observable<any>;
    public respuestaEmbarazo: Observable<any>;
    public respuestaEmbarazoFC: Observable<any>;
    public respuestaLactantes: Observable<any>;
    public respuestaAntecedentesAntropometricos: Observable<any>;
    public respuestaAntecedentesAntropometricosFC: Observable<any>;
    public respuestaAntecedentesNutricionalesPatologicos: Observable<any>;
    public respuestaRiesgosNinio: Observable<any>;
    public respuestaRiesgosEscolarAdolescente: Observable<any>;
    public respuestaRiesgosEmbarazo: Observable<any>;
    public respuestaEducacion: Observable<any>;
    public respuestaTrabajo: Observable<any>;
    public respuestaSanitariosServicios: Observable<any>;
    public respuestaInfraestructura: Observable<any>;
    public respuestaObservacionesUrgencias: Observable<any>;
    public respuestaFamilia: Observable<any>;
    public respuestaInteligenciaArtificial: Observable<any>;
    public respuestaAntropometria: Observable<any>;
    public respuestaOrigenesAntropometria: Observable<any>;
    public respuestaGraficoPE: Observable<any>;
    public respuestaGraficoTE: Observable<any>;
    public respuestaGraficoIMC: Observable<any>;
    public respuestaInformacionAdicional: Observable<any>;
    public respuestaSintys: Observable<any>;
    public respuestaFusionCasos: Observable<any>;
    public respuestaFusionArchivos: Observable<any>;
    public respuestaFusionIntervenciones: Observable<any>;
    public respuestaFusionChat: Observable<any>;
    public respuestaFusionTareas: Observable<any>;
    public respuestaCPI: Observable<any>;
    public respuestaConin: Observable<any>;

    public respuestaTematicasIA: Observable<any>;
    public respuestaResultadosIA: Observable<any>;

    //vivienda
    public respuestaUbicacionV: Observable<any>;
    public respuestaFotosV: Observable<any>;
    public respuestaVideos2DV: Observable<any>;
    public respuestaVideos360V: Observable<any>;
    public respuestaAguaColombia: Observable<any>;
    public respuestaPuntoInteresColombia: Observable<any>;
    public respuestaEquipamientosV: Observable<any>;
    public respuestaInfraestructuraV: Observable<any>;
    public respuestaIntegrantesV: Observable<any>;
    public respuestaInundacionesV: Observable<any>;
    public respuestaMaterialesV: Observable<any>;
    public respuestaObservacionesEncuestadorV: Observable<any>;
    public respuestaObservacionesUrgenciasV: Observable<any>;
    public respuestaRiesgosV: Observable<any>;
    public respuestaSanitariosServiciosV: Observable<any>;
    public respuestaInformacionAdicionalV: Observable<any>;
    
    
    public CodPersona: string;
    public CodVivienda: string;

    public meses = [
        { mes:"Enero"},{ mes:"Febrero"},{ mes:"Marzo"},{ mes:"Abril"},{ mes:"Mayo"},{ mes:"Junio"},{ mes:"Julio"},{ mes:"Agosto"},
        { mes:"Septiembre"},{ mes:"Octubre"},{ mes:"Noviembre"},{ mes:"Diciembre"}
    ]

    constructor(
        private http: HttpClient,
        private https: HttpClientService,
        private router: Router,
        private route: ActivatedRoute,
        //private authenticationService: AuthenticationService
        )
    {

        this.enviroment = 'produccion'; // set your eviroment

        switch (this.enviroment) {
            case 'localhost':
                this.urlWebApi = 'http://localhost:34480/api/Intermedia';
                this.urlWebApiHabitat = 'http://localhost:34480/api/Habitat';
                this.urlWebApiVivienda = "http://localhost:34480/api/IntermediaVivienda";
                this.urlWebApiUniversal = "http://localhost:34480/api/Universal";
                this.urlWebApiFusion = "https://apifusion.azurewebsites.net/api/";
                this.urlWebApiCPI = "https://apicpi.azurewebsites.net/api/admisiones/GetDatosAdmision";
                this.urlWebApiConin = "http://apicpiconin.azurewebsites.net/api/admisiones/GetDatosAdmision";
                this.urlWebApiIA = "https://mpiiabk.azurewebsites.net/api/";
                this.ApiFicha = 'http://localhost:34480/Token';
                break;

            case 'localhostCalidad':
                this.urlWebApi = 'http://localhost:34480/api/Intermedia';
                this.urlWebApiHabitat = 'http://localhost:34480/api/Habitat';
                this.urlWebApiVivienda = "http://localhost:34480/api/IntermediaVivienda";
                this.urlWebApiUniversal = "http://localhost:34480/api/Universal";
                this.urlWebApiFusion = "https://apifusion-quality.azurewebsites.net/api/";
                this.urlWebApiCPI = "https://apicpi-quality.azurewebsites.net/api/admisiones/GetDatosAdmision";
                this.urlWebApiConin = "https://apicpiconin-quality.azurewebsites.net/api/admisiones/GetDatosAdmision";
                this.urlWebApiIA = "https://mpiiabk.azurewebsites-quality.net/api/";
                this.urlWebApiBase64 = 'http://apisurvey-apisurveyquality.azurewebsites.net/api/multimedia/file/base64';
                this.ApiFicha = 'http://localhost:34480/Token';
                break;

            case 'calidad':
                this.urlWebApi = 'http://fichauniversalbackend-quality.azurewebsites.net/api/Intermedia';
                this.urlWebApiHabitat = 'http://fichauniversalbackend-quality.azurewebsites.net/api/Habitat';
                this.urlWebApiVivienda = "http://fichauniversalbackend-quality.azurewebsites.net/api/IntermediaVivienda";
                this.urlWebApiUniversal = "http://fichauniversalbackend-quality.azurewebsites.net/api/Universal";
                this.urlWebApiFusion = "https://apifusion-quality.azurewebsites.net/api/";
                // this.urlWebApiFusion = "http://192.168.3.62:5555/api/";
                this.urlWebApiCPI = "https://apicpi-quality.azurewebsites.net/api/admisiones/GetDatosAdmision";
                this.urlWebApiConin = "https://apicpiconin-quality.azurewebsites.net/api/admisiones/GetDatosAdmision";
                this.urlWebApiIA = "https://mpiiabk.azurewebsites-quality.net/api/";
                this.urlWebApiBase64 = 'http://apisurvey-apisurveyquality.azurewebsites.net/api/multimedia/file/base64';
                this.ApiFicha = 'http://fichauniversalbackend-quality.azurewebsites.net/Token';
                break;

            case 'produccion':
                this.urlWebApi = 'https://fichauniversalbackend.azurewebsites.net/api/Intermedia';
                this.urlWebApiHabitat = 'https://fichauniversalbackend.azurewebsites.net/api/Habitat';
                this.urlWebApiVivienda = "https://fichauniversalbackend.azurewebsites.net/api/IntermediaVivienda";
                this.urlWebApiUniversal = "https://fichauniversalbackend.azurewebsites.net/api/Universal";
                this.urlWebApiFusion = "https://apifusion.azurewebsites.net/api/";
                this.urlWebApiCPI = "https://apicpi.azurewebsites.net/api/admisiones/GetDatosAdmision";
                this.urlWebApiConin = "https://apicpiconin.azurewebsites.net/api/admisiones/GetDatosAdmision";
                this.urlWebApiIA = "https://mpiiabk.azurewebsites.net/api/";
                this.urlWebApiBase64 = 'http://apisurvey.azurewebsites.net/api/multimedia/file/base64';
                this.ApiFicha = 'https://fichauniversalbackend.azurewebsites.net/Token';
                break;

                case 'colombia':
                // this.urlWebApi = 'http://localhost:34480/api/Intermedia';
                // this.urlWebApiVivienda = "http://localhost:34480/api/IntermediaVivienda";
                this.urlWebApi = 'https://fichauniversalbackend-co.azurewebsites.net/api/Intermedia';
                this.urlWebApiVivienda = "https://fichauniversalbackend-co.azurewebsites.net/api/IntermediaVivienda";
                this.urlWebApiIA = "https://mpiiabk.azurewebsites.net/api/";
                this.urlWebApiBase64 = 'http://apisurvey.azurewebsites.net/api/multimedia/file/base64';
                this.ApiFicha = 'https://fichauniversalbackend-co.azurewebsites.net/Token';
                break;
        }
        
        
    }


    private handleError(error: Response) {
        this.NeedLogin = false;
        if(error.status == 401){
            // reset login status
            //this.authenticationService.logout();
            this.NeedLogin = true;
        }


    return Observable.throw(error.json().error || 'Server error');
    //return needLogin;
    }

    getLogedIn(): Observable<Array<any>>{
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };
        var Parent = this;        

            this.respuestaNeedLogin = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/LogedIn', { headers })
            //.catch(this.handleError)
            .catch(function(e){
                console.log(e.status);
                if(e.status == 401){
                    //Parent.router.navigate([""]);
                    Parent.route.params.subscribe((params:any) => {
                        if(typeof params !== "undefined"){
                            console.log(params);
                            if(params.returnUrl != ""){
                                //this.router.navigate([this.returnUrl]);
                                Parent.router.navigate(["#/Login/FichaPersona", Parent.getCodPersona()]);
                            }
                            
                        }
                    });
                };
                throw e;
            });

            return this.respuestaNeedLogin;

        
    }

    public getOrganizacionesByUsuario() {
        return new Promise((resolve, reject) => {
            this.getOrganizacionesUsuario().subscribe(resultado => {
                this.userSistemList = this.currentUser.CodSistemaPermisoList.toString().split(',');
                let listaOrganizaciones: string[] = resultado ? JSON.parse(resultado.toString()).reduce(function(result: any,organizacion: any) {
                    result = result || [];
                    result.push(organizacion.CodOrganizacion);
                    return result;
                }, []) : [];

                this.userSistemList.forEach((sistem:any) => {
                    listaOrganizaciones.push(sistem);
                });

                this.organizacionesUsuario = listaOrganizaciones;
                resolve();
            });
        });
    }

    getOrganizacionesUsuario():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        const url = this.urlWebApiUniversal + '/organizacionesUser?CodUsuario=' + this.CodUsuario;
        return this.https.get(url).map(response => response.json());
    }

    //PERSONA
    getPersonaDatosPersonales():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaDatosPersonales = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaDatosPersonales?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaDatosPersonales?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaVivienda():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            //this.respuestaVivienda = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaVivienda?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaVivienda?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaFotos():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaFotos = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaFotos?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaFotos?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getGrupoFamiliar():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            const url = this.urlWebApi + '/GrupoFamiliar?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaVideos2D():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaVideos2D = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaVideos2D?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaVideos2D?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaVideos360():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            this.respuestaVideos360 = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaVideos360?CodPersona=' + this.getCodPersona(), { headers });
            return this.respuestaVideos360;
        }
    }

    getPersonaSalud():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaSalud = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaSalud?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaSalud?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaDiscapacidades():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaDiscapacidades = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaDiscapacidades?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaDiscapacidades?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaRiesgosSalud():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaRiesgosSalud = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaRiesgosSalud?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaRiesgosSalud?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaEnfermedadesAgudas():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaEnfermedadesAgudas = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaEnfermedadesAgudas?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaEnfermedadesAgudas?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaEnfermedadesCronicas():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaEnfermedadesCronicas = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaEnfermedadesCronicas?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaEnfermedadesCronicas?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaVacunas():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaVacunas = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaVacunas?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaVacunas?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaEmbarazo():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaEmbarazo = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaEmbarazo?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaEmbarazo?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaEmbarazoFC():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaEmbarazoFC = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaEmbarazoFC?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaEmbarazoFC?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaLactantes():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaLactantes = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaLactantes?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaLactantes?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaAntecedentesAntropometricos():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaAntecedentesAntropometricos = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaAntecedentesAntropometricos?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaAntecedentesAntropometricos?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaAntecedentesAntropometricosFC():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaAntecedentesAntropometricosFC = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaAntecedentesAntropometricosFC?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaAntecedentesAntropometricosFC?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaAntecedentesNutricionales():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaAntecedentesNutricionalesPatologicos = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaAntecedentesNutricionalesPatologicos?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaAntecedentesNutricionalesPatologicos?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaRiesgosNinio():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaRiesgosNinio = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaRiesgosNinio?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaRiesgosNinio?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaRiesgosEscolarAdolescente():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaRiesgosEscolarAdolescente = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaRiesgosEscolarAdolescente?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaRiesgosEscolarAdolescente?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaRiesgosEmbarazo():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaRiesgosEmbarazo = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaRiesgosEmbarazo?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaRiesgosEmbarazo?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaEducacion():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaEducacion = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaEducacion?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaEducacion?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaTrabajo():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaTrabajo = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaTrabajo?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaTrabajo?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaSanitariosServicios():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaSanitariosServicios = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaSanitariosServicios?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaSanitariosServicios?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaInfraestructura():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            //this.respuestaInfraestructura = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaInfraestructura?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaInfraestructura?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaObservacionesUrgencias():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            //this.respuestaObservacionesUrgencias = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaObservacionesUrgencias?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaObservacionesUrgencias?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaFamilia():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            //this.respuestaFamilia = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaFamilia?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaFamilia?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaInteligenciaArtificial():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            //this.respuestaInteligenciaArtificial = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaInteligenciaArtificial?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaInteligenciaArtificial?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaAntropometria():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            //this.respuestaAntropometria = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaAntropometria?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaAntropometria?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaOrigenesAntropometria():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            //this.respuestaOrigenesAntropometria = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaOrigenesAntropometria?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaOrigenesAntropometria?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaGraficoPE():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            //this.respuestaGraficoPE = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaGraficoPE?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaGraficoPE?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaGraficoTE():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            //this.respuestaGraficoTE = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaGraficoTE?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaGraficoTE?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaGraficoIMC():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            //this.respuestaGraficoIMC = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaGraficoIMC?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaGraficoIMC?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaInformacionAdicional():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            //this.respuestaInformacionAdicional = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaInformacionAdicional?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaInformacionAdicional?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaSintys():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            //this.respuestaSintys = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaSintys?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaSintys?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaFusionCasos():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            const url = this.urlWebApiFusion + 'reports/v2/summary/cases/' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaFusionArchivos(_Case: string):Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaFusionArchivos = this.http.get<Observable<Array<any>>>(this.urlWebApiFusion + 'reports/multimedias/case/id/' + _Case, { headers })
            // .catch(function(e){
            //     if(e.status == 522){
            //         //this.getPersonaFusionArchivos();
            //     }else{
            //         console.log("ERROOOOOOOOOR", e);
            //     }
                
            //     throw e;
            // });

            const url = this.urlWebApiFusion + 'reports/multimedias/case/id/' + _Case;
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaFusionIntervenciones(_Case: string):Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaFusionIntervenciones = this.http.get<Observable<Array<any>>>(this.urlWebApiFusion + 'reports/interventions/case/id/' + _Case, { headers })
            // .catch(function(e){
            //     if(e.status == 522){
            //         this.getPersonaFusionIntervenciones(_Case);
            //     }else{
            //         console.log("ERROOOOOOOOOR", e);
            //     }
                
            //     throw e;
            // });

            const url = this.urlWebApiFusion + 'reports/interventions-full/case/id/' + _Case;
            return this.https.get(url).map(response => response.json()).timeout(this.timeOut);
        }
    }

    getchat(codCaso: any):Observable<Array<any>> {
        // console.log("ruben>>>", this.urlWebApiFusion + 'chat/report/case/code/' + codCaso);
        //const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };
        // this.respuestaFusionChat = this.http.get<Observable<Array<any>>>(this.urlWebApiFusion + 'chat/report/case/code/' + codCaso, { headers });        

        const url = this.urlWebApiFusion + 'chat/report/case/code/' + codCaso;
        //this.respuestaFusionChat = this.http.get<Observable<Array<any>>>(this.urlWebApiFusion + 'chat/report/case/code/' + codCaso, { headers }).timeout(this.timeOut);
        //console.log('respuestaFusionChat',this.https.get(url).map(response => response.json()));
        return this.https.get(url).map(response => response.json()).timeout(this.timeOut);;

    }

    getPersonaFusionTareas(_CodeCase: string):Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            // this.respuestaFusionTareas = this.http.get<Observable<Array<any>>>(this.urlWebApiFusion + 'case-tasks/report/case/code/' + _CodeCase, { headers })
            // .catch(function(e){
            //     if(e.status == 522){
            //         //this.getPersonaFusionTareas();
            //     }else{
            //         console.log("ERROOOOOOOOOR", e);
            //     }
                
            //     throw e;
            // });

            const url = this.urlWebApiFusion + 'case-tasks/report/case/code/' + _CodeCase;
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaCPI():Observable<Array<any>> {
        const headers = { 'Content-Type': 'application/x-www-form-urlencoded', 'Accept': this.HeadAccept/*, 'Authorization': this.HeadAuthorization */};

        if(this.cargarServicio("Persona")){
            this.respuestaCPI = this.http.get<Observable<Array<any>>>(this.urlWebApiCPI + '/' + this.getCodPersona(), { headers }).timeout(this.timeOut);
            return this.respuestaCPI;
            // const params = new URLSearchParams();
            // params.append('codPersona', this.getCodPersona());

            // return this.http.post<any>(this.urlWebApiCPI, params.toString(), { headers })
            // .map(result => {
            //    return this.respuestaCPI = result;
            // });
        }
    }

    getPersonaConin():Observable<Array<any>> {
        const headers = { 'Content-Type': 'application/x-www-form-urlencoded', 'Accept': this.HeadAccept/*, 'Authorization': this.HeadAuthorization */};

        if(this.cargarServicio("Persona")){
            this.respuestaConin = this.http.get<Observable<Array<any>>>(this.urlWebApiConin + '/' + this.getCodPersona(), { headers });
            return this.respuestaConin;
            // const params = new URLSearchParams();
            // params.append('codPersona', this.getCodPersona());

            // return this.http.post<any>(this.urlWebApiConin, params.toString(), { headers })
            // .map(result => {
            //    return this.respuestaConin = result;
            // });
        }
    }

    getPersonaTematicasIA():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            //this.respuestaTematicasIA = this.http.get<Observable<Array<any>>>(this.urlWebApiIA + 'tematicas/all', { headers });
            const url = this.urlWebApiIA + 'tematicas/all';
            return this.https.get(url).map(response => response.json());

        }
    }

    getPersonaResultadosIA(_CodTematica: string):Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            //this.respuestaResultadosIA = this.http.get<Observable<Array<any>>>(this.urlWebApiIA + 'personas/GetResultados/' + this.getCodPersona() + '/' + _CodTematica, { headers });
            const url = this.urlWebApiIA + 'personas/GetResultados/' + this.getCodPersona() + '/' + _CodTematica;
            return this.https.get(url).map(response => response.json());
            
        }
    }

    getPersonaTest():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            //this.respuestaVivienda = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaVivienda?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApi + '/PersonaTest?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }

    getPersonaHistoriaClinica():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Persona")){
            const url = this.urlWebApi + '/PersonaHistoriaClinica?CodPersona=' + this.getCodPersona();
            return this.https.get(url).map(response => response.json());
        }
    }
    

    //VIVIENDA
    getViviendaUbicacion():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Vivienda")){
            //this.respuestaUbicacionV = this.http.get<Observable<Array<any>>>(this.urlWebApiVivienda + '/ViviendaUbicacion?CodVivienda=' + this.getCodVivienda(), { headers });
            const url = this.urlWebApiVivienda + '/ViviendaUbicacion?CodVivienda=' + this.getCodVivienda();
            return this.https.get(url).map(response => response.json());
        }
    }

    getViviendaFotos():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Vivienda")){
            //this.respuestaFotosV = this.http.get<Observable<Array<any>>>(this.urlWebApiVivienda + '/ViviendaFotos?CodVivienda=' + this.getCodVivienda(), { headers });
            const url = this.urlWebApiVivienda + '/ViviendaFotos?CodVivienda=' + this.getCodVivienda();
            return this.https.get(url).map(response => response.json());
        }
    }

    getViviendaVideos2D():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Vivienda")){
            //this.respuestaVideos2DV = this.http.get<Observable<Array<any>>>(this.urlWebApiVivienda + '/ViviendaVideos2D?CodVivienda=' + this.getCodVivienda(), { headers });
            const url = this.urlWebApiVivienda + '/ViviendaVideos2D?CodVivienda=' + this.getCodVivienda();
            return this.https.get(url).map(response => response.json());
        }
    }

    getViviendaVideos360():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Vivienda")){
            this.respuestaVideos360V = this.http.get<Observable<Array<any>>>(this.urlWebApiVivienda + '/ViviendaVideos360?CodVivienda=' + this.getCodVivienda(), { headers });
            return this.respuestaVideos360V;
        }
    }

    getViviendaEquipamientos():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Vivienda")){
            //this.respuestaEquipamientosV = this.http.get<Observable<Array<any>>>(this.urlWebApiVivienda + '/ViviendaEquipamientos?CodVivienda=' + this.getCodVivienda(), { headers });
            const url = this.urlWebApiVivienda + '/ViviendaEquipamientos?CodVivienda=' + this.getCodVivienda();
            return this.https.get(url).map(response => response.json());
        }
    }

    getViviendaInfraestructura():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Vivienda")){
            //this.respuestaInfraestructuraV = this.http.get<Observable<Array<any>>>(this.urlWebApiVivienda + '/ViviendaInfraestructura?CodVivienda=' + this.getCodVivienda(), { headers });
            const url = this.urlWebApiVivienda + '/ViviendaInfraestructura?CodVivienda=' + this.getCodVivienda();
            return this.https.get(url).map(response => response.json());
        }
    }

    getViviendaIntegrantes():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Vivienda")){
            //this.respuestaIntegrantesV = this.http.get<Observable<Array<any>>>(this.urlWebApiVivienda + '/ViviendaIntegrantes?CodVivienda=' + this.getCodVivienda(), { headers });
            const url = this.urlWebApiVivienda + '/ViviendaIntegrantes?CodVivienda=' + this.getCodVivienda();
            return this.https.get(url).map(response => response.json());
        }
    }

    getViviendaInundaciones():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Vivienda")){
            //this.respuestaInundacionesV = this.http.get<Observable<Array<any>>>(this.urlWebApiVivienda + '/ViviendaInundaciones?CodVivienda=' + this.getCodVivienda(), { headers });
            const url = this.urlWebApiVivienda + '/ViviendaInundaciones?CodVivienda=' + this.getCodVivienda();
            return this.https.get(url).map(response => response.json());
        }
    }

    getViviendaMateriales():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Vivienda")){
            //this.respuestaMaterialesV = this.http.get<Observable<Array<any>>>(this.urlWebApiVivienda + '/ViviendaMateriales?CodVivienda=' + this.getCodVivienda(), { headers });
            const url = this.urlWebApiVivienda + '/ViviendaMateriales?CodVivienda=' + this.getCodVivienda();
            return this.https.get(url).map(response => response.json());
        }
    }

    getViviendaObservacionesEncuestador():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Vivienda")){
            //this.respuestaObservacionesEncuestadorV = this.http.get<Observable<Array<any>>>(this.urlWebApiVivienda + '/ViviendaObservacionesEncuestador?CodVivienda=' + this.getCodVivienda(), { headers });
            const url = this.urlWebApiVivienda + '/ViviendaObservacionesEncuestador?CodVivienda=' + this.getCodVivienda();
            return this.https.get(url).map(response => response.json());
        }
    }

    getViviendaObservacionesUrgencias():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Vivienda")){
            //this.respuestaObservacionesUrgenciasV = this.http.get<Observable<Array<any>>>(this.urlWebApiVivienda + '/ViviendaObservacionesUrgencias?CodVivienda=' + this.getCodVivienda(), { headers });
            const url = this.urlWebApiVivienda + '/ViviendaObservacionesUrgencias?CodVivienda=' + this.getCodVivienda();
            return this.https.get(url).map(response => response.json());
        }
    }

    getViviendaRiesgos():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Vivienda")){
            //this.respuestaRiesgosV = this.http.get<Observable<Array<any>>>(this.urlWebApiVivienda + '/ViviendaRiesgos?CodVivienda=' + this.getCodVivienda(), { headers });
            const url = this.urlWebApiVivienda + '/ViviendaRiesgos?CodVivienda=' + this.getCodVivienda();
            return this.https.get(url).map(response => response.json());
        }
    }

    getRiesgosVivienda():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Vivienda")){
            // this.respuestaRiesgosNinio = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaRiesgosNinio?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApiVivienda + '/RiesgosVivienda?CodVivienda=' + this.getCodVivienda();
            return this.https.get(url).map(response => response.json());
        }
    }

    getRiesgosFamilia():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Vivienda")){
            // this.respuestaRiesgosNinio = this.http.get<Observable<Array<any>>>(this.urlWebApi + '/PersonaRiesgosNinio?CodPersona=' + this.getCodPersona(), { headers });
            const url = this.urlWebApiVivienda + '/RiesgosFamilia?CodVivienda=' + this.getCodVivienda();
            return this.https.get(url).map(response => response.json());
        }
    }

    getViviendaAguaColombia():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Vivienda")){
            const url = this.urlWebApiVivienda + '/ViviendaAguaColombia?CodVivienda=' + this.getCodVivienda();
            this.respuestaAguaColombia = this.https.get(url).map(response => response.json());
            return this.respuestaAguaColombia;
        }
    }
    getViviendaSanitariosServicios():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Vivienda")){
            //this.respuestaSanitariosServiciosV = this.http.get<Observable<Array<any>>>(this.urlWebApiVivienda + '/ViviendaSanitariosServicios?CodVivienda=' + this.getCodVivienda(), { headers });
            const url = this.urlWebApiVivienda + '/ViviendaSanitariosServicios?CodVivienda=' + this.getCodVivienda();
            return this.https.get(url).map(response => response.json());
        }
    }

    getViviendaPuntoInteres():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Vivienda")){
            //this.respuestaPuntoInteresColombia = this.http.get<Observable<Array<any>>>(this.urlWebApiVivienda + '/ViviendaPuntoInteres?CodVivienda=' + this.getCodVivienda(), { headers });
            const url = this.urlWebApiVivienda + '/ViviendaPuntoInteres?CodVivienda=' + this.getCodVivienda();
            return this.https.get(url).map(response => response.json());
        }
    }

    getViviendaInformacionAdicional():Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        if(this.cargarServicio("Vivienda")){
            //this.respuestaInformacionAdicionalV = this.http.get<Observable<Array<any>>>(this.urlWebApiVivienda + '/ViviendaInformacionAdicional?CodVivienda=' + this.getCodVivienda(), { headers });
            const url = this.urlWebApiVivienda + '/ViviendaInformacionAdicional?CodVivienda=' + this.getCodVivienda();
            return this.https.get(url).map(response => response.json());
        }
    }

    getImagenBase64(url:any) {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };
        //const options = new RequestOptions({ headers: headers });

        let body = JSON.stringify({ fileName: url}); 
        return this.http.post(this.urlWebApiBase64, body, {headers});
    }

    getCalendarioVacunas(anio:string) {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        const url = this.urlWebApi + '/GetCatalogoVacunas';
        return this.https.get(url).map(response => response.json());
    }

    getEdadesCalendario(anio:string) {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        const url = this.urlWebApi + '/GetEdadesCalendario';
        return this.https.get(url).map(response => response.json());
    }

    getVacunasDosis(anio:string, codPersona: string) {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        const url = this.urlWebApi + '/GetVacunasDosis';
        return this.https.get(url).map(response => response.json());
    }

    /************************/

    private getCodPersona():string{
        return this.CodPersona;
    }

    private getCodVivienda():string{
        return this.CodVivienda;
    }

    private cargarServicio(entidad:string): boolean{
        var cargar = true;

        switch(entidad){
            case "Persona":
                if(typeof this.getCodPersona() === "undefined" || this.getCodPersona() == ""){
                    cargar = false;
                }
            break;
            case "Vivienda":
                if(typeof this.getCodVivienda() === "undefined" || this.getCodVivienda() == ""){
                    cargar = false;
                }
            break;
        }

        return cargar;
    }

    getBase64Foto(urlBase: string):Observable<Array<any>> {
        const headers = { 'Content-Type': this.HeadContentType, 'Accept': this.HeadAccept, 'Authorization': this.HeadAuthorization };

        const url = this.urlWebApiHabitat + '/getBase64?url=' + urlBase;
        return this.https.get(url).map(response => response.json());
    }

    public getBase64(url:string){
        return new Promise((resolve, reject) => {
            if (url && url != ""){
                var proxyUrl = 'https://cors-anywhere.herokuapp.com/';
                var mThis = this;
                //var url = 'http://upload.wikimedia.org/wikipedia/commons/4/4a/Logo_2013_Google.png';  
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {
                    var reader = new FileReader();
                    reader.onloadend = function() {
                        var img = new Image;
                        img.onload = function() {
                            let columns = img.width > img.height ? 2 : 3;
                            resolve({ base64: reader.result.replace('multipart/form-data','image/jpeg').replace('image/png','image/jpeg'), columns: columns});
                        };
                        img.src = reader.result;
                    }
                    reader.readAsDataURL(xhr.response);
                };
                xhr.onerror= function(e) {
                    console.log('onerror',e);
                    resolve(null);
                };
                xhr.open('GET', proxyUrl + url);
                xhr.responseType = 'blob';
                xhr.send();
            } else {
                resolve(null)
            }
        });
    }

    _arrayBufferToBase64(buffer:any) {
        var binary = '';
        var bytes = new Uint8Array(buffer);
        var len = bytes.byteLength;
        for (var i = 0; i < len; i++) {
            binary += String.fromCharCode(bytes[i]);
        }
        return window.btoa(binary);
    };

    public ordernarArrayPorFecha(array: any, column: any) {
            array.sort((a:any, b:any) => {
                if (new Date(a[column]).getTime() < new Date(b[column]).getTime()) {
                    return -1;
                }
                if (new Date(a[column]).getTime() > new Date(b[column]).getTime()) {
                    return 1;
                }
                return 0;
            });
            return array;
    }

}
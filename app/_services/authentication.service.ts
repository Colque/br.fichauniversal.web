﻿import { ApiBackEndService } from './apiBackEnd.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
    //public enviroment: string;
    private ApiFicha: string;

    constructor(private http: HttpClient, private apiBackEndService: ApiBackEndService) { 
        //this.enviroment = 'calidad'; // set your eviroment

        this.ApiFicha = this.apiBackEndService.ApiFicha;
    }
    

    login(username: string, password: string) {
        const headers = new HttpHeaders();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        const params = new URLSearchParams();
        params.append('grant_type', 'password');
        params.append('username', username);
        params.append('password', password);

        return this.http.post<any>(this.ApiFicha, params.toString(), { headers })
            .map(user => {
                console.log('user',user);
                // login successful if there's a jwt token in the response
                if (user && user.access_token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    localStorage.setItem('CodUsuario', user.UserCode);
                }

                return user;
            });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('CodUsuario');
    }
}
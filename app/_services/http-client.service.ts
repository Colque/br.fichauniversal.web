import { Router } from '@angular/router';
/**
 * @fileoverview Wraper de la libreria http que agrega las cabeceras basicas
 * necesarias.
 *
 * @author Carlos F. Colque
 * @version 0.1.1
 */

import { Injectable, Inject, forwardRef } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';


/**
 * Servicio creado en base a la clase Http de @Angular que implementa cabeceras y
 * el token de autenticacion automaticamente.
 */
@Injectable()
export class HttpClientService {
    /**
     * @constructor
     */
    private currentUser = JSON.parse(localStorage.getItem("currentUser"));//obtenemos los datos del token
    private HeadAuthorization = JSON.parse(localStorage.getItem("currentUser")) ? (JSON.parse(localStorage.getItem("currentUser")).token_type.toString() + " " +  JSON.parse(localStorage.getItem("currentUser")).access_token.toString()) : '';

    constructor (
        private router: Router,
        private http: Http
    ) {}


    /**
     * Realiza una peticion de tipo GET a la url solicitada, implementa el
     * token de authenticacion automaticamente, ademas de setear el formato
     * esperado a JSON.
     * Generalmente las peticiones de tipo GET siguiento el esquema REST se 
     * utilizan para consultar datos.
     *
     * @param {string} url url de la peticon
     * @return {Observable}
     */
    get(url: string) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': JSON.parse(localStorage.getItem("currentUser")) ? (JSON.parse(localStorage.getItem("currentUser")).token_type.toString() + " " +  JSON.parse(localStorage.getItem("currentUser")).access_token.toString()) : ''
        });
        const options = new RequestOptions({ headers: headers });
        // console.log('get', url, 'options', options);
        return this.http.get(url, options).map(response => {
            return response;
        }).catch(err => {
            console.log('err', err);
            const _err = err.json();
            //console.log('_err', _err);
            if (err.status === 401) { // Unauthorized
                localStorage.removeItem('currentUser');
                localStorage.removeItem('CodUsuario');
                location.reload();
                //this.router.navigate(['/login']);
            }
            return Observable.throw(err.json());
        });
    }


    /**
     * Realiza una peticion de tipo POST a la url solicitada, implementa el
     * token de authenticacion automaticamente, ademas de setear el formato
     * esperado a JSON.
     * Generalmente las peticiones de tipo POST siguiento el esquema REST se 
     * utilizan para insertar datos.
     * 
     * @param {string} url url de la peticon
     * @param {string} body JSON string con los datos formateados a string
     * @return {Observable}
     */
    post(url: string, body: any) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': this.HeadAuthorization
        });
        const options = new RequestOptions({ headers: headers });
        return this.http.post(url, body, options);
    }


     /**
     * Realiza una peticion de tipo PUT a la url solicitada, implementa el
     * token de authenticacion automaticamente, ademas de setear el formato
     * esperado a JSON.
     * Generalmente las peticiones de tipo PUT siguiento el esquema REST se
     * utilizan para actualizar datos.
     * 
     * @param {string} url url de la peticon
     * @param {string} body JSON string con los datos formateados a string
     * @return {Observable}
     */
    put(url: string, body: any) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': this.HeadAuthorization
        });
        const options = new RequestOptions({ headers: headers });
        return this.http.put(url, body, options);
    }


    /**
     * Realiza una peticion de tipo DELETE a la url solicitada, implementa el
     * token de authenticacion automaticamente, ademas de setear el formato
     * esperado a JSON.
     * Generalmente las peticiones de tipo DELETE siguiento el esquema REST
     * se utilizan para Borrar Elementos
     * 
     * @param {string} url url de la peticon
     * @return {Observable}
     */
    delete(url: string) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': this.HeadAuthorization
        });
        const options = new RequestOptions({ headers: headers });
        return this.http.delete(url, options);
    }
}


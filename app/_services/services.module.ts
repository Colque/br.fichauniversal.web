import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { HttpClientService } from './http-client.service';
import { AlertService } from './alert.service';
import { AuthenticationService } from './authentication.service';
import { UserService } from './user.service';
import { ApiBackEndService } from './apiBackEnd.service';

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
    ],
    providers: [
        HttpClientService,
        AlertService,
        AuthenticationService,
        UserService,
        ApiBackEndService
    ],
})
export class ServicesModule {}

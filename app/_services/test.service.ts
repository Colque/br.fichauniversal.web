import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

//import { User } from '../_models/index';

@Injectable()
export class TestService {
    constructor(private http: HttpClient) { }

    getTest() {
        var currentUser = JSON.parse(localStorage.getItem("currentUser"));

        const headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': currentUser ? (currentUser.token_type.toString() + " " +  currentUser.access_token.toString()) : '',
        }

        return this.http.get<any>('http://localhost:34480/api/Orders', { headers });
    }
}
﻿import { VersionCheckService } from './version-check.service';
import { Component } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import { ApiBackEndService } from './_services/index';
import { Favicons } from "./favicons";

@Component({
    moduleId: module.id,
    selector: 'app',
    templateUrl: 'app.component.html'
})

export class AppComponent { 
    constructor(
        title:Title,
        public favicons: Favicons,
        public apiBackEndService: ApiBackEndService,
        private versionCheckService: VersionCheckService
    ) {
        if (this.apiBackEndService.enviroment === "colombia"){
            title.setTitle('Ficha Universal - Colombia');
            this.favicons.activate( 'colombia' );
        } else{
            title.setTitle('Ficha Universal - MPI');
            this.favicons.activate( 'argentina' );
        }
    }
    ngOnInit(){
        this.versionCheckService.initVersionCheck('./version.json');
    }
}

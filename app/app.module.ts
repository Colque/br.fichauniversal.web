﻿import { VersionCheckService } from './version-check.service';
import { NgModule }      from '@angular/core';
import { BrowserModule, Title  } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { BROWSER_FAVICONS_CONFIG } from "./favicons";
import { BrowserFavicons } from "./favicons";
import { Favicons } from "./favicons";

// used to create fake backend
import { fakeBackendProvider } from './_helpers/index';

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';

import { AlertComponent } from './_directives/index';
import { AuthGuard } from './_guards/index';
import { JwtInterceptor } from './_helpers/index';
import {
    AlertService,
    AuthenticationService,
    UserService,
    ApiBackEndService
} from './_services/index';

import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';

import { Md2Module, Md2Dialog, Md2DialogConfig  } from 'Md2';
import {BrowserAnimationsModule} from  "@angular/platform-browser/animations";
import { TimelineComponent } from './components/timeline/timeline.component';
import { MaterialModule } from "@angular/material";

// import { SafePipe } from './myjs/utils/HtmlPipe';
import { RegisterComponent } from './register/register.component'
// import { ChatWindowComponent } from './components/chat/chat-window/chat-window.component';
import { DescargarFichaPComponent } from './components/descargarFichaP/descargarFichaP.component'
import { PagesModule } from './pages/pages.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BlockUIModule } from 'ng-block-ui';
import { BlockTemplateComponent } from './components/block-template/block-template.component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        HttpModule,
        routing,
        BrowserAnimationsModule,
        MaterialModule,
        Md2Module,
        PagesModule,
        NgbModule.forRoot(),
        BlockUIModule.forRoot({
            template: BlockTemplateComponent
        })
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        DescargarFichaPComponent,
        BlockTemplateComponent
        // SafePipe,
    ],
    entryComponents: [ BlockTemplateComponent ],
    exports: [
        //TimelineComponent
    ],
    providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
        VersionCheckService,
        //aqui nuestro provider
        ApiBackEndService,

        // provider used to create fake backend
        fakeBackendProvider,
        Title,

        // The Favicons is an abstract class that represents the dependency-injection
		// token and the API contract. THe BrowserFavicon is the browser-oriented
		// implementation of the service.
		{
			provide: Favicons,
			useClass: BrowserFavicons
		},
		// The BROWSER_FAVICONS_CONFIG sets up the favicon definitions for the browser-
		{
			provide: BROWSER_FAVICONS_CONFIG,
			useValue: {
				icons: {
					"argentina": {
						type: "image/png",
						href: "../../Content/Images/favicon-fu-bold-13.png",
						isDefault: true
					},
					"colombia": {
						type: "ico",
						href: "../../Content/Images/favicon-colombia.ico"
					},
				},

				// I determine whether or not a random token is auto-appended to the HREF
				// values whenever an icon is injected into the document.
				cacheBusting: true
			}
		}
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
﻿import { Routes, RouterModule } from '@angular/router';

//import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { AuthGuard } from './_guards/index';

// import { FichaPersonaComponent } from './fichaPersona/index';
// import { FichaViviendaComponent } from './fichaVivienda/index';

const appRoutes: Routes = [
    //{ path: '', component: HomeComponent, canActivate: [AuthGuard] },

    // { path: 'FichaPersona/:p', component: FichaPersonaComponent, canActivate: [AuthGuard] },
    // { path: 'FichaVivienda/:v', component: FichaViviendaComponent, canActivate: [AuthGuard] },
        
    //{ path: 'login', component: LoginComponent },
    { path: 'Login/:returnUrl', component: LoginComponent },
    { path: 'Login', component: LoginComponent},

    // otherwise redirect to home
    { path: '**', redirectTo: 'Login' }
];

export const routing = RouterModule.forRoot(appRoutes, {useHash: true});

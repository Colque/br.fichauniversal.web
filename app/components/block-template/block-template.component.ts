import { Component } from '@angular/core';

/**
 * Plantilla personalizada para block-ui
 * @author Carlos F. Colque
 */
@Component({
    selector: 'app-block-temp',
    templateUrl: 'block-template.component.html',
    styleUrls: ['block-template.component.scss']
})
export class BlockTemplateComponent {
    public message: string;

    constructor() {}
}

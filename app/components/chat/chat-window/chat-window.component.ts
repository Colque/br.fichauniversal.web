import {
    Component,
    Inject,
    ElementRef,
    OnInit,
    ChangeDetectionStrategy, Input, ViewChild
} from '@angular/core';
import { Observable } from 'rxjs';

// import { User } from '../user/user.model';
// import { UsersService } from '../user/users.service';
// import { Thread } from '../thread/thread.model';
// import { ThreadsService } from '../thread/threads.service';
// import { Message } from '../message/message.model';
// import { MessagesService } from '../message/messages.service';

@Component({
    selector: 'chat-window',
    templateUrl: './chat-window.component.html',
    styleUrls: ['./chat-window.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatWindowComponent implements OnInit {
    @Input() messages: any;
    @ViewChild('container') container: ElementRef;

    // public messages: any[] = [{authorAvatarSrc : 'author', sender: 'sender', sentAt:'senta'},
    //     {authorAvatarSrc : 'author', sender: 'sender', sentAt:'senta'},
    //     {authorAvatarSrc : 'author', sender: 'sender', sentAt:'senta'},
    //     {authorAvatarSrc : 'author', sender: 'sender', sentAt:'senta'},
    //     {authorAvatarSrc : 'author', sender: 'sender', sentAt:'senta'},
    //     {authorAvatarSrc : 'author', sender: 'sender', sentAt:'senta'}];

    // messages: Observable<any>;
    //   private message: any[];
    // currentThread: Thread;
    // draftMessage: Message;
    // currentUser: User;
    userColors: any[] = [];

    constructor(// public messagesService: MessagesService,
        // public threadsService: ThreadsService,
        // public UsersService: UsersService,
        public el: ElementRef) {
    }

    ngOnInit(): void {
    }

    getUserColor(codUser: any) {
        let color = "";
        let u: any = this.userColors.filter(m => {
            return m.codUser === codUser
        });
        if (u.length) {
            color = u[0].color;
        } else {
            color = this.getRandomColor();
            this.userColors.push({codUser: codUser, color: color})
        }
        return color;
    }


    private getColor() {
        var x = Math.round(0xffffff * Math.random()).toString(16);
        var y = (6 - x.length);
        var z = '000000';
        var z1 = z.substring(0, y);
        var color = '#' + z1 + x;
        return color;
    }

    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    public camelize(text: any) {
        let separator = " ";
        // Cut the string into words
        let words = text.split(separator);

        // Concatenate all capitalized words to get camelized string
        let result = "";
        for (let i = 0; i < words.length; i++) {
            let word = words[i];
            let capitalizedWord = word.charAt(0).toUpperCase() + word.slice(1);
            result += capitalizedWord;
        }
        return result;
    }

    getDate  (dateString: any) : number{
        let dateParts = dateString.split(" ")[0].split("/");
        let dateObject = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]); // month is 0-based
        if(dateObject.getTime()!==this.currentTime){
            this.currentTime = dateObject.getTime();
        }
        return dateObject.getTime();
    }

    getDateTime(dateString: any) : any{
        let dateParts = dateString.split(" ")[0].split("/");
        let dateObject = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]); // month is 0-based
        return dateObject;
    }

    currentTime = 0;
}

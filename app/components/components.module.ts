//import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TimelineComponent } from './timeline/timeline.component';
import { TimelineCPIComponent } from './timelineCPI/timelineCPI.component';
import { TimelineFusionComponent } from './timelineFusion/timelinefusion.component';
//import { DescargarFichaPComponent } from './descargarFichaP/descargarFichaP.component';
import { ChatWindowComponent } from './chat/chat-window/chat-window.component';
import { BlockUIModule } from 'ng-block-ui';
//import { BlockTemplateComponent } from './block-template/block-template.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { MaterialModule } from '@angular/material';
import { Md2Module, Md2Dialog, Md2DialogConfig } from 'Md2';
import { PreviewComponent } from './timeline/preview/preview.component';
import { PreviewCPIComponent } from './timelineCPI/preview/previewCPI.component';
import { PreviewFusionComponent } from './timelineFusion/preview/previewFusion.component';

import { TimelineNewComponent } from './timelineNew/timelineNew.component';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        NgbModule,
        MaterialModule,
        Md2Module,
        CommonModule,
        BlockUIModule
    ],
    declarations: [
        TimelineComponent,
        TimelineCPIComponent,
        TimelineFusionComponent,
        TimelineNewComponent,
        //DescargarFichaPComponent,
        ChatWindowComponent,
        PreviewComponent,
        PreviewCPIComponent,
        PreviewFusionComponent
        //BlockTemplateComponent
    ],
    exports: [
        TimelineComponent,
        TimelineCPIComponent,
        TimelineFusionComponent,
        TimelineNewComponent,
        //DescargarFichaPComponent,
        ChatWindowComponent,
        PreviewComponent,
        PreviewCPIComponent,
        PreviewFusionComponent
        //BlockTemplateComponent
    ],
    entryComponents: [
        
    ],
    providers: [],
})
export class ComponentsModule {}

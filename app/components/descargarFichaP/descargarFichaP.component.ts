import { Component, OnInit, ViewChild } from '@angular/core';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'descargar-ficha-p',
  templateUrl: './descargarFichaP.component.html'
})
export class DescargarFichaPComponent implements OnInit {
  closeResult: string;
  @ViewChild('content', undefined) content: NgbModal;
  constructor(private modalService: NgbModal) {}

    ngOnInit() {
        //this.getDatos();
        this.open(this.content);
    }

  public open(content: any) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
export const MONTHS: string[] = [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre',
];

export const CATEGORIES_ATTR = {
    vivienda: {color: '#808080', name: 'Vivienda', orden: 1},
    persona: {color: '#f95f5f', name: 'Persona', orden: 2},
    infraestructura: {color: '#5b3c82', name: 'Infraestructura', orden: 3},
    otro: {color: '#3f9e83', name: 'Otros', orden: 4}
};

export const TYPES_ATTR = {imagen: 'Imagen', audio: 'Audio', video: 'Video', pdf: 'PDF'};

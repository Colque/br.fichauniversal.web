export class TLDayMarker {
  name: string;
  left: number;
  opacity: number;
  date: Date;

  constructor(name: string, left: number, opacity: number, date: Date) {
    this.name = name;
    this.left = left;
    this.opacity = opacity;
    this.date = date;
  }
}

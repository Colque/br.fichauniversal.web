import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { slideInOutAnimation } from '../animation';
import { slideInOutAnimationFullScreen } from '../animation_full_screen';
import { TLItem } from '../model/tl-item';
import { CATEGORIES_ATTR } from '../tl-settings';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-timeline-previewCPI',
    templateUrl: './previewCPI.component.html',
    styleUrls: ['../timelineCPI.component.css', './previewCPI.component.css'],
    animations: [slideInOutAnimation, slideInOutAnimationFullScreen]
})
export class PreviewCPIComponent implements OnInit {
    @Input() itemdate: TLItem;
    @Input() orientation: TLItem;
    @Input() disabled = false;
    @Output() onImageClickEvent: EventEmitter<any> = new EventEmitter();
    errorThumbnail = false;

    constructor(private sanitizer: DomSanitizer) {

    }

    ngOnInit(): void {

    }

    getCategory(item: string) {
        return CATEGORIES_ATTR[item];
    }

    openNav() {
        console.log('itemdate', this.itemdate);
        this.onImageClickEvent.emit(this.itemdate);
    }

    getUrlPdf() {
        return this.sanitizer.bypassSecurityTrustResourceUrl(this.itemdate.url + '?#view=FitH');
    }

    errorHandlerLoad(event: any) {
        this.errorThumbnail = true;
        event.target.src = '/assets/media/images/caja_transparente.png';
    }
}

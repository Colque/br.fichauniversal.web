import { FichaPersonaDataService } from './../../pages/fichaPersona/fichaPersonaData.service';
import {
    ChangeDetectorRef,
    Component,
    DoCheck,
    ElementRef,
    HostListener,
    Input,
    IterableDiffers,
    OnInit, QueryList,
    ViewChild, ViewChildren
} from '@angular/core';
import { slideInOutAnimation } from './animation';
import { TLDayMarker } from './model/tl-day-marker';
import { TLItem } from './model/tl-item';
import { TLMonthsMarker } from './model/tl-months-marker';
import { slideInOutAnimationFullScreen } from './animation_full_screen';
import { NgxGalleryOptions, NgxGalleryAnimation, NgxGalleryOrder } from 'ngx-gallery';
import { DomSanitizer, SafeUrl }  from '@angular/platform-browser';
import { CATEGORIES_ATTR, MONTHS, TYPES_ATTR } from './tl-settings';
import { invalid } from 'moment';

declare var galeria: any;

export type Orientation = ( 'prev' | 'next' | 'none' );

export enum KEY_CODE {
    RIGHT_ARROW = 39,
    LEFT_ARROW = 37,
    ESC = 27
}

@Component({
    selector: 'app-timelineCPI',
    templateUrl: 'timelineCPI.component.html',
    styleUrls: ['timelineCPI.component.css'],
    animations: [slideInOutAnimation, slideInOutAnimationFullScreen]
})
export class TimelineCPIComponent implements OnInit {
    @ViewChild('dragElem') dragElem: ElementRef;
    @ViewChild('moveElem') moveElem: ElementRef;
    @Input() items: TLItem[] = [];


    @ViewChild('carousel') carousel: any;

    options: { [k: string]: any } = {};
    public selectedItem: TLItem;
    public prevItem: TLItem;
    public nextItem: TLItem;
    animate = true;
    _display_width = 0;
    orientation: Orientation = 'none';
    categories: any[] = [];
    multimediaTypes: any[] = [];
    showDays = true;
    enableZoomIn = true;
    enableZoomOut = true;
    dragElemWidht = 0;
    windowsResizedWidth = 0;
    tlSlideContentWidth = 1670;
    dataItems: TLItem[] = [];
    daysItems: TLDayMarker[] = [];
    monthsItems: TLMonthsMarker[] = [];
    dataItemsIndice = 0;
    position = 0;
    isMousePressed = true;
    data: { [k: string]: any } = {};
    fechaHasta: Date;
    fechaDesde: Date;
    maxZoom = 4000;
    minZoom = 2500;
    stepZoom = 300;
    initialZoom = 2500;
    widhtParam = this.initialZoom;
    length = 0;
    screenHeight: number;
    screenWidth: number;
    galleryOptions: NgxGalleryOptions[] = [];
    galleryImages: any[] = [];
    @ViewChildren('components') components: QueryList<ElementRef>;
    widthmodal = 0;
    private iterableDiffer: any;
    private changeDetectorRef: ChangeDetectorRef;
    public checkedCuadricula: true;
    private errorThumbnail = false;
    device:number = 1;
    galeriaCargada = false;
    private viewerActivado = false;

    constructor(public fichaPService: FichaPersonaDataService, changeDetectorRef: ChangeDetectorRef, private _iterableDiffers: IterableDiffers, private sanitizer: DomSanitizer) {
        this.changeDetectorRef = changeDetectorRef;
        this.orientation = 'none';
        this.iterableDiffer = this._iterableDiffers.find([]).create(null);


        this.galleryOptions = [
            {
                width: '100%',
                height: '90%',
                imageAnimation: NgxGalleryAnimation.Slide,
                thumbnailsRows: 2,
                preview: false,
                image: false,
                thumbnailsColumns: 3,
                thumbnailsPercent: 100,
                thumbnailMargin: 20,
                thumbnailsOrder: NgxGalleryOrder.Row
            },
            // max-width 800

            {
                breakpoint: 600,
                width: '100%',
                height: '90%',
                imagePercent: 80,
                thumbnailsPercent: 20,
                thumbnailsMargin: 20,
                thumbnailMargin: 20,
                thumbnailsRows: 2,
                thumbnailsColumns: 3,
                preview: false,
                image: false
            },

            // max-width 400
            {
                breakpoint: 400,
                preview: false,
                thumbnailsRows: 2,
                thumbnailsColumns: 3,
                image: false
            }
        ];
        console.log('this.galleryOptions>>>>>>>>>>>>', this.galleryOptions);
    }

    // onChange(value: any) {
    //     if (value.checked == true) {
    //       this.device = 1;
    //       console.log(1);
    //     } else {
    //       this.device = 0;
    //       console.log(0);
    //     }
    // }

    onClick() {
        let tmp;
        if (this.device == 1){
           tmp=0;
        } else
        if (this.device == 0){
           tmp=1;
        }
        this.device=tmp;
    }

    urlSafe(urlUnsafe: string): SafeUrl{
        return this.sanitizer.bypassSecurityTrustResourceUrl(urlUnsafe);
    }

    private activarViewer(i:any){
        if (!this.viewerActivado){
            this.viewerActivado = true;
            galeria.ActivarViewerFotosTarjeta('CPI');
        }
    }

    ngOnInit() {

        this.screenHeight = window.innerHeight;
        this.screenWidth = window.innerWidth;

        this.enableZoomIn = this.widhtParam < this.maxZoom;
        this.enableZoomOut = this.widhtParam > this.minZoom;

        this._display_width = window.screen.width;
        this.windowsResizedWidth = window.innerWidth;
        this.options = {
            snap: false,
            enable: {
                x: true,
                y: true
            },
            constraint: {
                top: false,
                bottom: false,
                left: 0,
                right: false
            },
            momentum_multiplier: 2000,
            duration: 1000
        };
        this.data = {
            sliding: false,
            direction: 'none',
            pagex: {
                start: 0,
                end: 0
            },
            pagey: {
                start: 0,
                end: 0
            },
            pos: {
                start: {
                    x: 0,
                    y: 0
                },
                end: {
                    x: 0,
                    y: 0
                }
            },
            new_pos: {
                x: 0,
                y: 0
            },
            new_pos_parent: {
                x: 0,
                y: 0
            },
            time: {
                start: 0,
                end: 0
            },
            touch: false
        };
        console.log('this.data>>>>>>>>>>>' , this.data)
        console.log('this.options>>>>>>>>>>>' , this.options)
        this.resume();
        console.log("this.galleryImages", this.galleryImages); 
    }

    // ngDoCheck() {
    //     const changes = this.iterableDiffer.diff(this.items);
    //     if (changes && this.length !== this.items.length) {
    //         this.length = this.items.length;
    //         this.resume();
    //     }
    // }

    getOrientation(src: any) {
        const img = new Image();
        img.src = src;
        const width = img.width;
        const height = img.height;
        // height = height + height // Double the height to get best
        // height = height + (height / 2) // Increase height by 50%
        if (width > height) {
            return 'landscape';
        } else {
            return 'portrait';
        }

    }

    resume() {
        if (this.items.length === 0) {
            return;
        }
        this.ordernarPorFecha();
        // // indexar items;
        // let i = 0;
        // this.items.forEach(e => {
        //     e.id = i++;
        // });

        this.groupData();

        if (this.dataItems.length === 0) {
            return;
        }
        this.calcularCalendario();
        this.position = 0 - this.getLeftItemPosition(this.dataItems[this.dataItemsIndice]) + (this.windowsResizedWidth / 2);
        const currentItem = this.dataItems[this.dataItemsIndice];
        if (currentItem.isGroup()) {
            this.selectedItem = currentItem.groupItems[currentItem.groupItemsIndice];
        } else {
            this.selectedItem = this.dataItems[this.dataItemsIndice];
        }
        this.calcularCategorias();
        this.calcularFiltros();
    }

    buildImageGallery() {
        this.galleryImages = [];
        let i = 0;
        this.dataItems.forEach(e => {
            if (e.groupItems.length) {
                e.groupItems.forEach(g => {
                    this.galleryImages.push(g);
                    this.galleryImages[i].id = i;
                    i++;
                });
            } else {
                this.galleryImages.push(e);
                this.galleryImages[i].id = i;
                i++;
            }
        });
    }

    calcularCategorias() {
        this.dataItems.forEach(e => {
            if (e.isGroup()) {
                e.groupItems.forEach(s => {
                    if (!this.categories.some(m => m.category === s.category)) {
                        this.categories.push({category: s.category, checked: false});
                    }
                });
            } else {
                if (!this.categories.some(m => m.category === e.category)) {
                    this.categories.push({category: e.category, checked: false});
                }
            }
        });
        this.categories.sort((a, b) => {
            if (CATEGORIES_ATTR[a.category].orden < CATEGORIES_ATTR[b.category].orden) {
                return -1;
            }
            if (CATEGORIES_ATTR[a.category].orden > CATEGORIES_ATTR[b.category].orden) {
                return 1;
            }
            return 0;
        });

    }

    calcularFiltros() {
        this.dataItems.forEach(e => {
            if (e.isGroup()) {
                e.groupItems.forEach(s => {
                    if (!this.multimediaTypes.some(m => m.type === s.type)) {
                        this.multimediaTypes.push({type: s.type, checked: false});
                    }
                });
            } else {
                if (!this.multimediaTypes.some(m => m.type === e.type)) {
                    this.multimediaTypes.push({type: e.type, checked: false});
                }
            }
        });
        console.log('multimeditype', this.multimediaTypes);
    }

    @HostListener('window:resize', ['$event'])
    onResize(event: any) {
        if (this.dataItems.length === 0) {
            return;
        }
        this.tlSlideContentWidth = event.target.innerWidth - 200;
        this.windowsResizedWidth = event.target.innerWidth;
        this.dragElemWidht = this.getPixelWidth() + event.target.innerWidth;
        this.position = 0 - this.getLeftItemPosition(this.dataItems[this.dataItemsIndice]) + (event.target.innerWidth / 2);
        this.calcularCalendario();
        this.position = 0 - this.getLeftItemPosition(this.dataItems[this.dataItemsIndice]) + (this.windowsResizedWidth / 2);


        this.screenHeight = event.target.innerHeight;
        this.screenWidth = event.target.innerWidth;
    }

    @HostListener('window:keyup', ['$event'])
    keyEvent(event: KeyboardEvent) {
        if (event.keyCode === KEY_CODE.RIGHT_ARROW) {
            if (this.getNextItem()) {
                this.moveSlide('right');
            }
        }

        if (event.keyCode === KEY_CODE.LEFT_ARROW) {
            if (this.getPrevItem()) {
                this.moveSlide('left');
            }
        }

        if (event.keyCode === KEY_CODE.ESC) {
            if (this.widthmodal !== 0) {
                this.fichaPService.openModalFotPersona = false;
                this.widthmodal = 0;
            }
        }
    }

    getPixelWidth() {
        return (2 * this._display_width);
    }

    getFilteredItems() {
        // filtrar por fechas
        let filtered:any[] = [];
        if (this.fechaDesde || this.fechaHasta) {
            this.items.forEach(e => {
                if (e.inRangeDate(this.fechaDesde, this.fechaHasta)) {
                    filtered.push(e);
                }
            });
        } else {
            filtered = this.items;
        }

        if (this.categories.some(m => m.checked)) { // hay seleccionado categorias
            const temp = filtered;
            filtered = [];
            temp.forEach(e => {
                if (this.categories.some(m => m.category === e.category && m.checked)) {
                    filtered.push(e);
                }
            });
        }

        if (this.multimediaTypes.some(m => m.checked)) {
            const temp = filtered;
            filtered = [];
            temp.forEach(e => {
                if (this.multimediaTypes.some(m => m.type === e.type && m.checked)) {
                    filtered.push(e);
                }
            });
        }
        return filtered;
    }

    groupData() {
        const itemsFiltered = this.ordernarArrayPorFecha(this.getFilteredItems());
        this.dataItems = [];
        let m = 0;
        let maxd = 1;
        if (this.widhtParam >= 2700) {
            maxd = 0;
        }
        if (this.widhtParam === 1400) {
            maxd = 2;
        }

        if (this.widhtParam === 1300) {
            maxd = 2;
        }
        if (this.widhtParam === 1200) {
            maxd = 3;
        }
        if (this.widhtParam === 1100) {
            maxd = 3;
        }

        if (this.widhtParam === 1000) {
            maxd = 3;
        }

        if (this.widhtParam === 900) {
            maxd = 4;
        }

        if (this.widhtParam === 800) {
            maxd = 5;
        }
        if (this.widhtParam === 700) {
            maxd = 6;
        }
        if (this.widhtParam <= 600) {
            maxd = 8;
        }

        while (m < itemsFiltered.length) {
            const e = itemsFiltered[m];
            const o: TLItem = new TLItem();

            let n = m + 1;
            let band = true;
            while (n < itemsFiltered.length && band) {
                const c = itemsFiltered[n];
                // if (this.diferencia(e.date, c.date) <= (maxd)) {
                if (this.diferencia(e.date, c.date) === 0) {
                    if (n === m + 1) {
                        o.groupItems.push(e);
                        o.groupItems.push(c);
                    } else {
                        o.groupItems.push(c);
                    }
                    n++;
                } else {
                    band = false;
                }
            }
            if (n === m + 1) {
                this.dataItems.push(e);
            } else {
                this.dataItems.push(o);
            }
            m = n;
        }
        // ordenar  cada grupo segun tipo
        for (let i = 0; i < this.dataItems.length; i++) {
            const item = this.dataItems[i];
            if (item.isGroup()) {
                item.groupItems = item.groupItems.sort((a, b) => {
                    return this.compare(a, b);
                });
                // agrupar los items de un grupo segun categoria
                item.groupingItemsByCategory();
            }
        }

        this.buildImageGallery();

        // verificar en que posicion quedo el elemtno actual
        // if (this.dataItemsIndice === 0) {
        //   return; // a lo sumo quedo en la misma posicion inicial
        // }
        if (this.selectedItem) {
            // buscar donde esta el item;
            for (let i = 0; i < this.dataItems.length; i++) {
                const item = this.dataItems[i];
                if (item.isGroup()) {
                    for (let j = 0; j < item.groupItems.length; j++) {
                        const subitem = item.groupItems[j];
                        if (subitem.id === this.selectedItem.id) {
                            this.selectedItem = subitem;
                            this.dataItemsIndice = i;
                            return;
                        }
                    }
                } else {
                    if (item.id === this.selectedItem.id) {
                        this.selectedItem = item;
                        this.dataItemsIndice = i;
                        return;
                    }
                }
            }
            if (this.dataItems.length) {
                const firstItem = this.dataItems[0];
                if (firstItem.isGroup()) {
                    this.selectedItem = firstItem.groupItems[0];
                    this.dataItemsIndice = 0;
                } else {
                    this.selectedItem = firstItem;
                    this.dataItemsIndice = 0;
                }
            }
        }
        console.log('TLL>>>>', 'no se encontro el item');
    }

    compare(a: TLItem, b: TLItem) {
        if (a.category < b.category) {
            return -1;
        }
        if (a.category > b.category) {
            return 1;
        }
        // a debe ser igual b
        return 0;
    }

    diferencia(date1: any, date2: any) {
        const timeDiff = Math.abs(date2.getTime() - date1.getTime());
        return Math.ceil(timeDiff / (1000 * 3600 * 24));
    }

    getPrevItem(): TLItem {
        if (this.dataItems.length === 0) {
            this.prevItem = null;
            return null;
        }
        const currentItem = this.dataItems[this.dataItemsIndice];
        // si el elemento actual es un grupo
        if (currentItem.isGroup()) {
            if (currentItem.groupItemsIndice > 0) { // aun no llego al principio del grupo
                this.prevItem = currentItem.groupItems[currentItem.groupItemsIndice - 1];
                return currentItem.groupItems[currentItem.groupItemsIndice - 1]; // devuelvo el anterior en el grupo
            }
        }

        // sino es un grupo, o si ya llego al primero elemento del grupo, obtengo el anterior item padre
        const item = this.dataItems[this.dataItemsIndice - 1];
        if (item) {
            if (item.isGroup()) {
                this.prevItem = item.groupItems[item.groupItems.length - 1];
                return item.groupItems[item.groupItems.length - 1]; // si item es un grupo devuelvo el ultimo
            } else {
                this.prevItem = item;
                return item;
            }
        }
        return null;
    }


    getNextItem(): TLItem {
        if (this.dataItems.length === 0) {
            return null;
        }
        const currentItem = this.dataItems[this.dataItemsIndice];
        // si el elemento actual es un grupo
        if (currentItem.isGroup()) {
            if (currentItem.groupItemsIndice < currentItem.groupItems.length - 1) { // aun no llego al final del grupo
                this.nextItem = currentItem.groupItems[currentItem.groupItemsIndice + 1];
                return currentItem.groupItems[currentItem.groupItemsIndice + 1]; // devuelvo el siguiente en el grupo
            }
        }

        // sino es un grupo, o si ya llego al ultimo elemento del grupo, obtengo el anterior item padre
        const item = this.dataItems[this.dataItemsIndice + 1];
        if (item) {
            if (item.isGroup()) { // si item es un grupo devuelvo el primero
                this.nextItem = item.groupItems[0];
                return item.groupItems[0];
            } else {
                this.nextItem = item;
                return item;
            }
        }

        return null; // no hay siguiente
    }

    getSelectedItem(clickedItem: any) {
        var itemAux = clickedItem;
        this.changeDetectorRef.detectChanges();
        if (clickedItem) {
            for (let i = 0; i < this.dataItems.length; i++) {
                const item = this.dataItems[i];
                if (item.isGroup()) {
                    for (let j = 0; j < item.groupItems.length; j++) {
                        const subitem = item.groupItems[j];
                        if (subitem.id === clickedItem.id) {
                            clickedItem = item;
                            if (i < this.dataItemsIndice) {
                                this.orientation = 'prev';
                            } else {
                                if (j < clickedItem.groupItemsIndice) {
                                    this.orientation = 'prev';
                                } else {
                                    this.orientation = 'next';
                                }
                            }
                            clickedItem.groupItemsIndice = j;
                            this.dataItemsIndice = i;
                            break;
                        }
                    }
                } else {
                    if (item.id === clickedItem.id) {
                        clickedItem = item;
                        if (i < this.dataItemsIndice) {
                            this.orientation = 'prev';
                        } else {
                            this.orientation = 'next';
                        }
                        this.dataItemsIndice = i;
                        break;
                    }
                }
            }
            if (clickedItem.isGroup()) {
                this.selectedItem = clickedItem.groupItems[clickedItem.groupItemsIndice];
                this.position = 0 - this.getLeftItemPosition(this.dataItems[this.dataItemsIndice]) + (this.windowsResizedWidth / 2);
            } else {
                this.selectedItem = this.dataItems[this.dataItemsIndice];
                this.position = 0 - this.getLeftItemPosition(this.dataItems[this.dataItemsIndice]) + (this.windowsResizedWidth / 2);
            }
            this.openNav(itemAux);
        }
    }

    selectItem(parentIndex: any) {
        if (parentIndex === this.dataItemsIndice) {
            return; // si el actual no hago nada
        }

        if (parentIndex < this.dataItemsIndice) {
            this.orientation = 'prev';
        } else {
            this.orientation = 'next';
        }

        // reseteo el anterior
        const itemPrev = this.dataItems[this.dataItemsIndice];
        itemPrev.collapsed = true;
        this.resetScroll(this.dataItemsIndice);

        this.changeDetectorRef.detectChanges();
        this.dataItemsIndice = parentIndex; // seteo el indice padre

        const currentItem = this.dataItems[this.dataItemsIndice];
        if (currentItem.isGroup()) { // si es grupo regreso el primer elemento del grupo
            currentItem.groupItemsIndice = 0;
            this.selectedItem = currentItem.groupItems[currentItem.groupItemsIndice];
            return;
        }

        this.selectedItem = this.dataItems[this.dataItemsIndice]; // regreso el elemento padre correspondiente
    }

    public moveSlide(direction: string) {
        if (direction === 'right') {
            this.orientation = 'next';
            this.changeDetectorRef.detectChanges();

            let currentItem = this.dataItems[this.dataItemsIndice];
            if (currentItem.isGroup()) {
                if (currentItem.groupItemsIndice < currentItem.groupItems.length - 1) {
                    currentItem.groupItemsIndice++;
                    this.selectedItem = currentItem.groupItems[currentItem.groupItemsIndice];
                    this.position = 0 - this.getLeftItemPosition(this.dataItems[this.dataItemsIndice]) + (this.windowsResizedWidth / 2);
                    return;
                }
            }
            this.dataItemsIndice++;
            currentItem = this.dataItems[this.dataItemsIndice];
            if (currentItem.isGroup()) {
                currentItem.groupItemsIndice = 0;
                this.selectedItem = currentItem.groupItems[currentItem.groupItemsIndice];
                this.position = 0 - this.getLeftItemPosition(this.dataItems[this.dataItemsIndice]) + (this.windowsResizedWidth / 2);
                return;
            }
            this.selectedItem = this.dataItems[this.dataItemsIndice];
            this.position = 0 - this.getLeftItemPosition(this.dataItems[this.dataItemsIndice]) + (this.windowsResizedWidth / 2);
        } else {
            this.orientation = 'prev';
            this.changeDetectorRef.detectChanges();

            let currentItem = this.dataItems[this.dataItemsIndice];
            if (currentItem.isGroup()) {
                if (currentItem.groupItemsIndice > 0) {
                    currentItem.groupItemsIndice--;
                    this.selectedItem = currentItem.groupItems[currentItem.groupItemsIndice];
                    this.position = 0 - this.getLeftItemPosition(this.dataItems[this.dataItemsIndice]) + (this.windowsResizedWidth / 2);
                    return;
                }
            }
            this.dataItemsIndice--;
            currentItem = this.dataItems[this.dataItemsIndice];
            if (currentItem.isGroup()) {
                currentItem.groupItemsIndice = currentItem.groupItems.length - 1;
                this.selectedItem = currentItem.groupItems[currentItem.groupItemsIndice];
                this.position = 0 - this.getLeftItemPosition(this.dataItems[this.dataItemsIndice]) + (this.windowsResizedWidth / 2);
                return;
            }
            this.selectedItem = this.dataItems[this.dataItemsIndice];
            this.position = 0 - this.getLeftItemPosition(this.dataItems[this.dataItemsIndice]) + (this.windowsResizedWidth / 2);
        }
    }

    public selectItemGroup(parentIndex: any, subitemindex: any) {
        if (this.dataItemsIndice === parentIndex) { // ya estoy parado en el grupo
            const currentItem = this.dataItems[parentIndex];
            if (subitemindex !== currentItem.groupItemsIndice) {
                if (subitemindex > currentItem.groupItemsIndice) {
                    this.orientation = 'next';
                    this.changeDetectorRef.detectChanges();
                    currentItem.groupItemsIndice = subitemindex;
                    this.selectedItem = currentItem.groupItems[currentItem.groupItemsIndice];
                    this.position = 0 - this.getLeftItemPosition(this.dataItems[this.dataItemsIndice]) + (this.windowsResizedWidth / 2);
                } else {
                    this.orientation = 'prev';
                    this.changeDetectorRef.detectChanges();
                    currentItem.groupItemsIndice = subitemindex;
                    this.selectedItem = currentItem.groupItems[currentItem.groupItemsIndice];
                    this.position = 0 - this.getLeftItemPosition(this.dataItems[this.dataItemsIndice]) + (this.windowsResizedWidth / 2);
                }
            }
        } else { // voy a ingresar al grupo desde otro lado
            //  reseto el anterior
            const currentItem = this.dataItems[this.dataItemsIndice];
            if (currentItem !== undefined) {
                currentItem.collapsed = !currentItem.collapsed;
                this.resetScroll(this.dataItemsIndice);
            }
            this.selectItem(parentIndex);
        }
    }

    public moveItemGroup(direction: string) {
        if (direction === 'right') {
            this.orientation = 'next';
            this.changeDetectorRef.detectChanges();

            const currentItem = this.dataItems[this.dataItemsIndice];
            currentItem.groupItemsIndice++;
            this.selectedItem = currentItem.groupItems[currentItem.groupItemsIndice];
            this.position = 0 - this.getLeftItemPosition(this.dataItems[this.dataItemsIndice]) + (this.windowsResizedWidth / 2);
        } else {
            this.orientation = 'prev';
            this.changeDetectorRef.detectChanges();

            const currentItem = this.dataItems[this.dataItemsIndice];
            currentItem.groupItemsIndice--;
            this.selectedItem = currentItem.groupItems[currentItem.groupItemsIndice];
            this.position = 0 - this.getLeftItemPosition(this.dataItems[this.dataItemsIndice]) + (this.windowsResizedWidth / 2);
        }
    }

    getLeftItemPosition(item: TLItem) {
        try{
        if (this.dataItems.length === 0) {
            return 0;
        }

        if (item.isGroup()) {
            item = item.groupItems[0];
        }
        const day: TLDayMarker = this.daysItems.find(e =>
            e.date.getTime() === item.date.getTime());
        return day.left;}
        catch (error){
            return 0;
        }
    }

    public calcularCalendario() {
        if (this.dataItems.length === 0) {
            return;
        }

        const startDate = this.dataItems[0].isGroup() ? this.dataItems[0].groupItems[0].date : this.dataItems[0].date;
        const lastItem = this.dataItems[this.dataItems.length - 1];
        let endDate;
        if (lastItem.isGroup()) {
            endDate = lastItem.groupItems[lastItem.groupItems.length - 1].date;
        } else {
            endDate = lastItem.date;
        }
        this.daysItems = [];
        this.monthsItems = [];
        let leftDistanceFromOrigin = 0;

        let startYear = startDate.getFullYear();
        let endYear = endDate.getFullYear();
        let startMonth = startDate.getMonth();
        let endMonth = endDate.getMonth();

        // aplicar padding de un mes a izquierda y derecha
        if (startMonth > 0) {
            startMonth--;
        } else {
            startYear--;
            startMonth = 11;
        }

        if (endMonth < 11) {
            endMonth++;
        } else {
            endYear++;
            endMonth = 0;
        }


        let yearIndice = startYear;
        do {
            let firstMonth = 0;
            let finalMonth = 11;
            if (yearIndice === startYear) {
                firstMonth = startMonth;
            }
            if (yearIndice === endYear) {
                finalMonth = endMonth;
            }
            for (let monthIndice = firstMonth; monthIndice <= finalMonth; monthIndice++) { // month in zero base
                const daysInMonth = this.daysInMonth(monthIndice + 1, yearIndice);
                const dayDistance = this.widhtParam / (daysInMonth);

                let mod = 1;
                if (dayDistance > 25) {
                    mod = 1;
                } else if (dayDistance > 20) {
                    mod = 2;
                } else if (dayDistance > 15) {
                    mod = 4;
                } else if (dayDistance > 10) {
                    mod = 6;
                } else if (dayDistance > 5) {
                    mod = 8;
                } else {
                    mod = daysInMonth + 1;
                }

                this.showDays = mod <= daysInMonth;
                let leftDay = leftDistanceFromOrigin;
                this.daysItems.push(new TLDayMarker(1 + '/' + (monthIndice + 1) + '/' + yearIndice, leftDay, 0, new Date(yearIndice, monthIndice, 1)));
                for (let day = 2; day <= daysInMonth; day++) {
                    leftDay = leftDay + dayDistance;
                    const r = day % mod;
                    this.daysItems.push(new TLDayMarker(day + '/' + (monthIndice + 1) + '/' + yearIndice, leftDay, r === 0 ? 1 : 0, new Date(yearIndice, monthIndice, day)));
                }
                this.monthsItems.push(new TLMonthsMarker(MONTHS[monthIndice] + '-' + yearIndice, leftDistanceFromOrigin));
                leftDistanceFromOrigin += this.widhtParam;
            }
            yearIndice++;
            this.dragElemWidht = leftDistanceFromOrigin;
        } while (yearIndice <= endYear);
    }

    daysInMonth(month: any, year: any) {
        // month in one base
        return new Date(year, month, 0).getDate();
    }

    zoomIn() {
        if (this.widhtParam >= this.maxZoom) {
            return;
        }
        this.widhtParam = this.widhtParam + this.stepZoom;
        this.enableZoomIn = this.widhtParam < this.maxZoom;
        this.enableZoomOut = this.widhtParam > this.minZoom;
        this.groupData();
        if (this.dataItems.length === 0) {
            return;
        }
        this.calcularCalendario();
        this.position = 0 - this.getLeftItemPosition(this.dataItems[this.dataItemsIndice]) + (this.windowsResizedWidth / 2);
    }

    zoomOut() {
        if (this.widhtParam <= this.minZoom) {
            return;
        }
        this.widhtParam = this.widhtParam - this.stepZoom;
        this.enableZoomIn = this.widhtParam < this.maxZoom;
        this.enableZoomOut = this.widhtParam > this.minZoom;
        this.groupData();
        if (this.dataItems.length === 0) {
            return;
        }
        this.calcularCalendario();
        this.position = 0 - this.getLeftItemPosition(this.dataItems[this.dataItemsIndice]) + (this.windowsResizedWidth / 2);
    }

    filter() {
        this.groupData();
        this.viewerActivado = false;
        if (this.dataItems.length === 0) {
            return;
        }
        this.calcularCalendario();
        this.position = 0 - this.getLeftItemPosition(this.dataItems[this.dataItemsIndice]) + (this.windowsResizedWidth / 2);
    }

    reset() {
        this.orientation = 'prev';
        this.changeDetectorRef.detectChanges();
        this.widhtParam = this.initialZoom;
        this.enableZoomOut = this.widhtParam > this.minZoom;
        this.enableZoomIn = this.widhtParam < this.maxZoom;
        this.groupData();
        if (this.dataItems.length === 0) {
            return;
        }
        this.calcularCalendario();
        this.position = 0 - this.getLeftItemPosition(this.dataItems[0]) + (this.windowsResizedWidth / 2);
        this.dataItemsIndice = 0;
        const currentItem = this.dataItems[this.dataItemsIndice];
        if (currentItem.isGroup()) {
            currentItem.groupItemsIndice = 0;
            this.selectedItem = currentItem.groupItems[currentItem.groupItemsIndice];
        } else {
            this.selectedItem = this.dataItems[this.dataItemsIndice];
        }
    }

    getCategory(item: string) {
        return CATEGORIES_ATTR[item];
    }

    getType(item: string) {
        return TYPES_ATTR[item];
    }

    getUrlPdf(itemdate: any) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(itemdate.url + '?#view=FitH');
    }

    errorHandlerLoad(event: any) {
        this.errorThumbnail = true;
        event.target.src = '/assets/media/images/caja_transparente.png';
    }

    /*******************************************************************************
     * Draggable
     *******************************************************************************/
    coordinates(event: MouseEvent): void {
        if (event.which === 1 && this.isMousePressed) { // move
            this.onDragMove(event);
        } else if (event.which === 1 && !this.isMousePressed) { // start
            this.onDragStart(event);
        } else if (event.which === 0 && this.isMousePressed) { // end
            this.onDragEnd(event);
        }
    }

    resetScroll(parentIndex: any) {
        this.components.toArray()[parentIndex].nativeElement.scrollLeft = 0;
    }

    public selectFromDate($event: any) {
        this.fechaDesde = new Date($event);
        this.fechaDesde.setSeconds(0);
        this.fechaDesde.setMinutes(0);
        this.fechaDesde.setHours(0);
        if (this.fechaHasta) {
            if (this.fechaDesde.getTime() > this.fechaHasta.getTime()) {
                console.log('TLL>>>>', 'la fecha hasta no puede ser mayor a la fecha desde');
                this.fechaDesde = null;
            }
        }
        this.filter();
    }

    public selectToDate($event: any) {
        this.fechaHasta = new Date($event);
        //this.fechaDesde = new Date($event);
        this.fechaHasta.setSeconds(0);
        this.fechaHasta.setMinutes(0);
        this.fechaHasta.setHours(0);
        if (this.fechaDesde) {
            if (this.fechaDesde.getTime() > this.fechaHasta.getTime()) {
                console.log('TLL>>>>', 'la fecha hasta no puede ser mayor a la fecha desde');
                this.fechaHasta = null;
            }
        }
        this.filter();
    }

    clearFilters() {
        this.fechaDesde = null;
        this.fechaHasta = null
        this.multimediaTypes.forEach(e => {
            e.checked = false;
        });
        this.categories.forEach(e => {
            e.checked = false;
        });
        this.filter();
    }

    private openNav(item?: any) {
        //this.widthmodal = 100;
        galeria.AbreViewerTarjeta(item.id,'CPI');
        //this.fichaPService.openModalFotPersona = true;//para poder quitar head hasta encontrar otra forma
    }

    closeNav() {
        //this.fichaPService.openModalFotPersona = false;//para volver a mostar head principal
        //this.widthmodal = 0;
    }

    private ordernarPorFecha() {
        this.items.sort((a, b) => {
            if (a.date.getTime() < b.date.getTime()) {
                return -1;
            }
            if (a.date.getTime() > b.date.getTime()) {
                return 1;
            }
            return 0;
        });
    }

    private ordernarArrayPorFecha(array: any) {
        array.sort((a:any, b:any) => {
            if (a.date.getTime() < b.date.getTime()) {
                return -1;
            }
            if (a.date.getTime() > b.date.getTime()) {
                return 1;
            }
            return 0;
        });
        return array;
    }

    private onDragStart(e:any) {
        this.animate = false;
        this.isMousePressed = true;

        this.data.pagex.start = e.pageX;
        this.data.pagey.start = e.pageY;

        this.data.pos.start = {x: this.moveElem.nativeElement.offsetLeft, y: this.moveElem.nativeElement.offsetTop};
    }

    private onDragMove(e: any) {
        const change = {
            x: 0,
            y: 0
        };
        this.data.sliding = true;

        this.data.pagex.end = e.pageX;
        this.data.pagey.end = e.pageY;

        change.x = this.data.pagex.start - this.data.pagex.end;
        change.y = this.data.pagey.start - this.data.pagey.end;

        this.data.pos.end = {x: this.dragElem.nativeElement.offsetLeft, y: this.dragElem.nativeElement.offsetTop};

        this.data.new_pos.x = -(change.x - this.data.pos.start.x);
        this.data.new_pos.y = -(change.y - this.data.pos.start.y);

        if (this.options.enable.x && (Math.abs(change.x) > Math.abs(change.y))) {
            e.preventDefault();
            if (this.data.new_pos.x < 500) {
                this.position = this.data.new_pos.x;
            } else {
                this.position = 0;
            }
        }
    }

    private onDragEnd(e: any) {
        if (this.data.new_pos.x > 0) {
            this.position = 0;
        }
        this.isMousePressed = false;
        this.data.sliding = false;
        this.animate = true;
    }
}

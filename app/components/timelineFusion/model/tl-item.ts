//import {Class} from "@angular/core";

export class TLItem {
  id: number;
  name: string;
  url: string;
  description: string;
  date: Date;
  groupItemsIndice = 0;
  groupItems: TLItem[] = [];
  collapsed = true;
  type: MultimediaType;
  category: CategoryType;
  usuario: string;
  urlVideoThumb: string;
  urlsmall: string;
  urlmedium: string;
  urlbig: string;
  registerDate: Date;
  esCPI: boolean;
  esFusion: boolean

  itemsGroupCategory: TLGroupCategory[] = [];

  groupItemArrayPostion: number = 0;


  public isGroup() {
    return this.groupItems.length > 0;
  }

  getWidht() {
    if (this.collapsed) {
      return 100;
    } else {
      if (this.groupItems.length > 4) {
        return 400;
      } else {
        return this.groupItems.length * 100;
      }

    }
  }

  contentContainerWidth(dataItemsIndice: any, parentIndex: any) {
    if (dataItemsIndice === parentIndex) {
      return this.getWidht() + 20;
    } else {
      return this.collapsed ? this.getWidht() + 5 : this.getWidht() + 20;
    }
  }

  inRangeDate(fechaDesde: Date, fechaHasta: Date) {
    if (fechaDesde && fechaHasta) {
      return fechaDesde.getTime() <= this.date.getTime() && this.date.getTime() <= fechaHasta.getTime();
    }
    if (fechaDesde && !fechaHasta) {
      return fechaDesde.getTime() <= this.date.getTime();
    }
    if (!fechaDesde && fechaHasta) {
      return this.date.getTime() <= fechaHasta.getTime();
    }
    return false;
  }

  groupingItemsByCategory() {
    let indice = 0;
    let tmpacumulado = 0;
    let tempCategory = '';
    let groupCategory: TLGroupCategory = new TLGroupCategory();
    this.groupItems.forEach(e => {
      if (tempCategory !== e.category) { // se cambio a una nueva categoria
        // guardamos el grupo que se estaba cargando
        if (tempCategory !== '') {
          groupCategory.width = groupCategory.items.length * 95;
          this.itemsGroupCategory.push(groupCategory);
        }
        // comenzamos a setear datos del nuevo grupo
        let left = 0;
        if (this.itemsGroupCategory[this.itemsGroupCategory.length - 1]) { // existe el anterior
          left = tmpacumulado + this.itemsGroupCategory[this.itemsGroupCategory.length - 1].items.length * 95;
          tmpacumulado += groupCategory.width;
          if (tempCategory !== '') {
            left += (5 * this.itemsGroupCategory.length);
          }
        }
        tempCategory = e.category;
        groupCategory = new TLGroupCategory();
        groupCategory.left = left;
        groupCategory.category = e.category;
      }
      e.groupItemArrayPostion = indice++;
      groupCategory.items.push(e);
    });
    if (tempCategory !== '') { // el ultimo grupo
      groupCategory.width = groupCategory.items.length * 95;
      this.itemsGroupCategory.push(groupCategory);
    }
  }
}


export class TLGroupCategory {
  category: string;
  items: TLItem[] = [];
  left: number = 0;
  width: number = 0;
}


export type MultimediaType = ( 'imagen' | 'audio' | 'video' );
export type CategoryType = ( 'vivienda' | 'persona' | 'otro' | 'infraestructura' );

export class TLMonthsMarker {
  name: string;
  left: number;

  constructor(name: string, left: number) {
    this.name = name;
    this.left = left;
  }
}

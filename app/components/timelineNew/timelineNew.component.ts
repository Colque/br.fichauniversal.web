import { Component, OnInit, HostListener, Input, EventEmitter, Output } from '@angular/core';
//import {NgControl} from '@angular/common';
import { ApiBackEndService } from './../../_services/apiBackEnd.service';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Sanitizer } from '@angular/core/src/security';
//paraloading
declare var initTime: any
declare var valueG: any;
declare var totWidthG: any;
declare var galeria: any;

import * as moment from 'moment';

@Component({
    selector: 'timelineNew',
    templateUrl: 'timelineNew.component.html',
    styleUrls: ['timelineNew.component.css']
})

export class TimelineNewComponent implements OnInit {
    @Input() fotos: any;
    @Input() tarjeta: any;
    dataItems: any = [];
    daysItems: any = [];
    monthsItems: any = [];
    eventsMinDistance = 12;
    maxZoom = 36;
    minZoom = 12;
    fechaDesde: Date;
    fechaHasta: Date;
    tipoFiltro = "1";
    dataFotos: any[];
    viewerActivado = false;
    listPadding: any = [
        {
            minZoom : 48,
            paddingleft: 10 
        },
        {
            minZoom : 60,
            paddingleft: 15 
        },
        {
            minZoom : 72,
            paddingleft: 20 
        }
    ]
    constructor(
        private apiBackEndService: ApiBackEndService,
        private sanitizer: DomSanitizer) {
    }


    ngOnInit() {
        this.loadData();
        this.onChanges();
    }

    scroll(el: HTMLElement) {
        el.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }

    loadData() {
        var principal = this;
    }

    getFotosEvent(index:any){
        this.viewerActivado = false;
        this.dataFotos = [];
        this.dataFotos = this.dataItems[index].fotos;
    }

    private activarViewer(i:any){
        if (!this.viewerActivado){
            this.viewerActivado = true;
            let mThis = this;
            setTimeout( function(){
            galeria.ActivarViewerFotosTarjeta(mThis.tarjeta,'body');
        }, 100);
        }
    }

    openViewer(index:any){
        let mThis = this;
        setTimeout( function(){
            galeria.AbreViewerTarjeta(index,mThis.tarjeta);
        }, 100);
    }

    initTimeline(resetAll? :any, idItem?: any){
        initTime.initTimeline(this.tarjeta,this.eventsMinDistance,resetAll,idItem);
    }

    zoomIn() {
        if (this.tipoFiltro === "1" && this.eventsMinDistance == this.maxZoom){
            this.tipoFiltro = "0";
            this.onChanges();
            return;
        }
        else if (this.eventsMinDistance >= this.maxZoom) {
            return;
        }
        this.eventsMinDistance = this.eventsMinDistance + 12;
        this.initTimeline();
    }

    zoomOut() {
        if (this.tipoFiltro === "0" && this.eventsMinDistance == this.minZoom){
            this.tipoFiltro = "1";
            this.onChanges();
            return;
        }
        else if (this.eventsMinDistance <= this.minZoom) {
            return;
        }
        this.eventsMinDistance = this.eventsMinDistance - 12;
        this.initTimeline();
    }

    reset() {
        // valueG = 0;
        // totWidthG = undefined;
        // this.eventsMinDistance = this.minZoom;
        // this.initTimeline();
        this.tipoFiltro = "1";
        this.onChanges();
    }

    onChanges(){
        this.filter();
        this.dataItems = this.ordernarArrayPorFecha(this.dataItems);
        this.calcularCalendario();
        if (this.tipoFiltro === "1"){
            this.minZoom = 48;
            this.maxZoom = 72;
        } else {
            this.minZoom = 24;
            this.maxZoom = 48;
        }
        let mThis = this;
        valueG = 0;
        mThis.eventsMinDistance = this.minZoom;
        let index = this.daysItems.findIndex((d:any) => d.tieneDatos === true);
        setTimeout( function(){
            mThis.initTimeline(true,index);
            mThis.getFotosEvent(0);
        }, 50);
    }

    public calcularCalendario() {
        // if (this.dataItems.length === 0) {
        //     return;
        // }
        const startDate = this.fechaDesde ? this.fechaDesde : this.dataItems[0] ? this.dataItems[0].date : this.fechaHasta;
        const lastItem = this.dataItems[this.dataItems.length - 1];
        let endDate = this.fechaHasta ? this.fechaHasta : lastItem ? lastItem.date : startDate;
        this.daysItems = [];
        this.monthsItems = [];
        let leftDistanceFromOrigin = 0;

        let startYear = startDate.getFullYear();
        let endYear = endDate.getFullYear();
        let startMonth = startDate.getMonth();
        let endMonth = endDate.getMonth();

        if (this.tipoFiltro === "0"){
            //De ser necesario agrego mas dias al intervalo para que se va correctamente la linea
            var fecha1 = moment(new Date(startYear, startMonth,1));
            var fecha2 = moment(new Date(endYear, endMonth,1));
            if (fecha2.diff(fecha1, 'months') <= 1){
                if (endMonth < 10){
                    endMonth = endMonth + 2;
                } else {
                    endYear = endYear + 1;
                    endMonth = 11 - endMonth;
                }
            }
        } else {
            startMonth = 1;
            endMonth = 10;
            var fecha1 = moment(new Date(startYear, startMonth,1));
            var fecha2 = moment(new Date(endYear, endMonth,1));
            if (fecha2.diff(fecha1, 'years') <= 1){
                endYear = endYear + 2;
            }
        }

        // aplicar padding de un mes a izquierda y derecha
        if (startMonth > 0) {
            startMonth--;
        } else {
            startYear--;
            startMonth = 11;
        }

        if (endMonth < 11) {
            endMonth++;
        } else {
            endYear++;
            endMonth = 0;
        }


        let yearIndice = startYear;
        let cant
        do {
            let firstMonth = 0;
            let finalMonth = 11;
            if (yearIndice === startYear) {
                firstMonth = startMonth;
            }
            if (yearIndice === endYear) {
                finalMonth = endMonth;
            }
            // if (firstMonth === 0){
            //     this.daysItems.push({name: yearIndice, date: null, tieneDatos: false, esMes: true});
            // }
            for (let monthIndice = firstMonth; monthIndice <= finalMonth; monthIndice++) { // month in zero base
                const daysInMonth = this.daysInMonth(monthIndice + 1, yearIndice);

                //this.showDays = mod < daysInMonth;
                let leftDay = leftDistanceFromOrigin;
                let pos;
                
                if (this.tipoFiltro === "0"){
                    //valido si esta fecha tiene fotos
                    var dayFilter = this.fotos.filter((d:any) => moment(d.FechaRegistro).format('DD-MM-YY') === moment(new Date(yearIndice, monthIndice, 1)).format('DD-MM-YY'));
                   
                    //inserto en el array el mes correspondiente(para figurar en la linea de tiempo)
                    if (yearIndice !== startYear)
                        this.daysItems.push({name: this.apiBackEndService.meses[monthIndice].mes + ' ' + yearIndice, date: new Date(yearIndice, monthIndice, 1),tieneDatos: false, esMes: true});
                    else if (monthIndice !== firstMonth)
                        this.daysItems.push({name: this.apiBackEndService.meses[monthIndice].mes + ' ' + yearIndice, date: new Date(yearIndice, monthIndice, 1),tieneDatos: false, esMes: true});
                    
                    //busco la posicion correspondiente en el array original, para determinar la ubicacion en la linea de tiempo
                    pos = this.dataItems.map(function(e:any) { return e.fecha; }).indexOf((moment(new Date(yearIndice, monthIndice, 1)).format('DD/MM/YYYY')));
                    
                    //inserto el primer dia del mes validando si es que tiene datos de fotos
                    this.daysItems.push({name: '01/' + ((monthIndice + 1) < 10 ? '0'+ (monthIndice + 1)  : (monthIndice + 1)) + '/' + yearIndice, date: new Date(yearIndice, monthIndice, 1), tieneDatos: dayFilter.length > 0 ? true : false, esMes: false, esPar: pos%2});

                    for (let day = monthIndice === firstMonth && yearIndice === startYear ? 25 : 2; day <= daysInMonth; day++) {
                        pos = null;
                        dayFilter = [];
                        //valido si esta fecha esta dentro de los filtros ingresados
                        if (this.inRangeDate(new Date(yearIndice, monthIndice, day))){
                            pos = this.dataItems.map(function(e:any) { return e.fecha; }).indexOf((moment(new Date(yearIndice, monthIndice, day)).format('DD/MM/YYYY')));
                            dayFilter = this.fotos.filter((d:any) => moment(d.FechaRegistro).format('DD-MM-YY') === moment(new Date(yearIndice, monthIndice, day)).format('DD-MM-YY'));
                            //console.log('indexOfFoto', pos);
                        }
                        
                        this.daysItems.push({name: (day < 10 ? '0'+ day  : day) + '/' + ((monthIndice + 1) < 10 ? '0'+ (monthIndice + 1)  : (monthIndice + 1)) + '/' + yearIndice, date: new Date(yearIndice, monthIndice, day), tieneDatos: dayFilter.length > 0 ? true : false, esMes: false, esPar: pos ? pos%2 : -1, pos: pos});
                    }
                } else{
                    pos = null;
                    dayFilter = [];
                    let fechaAc = new Date(yearIndice, monthIndice, 1);
                    if (this.daysItems.map(function(e:any) { return e.name; }).indexOf(yearIndice) === -1){
                        this.daysItems.push({name: yearIndice, date: new Date(yearIndice, 0, 1),tieneDatos: false, esMes: true});
                    }
                    var inRange = this.inRangeDate(fechaAc);
                    if (inRange){
                        pos = this.dataItems.map(function(e:any) { return e.fecha; }).indexOf(this.apiBackEndService.meses[fechaAc.getMonth()].mes + ' ' + fechaAc.getFullYear());
                    }    
                    dayFilter = this.fotos.filter((d:any) => moment(d.FechaRegistro).format('MM-YY') === moment(fechaAc).format('MM-YY'));
                    //console.log('indexOfFoto', pos);
                    //inserto en el array el mes correspondiente(para figurar en la linea de tiempo)
                    this.daysItems.push({name: this.apiBackEndService.meses[monthIndice].mes + ' ' + yearIndice, date: fechaAc,tieneDatos: dayFilter.length > 0 && inRange ? true : false, esMes: false, esPar: pos ? pos%2 : -1, pos: pos});
                    //}                    
                }
            }
            yearIndice++;
        } while (yearIndice <= endYear);
    }

    daysInMonth(month: any, year: any) {
        // month in one base
        return new Date(year, month, 0).getDate();
    }

    public selectFromDate($event: any) {
        this.fechaDesde = new Date($event);
        if (this.fechaHasta) {
            if (this.fechaDesde.getTime() > this.fechaHasta.getTime()) {
                console.log('TLL>>>>', 'la fecha hasta no puede ser mayor a la fecha desde');
                this.fechaDesde = null;
                return;
            }
        }
        this.filter();
        this.dataItems = this.ordernarArrayPorFecha(this.dataItems);
        this.calcularCalendario();
        //this.tipoFiltro = 1;
        let mThis = this;
        valueG = 0;
        mThis.eventsMinDistance = this.minZoom;
        setTimeout( function(){
            mThis.initTimeline(true);
        }, 50);
    }

    public selectToDate($event: any) {
        this.fechaHasta = new Date($event);
        //this.fechaDesde = new Date($event);
        if (this.fechaDesde) {
            if (this.fechaDesde.getTime() > this.fechaHasta.getTime()) {
                console.log('TLL>>>>', 'la fecha hasta no puede ser mayor a la fecha desde');
                this.fechaHasta = null;
                return;
            }
        }
        this.filter();
        this.dataItems = this.ordernarArrayPorFecha(this.dataItems);
        this.calcularCalendario();
        let mThis = this;
        valueG = 0;
        mThis.eventsMinDistance = this.minZoom;
        setTimeout( function(){
            mThis.initTimeline(true);
        }, 50);
    }

    clearFilters(){
        if (this.fechaDesde || this.fechaHasta){
            this.fechaDesde = null;
            this.fechaHasta = null;
            this.filter();
            this.dataItems = this.ordernarArrayPorFecha(this.dataItems);
            this.calcularCalendario();
            let mThis = this;
            valueG = 0;
            mThis.eventsMinDistance = this.minZoom;
            setTimeout( function(){
                mThis.initTimeline(true);
            }, 50);
        }
    }

    getFilteredItems() {
        // filtrar por fechas
        let filtered:any[] = [];
        let mThis = this;
        if (this.fechaDesde || this.fechaHasta) {
            filtered = this.fotos.filter((f:any) => mThis.inRangeDate(f.FechaRegistro));
        } else {
            filtered = this.fotos;
        }
        return filtered;
    }

    inRangeDate(fechaFilter: Date) {
        var formatFilter = this.tipoFiltro === "0" ? "YYYY-MM-DD" : "YYYY-MM";
        if (this.fechaDesde && this.fechaHasta) {
            return new Date(moment(fechaFilter).format(formatFilter)).getTime() >= new Date(moment(this.fechaDesde).format(formatFilter)).getTime() && new Date(moment(fechaFilter).format(formatFilter)).getTime() <= new Date(moment(this.fechaHasta).format(formatFilter)).getTime();
        }
        if (this.fechaDesde && !this.fechaHasta) {
            return new Date(moment(fechaFilter).format(formatFilter)).getTime() >= new Date(moment(this.fechaDesde).format(formatFilter)).getTime();
        }
        if (!this.fechaDesde && this.fechaHasta) {
            return new Date(moment(fechaFilter).format(formatFilter)).getTime() <= new Date(moment(this.fechaHasta).format(formatFilter)).getTime();
        }
        return true;
    }

    filter(){this.dataItems = this.ordernarArrayPorFecha(this.dataItems);
        var filter: any = this.getFilteredItems();
        if (filter){
            //initTime.destroy();
            this.daysItems = [];
            this.dataItems = [];

            filter.forEach((elementA: any, i: any) => {
                if (elementA["FotoMedium"] != null) {
                    var fechaArray = elementA["Fecha"].split("-");
                    var date = new Date(Number("20" + fechaArray[2]), fechaArray[1] - 1, fechaArray[0]);
                    if (this.tipoFiltro === "0"){
                        if (this.dataItems.filter((d: any) => moment(d.date).format('DD-MM-YY') === moment(date).format('DD-MM-YY')).length === 0){
                            var fotosDate = this.fotos.filter((f:any) => moment(f.FechaRegistro).format('DD-MM-YY') === moment(date).format('DD-MM-YY'));
                            this.dataItems.push({date : date, fecha: moment(date).format('DD/MM/YYYY'), fotos: fotosDate});
                        }
                    } else{
                        if (this.dataItems.filter((d: any) => moment(d.date).format('MM-YY')+ "-01" === moment(date).format('MM-YY')+ "-01").length === 0){
                            var fotosDate = this.fotos.filter((f:any) => moment(f.FechaRegistro).format('MM-YY') === moment(date).format('MM-YY'));
                            let fecha = moment(date).format('YYYY-MM-DD');
                            this.dataItems.push({date : new Date(date.getFullYear(),date.getMonth(),1), fecha: this.apiBackEndService.meses[date.getMonth()].mes + ' ' + date.getFullYear(), fotos: fotosDate});
                        }
                    }
                }
            });
        }
    }

    private ordernarArrayPorFecha(array: any) {
        array.sort((a:any, b:any) => {
            if (a.date.getTime() < b.date.getTime()) {
                return -1;
            }
            if (a.date.getTime() > b.date.getTime()) {
                return 1;
            }
            return 0;
        });
        return array;
    }

    urlSafe(urlUnsafe: string): SafeUrl {
        return this.sanitizer.bypassSecurityTrustResourceUrl(urlUnsafe);
    }

}
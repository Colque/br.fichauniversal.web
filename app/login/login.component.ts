﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService, AuthenticationService } from '../_services/index';

import { Inject, NgZone } from '@angular/core';
import { Title } from '@angular/platform-browser';
import './../../js/myplugins.js';
declare var LoginActive: any;

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.css']
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;
    error: any;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        title:Title,
        @Inject(NgZone) private zone: NgZone) { 
            title.setTitle('Inicio de Sesión - Ministerio de la Primera Infancia');
        }

    ngOnInit() {
        this.zone.runOutsideAngular(()=>{
            LoginActive.Activar();
        });
        // reset login status
        this.authenticationService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    }

    login() {
        

        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(
                data => {
                    this.route.params.subscribe(params => {
                        if(typeof params !== "undefined"){
                            if(params.returnUrl != ""){
                                //this.router.navigate([this.returnUrl]);
                                this.router.navigate([params.returnUrl]);
                            }
                            
                        }
                    });
                    
                },
                error => {
                    this.error = error.error;
                    this.alertService.error(error.error.error_description);
                    this.loading = false;
                });
    }
}

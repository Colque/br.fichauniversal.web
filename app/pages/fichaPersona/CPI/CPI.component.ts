import { Component, OnInit, ViewChild } from '@angular/core';
//import {NgControl} from '@angular/common';
import { ApiBackEndService } from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import 'rxjs/Rx';
import { Inject, NgZone, ElementRef } from '@angular/core';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var myModalCPIActividades: any;
declare var myModalTestResultados: any;
declare var myStringFunctions: any;

import { TLItem } from './../../../components/timeline/model/tl-item';
import { Md2Dialog } from 'Md2';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Title } from '@angular/platform-browser';
import { Observable } from 'rxjs/Rx';

@Component({
    selector: 'tarjetaCPI',
    templateUrl: 'CPI.component.html',
    styleUrls: ['CPI.component.scss']
})

export class CPIComponent implements OnInit {
    @ViewChild('ModalCPI2', undefined) ModalCPI2: Md2Dialog;
    @ViewChild('testResult') iframe: ElementRef;
    @ViewChild('ModalTest', undefined) ModalTest: Md2Dialog;
    @BlockUI() blockUI: NgBlockUI;
    error: string = "";
    testPersona: any;
    detalleTest: any = [];
    filterTest: any;
    admisiones: any;
    contador: any
    prefijo = 'CPI';
    timeLineItems: TLItem[] = [];
    tieneItemsTimeLine = false;
    public ActividadesFotos: any;
    private ActividadesFotosLinea: any;
    stringFunctions = myStringFunctions;
    loading: boolean = false;
    fotosAdmision: any = [];

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService,
        private title:Title,
        @Inject(NgZone) private zone: NgZone) {

    }

    ngOnInit() {
        //this.loadData();
        this.admisiones = this.fichaPService.admisionesCPI;
        this.testPersona = this.fichaPService.testPersona;
        this.detalleTest = this.fichaPService.detalleTest;
        //console.log('this.admisiones',this.admisiones );
        if (this.admisiones.length > 0) {
            this.admisiones.forEach((ad: any, i: any) => {
                ad.titulo = 'ADMISIÓN: ' + ad.FechaIngreso,
                    ad.active = i === 0 ? true : false;
            });
            //myLoading.Ocultar('Loading' + this.prefijo);
            this.fichaPService.tieneDatosCPI = true;
            this.GetItemsLinea(this.admisiones[0].Admision,this.admisiones[0].FechaIngreso);
        } else {
            this.fichaPService.tieneDatosCPI = false;
        }
    }

    open() {
        this.fichaPService.esVisible = true
        this.blockUI.stop();
        this.error = "";
        this.ModalCPI2.open();
    }

    async loadData() {
        if (this.apiBackEndService.currentUser.CodSistemaPermisoList.indexOf("CPI") != -1){
            let correcto = false;
            let count = 1;
            while (!correcto && count <= this.fichaPService.maxIntentos) {
                count++;
                await Promise.all([
                    this.fichaPService.getDatosCPI(),
                    this.fichaPService.getPersonaTest()
                ]).then((resultado: any) => {
                    if (resultado[0].name !== "TimeoutError") {
                        correcto = true;
                        this.fichaPService.admisionesCPI = resultado[0]["Result"].Admisiones;
                    }
                    this.testPersona = this.fichaPService.testPersona;
                    this.detalleTest = this.fichaPService.detalleTest;
                });
            }

            // this.admisiones = this.fichaPService.admisionesCPI;
            // //console.log('this.admisiones',this.admisiones );
            // if (this.admisiones.length > 0) {
            //     this.admisiones.forEach((ad: any, i: any) => {
            //         ad.titulo = 'ADMISIÓN: ' + ad.FechaIngreso,
            //             ad.active = i === 0 ? true : false;
            //     });
            //     //myLoading.Ocultar('Loading' + this.prefijo);
            //     this.fichaPService.tieneDatosCPI = true;
            //     this.GetItemsLinea(this.admisiones[0].Admision,this.admisiones[0].FechaIngreso);
            // } else {
            //     this.fichaPService.tieneDatosCPI = false;
            // }
        } else {
            this.fichaPService.tieneDatosCPI = false;
        }
    }

    // public openModal(index: any, modal: string) {
    //     //ByOso.openModalVisita('Historia Clínica: '+ index,modal);
    //     this.ModalCPI2.open();
    // }

    onChangeTab(event:any){
        this.GetItemsLinea(this.admisiones[event.index].Admision,this.admisiones[event.index].FechaIngreso);
    }

    public closeModal(){
        this.ModalCPI2.close()
    }

    GetItemsLinea(_Admision: string, _FechaIngreso: string): any {
        var resultadoTL: any = [];
        this.admisiones.forEach((element: any) => {
            if (element["Admision"] == _Admision && element["FechaIngreso"] == _FechaIngreso) {
                if (element["Archivos"] && element["Archivos"].length > 0) {
                    //agregamos fotos de Archivo
                    element["Archivos"].forEach((elementA: any) => {
                        if (elementA["URL"] != null) {
                            var fechaArray = elementA["FechaRegistro"].split("/");
                            var itemLinea = {
                                FotoTitulo : elementA["Titulo"],
                                url : elementA["URL"],
                                FotoDescrip : elementA["Descripcion"],
                                type : this.tipoArchivo(elementA["TipoArchivo"]),
                                FotoLarge : elementA.FotoFull || elementA.URL,
                                FotoSmall : elementA.FotoSmall || elementA.URL,
                                FotoMedium : elementA.FotoSmall || elementA.URL,
                                category : "persona",
                                FechaRegistro : new Date(fechaArray[2], fechaArray[1]-1, fechaArray[0]),
                                Fecha : fechaArray[0] + "-" + fechaArray[1] + "-" + fechaArray[2].substring(2,4),
                            }
                            resultadoTL.push(itemLinea);
                        }
                    });

                    //agregamos fotos de Talleres
                    element["Talleres"].forEach((elementC: any) => {
                        this.GetFotosActividadesParaLinea(elementC).forEach((elementT: any) => {
                            resultadoTL.push(elementT);
                        });
                    });

                }
            }
        });
        this.fotosAdmision = resultadoTL;
        console.log('this.fotosAdmision',this.fotosAdmision);
    }

    MostrarLinea(_Admision: string, _FechaIngreso: string): boolean {
        return (this.fotosAdmision.length > 0);
    }

    tipoArchivo(COD: string): any {
        var resultado = "";
        switch (COD) {
            case "COD_IMAGEN":
                resultado = "imagen";
                break;
            case "COD_VIDEO":
                resultado = "video";
                break;
            case "COD_AUDIO":
                resultado = "audio";
                break;
        }

        return resultado;
    }

    testEstado(_Seccion: any): boolean {
        var _Estado = true;
        if (typeof _Seccion !== "undefined") {
            if (typeof _Seccion["Preguntas"] !== "undefined") {
                _Seccion["Preguntas"].forEach((elementA: any) => {
                    if (elementA["CodEstado"] != "COD_LOGRADO") {
                        _Estado = false;
                    }
                });
            }
        }
        return _Estado;
    }

    testEstadoIcono(_Seccion: any): string {
        return (this.testEstado(_Seccion) ? "COD_LOGRADO.png" : "COD_NO_LOGRADO.png");
    }

    GetFotosActividades(_Taller: any) {
        this.ActividadesFotos = [];
        if (typeof _Taller !== "undefined") {
            if (typeof _Taller["Fotos"] !== "undefined") {
                _Taller["Fotos"].forEach((elementA: any) => {
                    if (elementA["URL"] != "" && elementA["URL"] != null) {
                        this.ActividadesFotos.push(
                            {
                                URL: elementA["URL"].toString(),
                                Titulo: _Taller["Taller"],
                                Fecha: elementA["FechaRegistro"],
                            }
                        );
                    }
                });
            }
        }

        if (this.ActividadesFotos.length > 0) {
            this.zone.runOutsideAngular(() => {
                myModalCPIActividades.Abrir(this.ActividadesFotos.length);//Título del Modal, CantidadFotos
            });
        }

        //return this.ActividadesFotos;
    }

    GetFotosActividadesParaLinea(_Taller: any): any {
        this.ActividadesFotosLinea = [];
        if (typeof _Taller !== "undefined") {
            if (_Taller["Fotos"] && _Taller["Fotos"].length > 0) {
                _Taller["Fotos"].forEach((elementA: any) => {
                    if (elementA["URL"] != "" && elementA["URL"] != null) {
                        var fechaArray = elementA["FechaRegistro"].split("/");
                        var itemLinea = {
                            FotoTitulo : _Taller["Taller"],
                            url : elementA["URL"],
                            FotoDescrip : elementA["Descripcion"],
                            type : this.tipoArchivo(elementA["TipoArchivo"]),
                            FotoLarge : elementA.FotoFull || elementA.URL,
                            FotoSmall : elementA.FotoSmall || elementA.URL,
                            FotoMedium : elementA.FotoSmall || elementA.URL,
                            category : "persona",
                            FechaRegistro : new Date(fechaArray[2], fechaArray[1]-1, fechaArray[0]),
                            Fecha : fechaArray[0] + "-" + fechaArray[1] + "-" + fechaArray[2].substring(2,4),
                        }                    

                        this.tieneItemsTimeLine = true;
                        this.ActividadesFotosLinea.push(itemLinea);
                    }
                });
            }
        }
        return this.ActividadesFotosLinea;
    }

    TieneFotosActividades(_Taller: any): boolean {
        var TieneFotos = false;
        if (typeof _Taller !== "undefined") {
            if (typeof _Taller["Fotos"] !== "undefined") {
                _Taller["Fotos"].forEach((elementA: any) => {
                    if (elementA["URL"] != "" && elementA["URL"] != null) {
                        TieneFotos = true;
                    }
                });
            }
        }

        return TieneFotos;
    }

    MostrarTest(_Titulo: string, _Fecha: string, _URL: string) {
        if (_URL != "") {
            this.zone.runOutsideAngular(() => {
                myModalTestResultados.Abrir(_Titulo, _Fecha, _URL);//Título del Modal, CantidadFotos
            });
        }
    }

    onLoadFunc(myIframe: any) {
        this.loading = !this.loading;
        console.log('myIframe', myIframe);
    }

    public openModal(CodCabeceraTest: string, CodTest: string, FechaCargaDate: any){
        this.filterTest = this.detalleTest.filter((d: any) => d.CodCabeceraTest === CodCabeceraTest && d.CodTest === CodTest && d.FechaCargaDate == FechaCargaDate);
        this.sort();
        this.ModalTest.open();
    }

    private sort(){
        var nivelMaximo = this.filterTest.reduce((max: any, p: any) => p.Nivel > max ? p.Nivel : max, this.filterTest[0].Nivel);
        const oldTreeData = JSON.parse(JSON.stringify(this.filterTest));
        let mThis = this;
         this.filterTest.forEach((nodo: any) => {
                var hijos = this.filterTest.filter((r:any) => r.CodItemPadre === nodo.CodItem);
                if (hijos.length > 0){
                        nodo.EsUltimoNivel = false;
                        nodo.hideChildren = false
                } else{
                        nodo.EsUltimoNivel = true;
                        nodo.hideChildren = true;
                }
                nodo.visible = true;
            });
    }

    public toggleChildren(codItem: string) {        
        const item = this.filterTest.find((i:any) => i.CodItem === codItem);
        item.hideChildren = !item.hideChildren;
        this.filterTest.filter((nodo:any) => nodo.CodItemPadre === codItem).forEach((_nodo:any) => {
            if (item.hideChildren) {
                _nodo.visible = false;
                this.toggleChildren(_nodo.CodItem);
            } else {
                _nodo.visible = true;
            }
        });
    }

}
import { Component, OnInit, ViewChild } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';
import { Inject, NgZone } from '@angular/core';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var myModalConinActividades: any;
declare var myModalTestResultadosConin: any;
declare var myStringFunctions: any;
declare var ByOso: any;

//linea de tiempo
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TLItem} from './../../../components/timeline/model/tl-item';
import { Md2Dialog } from 'Md2';

@Component({
    selector: 'tarjetaConin',
    templateUrl: 'Conin.component.html',
    styleUrls: ['Conin.component.scss']
})

export class ConinComponent implements OnInit{
    @ViewChild('ModalConin2', undefined) ModalConin2: Md2Dialog;
    admisiones: any;
    prefijo = 'Conin';
    timeLineItems: TLItem[] = [];
    tieneItemsTimeLine = false;
    public ActividadesFotos: any;
    private ActividadesFotosLinea: any;
    stringFunctions = myStringFunctions;

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService,
        @Inject(NgZone) private zone: NgZone) {

    }

    ngOnInit(){
         if (this.apiBackEndService.currentUser.CodSistemaPermisoList.indexOf("CONIN") != -1){
            this.loadData();
        } else {
            this.fichaPService.tieneDatosConin = false;
        }
    }



    loadData() {
        this.fichaPService.getPersonaConin().then(() => {
            this.admisiones = this.fichaPService.admisionesConin;
            console.log('admisionesConin',this.admisiones);
            if (this.admisiones && this.admisiones.length > 0){
                this.admisiones.forEach((ad:any, i:any) =>{
                    ad.titulo = 'ADMISIÓN: '+ ad.FechaIngreso,
                    ad.active = i === 0 ? true : false;
                });
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaPService.tieneDatosConin = false;
            }
        })
    }

    public openModal(index: any, modal: string){
        //ByOso.openModalVisita('Historia Clínica: '+ index,modal);
        this.ModalConin2.open();
    }

    GetItemsLinea(_Admision: string): TLItem[]{
        var resultadoTL: TLItem[] = [];

        this.admisiones.forEach((element: any) => {
            if(element["Admision"] == _Admision){
                if(element["Archivos"] !== []){
                    //agregamos fotos de Archivo
                    element["Archivos"].forEach((elementA: any) => {
                        if(elementA["URL"] != null){
                            var fechaArray = elementA["FechaRegistro"].split("/");
                            var itemLinea: TLItem = new TLItem();
                            itemLinea.name = elementA["Titulo"];
                            itemLinea.url = elementA["URL"];
                            itemLinea.description = elementA["Descripcion"];
                            itemLinea.type = this.tipoArchivo(elementA["TipoArchivo"]);
                            itemLinea.category = "persona";
                            itemLinea.date = new Date(fechaArray[2], fechaArray[1]-1, fechaArray[0]);
                            this.tieneItemsTimeLine = true;
                            resultadoTL.push(itemLinea);
                        }
                    });
                    
                    //agregamos fotos de Talleres
                    element["Talleres"].forEach((elementC: any) => {
                        this.GetFotosActividadesParaLinea(elementC).forEach((elementT: any) => {
                            resultadoTL.push(elementT);
                        });
                    });
                    
                }
            }
        });

        return resultadoTL;

    }

    MostrarLinea(_Admision: string): boolean{
        return (this.GetItemsLinea(_Admision).length > 0);
    }

    tipoArchivo(COD: string): any{
        var resultado = "";
        switch(COD){
            case "COD_IMAGEN":
                resultado = "imagen";
            break;
            case "COD_VIDEO":
                resultado = "video";
            break;
            case "COD_AUDIO":
                resultado = "audio";
            break;
        }

        return resultado;
    }

    testEstado(_Seccion: any): boolean{
        var _Estado = true;
        if(typeof _Seccion !== "undefined"){
            if(typeof _Seccion["Preguntas"] !== "undefined"){
                _Seccion["Preguntas"].forEach((elementA: any) => {
                    if(elementA["CodEstado"] != "COD_LOGRADO"){
                        _Estado = false;
                    }
                });
            }
        }
        return _Estado;
    }

    testEstadoIcono(_Seccion: any): string{
        return (this.testEstado(_Seccion) ? "COD_LOGRADO.png" : "COD_NO_LOGRADO.png");
    }

    GetFotosActividades(_Taller: any){
        this.ActividadesFotos = [];
        if(typeof _Taller !== "undefined"){
            if(typeof _Taller["Fotos"] !== "undefined"){
                _Taller["Fotos"].forEach((elementA: any) => {
                    if(elementA["URL"] != "" && elementA["URL"] != null){
                        this.ActividadesFotos.push(
                            {
                                URL: elementA["URL"].toString(),
                                Titulo: _Taller["Taller"],
                                Fecha: elementA["FechaRegistro"],
                            }
                        );
                    }
                });
            }
        }

        if(this.ActividadesFotos.length > 0){
            this.zone.runOutsideAngular(()=>{
                myModalConinActividades.Abrir(this.ActividadesFotos.length);//Título del Modal, CantidadFotos
            });
        }

        //return this.ActividadesFotos;
    }

     GetFotosActividadesParaLinea(_Taller: any): any{
        this.ActividadesFotosLinea = [];
        if(typeof _Taller !== "undefined"){
            if(typeof _Taller["Fotos"] !== "undefined"){
                _Taller["Fotos"].forEach((elementA: any) => {
                    if(elementA["URL"] != "" && elementA["URL"] != null){
                        var fechaArray = elementA["FechaRegistro"].split("/");

                        //console.log(new Date(fechaArray[2], fechaArray[1], fechaArray[0]));

                        var itemLinea: TLItem = new TLItem();
                        itemLinea.name = _Taller["Taller"];
                        itemLinea.url = elementA["URL"];
                        itemLinea.description = elementA["Descripcion"];
                        itemLinea.type = this.tipoArchivo(elementA["TipoArchivo"]);
                        itemLinea.category = "persona";
                        itemLinea.date = new Date(fechaArray[2], fechaArray[1]-1, fechaArray[0]);
                        this.tieneItemsTimeLine = true;
                        this.ActividadesFotosLinea.push(itemLinea);
                    }
                });
            }
        }

        return this.ActividadesFotosLinea;
    }

    TieneFotosActividades(_Taller: any): boolean{
        var TieneFotos = false;
        if(typeof _Taller !== "undefined"){
            if(typeof _Taller["Fotos"] !== "undefined"){
                _Taller["Fotos"].forEach((elementA: any) => {
                    if(elementA["URL"] != "" && elementA["URL"] != null){
                        TieneFotos = true;
                    }
                });
            }
        }

        return TieneFotos;
    }

    MostrarTest(_Titulo:string , _Fecha: string, _URL:string){
        if(_URL != ""){
            this.zone.runOutsideAngular(()=>{
                myModalTestResultadosConin.Abrir(_Titulo, _Fecha, _URL);//Título del Modal, CantidadFotos
            });
        }
    }

}
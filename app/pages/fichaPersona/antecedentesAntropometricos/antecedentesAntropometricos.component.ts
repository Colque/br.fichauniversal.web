import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var ByOso: any;

@Component({
    selector: 'tarjetaAntecedentesAntropometricos',
    templateUrl: 'antecedentesAntropometricos.component.html',
    styleUrls: ['antecedentesAntropometricos.component.scss']
})

export class AntecedentesAntropometricosComponent implements OnInit {
    antecedentes: any;
    antecedentesFC: Observable<Array<any>>;
    prefijo = 'ANTA';

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService
    ) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        var promises :any = [];
        promises.push(
            new Promise((resolve, reject) => { 
                this.fichaPService.getPersonaAntecedentesAntropometricos().then(() =>{
                    this.antecedentes = this.fichaPService.antecedentes;
                    resolve();
                });
            })
        )

        promises.push(
            new Promise((resolve, reject) => { 
                this.fichaPService.getPersonaAntecedentesAntropometricosFC().then(() =>{
                    this.antecedentesFC = this.fichaPService.antecedentesFC;
                    resolve();
                });
            })
        )

        Promise.all(promises).then(() => 
            {
                if (this.antecedentes && this.antecedentes.length > 0){
                    myLoading.Ocultar('Loading'+ this.prefijo);
                } else{
                    this.fichaPService.tieneAntAntroP = false;
                }
            }
        );
    }
    
    public openModal(index: any, modal: string){
        ByOso.openModalVisita(index,modal);
    }
}
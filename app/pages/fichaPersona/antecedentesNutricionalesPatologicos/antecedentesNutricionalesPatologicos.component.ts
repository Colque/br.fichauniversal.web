import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaAntecedentesNutricionalesPatologicos',
    templateUrl: 'antecedentesNutricionalesPatologicos.component.html',
    styleUrls: ['antecedentesNutricionalesPatologicos.component.scss']
})

export class AntecedentesNutricionalesPatologicosComponent implements OnInit {
    fechasAgrupadas: any;
    antecedentesNutricionales: Observable<Array<any>>;
    antecedentesPatologicos: Observable<Array<any>>;
    prefijo = 'ANTNP';

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService
    ) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fichaPService.getPersonaAntecedentesNutricionales().then(() => {
            this.fechasAgrupadas = this.fichaPService.fechasNutr;
            this.antecedentesNutricionales = this.fichaPService.antecedentesNutricionales;
            this.antecedentesPatologicos = this.fichaPService.antecedentesPatologicos;
            if (this.fechasAgrupadas && this.fechasAgrupadas.length > 0){
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaPService.tieneAntNutPat = false;
            }
        })
    }

    tieneVisita(EsNutricional: boolean, Fecha: string, Origen: string, NroRelevamiento: string): boolean{
        var EsIgual = false;
        if(EsNutricional){
            this.antecedentesNutricionales.forEach(element => {
                if(element['Fecha'] == Fecha && element['Origen'] == Origen && element['NroRelevamiento'] == NroRelevamiento){
                    EsIgual = true;
                }
            });
        }else{
            this.antecedentesPatologicos.forEach(element => {
                if(element['Fecha'] == Fecha && element['Origen'] == Origen && element['NroRelevamiento'] == NroRelevamiento){
                    EsIgual = true;
                }
            });
        }

        return EsIgual;
    }

    tabActiva(EsNutricional: boolean, Fecha: string, Origen: string, NroRelevamiento: string): string{
        var EsActiva = false;

        if(EsNutricional){
            if(this.tieneVisita(true, Fecha, Origen, NroRelevamiento)){
                EsActiva = true;
            }
        }else{
            if(!this.tieneVisita(true, Fecha, Origen, NroRelevamiento)){
                if(this.tieneVisita(false, Fecha, Origen, NroRelevamiento)){
                    EsActiva = true;
                }
            }
        }

        return (EsActiva ? "active" : "");
    }
    
}
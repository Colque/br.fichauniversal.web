import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var ByOso: any;

@Component({
    selector: 'tarjetaAntropometriaColombia',
    templateUrl: 'antropometriaColombia.component.html',
    styleUrls: ['antropometriaColombia.component.scss']
})

export class AntropometriaColombiaComponent implements OnInit {
    antropometria: any;
    origenes: Observable<Array<any>>;
    prefijo = 'ANTROCOLOMBIA';

    constructor(
        public fichaPService: FichaPersonaDataService,
        public apiBackEndService: ApiBackEndService
    ) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.antropometria = this.fichaPService.antropometria;
    }

    Color(_Color: string): string {
        _Color = _Color == null ? "" : _Color;
        return _Color;
    }

    EdadSmall(_Edad: string): string{
        if(typeof _Edad !== "undefined"){
            _Edad = _Edad.replace("meses", "m");
            _Edad = _Edad.replace("mes", "m");
            _Edad = _Edad.replace("años", "a");
            _Edad = _Edad.replace("año", "a");
            _Edad = _Edad.replace("dias", "d");
            _Edad = _Edad.replace("dia", "d");
        }

        return _Edad;
        
    }

    public openModal(index: any, modal: string){
        ByOso.openModalVisita(index,modal);
    }
}
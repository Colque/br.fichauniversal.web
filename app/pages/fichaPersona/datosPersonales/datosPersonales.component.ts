import { Component, OnInit, Output, EventEmitter, Input, ViewChild, ElementRef } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import { Title }     from '@angular/platform-browser';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaDatosPersonales',
    templateUrl: 'datosPersonales.component.html',
    //styleUrls: ['./app/components/todolist/todolist.component.css']
})

export class DatosPersonalesComponent implements OnInit {
    @ViewChild('myImage') myImage: ElementRef;
    public datosPersonales: any;
    public grupoFamiliar: any
    prefijo = 'PER';
    //@Output() datosPers: EventEmitter<any>;

    constructor(
        public apiBackEndService: ApiBackEndService,
        public fichaPService: FichaPersonaDataService,
        private title:Title
    ) {

    }

    ngOnInit(){
        this.loadData();
    }

    loadData() {
        this.fichaPService.getDatosPersonales().then(()=>{
            this.datosPersonales = this.fichaPService.datosPersonales;
            console.log('this.datosPersonales', this.datosPersonales);
            if (this.datosPersonales && this.datosPersonales.length > 0){
                let titulo = this.fichaPService.nombreCompleto !== null &&  this.fichaPService.edad !== null ? this.fichaPService.nombreCompleto+ ' - ' + this.fichaPService.edad + ' - ' + 'Ficha Universal - ' : 'Ficha Universal - ';
                this.title.setTitle(this.apiBackEndService.enviroment === 'colombia' ? titulo + 'Colombia' : titulo + 'MPI');
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaPService.nombreCompleto = '';
                this.fichaPService.tieneDatosP = false;
                this.title.setTitle('Ficha Universal - MPI');
            }
        });
    }

    loadImg(){
        let heightAux = this.myImage.nativeElement.offsetWidth + (this.myImage.nativeElement.offsetWidth * 0.2);
        if (this.myImage.nativeElement.offsetHeight > heightAux){
            this.datosPersonales[0].columns = 3;
        } else{
            this.datosPersonales[0].columns = 2;
        }
    }
}
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaDatosPersonalesColombia',
    templateUrl: 'datosPersonalesColombia.component.html',
    //styleUrls: ['./app/components/todolist/todolist.component.css']
})

export class DatosPersonalesColombiaComponent implements OnInit {
    public datosPersonales: any;
    prefijo = 'PERColombia';
    //@Output() datosPers: EventEmitter<any>;

    constructor(
        public apiBackEndService: ApiBackEndService,
        public fichaPService: FichaPersonaDataService,
    ) {

    }

    ngOnInit(){
        this.loadData();
    }

    loadData() {
        this.datosPersonales = this.fichaPService.datosPersonales;
    }
    
}
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import { ApiBackEndService } from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {
    Component,
    EventEmitter,
    OnInit,
    Output,
    ViewChild
    } from '@angular/core';
import { Md2Autocomplete } from 'Md2';
import { Md2Dialog, Md2DialogConfig } from 'Md2';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
//import { BlockTemplateComponent } from './../../../components/block-template/block-template.component';

//import * as jsPDF from 'jspdf';
//require('jspdf-autotable');
//declare var jsPDF: any; // Important 
//import 'jspdf-autotable';
import * as $ from 'jquery';
import * as moment from 'moment';

declare var html2canvas: any;
declare var jsPDF: any;
declare var ByOsoAntro: any;
declare var graficoPECanvas: any;
declare var graficoTECanvas: any;
declare var graficoIMCCanvas: any;

@Component({
    selector: 'descargar-ficha-p',
    templateUrl: './descargarFichaP.component.html',
    styleUrls: ['./descargarFichaP.component.scss']
})
export class DescargarFichaPComponent implements OnInit {
    @ViewChild('modal', undefined) modal: Md2Dialog;
    @BlockUI() blockUI: NgBlockUI;
    //blockTemplate: BlockTemplateComponent = BlockTemplateComponent;
    public disable: boolean;
    public rubros: any;
    public unidadesMedida: any;
    public item: any;
    public codigoPadre: string;
    aframe: any;
    public url: string;
    error: string = "";
    maxRows = 34;
    config: Md2DialogConfig = new Md2DialogConfig();

    datosPersonales: any = [];
    visitas: any = [];//{Fecha:'09-04-18'},{Fecha:'18-02-16'},{Fecha:'09-04-18'}];
    tokenMultimedia = '?st=2018-06-21T12%3A58%3A00Z&se=2019-06-22T12%3A58%3A00Z&sp=rl&sv=2017-07-29&sr=c&sig=Mz8f3PBwo1LS0s%2Bs2UEDVM7yoW1FxEaouW1mRXp%2FTPU%3D';
    base64Characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
    private CountFicha : number = 0;
    private first: any;

    //img de checkbox
    private checkBlack = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAA4UlEQVRIS+2W0Q2CMBCG7ygDMEpNy7tM4Ag6gk5gnEBHcAUn0HdK7CYwAHDmiCASX7BIfGgfm/b/8v+55H4EAJBSRmEY7oloAwAR301wCkQ8l2V5sNYWyBAhxJV5E4h/krBVVSWotT4S0ZZfIOIJAC7GmJsLVGu9BIBVXxeVUjnHxRBjzM4FMPzbM1EwiJ5uElcnQ1Acx7Ku63uj34KyLMMp3bRanREPGhuvj25sYt17H52Pzg/D1zPwB9EFQbBI09S6e3gp8EonIu4jzeKbZ5X/opzwCiei9Vs5ma1uzVUgH8q9xnET1/juAAAAAElFTkSuQmCC';
    private checkRed = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAA40lEQVRIie2UMQrCMBSGP0Xo6uCgo5NHcHfSI4i7B/AYPYC7JxCcRBSv4Cnagji6+ruk8ITGJuKkfRBo0y//92jTQFP/WYJEkApygSrG2bBTwUkwixGknuAXgWAouLu5Y4yg7HzyhmkJ9ka6ihFIoBpmYcIvgs7HAkFfsBQM3H1PcHXcQzAODvcItm4uE4wEG9P9OircI7Af/WauC0H3G4JEsKvYTXP3/Gy3brTAIzkIWj4+WmAkqet4WMdHCz7h2541hVvo/dFMeMnkoQ2FHBVVI40RlO86CwjOHJsEC5r6rXoCCNLsTuA7niUAAAAASUVORK5CYII=';

    //foto default
    private fotoDefault = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKcAAACnCAYAAAB0FkzsAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAABSjSURBVHhe7Z3pVxtnlsavtirtCDBeweANO3HcnqSnz5wz58yH+cvnw3zok0y3gx1jbIMNmEUsQkJ7aZ/73FKlaU8mToKAqrfuk5S1IFAtv/cu73IrsrtfHJFK5UNFx48qle+kcKp8K4VT5VspnCrfSuFU+VYKp8q3UjhVvpXCqfKtFE6Vb6VwqnwrhVPlWymcKt9K4VT5VgqnyrdSOFW+lcKp8q0UTpVvpXCqfCuFU+VbKZwq30rhVPlWCqfKt1I4Vb6VwqnyrRROlW+lcKp8K4VT5VspnCrfSuFU+VYKp8q3UjjPqeFoRMQbHke/sKn+uBTO3yHANhwOf36EYlE+hbzFYzGKxaLuxq+j441/iz/rgur9ruq3SYvHfkGASTZ+DgCxjSjCoPWp3+tRvz8Q+HqDPn+Y4eOfRSMxisYjFI/GKR6PkZWI8/uANUKDPv/eYCB/OxKJyKb6ZSmcvyJYOsaH7KQtgNVrDarW63RSqVK706Jeb8BwAtA+uOSzyRtABpzRISVigDNBlp2gXDpLuVyGCoU85bI5cpwOdbtd+Y4YA6/6v1I4P5PrfkcMTIRSySRbugEdHB/TxuYWdRkowRUWT9w2fsO1fIB4/BSeXP7xXPiALSWeeZ/MZdN0f2mR5mav8esRdcaQqiX9ZymcY3nuG3CkUkkGpkcft7bo094+Q9mlTDrlAskxpQfZb5ZYU/68fAexxe1S23Hkb96/u0ALCwvyvXgfDcONVVUKJ8uDMh6Ps6vt0MdPO7T9aZfPToQyDCpcs5cAnVeeNQWAaAAdhjTOIcODxSW6ffs6hw8W9ThU8EytWOSQKtRwetYywVDCR+/t7dH2TpFaTouSti2wep+5KHcL+ACjw6CKu1+8S7ev36AuvzcYDuTnYXX1oYYTmXM6k6E6JzlvNz5SpVLhWDNOlpUYf+Jy1el0GMghzc3M0PNnXwuYDsej0l0VQoUWTrjpbDZLx8cl+uHHFekisizryq0U9qvNiVfStugv3/4LZTkubbHrD2McGjo4vZgvyZk4Hzu9fL1K6XSa4rj4PnGf2Atk8F3O8v/1+XOaK+Sp3evJ+2Fy8aFqjh6YiCXX3q/Ti5cvKZvJitX0C5gQ9hJW3Ob9/Ov3/0MftnbEgrpDpO5nwqDQwZnLZ+n9+oZk41P5KYr62BAlEgmamsrR2vo6J2p7lMukf25gYVBo4MRFzQNMTnzWP25RJpty4zifu0nsYzaboVdr7zgMOaB0yqbBIByAhgJOJBlwkx8+bHOM+YZmr83ym8GJ3TC8mU7a9Gr1DR2USpS04pLVmy7j4QSY6MesVKu0vrlFU7kcDTHxImB5BQCFm3/1eo0G7AUS/Np0F288nLh8GCaExeR0QpKhoGa8gBPj9K/4WNDbgOMwGVCj4RwOB1TI5+j7FyvUcTpks2sPutKpFB0cHUkGb8fdwQJT+TQWTliURMKivf0ibweSVJggWEsMHrz/sEHVZp09AS6hmXQaCycuIiznx+1dsu3gW8yzQgaPxvdpZ4+PMxbYMOVLMhJO2BEkEIdHJU6ETiVTN00Y3tw/OKJTTvRMHdo08qhgRzCjB8OTFicRJlqWaBTZ+lCsZ3ycKJkm4+BE1xGGI49LZaqcViU7N1WZTIY+7e9To86xp4FdS0ZaTliV3b39QHcb/SYxjMjeX6+9c/tvDeuYNwpOWA4sza036lSqlGW5hdHihpeIJ+i0VqNqo2Gc9TQKTsRduWyeNja3pRspDEJjRJRdPDyUTnop8mCIjIETlwRZa7/XpePyiQzvhUVYtoyOeTh1kzJ3c+Bki2FxjHl4UmZAB0YnQp8Lx1pvtKjBrh1LOkxx7eY0M74g8USMTspljsPCVaTAHXAYUa1WdxfrGSJz4OS4qz8YUq1ekw54o7P0zyTHOuJj50QQk1tMkRFwull6hDqdLrXq7NpCFG96wtr609M69foDgdUERI2xnNFIlBynSz22nqjMETahSFin36XBoD+2pMHH0xw4Y1FqtVtiMcLj0M8I1nI4pE7bceHEFnAZAyeyVFRuGw7DWSHDPeIItTsdOX6AGnQZAyfqYjocc6L0YBgFIIeDkcAZNaRxGgMn1gUNpUhm+OJNCHBGIijfyOfBkIzdIMuJ4UskA+7rUAruHMuGhwqnbyQxFmenJsRZ51PEXTJsSAM1xnLiQAK0FP1CZNrhmwEnrCZbz6g5be0PitNCGVsfvwy4jLiauBiINWU+4/i9MArHjs54bCbIDFPD1wIjRDFZVxNWPN3jlop5hjh4Y/wgkiLLYjgNyVR/rzAriWMbd5K1IQ3UGDixfiaVcivHmTQz5/coGhlRJmlLf68J8wuMgRNdKIAT83HC6NkRzsB72HwOpG0acBKMspxJthoRJEUh7O+EV7dsi1CeRtYRIUMMuIyAExYDi9tsy6Z8NmtkgYEvCWun8rk8WXHLtaLj94MsYywnhEwV95fELQHDmLVP8bEj3jblyI2BE9YTd+Odm7lG/WG44ERIE+WGmc/n5SaxZnQkmQYnX5jZ6WmxoKZVv/g1IYxJp1OUzWSkSwnnwgQZ5dZRvCuVTtJ0YYp6vf74XfPV6fXoxrXrnAyZ1SiNghMjRFgeu/zgnky6DYPk3kQcY8/fvk5duZGWKU7dMDjduLNPU5yxz81Oy7IN09XtcpbOnqKQm+KMneNNQ8bVIaPg9IQb9t+dv0M9jkFNT4y6jkNfP35E9WZDRsdMknFwwnoCytnpGU4Q0kb3eWK16ezsHM0UpqmLLN2QRMiTkXAiKbCtOC0u3JG17CYKjQ5e4dH9Reqzazex9LaRbh0XCtn6zevXKZ/PyX3MTVO3yxn63HUqFPLSr2uWzXRlJJwQOlTQ3/lwaUG6WkySdBdFRrS0uCD9mqbKWDixdhv3LF/gxGg6P8XxWXv8kwALHPIVazYatLSwQAVvRMiwWNOTsXBCKOhVrdXpP//j3/kCsisMugXlY2g3HSrMTNPTx8vSjWRirOnJaDg9OWw1v33+jEaDobt0NqCeUDrZoxH69k/PZJDB9CFa4+GEZen0ujQ7XaDFxXmqN5pS9Csoc3e8/UR2jiToq+WHUsEZSZ7JVhMKheVETNZxuvT0yRN6yElEpVJhD+n/Gp4AE8OR2P8mW/+nT5Zp/tYtcedhqEEaCjglWOMjrVar4t5v3rxBjUbN96NHABML9k7KFfp6+RHd54bljC2mqUnQWYUEThyoe0FPqzX6M8dsd+/coWarJYD6FVHMT222W+zKH9Dy/SVuUE24gfFPzVdo4IQ8a9PjxOKrx4/o4fiCo0Kd3wTX3eNY+ZvlZXr84AFV6w0BMzxohgxOCIBimhnG37969Iief/M1tZpNSTDEil6xq8f3O44j5XW+e/YNzd+949bcDIkrP6vI7n4xGGnrBQgZcD6XpdJJhdbWN+T20Klk8h/JBkC9RCDQYBBTzk1P0/PnTykZt6nltHgXQmdDRKGGE1YKfYWWlRAON7c/0ae9fXHzlm27N5ziz10cnu5fHzCU3X5Pbk19/+5dmr99U/at1xtwQwknmFCo4fQEQGEtbYaj0W7TxsePdHBUYss6pGw2SxFUTJ6wBYWLxoBAvdGgpGUxkLckScukUtTud2XAwPR+zC9J4RxLrChDiLvwAtRGsyGW9P3Gpiw3TtpJAeq8wMhUN/6v2WzJuPjj5Ye0ePs2JRnKASdqfQYWbjxk4eUvSuE8I0DjFQLDeiQslsPt+lZW39Bh8UjAGY2tKNjBclw8AlrA7SUseO7Je467y6FUDuC2uAHML9yiJ5yQdTnGbDsdgTYMHeu/RwrnrwjAME+ymhPZPLpzcPvCGj822450SeEz2KQEzBhEz8IC8GgsQja77Vw2R5l0StaWFwo5irN1RJ8rCm7JJr+pOiuF85cEyIQW/DOS2DAO0ODSGTy43y6/J4lMty+L6tz6TG5XlNRYjkcpEYtTgpMt3F4byQ6SG1hmdK57llasrXyf4vm5FE7WWZeM5EjKB/JZwft47f4c95eMcWZvUSqVHgMXd0foZcXjZ3Dx7+DEYkZ+f9Alp9OX/kuAid4Az7pCeMT3eK+hs/sUVoUOTlz0s49nIYFi/Bqzy0dRWEF+YxQhNoBiITFhGfc0PzmpUa/rMHA96gx6TDQgBoyIK8FqnAEfSVcUKt9ZKDCWy1Ihn6N0Jk0p2+JPsQXmL2B7LPcNwj4gnj07s10aymeAhgnYUMDpgQjhOUp0wwpi6hz6ElGhDUkJOsCdjsMQcpLCjx28x9au2XLEhcfYVcc4mbHZVQvUY7Dd7Brf4YGDzB9wwX3zK4YM39NjkFFkDONySQYWFjjNoCZTSekNSCX5eZIf+bWVsOW+8Zi/6ca2sOCwsv+A03RQjYXTBRJWEC7UhRFriuK4WT6DVDw8pnKlQq1mWyZXDPnie+vcx78qlk9qrDMQiDnlROHneJSnv+565ed4Mv4MPotniE6H/F3I/pFIwc171lsaDocLSduWBCrHFvf2jRuU5ee4fSKsKbqgEAe7DeP///6gy0g4vfgtzpYRMOIe5FjjjSy7WDyi7Z1P0rmO+FE+ywACRO9Ce48efAI63vsCjF8S/gz/K38rcuZvuo/4Hw3D3bz+ULj5ZrMh2f69xQVZTVrIF8jixga40aA+j2FNkRFwCjxjwdUiMwZ0lWqVSuUyVcoVqaHU46xa3CdKU7PO/t5VCXvwJdyjbLU7HGag+woQZtntF2YKUlFvtjDNFtamLideA8QS42M6TyPyiwIPp5tAjMQN4lmbk5bdYpHK5So1nTaNcD9MvrhxzmpgSeXTAbuA0vHPCMMyem4dWT/2Ho1til3/nTu3aIqtapxjYiznwH1Ag25JAwsnYjW4RpuhRJKAsXA+Fqo26tTnzDo2jjEFP0lY5NeMEkDFBteOsCTNHuH63DVavHNbzkuLre2wjxAHY1PBaYyeAgUnLgSEk4xRF0zYKZ1U6e8/vZJsVhIYjjGxwTqa4Np+q8SisjVFbwD6Vh8/ukf3l+5Jtt/nbB8ZP85HkKxpYOB0XXGELM5k4db2Odve3N6mk9NTmpuZCbwLm6TQJsvVOkU45FlaWpCaUflMVpYWn23gfpev4QSQ2AAeNpzP45Myvd/4wLGlI9m23LFMnJbKk5wzPlmwpEikMOJ1b/4uzc/fkrmr/TM3dPAzpL6GE90pCbaUiB33i4e0e3BAJ+VTTgIAZWL8Kdeiqv5ZgM8DD32i7VabUimbbt28KaVsACmSR7fR+/P8+RJOnFiAOTU1RQ5byLW1t7RfKklMCWsZBJfkN3nntNPtcHY/RcsPl+jOrZt0yu5fLC2fW7/JV3B6rgYuCZnn1s4OrayuyQx1ZJ8QwMTHlM8/IoRJxG6dLanTols3btB3z59JDO/VXfJTw/cNnAATG7JwjG+vvl2nvcNDms7nfdmqTVC90ZDw6E9Pn9Dc7DV3+JZDAL8A6gs4BUx+xMrHg6MjevN+nbqdnlhPtZAXK6yNR6f94t15WR+P+BSW1A8G4crhBJhYnoCFXavv1mn9wyYlU5zwxL2ER3XRQvcSkqMp9lL/9udvJWbCDC3E+FepK4UTJwUd5hjtefXTazoqnUgS5MWeqsuVg+FeQjGHp3RteprabFHhua7KzV9Z0wCYWBKLOZM//P1HKldrsr7G6yRWXb6SyZRYy7+9/Il2igeUySTl/asyFlcCJw4W8SVmCf2w8lIA9WYKaTfR1QpddegZefPuPe3sFmU+6VXpUuEElLCMyBBR+uW//vuv8trGTfzHn1FdvTDoAUBXXr+hN2/fi4fDdbpsC3rpcAJMFBT44cVLmfGNriOV/4QkNZfNSA2pje1tyqTTuICXCuilwYmWB5eBUYrv//aC/Te7kJ+HIFV+FEKsfDZHa2/XaXtnn9Ick0KXBeilwIkJwQARi7wAJsJKxJwq/wtr7bE0ZPXtGu0fHYmBuSxdCpyxmFsTc+3dW6k1ibpAquAIHfKAEi4ey10uq2zOhcMJdx6NxunD5rZMd4PFvOzAWnV+IVfAROaXq6vw95cSf14onNh5ZH2V2qmUFcRz7SoKrmzbolqjRatr76XIGa7vRQJ6YXBip6UgQKdLP/74WvoxdQJH8DU9lefkaIe2Pu1IAYhAwgmhFMvrd+9oyP95U95UwRZgxCrPN+8+yK20sar1onQhcEq3UcKiw8MT2j84lpvyX2QLU12ukBBhzu3K61V277Z0D16ELgROuO/haEArqz9RPgswxz9QGaN0OsUJboXd++6FJbkThxN9mhj12dzakVqUKGigMk+A0eIEaWevKJOUUePJnZU7OU0UTjQe9Gm2nDYVj46k+0GTc3OFkuSNZpOOsb4rHmFjNNmLPWE43fmZe/tFuRGqThg2WwjfEHt+YNcOj4nOmEm694nBiZ1CH2av26P9wyOKJtSdh0EJDuFa9TqVypWfK61MShOF07YTVD6tULvpyLQrlfmCI0dVv73igYzDT3KQZXJuHTvFjWa3eMg763Y1qMIh5Bbl0yo1G22ZSY+uxEloInDCasJSokRMhXcSZfhU4RFiz16nT4cnR2JFJ2U9J2Y50WJK5VOpz4Na66pwKZm06OCgxB5z/MYENDGKcDOoo+OSDGepQw+fUNOqWqtRi72nO4fi/InRROCEGUeSVj51i2xNMmNTBUNgAAV7S5WK9H9OIuw8N5wAES0FBaFwY1MXVIUzbPI4qFVrkgxPIuycCJy4X061yonQuPvINeuqMAlGCWXQkRRjOc4kYrtzU+RlZg2nhRdSCEoVTsEoOd0uDYZ9ZvP8dJ7fcvI+yA2mpHQJ75JazdAK136ImvT9/vid8+nctZLg1jG/7/sXK1Q9rVKMs7YJT05RBUEwUhhfj4zoL989p2z6/JVCzg+n/DOkw9IxDXqcEKGjS+EMn3DZGU5c+mszMzKd7ryjhBOpMgfriUF/7IxyGW4BRylCy0x4+cgf1cRKIE5qPFUVfEnucU4woYnBiZaiUkGTABOaGJwq1aSl/T4q30rhVPlWCqfKt1I4Vb6VwqnyrRROlW+lcKp8K4VT5VspnCrfSuFU+VYKp8qnIvpf3DYieukz3doAAAAASUVORK5CYII=';
    
    //foto no disponible
    private fotoNoDisp = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARcAAAEjCAYAAAAYOEVvAAAAA3NCSVQICAjb4U/gAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAABH6SURBVHhe7d3Zb1R1H8fxXwtUVquIJmrQuBHUJ0YvjHqtMdFLFbzxxsilif4R9cILY0hMuFAhQNw3VFwRFde6iwtoRXBXcAGVglCBx/fPHp4+0JZpO9+ZM2fer+SknTPtdGY653N+++no6+s7kCSpzjoHv0pSXRkukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkJ09PX1HRj8vtI6OjpSZ6dZqvI5cOBA2r9//+Ct6miLcCFYdu/enXbs2JEDhttSGezbty9NmTIlHX/88ZULmLYIl8mTJ6cNGzakp59+OnV1dVmCUWns2bMnzZkzJy1atCifAKukLcKFQOnt7U3Lly9P06ZNG9wrNR8ll+7u7nTrrbemP//8c3BvNbTNKZzSCiUYvrq5lWmjWlRFbVk/oM3Fza0MW5XZ+CAphOEiKYThIimE4SIphOEiKYThIimE4SIphOEiKYThIimE4SIpRNtMXHz77bfTypUr01FHHTWmYdfFWht///13/l4aDp8pPmdjHdLP52r27Nmpp6enchMXDZdRECZsxx57bJo/f36eYMYsVmmoSZMmpV27dqX169fnsBhLwBguLW4i4UKp5ZxzzkkLFy7Mj1PFFcM0MYTLH3/8ke64444cENyuleHS4iYaLmeffXYOl5kzZxouOgxhsn379nT77bfnEozh8i8bdGtQVI/c3IbbOOF40jmc4SIphOEiKYThIimE4SIphOEiKYThIimE4aKWxXilQzeVh+GilkKAMEiNr0zFGBgYOLhxe+j9ai7DRS2D0CBEfvrpp/T++++nxx9/PK1YsSItXbo0f33sscfy/m3btuWfG8tIWdWf4aLSoxTCpNFvvvkmX+972bJl6f77709vvPFG2rhxY9q0aVP++uabb+b9d911V1qzZk3+eX7XUkxzGC4qtaKa884776T77rsvrVu3Lv366695H4HDvLHiKxv7f/vtt/Tyyy/noHnrrbcMmCYxXFRqXN+boFi1alXaunVrnnjKvuECo9jH/YQM1afVq1entWvXpqlTpw7+lBrFcFFpURLp7e3N4bB79+5cQqm1BMLP8ft79uxJzz77bHr33XcNmAYzXFRKlD6+//779Oqrr6b+/v5cEqk1WIbicUAVaceOHamz0498o/hOq3QIEdY5oYH2hx9+OFgNGi+C6a+//solGEozagzDRaVD6eLHH39MW7ZsybcnEizg93nMTz75JLfDEDaKZ7iodAiDn3/++WCvUD0QLox9+eqrr6waNYjvskpn7969eSAcjbETLbUUeByqWpSIWD1O8QwXlQohQAmDBa/rHQIsRcnjEjKKZ7iodAiViEu4FI9LyNSrRKSRGS4qHdpE6NWpdwDweMUoXqtG8QyXkuOA4GBga4ezLQc9o3CPPvrowT31w3vY3d1dt0Zijc5wKTHChGL8888/n1588cX8fTv0dDCu5eSTT04zZsyo2yU7CC0e95RTTmmLkC4Dw6VGzShGEySMUmXoOssLMBSe51H1g4MQPeGEE9JJJ52Uw2Wi7z2/z+Mcc8wx6bTTTrNBt0EMlxrw4Wx0uBAgdMl+9NFHebwH82qeeuqpPMu36lUkguC4445L5557bq4i1aP0wmNccsklubrV6P9luzJcatCscKHU8sEHH+QSDEV6zrhPPvlk+vDDDys/jJ33+7zzzktnnXXWhN9/xsuceeaZ6cILL8zfqzEMlxIiWDjTcn3r33///WA7CwHDwcGKa+vXr0/Tpk3L+6uI10/j66WXXpqrSIx9GU/A8H5xLeZrrrkml4IafZJoZ4ZLCVHt+fbbb/OSjYdO2uM2FyynDebjjz+u9DIClNRogL322mtzNYnJh7WGAz/HMg0E03XXXZdOPPHEHFBqHMOlhKjyPPfcc/lgOLRthdsEDKutPfroo6mvr6/SJRgad0899dR000035aoNwUpbFCUbAuTQjf3cT4mFatUNN9yQzjjjjLxPjWW4lAzBQiMu7SojLY7EPko3TO574IEH0hdffFHpNhgCZubMmenmm29ON954Y5o/f35+/ZRsuK/YuE0VkvsXLVqUrr/++lxysXeoOTr+OfNVvhLKgUf7xcqVK3O9e7gDdjjFmXDevHlpwYIFuQ2A21F4XvzN2267LU/co4QyGn6WM/TcuXPTwoULcxWi6gcS/0teN+8Pa73Qk1aEypw5c/L4GL4SPuNtpxkr/vb27dvT4sWL065du/LfrhXPnTahnp6eXCqrEksuJUJJhRCsdc0Rwog2F1a5p5G3WFipyqjeEBoEyPnnn58uu+yydMUVV6TLL788XXDBBXk/pRh+rhHBopEZLiVBmNCOwuUyxnLmAwHz5Zdf5oD55ZdfckhVHQFCyBTtK0XosF/lYLiUBCUOBshR3KdEUmvVDUUJhraXBx98MO3cubPyJRiVn+FSAgTB119/nRtyOfOOJVgK/A7tEZ9//nlejJqz+VhLQFI9GS4lQKC89957udRCIIwnXMDv0WD96aef5guIUU1oVsAQmIZbezNcmoz2ERpkuRxpvXqiOKhZjJo2GHojGn2Q8zoYf0PDNGGn9mS4NBElDbouuVRp0UM03lJLgd8vNnqennjiiVxFKqYQROM10C1Lt//dd9+dQ4ZSzERfl1qP4dJEHPDfffddrsbU8wDkcXjsImBeeumlXEWKDhj+HqUWrjfEmA16ru69995ciuK+er0+tQbDpUk40Jn7wsEfdSVAShG053CtZa5cON7G4lrx95jJzbozhCVVIrrXGUXMPkQHnMrD/3STcJBzDR1mN9PLE3XQFwHzwgsvpNdee60uVa/h8JhsLAnBBEP+DnhtVP24kDzPgTYggkfVZ7g0AQch1ZR169blwV/RZ3MOdP4ekyEZpBcx0ZEQYT4UXeF8PxRhQnWJ5TqZzU2VqR0G+rU7w6UJOPhYLmG4AzEKBzgBQwMvVbHp06cP3jNxhCWlE1bKG2myJQFHiDJQ8OGHH85zghr12tUchksTcKBxUXQOtuEOxChFCYaDmxXu6hUwPA5VriNdfpXXykaw0tDL1Q/tqq4uw6XBGKa/du3apkwy5MDm4KchmVG8tPdM9ODmNdB29MorrxwxKItw4Xc2bdqUli1bludEUYI50u+q9RguDcSBzbKVa9asye0ezTig+Js8D+YfPfTQQ2nDhg35YB/Pc+F3aCwmLPv7+2sqiXE/G4HC2B4ChlJUcZ+qw3BpIA4+gqU4EJulOLjpJmYULxMex/N8CCmqOPw+xhIO/CylJhp3qSIxNoYlEkarVqm1GC4NQkMn6+IyObEsZ+ii9EAjL1WbsVTTCAHG5zC6mFLQeMOS94XSD0t2cuE3urEbXV1UDMOlAQgTxnfQU8KZupbqQyPwHAgYQo8uYtqBau0ipmuZkcVbtmyZ8OshqHgMusoZJ0PV0a7q1me4NAAHCgfhZ599VrrV0YqAYckH2mA4sI9UcuB+qlRcCZKSRj2CsggYAphSDDPE7UlqbYZLMA48xoCwpAJdtWUptQzF8yEAqRqtWLEid1eP1PbBz1IKY8BcUZWq1+vhvWFjLhKLXlGiIvjUmgyXYMVBS69MGYOlUAQMXcNLly4dsXGV10CphflK3F/v18Pj87g8j+XLl6fNmzfnEkxZ3zeNzHAJxIFCo+frr7+ev45UGiiLImCovt155525xDX0OXM/ofPMM8/ktqOo18PfoURE1WjJkiW50Zj3kk2tw/9WMM7AjRzmP1HFgU33Mo2rQ3uCCB5eC9MHoq/0yPPgPaMKxtowzMOyq7q1GC5BOCA58zNRkDaMVjrr8lw5uLmcLAPkiu5huoxXr17d0GoKf5eNRl4mPjK6mNsqP8MlCAcfS1dScuGM32oImGLhJ4b2Eyz05NCr1OiDm+dCoBEujMmhzacV39N2Y7gE4GBgaUnGbaBRZ/l6owpCqLDQFFUkGnGjq0Mj4T0l1Gh/KS4AZ09SuRkuATgAqQ7RINnqRXgChqCkBNPsrnT+NhslQrqqKRVaRSovw6XO+PAzEI0eFc6srVpqGYqAoTGV19Ls11MEDFdMYE4So4Sr8j5XjeFSZ8x2pm2g1Rpxj6QMwVLgeRB4tL0w6I9SogFTPoZLHVFEZ5g/VQi+98MepwgY2oTuueeevAoeYc4+lYPhUidFkNDoydiMKpVayqoIGNq4aHCmoddZ1eXhEVAnhAn1f0a3wlJL4/Des9QmvVrF+rwGTPMZLnXEqFYGeVlqaTzecwKFCZUEDPO5KNUY8s3jUVAn9KawqXkIGDZCnioSXdbcNmCaw3CpIz/IzVcEDJfJJWCYwuBo3uYwXOrMcGk+/gdUibggPlc5oJF91qxZ/m8azHBRJREklGBAVzUjepmfVOxTPN9pVRYBw9bd3Z0HNrJ0w9DrWCuW4aJKKwKGkdOsQ1Nc6dH2sXiGi9oCQUJXNeOQWBuGKz4WwaMYhovaRlFaYQzMqlWr8jWkLMHEMVzqyA9p+REmbFu3bk2PPPJIXgDLBcBjdPT19VV+5BczZqlv06A3lg8Sg+JYjW3evHlpwYIFuWGQ28OhkZBeid7eXkeGtgD+t0x6pKp00UUXpauuuip/P56BkIQV3d6LFy8+bFHzI2Ee2uzZs1NPT09e9LxKDJdRjCVceEyGnluXby38PynFXHnllen0008f8f87GsNleIbLKMYSLmAkqBPmWgufBf6n/f394+6iNlyGZ7iMYqzhovZkuAzPBl1JIQwXSSEMF0khDBdJIQwXSSEMlxrQG0APQDF+xc1t6FZ8Pviq/7ErehRFV/T8+fPT1VdfnVeZtytahyJU6EZesmRJ/mpX9L8Ml1EUQ8FZWX7u3Ll5gBxDxqWhCBMuebt58+Z88qn18wXDpcWNN1xAwLARKkXYSIfiM8XJZyyfLRguLW4i4SJFcoSuJI2R4SIphOEiKYThIimE4SIphOEiKYThIimE4SIphOEiKYThIilEW4VLMU+IyWVubmXY+DwyBaCK2mZuEVfW46JlXJDcuUUqC4Jl1qxZ6ZZbbkk7d+4c3FsNbREurLexbdu2tHHjxjxz1UV9VBYDAwP5hHfxxRfn76ukLcIFBAoXLZPKhupR1YIFbRMukhrL+oGkEIaLpBCGi6QQhoukEIaLpBCGi6QQhoukEIaLpBCGi6QQhoukEIaLpBCGi6QQhoukEIaLpBCGi6QQhoukEIaLpBCGi6QQhoukEIaLpBCGi6QQhoukEIaLpBCGi6QQhoukEIaLpBCGi6QQhoukEIaLpBCGi6QQhoukEIaLpBCGi6QQ5Q2XzklpypSu1NXVNbhDOhyfj66uKamzY3AHOobeULOUN1z270sDA3vT3r17B3dIh+PzsXfvQNp/YHAHwTKwa/CGIkyaNCnVEt9Wi1QtB/5JmcnTBm8owvrf9qXO/ysqDs9wkTQm/+lOad/BouLIDBdJIQwXSSEMF0khDBdJIQwXSSEMF0khDBdJIQwXSSFqD5eOztQ1bVaaNWtGmtr1/8N/OyZP/Wf/P/fNmJ6OmnzIQ3ZOSdO5b3CbPrUrTXLqh1R5HX19fUceaidJY2S1SFIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVKAlP4L5HBNrN5ASvkAAAAASUVORK5CYII=';

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService,
    ) {
        this.item = {};
        this.disable = false;
    }


    ngOnInit() {
    }


    open() {
        this.blockUI.stop();
        this.error = "";
        this.config.disableClose = false;
        this.config.class = "md2-dialog-panel-test";
        this.modal.open(this.config);
    }


    close(flag: boolean = false) {
        if (!flag) {
            this.modal.close();
        }
    }

    fetchBlob(uri:any) {
        return new Promise((resolve, reject) => {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', uri, true);
        xhr.responseType = 'arraybuffer';

        xhr.onload = function(e) {
            if (xhr.status == 200) {
                var blob = xhr.response;
                resolve( blob);
                // if (callback) {
                //     callback(blob);
                // }
            }
        };
        xhr.send();
        });
    };

    _arrayBufferToBase64(buffer:any) {
        var binary = '';
        var bytes = new Uint8Array(buffer);
        var len = bytes.byteLength;
        for (var i = 0; i < len; i++) {
            binary += String.fromCharCode(bytes[i]);
        }
        return window.btoa(binary);
    };

    private getListVisitas(){
        return new Promise((resolve, reject) => {
            this.visitas = [];
            let listV = this.fichaPService.listTarjetas.filter((v:any) => v.Checked === true);
            listV.forEach((list:any,i:any) => {
                if (list.Array.length > 0){
                    this.readArray(list.Array[0]).then(() => {
                        if (i === listV.length - 1) {
                            resolve();
                        }
                    })
                } else{
                    if (i === listV.length - 1) {
                            resolve();
                        }
                }
            });
        });
    }
    private readArray(array: any){
        return new Promise((resolve, reject) => {
            if (!!array){
                array.forEach((a:any, i:any) => {
                    let fecha = a.Fecha.indexOf(':') > 0 ? moment(a.Fecha).format('DD-MM-YY') : a.Fecha;
                    if (this.visitas.filter((v: any) => v.Fecha === fecha && v.Origen === a.Origen && v.NroRelevamiento === a.NroRelevamiento).length === 0){
                        let parts = fecha.split('-');
                        let Fechadate = new Date(20 + parts[2], parts[1] - 1, parts[0]);
                        this.visitas.push({Fecha : fecha, Date: Fechadate, Origen: a.Origen, NroRelevamiento: a.NroRelevamiento});//, Date: Fechadate
                    }
                    if (i === array.length - 1){
                        resolve();
                    }
                });
            } else{
                resolve();
            }
        });
    }

    private orderListVisitas(){
        return new Promise((resolve, reject) => {
            this.visitas.sort((a:any, b:any) => {
                if (a.Date.getTime() < b.Date.getTime()) {
                    return 1;
                }
                if (a.Date.getTime() > b.Date.getTime()) {
                    return -1;
                }
                return 0;
            });
            resolve();
        });
    }

    public loadAllImagesViviendas(array: any){
        return new Promise((resolve, reject) => {
            if (array && array.length > 0){
                array.forEach((a:any,i: any) => {
                    let url = 'https://maps.googleapis.com/maps/api/staticmap?center='+ String(a.Latitud)+',' + String(a.Longitud) + '&zoom=16&scale=false&size=500x500&maptype=hybrid&key=' + this.apiBackEndService.apiKeyGoogleMap + '&format=png&visual_refresh=true&markers=color:red%7Clabel:%7C'+ String(a.Latitud)+',' + String(a.Longitud);
                    this.cargarImagen(url).then((base64)=>{
                        a.base64 = base64;
                        if (i === array.length - 1){
                            console.log('loadAllImagesViviendas',array);
                            resolve();
                        }
                    });
                });
            } else {
                resolve();
            }
        });
    }

    public loadAllImages(array: any,key: string){
        return new Promise((resolve, reject) => {
            let mThis = this;
            //array.forEach((a:any,i: any) => {
                //array[key] = array[key] && array[key] !== 'about:Blank' ? array[key]: '/Content/Images/default.jpg';
                if (array[key] && array[key] !== 'about:Blank'){
                    let tipo = array[key].substring(array[key].length - 3) === 'peg' ? array[key].substring(array[key].length - 4) : array[key].substring(array[key].length - 3) ;
                    this.cargarImagenFotos(array[key],array,tipo,true).then((base64:any)=>{
                        array.base64 = base64.replace('multipart/form-data','image/jpeg');
                        // if (i === array.length - 1){
                        //     console.log(array);
                        //     resolve()
                        // }
                        console.log(array);
                        resolve();
                    });
                } else{
                    array.base64 = mThis.fotoDefault;
                    resolve();
                }
            //});
        });
    }

    public loadAllImagesFotos(array: any,key: string){
        return new Promise((resolve, reject) => {
            let mThis = this;
            var promises :any = [];
            array.forEach((a:any,i: any) => {
            // Observable.interval(250).take(array.length).subscribe((i) => {
            //     let a = array[i];
                promises.push(
                    new Promise((resolve, reject) => { 
                        //valido el length del campo base64 para que no se rompa en la impresion
                        let lengthBase = a.base64 ? 4*(a.base64.length/3) : 0;
                        if (a.base64 === undefined || lengthBase > 500000.00){
                            let tipo = a[key].substring(a[key].length - 3) === 'peg' ? a[key].substring(a[key].length - 4) : a[key].substring(a[key].length - 3);
                            let customSize = a[key].substring(a[key].length - 5) === 'h=300' ? '' : 'h=300';
                            this.cargarImagenFotos(a[key] + customSize,a,tipo).then((base64:any)=>{
                                a.base64 = base64.replace('multipart/form-data','image/jpeg');
                                resolve()
                                // if (i === array.length - 1){
                                //     console.log(array);
                                //     resolve()
                                // }
                            });
                        } else{
                            resolve();
                        }
                    })
                )
            });
            
            Promise.all(promises).then(() => 
                resolve()
            );

        });
    }

    private cargarImagen(url:string){
        return new Promise((resolve, reject) => {
            //var mThis = this;
            //var url = 'http://upload.wikimedia.org/wikipedia/commons/4/4a/Logo_2013_Google.png';  
            var xhr = new XMLHttpRequest();
            xhr.onload = function() {
                var reader = new FileReader();
                reader.onloadend = function() {
                    resolve(reader.result);

                }
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();
        });
    }

    private cargarImagenFotos(url:string, array:any, tipo?: any, datosP?: boolean){
        return new Promise((resolve, reject) => {
            var mThis = this;
            this.apiBackEndService.getBase64(url).then((base:any) => {
                if (base && base.base64){
                    let base64 = base.base64;//'data:image/jpg;base64,' + base;
                    array.columns = base.columns;
                    resolve(base64);
                    // var img = new Image;
                    // img.onload = function() {
                    //     array.columns = img.width > img.height ? 2 : 3;
                    //     // mThis.cargarImagenCanvas(img).then((base) => {
                    //     //     resolve(base);
                    //     // });
                    //     resolve(base64);
                    // };
                    // img.src = base64;
                } else{
                    array.columns = 3;
                    array.fotoNoDisp = true;
                    resolve(mThis.fotoNoDisp);
                }
            },
            (err: any) =>{
                array.columns = 3;
                array.fotoNoDisp = true;
                resolve(mThis.fotoNoDisp);
            })
        });
    }

    public cargarImagenCanvas(img: any) {
    return new Promise((resolve, reject) => {
            // var img = new Image;
            var canvas = document.createElement("canvas");
            var reduce = img.width > img.height ? 2 : 4;
            canvas.width = img.width / reduce;
            canvas.height = img.height / reduce;
            var ctx = canvas.getContext("2d");
            ctx.imageSmoothingEnabled = true;
            ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
            var dataURL = canvas.toDataURL();
            resolve(dataURL);
        });
    }

    public imprimir(){
        let mThis = this;
        this.error = "";
        this.aframe = undefined;
        var images: any = [];
        this.blockUI.start('Generando PDF ...');
        console.log('this.fichaPService.listTarjetas',this.fichaPService.listTarjetas);
        if (!!this.fichaPService.datosPersonales){
            this.getListVisitas().then(()=> this.getDatosPersonales())
            //.then(() => this.cargarImagen('https://sprodresources.blob.core.windows.net/multimedia/relevamiento/cod_form_133_re_102940917/cod_ver_308_re102941117/ce_38_3c07488a7b76a091_20180421180334467/fotos/c3f21be4-af47-0b59-8a4a-ab70c8012d7f.jpg?st=2018-05-22T13%3A12%3A42Z&se=2018-05-23T13%3A12%3A42Z&sp=rl&sv=2017-07-29&sr=b&sig=73w%2B5Kz6dHz4TGtkpeegOCkH%2FD73blW4mkvlJ0LpO6E%3D'))
            .then(() => this.orderListVisitas())
            .then(() => this.getDatosFotos())
            .then(() => this.getDatosViviendas().then(()=> {
                    console.log('visitas',this.visitas);
                    this.tablaDP().then((doc: any) => {
                        this.generaVisitas(doc).then((doc: any) => this.generaGrupoFamiliar(doc))
                        .then((doc:any) => this.generaAntropometriaTodas(doc))
                        .then((doc:any) => this.generaGraficoPE(doc))
                        .then((doc:any) => this.generaGraficoTE(doc))
                        .then((doc:any) => this.generaGraficoIMC(doc))
                        .then((doc:any) => this.generaFotos(doc))
                        .then((doc:any) => this.generaHeaderFooter(doc))
                        .then((doc:any)=>{
                            doc.setProperties({
                                title: 'Ficha de Persona de: ' + this.fichaPService.nombreCompleto,
                                subject: this.fichaPService.datosPersonales[0].CodPersona
                            });
                            this.blockUI.stop();
                            //mThis.aframe = doc.output('datauristring');
                            doc.save(this.fichaPService.datosPersonales[0].CodPersona);
                        });
                    });
                })
            );
        } else{
            this.error = 'Faltan cargar datos Personales';
            this.blockUI.stop();
        }
    }

    private getDatosViviendas(){
        return new Promise((resolve, reject) => {
            this.loadAllImagesViviendas(this.fichaPService.vivienda).then(()=>{
                resolve();
            });
        });
    }

    private generaVisitas(doc: any){
        return new Promise((resolve, reject) => {
            let mThis = this;

            console.log('this.visitas',this.visitas);
            if (this.visitas.length > 0) {
                //var promises :any = [];
                Observable.interval(300).take(this.visitas.length).subscribe((index) => {
                    //this.visitas.forEach((array:any, index:any) => {
                    // promises.push(

                    // new Promise((resolve, reject) => { 
                    let array = this.visitas[index];
                    this.CountFicha = 0;
                    let rows = 7;
                    this.first = doc.autoTable.previous;
                    let mThis = this;

                    let previous: any =[];
                    let columns: any[];
                    let data: any[];
                        

                    let vivienda = this.fichaPService.vivienda !== undefined ? this.fichaPService.vivienda.filter((viv: any) => viv.Fecha === array.Fecha && viv.Origen === array.Origen && viv.NroRelevamiento === array.NroRelevamiento) : []; 
                    this.getRowsList(array.Fecha,array.Origen,array.NroRelevamiento).then(() => this.generaHeaderVisita(doc,array.Fecha,array.Origen,array.NroRelevamiento))
                    //this.generaHeaderVisita(doc,array.Fecha).then((doc:any) => this.generaVivienda(doc,vivienda))
                    .then((doc:any) => this.generaVivienda(doc,vivienda,array.Fecha,array.Origen,array.NroRelevamiento))
                    .then((doc: any) => this.generaMapa(doc,vivienda,array.Fecha,array.Origen,array.NroRelevamiento))
                    .then((doc:any) =>  this.generaSalud(doc,array.Fecha,array.Origen,array.NroRelevamiento))
                    .then((doc:any) =>  this.generaVacunasAll(doc,array.Fecha,array.Origen,array.NroRelevamiento))
                    .then((doc:any) =>  this.generaEmbarazadas(doc,array.Fecha,array.Origen,array.NroRelevamiento))
                    .then((doc:any) =>  this.generaAntAntropometricos(doc,array.Fecha,array.Origen,array.NroRelevamiento))
                    .then((doc:any) =>  this.generaAntNutricionales(doc,array.Fecha,array.Origen,array.NroRelevamiento))
                    .then((doc:any) =>  this.generaAntPatologicos(doc,array.Fecha,array.Origen,array.NroRelevamiento))
                    .then((doc:any) =>  this.generaRiesgoNinio(doc,array.Fecha,array.Origen,array.NroRelevamiento))
                    .then((doc:any) =>  this.generaRiesgoAdolescente(doc,array.Fecha,array.Origen,array.NroRelevamiento))
                    .then((doc:any) =>  this.generaRiesgoEmbarazada(doc,array.Fecha,array.Origen,array.NroRelevamiento))
                    .then((doc:any) =>  this.generaEducacion(doc,array.Fecha,array.Origen,array.NroRelevamiento))
                    .then((doc:any) =>  this.generaTrabajo(doc,array.Fecha,array.Origen,array.NroRelevamiento))
                    .then((doc:any) =>  this.generaSanitarios(doc,array.Fecha,array.Origen,array.NroRelevamiento))
                    .then((doc:any) =>  this.generaServicios(doc,array.Fecha,array.Origen,array.NroRelevamiento))
                    .then((doc:any) =>  this.generaInfraEstructura(doc,array.Fecha,array.Origen,array.NroRelevamiento))
                    .then((doc:any) =>  this.generaObservaciones(doc,array.Fecha,array.Origen,array.NroRelevamiento))
                    //.then((doc:any) =>  this.generaGrupoFamiliar(doc,array.Fecha))
                    .then((doc:any) =>{
                    previous = doc.autoTable.previous;

                    if (index === mThis.visitas.length - 1){
                        console.log(mThis.visitas);
                        resolve(doc)
                    }
                     //resolve()
                });
                
                //  })
                // )
            });

            // Promise.all(promises).then(() => 
            //         resolve(doc)
            //     );
            
            } else {
                resolve(doc);
            }
        });
    }

    private getRowsList(Fecha:any, Origen: any, NroRelevamiento: any){
        return new Promise((resolve, reject) => {
            this.fichaPService.listTarjetas.forEach((tarj:any, i:any) => {
                tarj.Fecha = null;
                let rows = 4;
                if (tarj.Checked) {

                    if (i === 2) {
                        let salud = this.fichaPService.salud !== undefined ? this.fichaPService.salud.filter((s: any) => s.Fecha === Fecha && s.Origen === Origen && s.NroRelevamiento === NroRelevamiento) : [];
                        if (salud.length > 0) {
                            tarj.Fecha = Fecha;
                            tarj.Origen = Origen;
                            tarj.NroRelevamiento = NroRelevamiento;
                        }
                    }

                    if (i === 3) {
                        let vacunas = this.fichaPService.vacunas !== undefined ? this.fichaPService.vacunas.filter((v: any) => v.Fecha === Fecha && v.Origen === Origen && v.NroRelevamiento === NroRelevamiento) : [];
                        if (vacunas.length > 0) {
                            tarj.rows = vacunas.length + 2;
                            tarj.Fecha = Fecha;
                            tarj.Origen = Origen;
                            tarj.NroRelevamiento = NroRelevamiento;
                        }
                    }

                    if (i === 4) {
                        let embarazo = this.fichaPService.embarazo !== undefined ? this.fichaPService.embarazo.filter((e: any) => e.Fecha === Fecha && e.Origen === Origen && e.NroRelevamiento === NroRelevamiento) : [];
                        if (embarazo.length > 0) {
                            tarj.Fecha = Fecha;
                            tarj.Origen = Origen;
                            tarj.NroRelevamiento = NroRelevamiento;
                        }
                    }

                    if (i === 5) {
                        let antecedentesAntr = this.fichaPService.antecedentes !== undefined ? this.fichaPService.antecedentes.filter((ant: any) => ant.Fecha === Fecha && ant.Origen === Origen && ant.NroRelevamiento === NroRelevamiento) : [];
                        if (antecedentesAntr.length > 0) {
                            tarj.Fecha = Fecha;
                            tarj.Origen = Origen;
                            tarj.NroRelevamiento = NroRelevamiento;
                        }
                    }

                    if (i === 6){
                        let antNutricionales = this.fichaPService.antecedentesNutricionales !== undefined ? this.fichaPService.antecedentesNutricionales.filter((antN: any) => antN.Fecha === Fecha && antN.Origen === Origen && antN.NroRelevamiento === NroRelevamiento) : [];
                        let antPatologicos = this.fichaPService.antecedentesPatologicos !== undefined ? this.fichaPService.antecedentesPatologicos.filter((antP: any) => antP.Fecha === Fecha && antP.Origen === Origen && antP.NroRelevamiento === NroRelevamiento) : [];
                        
                        if (antNutricionales.length > 0 || antPatologicos.length > 0){
                            tarj.Fecha = Fecha;
                            tarj.Origen = Origen;
                            tarj.NroRelevamiento = NroRelevamiento;
                        }
                    }

                    if (i === 7){
                        let riesgoNinio = this.fichaPService.riesgoNinio !== undefined ? this.fichaPService.riesgoNinio.filter((r: any) => moment(r.Fecha).format('DD-MM-YY') === Fecha && r.Origen === Origen && r.NroRelevamiento === NroRelevamiento) : [];
                        tarj.rows = riesgoNinio.length + 5

                        if (riesgoNinio.length > 0){
                            tarj.rows = riesgoNinio.length + 5
                            tarj.Fecha = Fecha;
                            tarj.Origen = Origen;
                            tarj.NroRelevamiento = NroRelevamiento;
                        }
                    }

                    if (i === 8){
                        let riesgoAdolescente = this.fichaPService.riesgoAdolescente !== undefined ? this.fichaPService.riesgoAdolescente.filter((r: any) => moment(r.Fecha).format('DD-MM-YY') === Fecha && r.Origen === Origen && r.NroRelevamiento === NroRelevamiento) : [];
                        
                        if (riesgoAdolescente.length > 0){
                            tarj.rows = riesgoAdolescente.length + 5
                            tarj.Fecha = Fecha;
                            tarj.Origen = Origen;
                            tarj.NroRelevamiento = NroRelevamiento;
                        }
                    }

                    if (i === 9){
                        let riesgoEmbarazada = this.fichaPService.riesgoEmbarazo !== undefined ? this.fichaPService.riesgoEmbarazo.filter((r: any) => moment(r.Fecha).format('DD-MM-YY') === Fecha && r.Origen === Origen && r.NroRelevamiento === NroRelevamiento) : [];
                        
                        if (riesgoEmbarazada.length > 0){
                            tarj.rows = riesgoEmbarazada.length + 5
                            tarj.Fecha = Fecha;
                            tarj.Origen = Origen;
                            tarj.NroRelevamiento = NroRelevamiento;
                        }
                    }

                    if (i === 10){
                        let educacion = this.fichaPService.educacion !== undefined ? this.fichaPService.educacion.filter((ed: any) => ed.Fecha === Fecha && ed.Origen === Origen && ed.NroRelevamiento === NroRelevamiento) : [];
                        
                        if (educacion.length > 0){
                            tarj.rows = !!educacion[0].Concurre ? educacion[0].Concurre == 'NO' ? 7 : 5 : 5;
                            tarj.Fecha = Fecha;
                            tarj.Origen = Origen;
                            tarj.NroRelevamiento = NroRelevamiento;
                        }
                    }

                    if (i === 11){
                        let trabajo = this.fichaPService.trabajo !== undefined ? this.fichaPService.trabajo.filter((tr: any) => tr.Fecha === Fecha && tr.Origen === Origen && tr.NroRelevamiento === NroRelevamiento) : [];
                        
                        if (trabajo.length > 0){
                            tarj.Fecha = Fecha;
                            tarj.Origen = Origen;
                            tarj.NroRelevamiento = NroRelevamiento;
                        }
                    }

                    if (i === 12){
                        let sanitario = this.fichaPService.sanitarios !== undefined ? this.fichaPService.sanitarios.filter((san: any) => san.Fecha === Fecha && san.Origen === Origen && san.NroRelevamiento === NroRelevamiento) : [];
                        let servicios = this.fichaPService.servicios !== undefined ? this.fichaPService.servicios.filter((serv: any) => serv.Fecha === Fecha && serv.Origen === Origen && serv.NroRelevamiento === NroRelevamiento) : [];
                        
                        if (sanitario.length > 0 || servicios.length > 0){
                            tarj.rows = sanitario.length > 0 ? 5 : 7;
                            tarj.Fecha = Fecha;
                            tarj.Origen = Origen;
                            tarj.NroRelevamiento = NroRelevamiento;
                        }
                    }

                    if (i === 13) {
                        let infra = this.fichaPService.infraestructura !== undefined ? this.fichaPService.infraestructura.filter((inf: any) => inf.Fecha === Fecha && inf.Origen === Origen && inf.NroRelevamiento === NroRelevamiento) : [];
                        if (infra.length > 0) {
                            tarj.Fecha = Fecha;
                            tarj.Origen = Origen;
                            tarj.NroRelevamiento = NroRelevamiento;
                        }
                    }

                    if (i === 14){
                        let observaciones = this.fichaPService.observaciones !== undefined ? this.fichaPService.observaciones.filter((obs: any) => obs.Fecha === Fecha && obs.Origen === Origen && obs.NroRelevamiento === NroRelevamiento) : [];
                        let urgencias = this.fichaPService.urgencias !== undefined ? this.fichaPService.urgencias.filter((urg: any) => urg.Fecha === Fecha && urg.Origen === Origen && urg.NroRelevamiento === NroRelevamiento) : [];

                        if (observaciones.length > 0 || urgencias.length > 0) {
                            tarj.Fecha = Fecha;
                            tarj.Origen = Origen;
                            tarj.NroRelevamiento = NroRelevamiento;
                        }
                    }

                    if (i === 15){
                        let grupoFamiliar = this.fichaPService.grupoFamiliar !== undefined ? this.fichaPService.grupoFamiliar.filter((gf: any) => gf.Fecha === Fecha && gf.Origen === Origen && gf.NroRelevamiento === NroRelevamiento) : [];
                        
                        if (grupoFamiliar.length > 0){
                            tarj.rows = rows + grupoFamiliar.length;
                            tarj.Fecha = Fecha;
                            tarj.Origen = Origen;
                            tarj.NroRelevamiento = NroRelevamiento;
                        }
                    }

                    if (i === 16){
                        if (!!this.fichaPService.antropometria && this.fichaPService.antropometria.length > 0){
                            tarj.Fecha = Fecha;
                            tarj.Origen = Origen;
                            tarj.NroRelevamiento = NroRelevamiento;
                        }
                    }
                }
                if (i === this.fichaPService.listTarjetas.length - 1){
                    console.log('tarjetas',this.fichaPService.listTarjetas);
                    resolve()
                }
            });
        });
    }

    private generaVivienda(doc: any, vivienda: any, Fecha:any, Origen:string, NroRelevamiento:string){
        return new Promise((resolve, reject) => {
            let previous = doc.autoTable.previous;

                //Vivienda
                if (vivienda.length > 0 && this.fichaPService.listTarjetas[0].Checked) {
                    var columns = [
                        {title: "column1", dataKey: "column1"},
                        {title: "column2", dataKey: "column2"},
                        // {title: "column3", dataKey: "column3"},
                    ];

                    let rows = this.fichaPService.listTarjetas[0].rows;

                    let listTarj = this.fichaPService.listTarjetas.slice(1);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha && list.Origen === Origen && list.NroRelevamiento === NroRelevamiento);

                    rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                    
                    let width = 88.8908362253333;
                    let cellWidth = 44.445418112666644;
                    let pos = {x:0,textx:0};

                    var data = this.generaTemplate(rows);
                    let colN = 2;
                    doc.autoTable(columns, data, {
                        startY: previous.finalY + 2.5,
                        showHeader: 'never',
                        theme: 'plain',
                        pageBreak:'avoid',
                        margin: {right: 107},
                        styles: {
                            halign: 'left'
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {
                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Ubicación de la Vivienda',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                    // doc.autoTableText(data.row.index / 5 + 1 + '-', cell.x + cell.width / 2, cell.y + cell.height * 5 / 2, {
                                    //     halign: 'center',
                                    //     valign: 'middle'
                                    // });
                                    pos.textx = cell.textPos.x;
                                    pos.x = cell.x;
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('País',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var Pais = doc.splitTextToSize(vivienda[0].Pais ? vivienda[0].Pais : '-', (data.table.width / colN) + 2);
                                    doc.setFontSize(8);
                                    doc.text(Pais,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Provincia',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var Provincia = doc.splitTextToSize(vivienda[0].Provincia ? vivienda[0].Provincia : '-', (data.table.width / colN) + 2);
                                    doc.setFontSize(8);
                                    doc.text(Provincia,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 5) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Departamento',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 6) {
                                    var Departamento = doc.splitTextToSize(vivienda[0].Departamento ? vivienda[0].Departamento : '-', (data.table.width / colN) + 2);
                                    doc.setFontSize(8);
                                    doc.text(Departamento,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 7) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Localidad',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 8) {
                                    var Localidad = doc.splitTextToSize(vivienda[0].Localidad ? vivienda[0].Localidad : '-', (data.table.width / colN) + 2);
                                    doc.setFontSize(8);
                                    doc.text(Localidad,cell.textPos.x, cell.y - 4);
                                } 

                                if (data.row.index === 9) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Municipio',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 10) {
                                    var Municipio = doc.splitTextToSize(!!vivienda[0].Municipio ? vivienda[0].Municipio : '-', (data.table.width / colN) + 2);
                                    doc.setFontSize(8);
                                    doc.text(Municipio,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 11) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Paraje',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 12) {
                                    var Paraje = doc.splitTextToSize(!!vivienda[0].paraje ? vivienda[0].paraje : '-', (data.table.width / colN) + 2);
                                    doc.setFontSize(8);
                                    doc.text(Paraje,cell.textPos.x, cell.y - 4);
                                }             

                                // if (data.row.index === 11) {
                                //     doc.setFontStyle('bold');
                                //     doc.setFontSize(8);
                                //     doc.text('Latitud',cell.textPos.x, cell.y);
                                // }

                                // if (data.row.index === 12) {
                                //     var Latitud = doc.splitTextToSize(String(!!vivienda[0].Latitud ? vivienda[0].Latitud : '-'), (data.table.width / 2) + 4);
                                //     doc.setFontSize(8);
                                //     doc.text(Latitud,cell.textPos.x, cell.y - 4);
                                //}
                                return false;
                            }

                            if (data.column.dataKey === 'column2') {
                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Barrio',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var Barrio = doc.splitTextToSize(!!vivienda[0].Barrio ? vivienda[0].Barrio : '-', (data.table.width / colN) + 2);
                                    doc.setFontSize(8);
                                    doc.text(Barrio,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Domicilio',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var domicilio = doc.splitTextToSize(!!vivienda[0].Domicilio ? vivienda[0].Domicilio :  '-', (data.table.width / colN) + 2);
                                    doc.setFontSize(8);
                                    doc.text(domicilio,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 5) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Referencia',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 6) {
                                    let refAux = vivienda[0].Domicilio2 ? vivienda[0].Domicilio2 : '-';
                                    var referencia = doc.splitTextToSize(vivienda[0].Domicilio2 ? vivienda[0].Domicilio2 : '-', (data.table.width / colN) + 2);
                                    doc.setFontSize(8);
                                    doc.text(refAux.replace(/\s/g, '').length > 0 ? referencia : '-',cell.textPos.x, cell.y - 4);
                                } 

                                if (data.row.index === 7) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('NIM',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 8) {
                                    var NIM = doc.splitTextToSize(String(!!vivienda[0].NIM ? vivienda[0].NIM :  '-'), (data.table.width / colN) + 2);
                                    doc.setFontSize(8);
                                    doc.text(NIM,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 9) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Teléfono de Contacto',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 10) {
                                    var TelefonoContacto = doc.splitTextToSize(!!vivienda[0].TelefonoContacto ? vivienda[0].TelefonoContacto : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(TelefonoContacto,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 11) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Referencia de Contacto',pos.textx + cellWidth, cell.y);
                                }

                                if (data.row.index === 12) {
                                    var ReferenciaContacto = doc.splitTextToSize(!!vivienda[0].ReferenciaContacto ? vivienda[0].ReferenciaContacto : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(ReferenciaContacto,pos.textx + cellWidth, cell.y - 4);
                                }

                                // if (data.row.index === 11) {
                                //     doc.setFontStyle('bold');
                                //     doc.setFontSize(8);
                                //     doc.text('Longitud',pos.textx + cellWidth, cell.y);
                                // }

                                // if (data.row.index === 12) {
                                //     var Longitud = doc.splitTextToSize(String(!!vivienda[0].Longitud ? vivienda[0].Longitud : '-'), (data.table.width / 2) + 4);
                                //     doc.setFontSize(8);
                                //     doc.text(Longitud,pos.textx + cellWidth, cell.y - 4);
                                // }
                                return false;
                            }

                            if (data.column.dataKey === 'column3') {
                                return false;
                            }
                        },
                    });

                    this.first = doc;//.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                    resolve(doc);
                }
        });
    }

    private generaMapa(doc:any, vivienda: any, Fecha:any, Origen:string, NroRelevamiento:string){
        return new Promise((resolve, reject) => {
            //Tarjeta Mapa
            let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};

            let rows = this.fichaPService.listTarjetas[1].rows;

            if (vivienda.length > 0 && this.fichaPService.listTarjetas[1].Checked) {
                var map: any = [];

                let listTarj = this.fichaPService.listTarjetas.slice(2);//.filter((list:any) => list.Checked === true)
                listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha && list.Origen === Origen && list.NroRelevamiento === NroRelevamiento);

                if (previous.pageStartX !== 107 && this.CountFicha > 0){
                    // Reset page to the same as before previous table
                    doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                    rows = previous.rows.length > rows ? previous.rows.length : rows;
                    startY = previous.pageStartY;
                    margin = {left: 107};
                } else {
                    rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                    startY = previous.finalY + 2.5;
                    margin = {right: 107};
                }

                //previous = this.this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                columns = [
                    {title: "columnaI", dataKey: "column1"},
                    {title: "columnad", dataKey: "column2"},
                ];
                data = this.generaTemplate(rows);

                doc.autoTable(columns, data, {
                    startY: startY,
                    showHeader: 'never',
                    pageBreak:'avoid',
                    margin: margin,
                    styles: {
                        halign: 'left',
                    },
                    drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {
                                if (data.row.index === 0) {
                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                    // doc.autoTableText(data.row.index / 5 + 1 + '-', cell.x + cell.width / 2, cell.y + cell.height * 5 / 2, {
                                    //     halign: 'center',
                                    //     valign: 'middle'
                                    // });
                                    map.push({
                                        url: vivienda[0].base64,
                                        x: cell.textPos.x,
                                        y: cell.textPos.y,
                                        width: data.table.width,
                                        height: data.table.height
                                    });
                                }
                            }
                            return false;
                    },
                    addPageContent: function() {
                        if (map[0].url !== undefined){
                            doc.addImage(map[0].url,'JPG', map[0].x, map[0].y - 1, map[0].width - 3, map[0].height - 12);//Width, heith
                            mThis.first = doc;
                            mThis.CountFicha++;
                            resolve(doc); 
                        }
                    }
                })

            } else {
                resolve(doc);
            }
        });
    }

    private generaSalud(doc:any, Fecha: string, Origen:string, NroRelevamiento:string){
        return new Promise((resolve, reject) => {
            //Tarjeta Salud
            let previous = doc.autoTable.previous;

            let mThis = this;
            var columns = [];
            var data: any;
            let rows = 4;

                let salud = this.fichaPService.salud !== undefined ? this.fichaPService.salud.filter((s: any) => s.Fecha === Fecha && s.Origen === Origen && s.NroRelevamiento === NroRelevamiento) : [];

                let discapacidades = this.fichaPService.discapacidades.filter((d: any) => d.Fecha === Fecha && d.Discapacidad !== null && d.Origen === Origen && d.NroRelevamiento === NroRelevamiento);
                let discapacidadesCognitivas = !!this.fichaPService.discapacidadesCognitivas ? this.fichaPService.discapacidadesCognitivas.filter((dc: any) => dc.Fecha === Fecha && dc.Origen === Origen && dc.NroRelevamiento === NroRelevamiento) : [];
                let enfAgudas = this.fichaPService.enfermedadesAgudas.filter((ea: any) => ea.Fecha === Fecha && ea.Origen === Origen && ea.NroRelevamiento === NroRelevamiento);
                let enfCronicas = this.fichaPService.enfermedadesCronicas.filter((ec: any) => ec.Fecha === Fecha && ec.Origen === Origen && ec.NroRelevamiento === NroRelevamiento);

                console.log('enfCronicas',enfCronicas);
                console.log('discapacidades',discapacidades);
                console.log('discapacidadesCognitivas',discapacidadesCognitivas);
                console.log('enfAgudas',enfAgudas);

                let startY = mThis.first.finalY + 2.5;
                let margin: any = {right: 107};

                if (salud.length > 0 && this.fichaPService.listTarjetas[2].Checked) {

                    rows = 2;
                    let splitSize = 49.44541811266665;
                    //Determino el heigth de la tarjeta
                    let lengthEA = !!enfAgudas && enfAgudas.length > 0 ? enfAgudas.length : 1;
                    rows = rows + lengthEA;
                    //rows = !!enfAgudas && enfAgudas.length > 3 ? rows + 1 : rows;
                    //rows = rows + lengthEC;

                    //Determino si las descripciones ocuparan mas de una fila
                    let lengthEC = 2//!!enfCronicas && enfCronicas.length > 0 ? enfCronicas.length + 1 : 2;
                    let iniEC = rows + 1;
                    var ecronicas: any = []

                    if (enfCronicas.length > 0){
                        enfCronicas.forEach((ec:any, i:any) => {
                            if (!!ec.Enfermedad){
                                let SizeEnfermedad = Math.round(doc.splitTextToSize(ec.Enfermedad,splitSize).length / 2);
                                let SizeMedicamentos = Math.round(doc.splitTextToSize(ec.TratamientoMedicacion,splitSize).length / 2);
                                let maxLength = SizeEnfermedad > SizeMedicamentos ? SizeEnfermedad : SizeMedicamentos;
                                console.log('maxLength',maxLength);
                                lengthEC = maxLength > 1 ? lengthEC + maxLength - 1 : lengthEC;
                                //ec.ini = iniEC;

                                ecronicas.push({
                                    Enfermedad:ec.Enfermedad, 
                                    TratamientoMedicacion: !!ec.TratamientoMedicacion && ec.TratamientoMedicacion !== '' ? ec.TratamientoMedicacion : '-',
                                    ini: iniEC
                                });

                                iniEC = maxLength > 1 ? iniEC + maxLength - 1: iniEC + 1;
                            }
                            if (i === enfCronicas.length - 1 && lengthEC > 3){
                                lengthEC = lengthEC - 2;
                                rows = rows + lengthEC;
                            } else {
                                if (i === enfCronicas.length - 1){
                                    console.log('lengthEC',lengthEC);
                                    rows = rows + lengthEC;
                                }
                            }
                        });
                    } else {
                        ecronicas.push({
                            Enfermedad: salud[0].TieneEnferCronicas ? salud[0].TieneEnferCronicas : '-', 
                            TratamientoMedicacion: '-',
                            ini: iniEC
                        });
                        rows = rows + lengthEC;
                    }
                    //rows = rows + lengthEC;
                    //console.log('enfCronicas',enfCronicas,rows + lengthEC);

                    //rows = rows + lengthDisc;
                    // let lengthDisc = 1;

                    // if (!!discapacidades && discapacidades.length > 0){
                    //     lengthDisc = lengthDisc + discapacidades.length;
                    //     lengthDisc = !!discapacidadesCognitivas && discapacidadesCognitivas.length > lengthDisc ? discapacidadesCognitivas.length : lengthDisc;
                    //     rows = rows + lengthDisc;
                    // } else {
                    //     rows = rows + 2;
                    // }

                    //Determino si las descripciones ocuparan mas de una fila
                    console.log('rowsBefore',rows);
                    let lengthDisc = 2;//lengthEC > 2 ? 2 : 1;
                    let iniDisc = rows + 1;
                    var arrayDisc: any = []

                    if (!!discapacidades && discapacidades.length > 0){
                        discapacidades.forEach((d:any, i:any) => {
                            if (!!d.Discapacidad){
                                let SizeDiscapacidad= Math.round(doc.splitTextToSize(d.Discapacidad,splitSize).length / 2);
                                let SizeDCognitiva = 0;//Math.round(doc.splitTextToSize(d.TratamientoMedicacion,splitSize).length / 2);
                                if (d.DiscapacidadCognitiva > 0){
                                    var discCognitiva: any = []// = doc.splitTextToSize(enfAgudas[0].Enfermedad, (data.table.width / 2) + 4);
                                    for (var j = 0; j < d.DiscapacidadCognitiva.length; i++) {
                                        discCognitiva.push(d.DiscapacidadCognitiva[j].Discapacidad);
                                    }
                                    SizeDCognitiva = Math.round(doc.splitTextToSize(discCognitiva, splitSize).length / 2);

                                }
                                console.log('SizeDiscapacidad',SizeDiscapacidad);
                                console.log('SizeDCognitiva',SizeDCognitiva);
                                let maxLength = SizeDiscapacidad > SizeDCognitiva ? SizeDiscapacidad : SizeDCognitiva;

                                lengthDisc = maxLength > 1 ? lengthDisc + maxLength - 1 : lengthDisc;

                                arrayDisc.push({
                                    Discapacidad:d.Discapacidad, 
                                    DiscapacidadCognitiva: d.DiscapacidadCognitiva,
                                    ini: iniDisc
                                });

                                iniDisc = maxLength > 1 ? iniDisc + maxLength - 1: iniDisc + 1;
                            }
                            if (i === discapacidades.length - 1 && lengthDisc > 5){
                                lengthDisc = lengthDisc - 2;
                                console.log('lengthDisc',lengthDisc);
                                rows = rows + lengthDisc;
                            } else{
                                if (i === discapacidades.length - 1){
                                    console.log('lengthDisc',lengthDisc);
                                    rows = rows + lengthDisc;
                                }
                            }
                        });
                    } else{
                        arrayDisc.push({
                            Discapacidad:'No tiene Discapacidades', 
                            DiscapacidadCognitiva: [],
                            ini: iniDisc
                        });
                        //lengthDisc++;
                        rows = rows + lengthDisc;
                    }

                    console.log('rows',rows);

                    let rowsAnexo = rows;
                

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        rows = 7;
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }

                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Salud',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Estado',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var estado = doc.splitTextToSize(!!salud[0].Discapacitado_txt ? salud[0].Discapacitado_txt : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    if (salud[0].Discapacitado === 'SÍ'){
                                        doc.setTextColor(255,0,0);
                                    } 
                                    doc.text(estado,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Cobertura Médica',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var cobertura = doc.splitTextToSize(!!salud[0].PoseeCoberturaMedica_txt_n2 ? salud[0].PoseeCoberturaMedica_txt_n2 : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    if (salud[0].PoseeCoberturaMedica !== 'SÍ'){
                                        doc.setTextColor(255,0,0);
                                    } 
                                    doc.text(cobertura,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 5) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Enf. Crónicas',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 6) {
                                    let tieneEnfCr = salud[0].TieneEnferCronicas !== 'SÍ'? salud[0].TieneEnferCronicas ? salud[0].TieneEnferCronicas : '-' : salud[0].TieneEnferCronicas_txt;
                                    var enferCronica = doc.splitTextToSize(tieneEnfCr, (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    if (salud[0].TieneEnferCronicas === 'SÍ'){
                                        doc.setTextColor(255,0,0);
                                    } 
                                    doc.text(enferCronica,cell.textPos.x, cell.y - 4);
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Certificado',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var certificado = doc.splitTextToSize(!!salud[0].Certificado ? salud[0].Certificado : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(certificado,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Enf. Agudas',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    let tieneEnfAguda = salud[0].TieneEnferAgudas !== 'SÍ'? salud[0].TieneEnferAgudas ? salud[0].TieneEnferAgudas : '-' : salud[0].TieneEnferAgudas_txt;
                                    var enfAgudas = doc.splitTextToSize(tieneEnfAguda, (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    if (salud[0].TieneEnferAgudas === 'SÍ'){
                                        doc.setTextColor(255,0,0);
                                    }
                                    doc.text(enfAgudas,cell.textPos.x, cell.y - 4);
                                }

                                return false;
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;

                    //Salud Anexo
                    rows = rowsAnexo;

                    let listTarj = this.fichaPService.listTarjetas.slice(3);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha && list.Origen === Origen && list.NroRelevamiento === NroRelevamiento);

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }

                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Salud (Anexo)',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Enfermedades Agudas',cell.textPos.x, cell.textPos.y);
                                }

                                /*if (data.row.index === 2) {
                                    var eagudas: any = []// = doc.splitTextToSize(enfAgudas[0].Enfermedad, (data.table.width / 2) + 4);
                                    if (!!enfAgudas && enfAgudas.length > 0) {
                                        for (var i = 0; i < enfAgudas.length; i++) {
                                            eagudas.push(enfAgudas[i].Enfermedad);
                                        }
                                    } else{
                                        eagudas.push('No Tiene Enfermedades Agudas');
                                    }
                                    eagudas = doc.splitTextToSize(eagudas, (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(eagudas,cell.textPos.x, cell.y - 2);
                                }*/

                                if (data.row.index >= 2 && data.row.index < 2 + lengthEA) {
                                    var eagudas = doc.splitTextToSize(!!enfAgudas[data.row.index - 2] ? enfAgudas[data.row.index - 2].Enfermedad : salud[0].TieneEnferAgudas ? salud[0].TieneEnferAgudas : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(eagudas,cell.textPos.x, data.row.index === 2 ? cell.y - 2 : cell.y - 4);
                                }

                                if (data.row.index === 2 + lengthEA ) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Enfermedades Crónicas',cell.textPos.x, cell.y);
                                }
                                let rowsEC = 2 + lengthEA;

                                if (data.row.index > rowsEC && data.row.index < rowsEC + lengthEC) {
                                    let array = ecronicas.filter((e:any) => e.ini === data.row.index);
                                    console.log('array',array);
                                    if (!!array && array.length > 0) {
                                        //var textCronicas = doc.splitTextToSize(!!enfCronicas[data.row.index - rowsEC - 1] ? enfCronicas[data.row.index - rowsEC - 1].Enfermedad : 'No Tiene Enfermedades Crónicas', (data.table.width / 2) + 4);
                                        var textCronicas = doc.splitTextToSize(array[0].Enfermedad,(data.table.width / 2) + 5);
                                        doc.setFontSize(8);
                                        doc.text(textCronicas,cell.textPos.x, cell.y - 4);
                                    }
                                }

                                // if (data.row.index > rowsEC && data.row.index < rowsEC + lengthEC) {
                                //     var ecronicas: any = []// = doc.splitTextToSize(enfAgudas[0].Enfermedad, (data.table.width / 2) + 4);
                                //     if (!!enfCronicas && enfCronicas.length > 0) {
                                //         for (var i = 0; i < enfCronicas.length; i++) {
                                //             ecronicas.push(enfCronicas[i].Enfermedad);
                                //         }
                                //     } else{
                                //         ecronicas.push('No Tiene Enfermedades Crónicas');
                                //     }
                                //     ecronicas = doc.splitTextToSize(ecronicas, (data.table.width / 2) + 4);
                                //     doc.setFontSize(8);
                                //     doc.text(ecronicas,cell.textPos.x, cell.y - 2);
                                //}

                                if (data.row.index === rowsEC + lengthEC) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Tipo Discapacidad',cell.textPos.x, cell.y);
                                }

                                let rowDisc = rowsEC + lengthEC + 1;
                                if (data.row.index >= rowDisc) {
                                    // var discapacidad = doc.splitTextToSize(!!discapacidades[data.row.index - rowDisc] ? discapacidades[data.row.index - rowDisc].Discapacidad : 'No tiene Discapacidades', (data.table.width / 2) + 4);
                                    // doc.setFontSize(8);
                                    // doc.text(discapacidad,cell.textPos.x, cell.y - 4);
                                    let array = arrayDisc.filter((a:any) => a.ini === data.row.index);
                                    console.log('array',array);
                                    if (!!array && array.length > 0) {
                                        //var textCronicas = doc.splitTextToSize(!!enfCronicas[data.row.index - rowsEC - 1] ? enfCronicas[data.row.index - rowsEC - 1].Enfermedad : 'No Tiene Enfermedades Crónicas', (data.table.width / 2) + 4);
                                        let textDisc = doc.splitTextToSize(array[0].Discapacidad,(data.table.width / 2) + 5);
                                        doc.setFontSize(8);
                                        doc.text(textDisc,cell.textPos.x, cell.y - 4);
                                    }
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Medicamentos',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index >= 2 && data.row.index < 2 + lengthEA) {
                                    var eagudas = doc.splitTextToSize(!!enfAgudas[data.row.index - 2] ? enfAgudas[data.row.index - 2].TratamientoMedicacion : '-', (data.table.width / 2) + 5);
                                    doc.setFontSize(8);
                                    doc.text(eagudas,cell.textPos.x, data.row.index === 2 ? cell.y - 2 : cell.y - 3);
                                }

                                if (data.row.index === 2 + lengthEA ) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Medicamentos',cell.textPos.x, cell.y);
                                }

                                let rowsEC = 2 + lengthEA;

                                // if (data.row.index > rowsEC && data.row.index < rowsEC + lengthEC) {
                                //     var traCronica = doc.splitTextToSize(!!enfCronicas[data.row.index - rowsEC - 1] ? enfCronicas[data.row.index - rowsEC - 1].TratamientoMedicacion : '-', (data.table.width / 2) + 5);
                                //     doc.setFontSize(8);
                                //     doc.text(traCronica,cell.textPos.x, cell.y - 4);
                                // }

                                if (data.row.index > rowsEC && data.row.index < rowsEC + lengthEC) {
                                    let array = ecronicas.filter((e:any) => e.ini === data.row.index);
                                    console.log('array',array);
                                    if (!!array && array.length > 0) {
                                        //var textCronicas = doc.splitTextToSize(!!enfCronicas[data.row.index - rowsEC - 1] ? enfCronicas[data.row.index - rowsEC - 1].Enfermedad : 'No Tiene Enfermedades Crónicas', (data.table.width / 2) + 4);
                                        let traCronica = doc.splitTextToSize(array[0].TratamientoMedicacion,(data.table.width / 2) + 5);
                                        doc.setFontSize(8);
                                        doc.text(traCronica,cell.textPos.x, cell.y - 4);
                                    }
                                }

                                if (!!discapacidadesCognitivas && discapacidadesCognitivas.length > 0) {
                                    if (data.row.index === rowsEC + lengthEC) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Tipo de Discapacidad Cognitiva',cell.textPos.x, cell.y);
                                    }

                                    let rowDisc = rowsEC + lengthEC + 1;
                                    if (data.row.index >= rowDisc) {
                                        var cognitiva = doc.splitTextToSize(!!discapacidadesCognitivas[data.row.index - rowDisc] ? discapacidadesCognitivas[data.row.index - rowDisc].Discapacidad : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        doc.text(cognitiva,cell.textPos.x, cell.y - 4);
                                    }
                                }


                                return false;
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    resolve(doc);
                    this.CountFicha++;
                } else {
                    resolve(doc);
                }
        });
    }

    private rowsSaludAnexo(doc:any, enfAgudas:any, enfCronicas: any, discapacidades:any){
        return new Promise((resolve, reject) => {
                let rows = 2;
                let splitSize = 49.44541811266665;
                //Determino el heigth de la tarjeta
                let lengthEA = !!enfAgudas && enfAgudas.length > 0 ? enfAgudas.length : 1;
                rows = rows + lengthEA;
                //rows = !!enfAgudas && enfAgudas.length > 3 ? rows + 1 : rows;
                //rows = rows + lengthEC;

                //Determino si las descripciones ocuparan mas de una fila
                let lengthEC = 2//!!enfCronicas && enfCronicas.length > 0 ? enfCronicas.length + 1 : 2;
                let iniEC = rows + 1;
                var ecronicas: any = []

                if (enfCronicas.length > 0){
                    enfCronicas.forEach((ec:any, i:any) => {
                        if (!!ec.Enfermedad){
                            let SizeEnfermedad = Math.round(doc.splitTextToSize(ec.Enfermedad,splitSize).length / 2);
                            let SizeMedicamentos = Math.round(doc.splitTextToSize(ec.TratamientoMedicacion,splitSize).length / 2);
                            let maxLength = SizeEnfermedad > SizeMedicamentos ? SizeEnfermedad : SizeMedicamentos;

                            lengthEC = maxLength > 1 ? lengthEC + maxLength - 1 : lengthEC;
                            //ec.ini = iniEC;

                            ecronicas.push({
                                Enfermedad:ec.Enfermedad, 
                                TratamientoMedicacion: !!ec.TratamientoMedicacion && ec.TratamientoMedicacion !== '' ? ec.TratamientoMedicacion : '-',
                                ini: iniEC
                            });

                            iniEC = maxLength > 1 ? iniEC + maxLength - 1: iniEC + 1;
                        }
                        if (i === enfCronicas.length - 1 && lengthEC > 4){
                            lengthEC = lengthEC - 2;
                        }
                    });
                } else{
                    ecronicas.push({
                        Enfermedad:'No Tiene Enfermedades Crónicas', 
                        TratamientoMedicacion: '-',
                        ini: iniEC
                    });
                }
                rows = rows + lengthEC;
                console.log('enfCronicas',enfCronicas,rows + lengthEC);
                //rows = !!enfCronicas && enfCronicas.length > 3 ? rows + 1 : rows;


                //Determino si las descripciones ocuparan mas de una fila
                let lengthDisc = 1;
                let iniDisc = rows + 1;
                var arrayDisc: any = []

                if (!!discapacidades && discapacidades.length > 0){
                    discapacidades.forEach((d:any, i:any) => {
                        if (!!d.Discapacidad){
                            let SizeDiscapacidad= Math.round(doc.splitTextToSize(d.Discapacidad,splitSize).length / 2);
                            let SizeDCognitiva = 0;//Math.round(doc.splitTextToSize(d.TratamientoMedicacion,splitSize).length / 2);
                            if (d.DiscapacidadCognitiva > 0){
                                var discCognitiva: any = []// = doc.splitTextToSize(enfAgudas[0].Enfermedad, (data.table.width / 2) + 4);
                                for (var j = 0; j < d.DiscapacidadCognitiva.length; i++) {
                                    discCognitiva.push(d.DiscapacidadCognitiva[j].Discapacidad);
                                }
                                SizeDCognitiva = Math.round(doc.splitTextToSize(discCognitiva, splitSize).length / 2);

                            }
                            console.log('SizeDiscapacidad',SizeDiscapacidad);
                            console.log('SizeDCognitiva',SizeDCognitiva);
                            let maxLength = SizeDiscapacidad > SizeDCognitiva ? SizeDiscapacidad : SizeDCognitiva;

                            lengthDisc = maxLength > 1 ? lengthDisc + maxLength - 1 : lengthDisc;

                            arrayDisc.push({
                                Discapacidad:d.Discapacidad, 
                                DiscapacidadCognitiva: d.DiscapacidadCognitiva,
                                ini: iniDisc
                            });

                            iniDisc = maxLength > 1 ? iniDisc + maxLength - 1: iniDisc + 1;
                        }
                        if (i === discapacidades.length - 1 && lengthDisc > 4){
                            lengthDisc = lengthDisc - 2;
                            rows = rows + lengthDisc;
                        }
                    });
                } else{
                    arrayDisc.push({
                        Discapacidad:'No tiene Discapacidades', 
                        DiscapacidadCognitiva: [],
                        ini: iniDisc
                    });
                    lengthDisc++;
                    rows = rows + lengthDisc;
                }
            
            resolve({lengthEA: lengthEA, lengthEC:lengthEC, lengthDisc:lengthDisc, ecronicas: ecronicas, arrayDisc: arrayDisc});
        });
    }

    async generaVacunasAll(doc:any, Fecha:string, Origen:string, NroRelevamiento:string){            
        let vacunas = this.fichaPService.vacunas !== undefined ? this.fichaPService.vacunas.filter((v: any) => v.Fecha === Fecha && v.Origen === Origen && v.NroRelevamiento === NroRelevamiento) : [];
        let i = 0;
        let ini = 0;
        let end = 33;
        let count = Math.trunc(vacunas.length/33);

        while (i<= count){
            let vacunasAux = vacunas;
                await this.generaVacunas(doc,Fecha,Origen,NroRelevamiento,vacunasAux,(i%2)== 0).then((docAux:any) =>{
                    doc = docAux;

                    vacunas = vacunas.slice(end,vacunas.length - 1);
                    //console.log('vacunas',vacunas);
                    ini = ini + 33;
                    end = end + 33;
                });
            i++;
        }
        
        return(doc);
    }

    private generaVacunas(doc:any, Fecha:string, Origen:string, NroRelevamiento:string, vacunasAux?:any, esPar?:boolean){
        return new Promise((resolve, reject) => {
	        let previous = doc.autoTable.previous;
            let mThis = this;

            var columns = [];
            var data: any;

            let rows = this.fichaPService.listTarjetas[3].rows;

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};
            
            //Servicios
                let vacunasAux2 = this.fichaPService.vacunas !== undefined ? this.fichaPService.vacunas.filter((v: any) => v.Fecha === Fecha && v.Origen === Origen && v.NroRelevamiento === NroRelevamiento) : []
                let vacunas = vacunasAux.length < this.maxRows ? vacunasAux : vacunasAux.slice(0,33);//this.fichaPService.vacunas !== undefined ? this.fichaPService.vacunas.filter((v: any) => v.Fecha === Fecha && v.Origen === Origen && v.NroRelevamiento === NroRelevamiento) : [];
                //console.log('servicios '+ index,servicios);
                if (vacunas.length > 0 && this.fichaPService.listTarjetas[3].Checked) {

                    rows = vacunas.length + 2;

                    let widthCol1 = 53.63027874177776;
                    let widthCol2 = 23.63027874177776;
                    let widthCol3 = 23.63027874177776; 

                    let listTarj = this.fichaPService.listTarjetas.slice(4);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha && list.Origen === Origen && list.NroRelevamiento === NroRelevamiento)

                    if (vacunasAux.length < this.maxRows){
                        if (previous.pageStartX !== 107 && this.CountFicha > 0){
                            // Reset page to the same as before previous table
                            let pageRest = vacunasAux2.length >= this.maxRows ? 0 : 1;
                            doc.setPage(pageRest + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                            rows = previous.rows.length > rows ? previous.rows.length : rows;
                            startY = previous.pageStartY;
                            margin = {left: 107};
                        } else {
                            rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                            startY = previous.finalY + 2.5;
                            margin = {right: 107};
                        }
                    } else{
                        if (!esPar){
                            // Reset page to the same as before previous table
                            doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                            rows = previous.rows.length > rows ? previous.rows.length : rows;
                            startY = previous.pageStartY;
                            margin = {left: 107};
                        } else {
                            //rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                            startY = previous.finalY + 2.5;
                            margin = {right: 107};
                        }
                    }

                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                        {title: "columnad", dataKey: "column3"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Vacunas',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Vacuna' ,cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index >= 2 && data.row.index < vacunas.length + 2) {
                                    let row = data.row.index - 2
                                    var vacuna = doc.splitTextToSize(vacunas[row] && vacunas[row].Vacuna ? vacunas[row].Vacuna : '-', widthCol1);
                                    doc.setFontSize(8);
                                    doc.text(vacuna,cell.textPos.x, cell.y - 2);

                                    if (vacunas[row] && data.row.index < rows - 1){
                                        //doc.setDrawColor(0, 0, 0);
                                        doc.rect(cell.x, cell.y - 5, data.table.width, cell.height );
                                    }
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Vacunatorio',cell.textPos.x + 18, cell.textPos.y);
                                }

                                if (data.row.index >= 2 && data.row.index < vacunas.length + 2) {
                                    let row = data.row.index - 2
                                    var vacunatorio = doc.splitTextToSize(vacunas[row] && vacunas[row].Vacunatorio ? vacunas[row].Vacunatorio : '-', widthCol2);
                                    doc.setFontSize(8);
                                    doc.text(vacunatorio,cell.textPos.x + 18, cell.y - 2);
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column3') {

                                if (data.row.index === 1) {
                                    let titlefechaAdm = doc.splitTextToSize('Fecha Admin.', widthCol3);
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text(titlefechaAdm,cell.textPos.x + 10, cell.textPos.y);
                                }

                                if (data.row.index >= 2 && data.row.index < vacunas.length + 2) {
                                    let row = data.row.index - 2
                                    var fechaAdm = doc.splitTextToSize(vacunas[row] && vacunas[row].FechaAdministracion ? vacunas[row].FechaAdministracion : '-', widthCol3);
                                    doc.setFontSize(8);
                                    doc.text(fechaAdm,cell.textPos.x + 10, cell.y - 2);
                                }

                                return false;
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                    resolve(doc);
                    //return doc;
                }
        });
    }

    private generaEmbarazadas(doc:any, Fecha:string, Origen:string, NroRelevamiento:string){
        return new Promise((resolve, reject) => {
	        let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = this.fichaPService.listTarjetas[4].rows;

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};
            
            //Servicios
                let embarazo = this.fichaPService.embarazo !== undefined ? this.fichaPService.embarazo.filter((e: any) => e.Fecha === Fecha && e.Origen === Origen && e.NroRelevamiento === NroRelevamiento) : [];
                //console.log('servicios '+ index,servicios);
                if (embarazo.length > 0 && this.fichaPService.listTarjetas[4].Checked) {

                    let listTarj = this.fichaPService.listTarjetas.slice(5);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha && list.Origen === Origen && list.NroRelevamiento === NroRelevamiento)

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }

                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                        {title: "columnad", dataKey: "column3"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Embarazo',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Edad Gestacional' ,cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var edadG = doc.splitTextToSize(!!embarazo[0].EG ? embarazo[0].EG : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(edadG,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Peso',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var peso = doc.splitTextToSize(!!embarazo[0].Peso ? embarazo[0].Peso + 'kg' : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(peso,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 5) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('IMC',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 6) {
                                    var imc = doc.splitTextToSize(!!embarazo[0].IMC ? embarazo[0].IMC : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(imc,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 7) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Ácido Fólico',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 8) {
                                    var acidofolico = doc.splitTextToSize(!!embarazo[0].TomaAcidoFolico ? embarazo[0].TomaAcidoFolico : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    if (embarazo[0].TomaAcidoFolico !== 'SI'){
                                        doc.setTextColor(255,0,0);
                                    }
                                    doc.text(acidofolico,cell.textPos.x, cell.y - 4);
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Embarazo Actual',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var embarazoActual = doc.splitTextToSize(!!embarazo[0].EmbarazoEnMesActual ? embarazo[0].EmbarazoEnMesActual : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(embarazoActual,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Talla',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var talla = doc.splitTextToSize(!!embarazo[0].Talla ? embarazo[0].Talla + 'cm' : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(talla,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 5) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('IMC para la EG',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 6) {
                                    var imcEG = doc.splitTextToSize(!!embarazo[0].R_IMC ? embarazo[0].R_IMC : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(imcEG,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 7) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Hierro',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 8) {
                                    var tomaHierro = doc.splitTextToSize(!!embarazo[0].TomaHierro_txt ? embarazo[0].TomaHierro_txt : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    if (embarazo[0].TomaHierro !== 'SI'){
                                        doc.setTextColor(255,0,0);
                                    }
                                    doc.text(tomaHierro,cell.textPos.x, cell.y - 4);
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column3') {

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Fecha de Medición',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var fechaMedicion = doc.splitTextToSize(!!embarazo[0].FechaMedicion ? embarazo[0].FechaMedicion : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(fechaMedicion,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Estado Medición',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var estadoMedicion = doc.splitTextToSize(!!embarazo[0].EstadoMedicion ? embarazo[0].EstadoMedicion : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(estadoMedicion,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 5) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Controles',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 6) {
                                    var controles = doc.splitTextToSize(!!embarazo[0].CantControles_txt ? embarazo[0].CantControles_txt : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(controles,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 7) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Factores Riesgo',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 8) {
                                    var factorRiesgo = doc.splitTextToSize(!!embarazo[0].TieneFactoresRiesgos_txt ? embarazo[0].TieneFactoresRiesgos_txt : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    if (embarazo[0].TieneFactoresRiesgos !== 'NO'){
                                        doc.setTextColor(255,0,0);
                                    }
                                    doc.text(factorRiesgo,cell.textPos.x, cell.y - 4);
                                }

                                return false;
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                    resolve(doc);
                }
        });
    }

    private generaAntAntropometricos(doc:any, Fecha: string, Origen:string, NroRelevamiento:string){
        return new Promise((resolve, reject) => {
            let previous = doc.autoTable.previous;
            console.log('generaAntAntropometricos');

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = this.fichaPService.listTarjetas[5].rows;
        //Antecedentes Antropométricos

                let antecedentesAntr = this.fichaPService.antecedentes !== undefined ? this.fichaPService.antecedentes.filter((ant: any) => ant.Fecha === Fecha && ant.Origen === Origen && ant.NroRelevamiento === NroRelevamiento) : [];
                
                let startY = mThis.first.finalY + 2.5;
                let margin: any = {right: 107};

                if (antecedentesAntr.length > 0 && this.fichaPService.listTarjetas[5].Checked) {

                    let listTarj = this.fichaPService.listTarjetas.slice(6);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha && list.Origen === Origen && list.NroRelevamiento === NroRelevamiento)

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }

                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Antecedentes Antropométricos',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Nació Prematuro',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var prematuro = doc.splitTextToSize(!!antecedentesAntr[0].Prematuro ? antecedentesAntr[0].Prematuro : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(prematuro,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Peso al Nacer',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var pesonacer = doc.splitTextToSize(!!antecedentesAntr[0].PesoAlNacer ? antecedentesAntr[0].PesoAlNacer+'kg' : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(pesonacer,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 5) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Tipo de Parto',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 6) {
                                    var tipoParto = doc.splitTextToSize(!!antecedentesAntr[0].TipoPartoNacer ? antecedentesAntr[0].TipoPartoNacer : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(tipoParto,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 7) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Muestra Libreta Sanitaria',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 8) {
                                    var libreta = doc.splitTextToSize(!!antecedentesAntr[0].MuestraLibSanitaria ? antecedentesAntr[0].MuestraLibSanitaria : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(libreta,cell.textPos.x, cell.y - 4);
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Edad Gestacional al Nacer',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var edadG = doc.splitTextToSize(!!antecedentesAntr[0].EdadGestacional ? antecedentesAntr[0].EdadGestacional : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(edadG,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Talla al Nacer',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var talla = doc.splitTextToSize(!!antecedentesAntr[0].TallaAlNacer ? antecedentesAntr[0].TallaAlNacer+'cm' : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(talla,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 5) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    var titleComplicaciones = doc.splitTextToSize('Complicaciones en su gestación', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(titleComplicaciones,cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 6) {
                                    var complicaciones = doc.splitTextToSize(!!antecedentesAntr[0].TuvoComplicacionesEmbarazo ? antecedentesAntr[0].TuvoComplicacionesEmbarazo : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(complicaciones,cell.textPos.x, cell.y - 4);
                                }
                                
                                if(antecedentesAntr[0].MuestraLibSanitaria_txt_n2 === 'No'){
                                    if (data.row.index === 7) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Motivo No Muestra',cell.textPos.x, cell.y);
                                    }

                                    if (data.row.index === 8) {
                                        var motivoNoMuestra = doc.splitTextToSize(antecedentesAntr[0].MotivoNoMuestraLibSanitaria_txt_n2 ? antecedentesAntr[0].MotivoNoMuestraLibSanitaria_txt_n2 : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        doc.text(motivoNoMuestra,cell.textPos.x, cell.y - 4);
                                    }

                                }

                                return false;
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                resolve(doc);
            }
        });
    }

    private generaRiesgoNinio(doc:any, Fecha:string, Origen:string, NroRelevamiento:string){
        return new Promise((resolve, reject) => {
            let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = 4;

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};

        //RiesgoNiño
                let riesgoNinio = this.fichaPService.riesgoNinio !== undefined ? this.fichaPService.riesgoNinio.filter((r: any) => moment(r.Fecha).format('DD-MM-YY') === Fecha && r.Origen === Origen && r.NroRelevamiento === NroRelevamiento) : [];
                
                if (riesgoNinio.length > 0 && this.fichaPService.listTarjetas[7].Checked) {

                    var images: any = []; 

                    var TotalPuntos = 0;
                    var TotalFactoresConRiesgo = 0;
                    var TotalFactores = 0;

                    riesgoNinio.forEach((element: any) => {
                        if(Number(element['Valor']) > 0){
                            TotalFactoresConRiesgo++;
                            TotalPuntos += Number(element['Valor']);
                        }

                        TotalFactores++;
                    });

                    var Colores = [
                        {R:200, G:200, B:200}//color indice 0 --BASE
                        , {R:10, G:200, B:53}//color indice 1 --VERDE
                        , {R:230, G:255, B:80}//color indice 2 --VERDE/AMARILLO
                        , {R:255, G:255, B:0}//color indice 3 --AMARILLO
                        , {R:252, G:180, B:52}//color indice 4 --NARANJA
                        , {R:255, G:0, B:0}//color indice 5 --ROJO
                    ];
                    
                    var _Valor = TotalPuntos;
                    var _ValorFixed = _Valor.toFixed(0);
                    var _Columnas = TotalFactores;

                    var _PorcentajeColumnaPintada = TotalFactoresConRiesgo * 100 / _Columnas;
                    var _PorcentajeColumnaGris = 100 - _PorcentajeColumnaPintada;

                    var MaximaPonderacion = 20;
                    var PorcentajeLeyendaP = Number(_Valor * 100 / MaximaPonderacion).toFixed(2);
                    var PorcentajeLeyendaI = Number(TotalFactoresConRiesgo * 100 / _Columnas).toFixed(2);

                    var ValorIndice = 0;
                    if (Number(PorcentajeLeyendaP) >= 0 && Number(PorcentajeLeyendaP) < 20) ValorIndice = 1;
                    if (Number(PorcentajeLeyendaP) >= 20 && Number(PorcentajeLeyendaP) < 40) ValorIndice = 2;
                    if (Number(PorcentajeLeyendaP) >= 40 && Number(PorcentajeLeyendaP) < 60) ValorIndice = 3;
                    if (Number(PorcentajeLeyendaP) >= 60 && Number(PorcentajeLeyendaP) < 80) ValorIndice = 4;
                    if (Number(PorcentajeLeyendaP) >= 80 && Number(PorcentajeLeyendaP) < 100) ValorIndice = 5;

                    rows = riesgoNinio.length + 5;

                    let listTarj = this.fichaPService.listTarjetas.slice(8);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha && list.Origen === Origen && list.NroRelevamiento === NroRelevamiento);

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }


                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Nivel de Riesgos del Niño',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    let width = data.table.width - 4;
                                    doc.setDrawColor(0, 0, 0);
                                    doc.setFillColor(187,187,187);
                                    doc.roundedRect(cell.textPos.x, cell.y, data.table.width - 4, cell.height,2,2,'F');

                                    doc.setFillColor(Colores[ValorIndice].R,Colores[ValorIndice].G,Colores[ValorIndice].B);
                                    doc.roundedRect(cell.textPos.x, cell.y, ((data.table.width - 4) * _PorcentajeColumnaPintada / 100), cell.height,2,2,'F');
                                }

                                if (data.row.index === 3) {
                                    //doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Ponderación de Riesgo: '+ (TotalPuntos.toFixed(2)),cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    doc.setFontSize(8);
                                    doc.text('Indicadores de Riesgo: '+ PorcentajeLeyendaI  + '%', cell.textPos.x, cell.y - 3);
                                }

                                if (data.row.index >= 5 && data.row.index < riesgoNinio.length + 5) {
                                    var riesgo = doc.splitTextToSize(riesgoNinio[data.row.index - 5].AlertaTitulo, data.table.width + 10);
                                    doc.setFontSize(8);

                                    if (riesgoNinio[data.row.index - 5].Valor > 0){
                                        doc.setTextColor(255,0,0);
                                    } 

                                    doc.text(riesgo,cell.textPos.x + 4, cell.y - 3);

                                    images.push({
                                        url: riesgoNinio[data.row.index - 5].Valor === 0 ? mThis.checkBlack : mThis.checkRed,
                                        x: cell.textPos.x,
                                        y: cell.textPos.y,
                                        cellx: cell.x,
                                        celly: cell.y,
                                        valor: riesgoNinio[data.row.index - 5].Valor
                                    });
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {
                                return false;
                            }
                        },
                        addPageContent: function() {
                            for (var i = 0; i < images.length; i++) {
                                if (images[i].url !== undefined){
                                    doc.addImage(images[i].url,'JPG', images[i].x - (images[i].valor === 0 ? 0 : 1), images[i].celly - (images[i].valor === 0 ? 5 : 6), images[i].valor === 0 ? 2 : 4, images[i].valor === 0 ? 2 : 4);
                                }
                            }
                        }
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                resolve(doc);
            }
        });
    }

    private generaRiesgoAdolescente(doc:any, Fecha:string, Origen:string, NroRelevamiento:string){
        return new Promise((resolve, reject) => {
            let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = 4;

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};

        //Tarjeta riesgoAdolescente
                let riesgoAdolescente = this.fichaPService.riesgoAdolescente !== undefined ? this.fichaPService.riesgoAdolescente.filter((r: any) => moment(r.Fecha).format('DD-MM-YY') === Fecha && r.Origen === Origen && r.NroRelevamiento === NroRelevamiento) : [];
                
                if (riesgoAdolescente.length > 0 && this.fichaPService.listTarjetas[8].Checked) {

                    var images: any = []; 

                    var TotalPuntos = 0;
                    var TotalFactoresConRiesgo = 0;
                    var TotalFactores = 0;

                    riesgoAdolescente.forEach((element: any) => {
                        if(Number(element['Valor']) > 0){
                            TotalFactoresConRiesgo++;
                            TotalPuntos += Number(element['Valor']);
                        }

                        TotalFactores++;
                    });

                    var Colores = [
                        {R:200, G:200, B:200}//color indice 0 --BASE
                        , {R:10, G:200, B:53}//color indice 1 --VERDE
                        , {R:230, G:255, B:80}//color indice 2 --VERDE/AMARILLO
                        , {R:255, G:255, B:0}//color indice 3 --AMARILLO
                        , {R:252, G:180, B:52}//color indice 4 --NARANJA
                        , {R:255, G:0, B:0}//color indice 5 --ROJO
                    ];
                    
                    var _Valor = TotalPuntos;
                    var _ValorFixed = _Valor.toFixed(0);
                    var _Columnas = TotalFactores;

                    var _PorcentajeColumnaPintada = TotalFactoresConRiesgo * 100 / _Columnas;
                    var _PorcentajeColumnaGris = 100 - _PorcentajeColumnaPintada;

                    var MaximaPonderacion = 20;
                    var PorcentajeLeyendaP = Number(_Valor * 100 / MaximaPonderacion).toFixed(2);
                    var PorcentajeLeyendaI = Number(TotalFactoresConRiesgo * 100 / _Columnas).toFixed(2);

                    var ValorIndice = 0;
                    if (Number(PorcentajeLeyendaP) >= 0 && Number(PorcentajeLeyendaP) < 20) ValorIndice = 1;
                    if (Number(PorcentajeLeyendaP) >= 20 && Number(PorcentajeLeyendaP) < 40) ValorIndice = 2;
                    if (Number(PorcentajeLeyendaP) >= 40 && Number(PorcentajeLeyendaP) < 60) ValorIndice = 3;
                    if (Number(PorcentajeLeyendaP) >= 60 && Number(PorcentajeLeyendaP) < 80) ValorIndice = 4;
                    if (Number(PorcentajeLeyendaP) >= 80 && Number(PorcentajeLeyendaP) < 100) ValorIndice = 5;

                    rows = riesgoAdolescente.length + 5;

                    let listTarj = this.fichaPService.listTarjetas.slice(9);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha && list.Origen === Origen && list.NroRelevamiento === NroRelevamiento);

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }


                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Nivel de Riesgo del Escolar/Adolescente',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    let width = data.table.width - 4;
                                    doc.setDrawColor(0, 0, 0);
                                    doc.setFillColor(187,187,187);
                                    doc.roundedRect(cell.textPos.x, cell.y, data.table.width - 4, cell.height,2,2,'F');

                                    doc.setFillColor(Colores[ValorIndice].R,Colores[ValorIndice].G,Colores[ValorIndice].B);
                                    doc.roundedRect(cell.textPos.x, cell.y, ((data.table.width - 4) * _PorcentajeColumnaPintada / 100), cell.height,2,2,'F');
                                }

                                if (data.row.index === 3) {
                                    //doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Ponderación de Riesgo: '+ (TotalPuntos.toFixed(2)),cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    doc.setFontSize(8);
                                    doc.text('Indicadores de Riesgo: '+ PorcentajeLeyendaI + '%', cell.textPos.x, cell.y - 3);
                                }

                                if (data.row.index >= 5 && data.row.index < riesgoAdolescente.length + 5) {
                                    var riesgo = doc.splitTextToSize(riesgoAdolescente[data.row.index - 5].AlertaTitulo, data.table.width + 10);
                                    doc.setFontSize(8);

                                    if (riesgoAdolescente[data.row.index - 5].Valor > 0){
                                        doc.setTextColor(255,0,0);
                                    } 

                                    doc.text(riesgo,cell.textPos.x + 4, cell.y - 3);

                                    images.push({
                                        url: riesgoAdolescente[data.row.index - 5].Valor === 0 ? mThis.checkBlack : mThis.checkRed,
                                        x: cell.textPos.x,
                                        y: cell.textPos.y,
                                        cellx: cell.x,
                                        celly: cell.y,
                                        valor: riesgoAdolescente[data.row.index - 5].Valor
                                    });
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {
                                return false;
                            }
                        },
                        addPageContent: function() {
                            for (var i = 0; i < images.length; i++) {
                                if (images[i].url !== undefined){
                                    doc.addImage(images[i].url,'JPG', images[i].x - (images[i].valor === 0 ? 0 : 1), images[i].celly - (images[i].valor === 0 ? 5 : 6), images[i].valor === 0 ? 2 : 4, images[i].valor === 0 ? 2 : 4);
                                }
                            }
                        }
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                    resolve(doc);
                }
        });        
    }

    private generaRiesgoEmbarazada(doc:any, Fecha:string, Origen:string, NroRelevamiento:string){
        return new Promise((resolve, reject) => {
            let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = 4;

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};

        //Tarjeta riesgoEmbarazada
                let riesgoEmbarazada = this.fichaPService.riesgoEmbarazo !== undefined ? this.fichaPService.riesgoEmbarazo.filter((r: any) => moment(r.Fecha).format('DD-MM-YY') === Fecha && r.Origen === Origen && r.NroRelevamiento === NroRelevamiento) : [];
                
                if (riesgoEmbarazada.length > 0 && this.fichaPService.listTarjetas[9].Checked) {

                    var images: any = []; 

                    var TotalPuntos = 0;
                    var TotalFactoresConRiesgo = 0;
                    var TotalFactores = 0;

                    riesgoEmbarazada.forEach((element: any) => {
                        if(Number(element['Valor']) > 0){
                            TotalFactoresConRiesgo++;
                            TotalPuntos += Number(element['Valor']);
                        }

                        TotalFactores++;
                    });

                    var Colores = [
                        {R:200, G:200, B:200}//color indice 0 --BASE
                        , {R:10, G:200, B:53}//color indice 1 --VERDE
                        , {R:230, G:255, B:80}//color indice 2 --VERDE/AMARILLO
                        , {R:255, G:255, B:0}//color indice 3 --AMARILLO
                        , {R:252, G:180, B:52}//color indice 4 --NARANJA
                        , {R:255, G:0, B:0}//color indice 5 --ROJO
                    ];
                    
                    var _Valor = TotalPuntos;
                    var _ValorFixed = _Valor.toFixed(0);
                    var _Columnas = TotalFactores;

                    var _PorcentajeColumnaPintada = TotalFactoresConRiesgo * 100 / _Columnas;
                    var _PorcentajeColumnaGris = 100 - _PorcentajeColumnaPintada;

                    var MaximaPonderacion = 20;
                    var PorcentajeLeyendaP = Number(_Valor * 100 / MaximaPonderacion).toFixed(2);
                    var PorcentajeLeyendaI = Number(TotalFactoresConRiesgo * 100 / _Columnas).toFixed(2);

                    var ValorIndice = 0;
                    if (Number(PorcentajeLeyendaP) >= 0 && Number(PorcentajeLeyendaP) < 20) ValorIndice = 1;
                    if (Number(PorcentajeLeyendaP) >= 20 && Number(PorcentajeLeyendaP) < 40) ValorIndice = 2;
                    if (Number(PorcentajeLeyendaP) >= 40 && Number(PorcentajeLeyendaP) < 60) ValorIndice = 3;
                    if (Number(PorcentajeLeyendaP) >= 60 && Number(PorcentajeLeyendaP) < 80) ValorIndice = 4;
                    if (Number(PorcentajeLeyendaP) >= 80 && Number(PorcentajeLeyendaP) < 100) ValorIndice = 5;

                    rows = riesgoEmbarazada.length + 5;

                    let listTarj = this.fichaPService.listTarjetas.slice(10);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha && list.Origen === Origen && list.NroRelevamiento === NroRelevamiento);

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }


                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Nivel de Riesgos del Embarazo',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    let width = data.table.width - 4;
                                    doc.setDrawColor(0, 0, 0);
                                    doc.setFillColor(187,187,187);
                                    doc.roundedRect(cell.textPos.x, cell.y, data.table.width - 4, cell.height,2,2,'F');

                                    doc.setFillColor(Colores[ValorIndice].R,Colores[ValorIndice].G,Colores[ValorIndice].B);
                                    doc.roundedRect(cell.textPos.x, cell.y, ((data.table.width - 4) * _PorcentajeColumnaPintada / 100), cell.height,2,2,'F');
                                }

                                if (data.row.index === 3) {
                                    //doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Ponderación de Riesgo: '+ (TotalPuntos.toFixed(2)),cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    doc.setFontSize(8);
                                    doc.text('Indicadores de Riesgo: '+ PorcentajeLeyendaI  + '%', cell.textPos.x, cell.y - 3);
                                }

                                if (data.row.index >= 5 && data.row.index < riesgoEmbarazada.length + 5) {
                                    var riesgo = doc.splitTextToSize(riesgoEmbarazada[data.row.index - 5].AlertaTitulo, data.table.width + 10);
                                    doc.setFontSize(8);

                                    if (riesgoEmbarazada[data.row.index - 5].Valor > 0){
                                        doc.setTextColor(255,0,0);
                                    } 

                                    doc.text(riesgo,cell.textPos.x + 4, cell.y - 3);

                                    images.push({
                                        url: riesgoEmbarazada[data.row.index - 5].Valor === 0 ? mThis.checkBlack : mThis.checkRed,
                                        x: cell.textPos.x,
                                        y: cell.textPos.y,
                                        cellx: cell.x,
                                        celly: cell.y,
                                        valor: riesgoEmbarazada[data.row.index - 5].Valor
                                    });
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {
                                return false;
                            }
                        },
                        addPageContent: function() {
                            for (var i = 0; i < images.length; i++) {
                                if (images[i].url !== undefined){
                                    doc.addImage(images[i].url,'JPG', images[i].x - (images[i].valor === 0 ? 0 : 1), images[i].celly - (images[i].valor === 0 ? 5 : 6), images[i].valor === 0 ? 2 : 4, images[i].valor === 0 ? 2 : 4);
                                }
                            }
                        }
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                    resolve(doc);
                }
        });        
    }

    private generaEducacion(doc:any, Fecha:string, Origen:string, NroRelevamiento:string){
        return new Promise((resolve, reject) => {
	        let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = this.fichaPService.listTarjetas[10].rows;

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};
            
            //Educacion
                let educacion = this.fichaPService.educacion !== undefined ? this.fichaPService.educacion.filter((ed: any) => ed.Fecha === Fecha && ed.Origen === Origen && ed.NroRelevamiento === NroRelevamiento) : [];
                //console.log('servicios '+ index,servicios);
                if (educacion.length > 0 && this.fichaPService.listTarjetas[10].Checked) {

                    rows = !!educacion[0].Concurre ? educacion[0].Concurre == 'NO' ? 7 : 5 : 5;

                    let listTarj = this.fichaPService.listTarjetas.slice(11);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha && list.Origen === Origen && list.NroRelevamiento === NroRelevamiento);

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }

                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Educación',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('¿Analfabetismo?' ,cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var analfabetismo = doc.splitTextToSize(!!educacion[0].Analfabeto_txt ? educacion[0].Analfabeto_txt : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    if (educacion[0].Analfabeto !== 'NO'){
                                        doc.setTextColor(255,0,0);
                                    } 
                                    doc.text(analfabetismo,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('¿Concurre a un establecimiento educativo?',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var concurre = doc.splitTextToSize(!!educacion[0].Concurre ? educacion[0].Concurre : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    if (educacion[0].Concurre !== 'SÍ'){
                                        doc.setTextColor(255,0,0);
                                    } 
                                    doc.text(concurre,cell.textPos.x, cell.y - 4);
                                }

                                if (educacion[0].Concurre === 'NO'){
                                    if (data.row.index === 5) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Hace cuanto no concurre',cell.textPos.x, cell.y);
                                    }

                                    if (data.row.index === 6) {
                                        var cuantoNoConcurre = doc.splitTextToSize(!!educacion[0].HaceCuantoNoConcurre ? educacion[0].HaceCuantoNoConcurre : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        doc.text(cuantoNoConcurre,cell.textPos.x, cell.y - 4);
                                    }
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Nivel Educativo Alcanzado',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var nivelEd = doc.splitTextToSize(!!educacion[0].MNE ? educacion[0].MNE : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(nivelEd,cell.textPos.x, cell.y - 2);
                                }

                                if (educacion[0].Concurre === 'NO'){
                                    if (data.row.index === 5) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Motivo por el que no concurre',cell.textPos.x, cell.y);
                                    }

                                    if (data.row.index === 6) {
                                        var motivoNoConcurre = doc.splitTextToSize(!!educacion[0].MotivoNoConcurre ? educacion[0].MotivoNoConcurre : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        doc.text(motivoNoConcurre,cell.textPos.x, cell.y - 4);
                                    }
                                }

                                return false;
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                    resolve(doc);
                }
        });
    }

    private generaTrabajo(doc:any, Fecha:string, Origen:string, NroRelevamiento:string){
        return new Promise((resolve, reject) => {
	        let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = this.fichaPService.listTarjetas[11].rows;

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};
            
            //Trabajo
                let trabajo = this.fichaPService.trabajo !== undefined ? this.fichaPService.trabajo.filter((tr: any) => tr.Fecha === Fecha && tr.Origen === Origen && tr.NroRelevamiento === NroRelevamiento) : [];
                let ingresos = this.fichaPService.ingresos !== undefined ? this.fichaPService.ingresos.filter((ing: any) => ing.Fecha === Fecha && ing.Origen === Origen && ing.NroRelevamiento === NroRelevamiento) : [];

                let listTarj = this.fichaPService.listTarjetas.slice(12);//.filter((list:any) => list.Checked === true)
                listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha && list.Origen === Origen && list.NroRelevamiento === NroRelevamiento)
                //console.log('servicios '+ index,servicios);
                if (trabajo.length > 0 && this.fichaPService.listTarjetas[11].Checked) {

                    rows = trabajo[0].Trabaja === 'NO'&& trabajo[0].BuscoTrabajoU30D !== 'NO' ? 5 : rows;

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        let rowsAux = !!listTarj && listTarj.length > 0 ? listTarj[0].rows : rows;

                        if (ingresos.length > 0){
                            rowsAux = (ingresos.length + 4) > rows ? ingresos.length + 4 : rows;
                        }

                        rows = rowsAux > rows ? rowsAux : rows;
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }

                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {

                            if (trabajo[0].Trabaja == 'SÍ' || trabajo[0].Trabaja  == 'SI') {
                                                // Rowspan
                                if (data.column.dataKey === 'column1') {

                                    if (data.row.index === 0) {
                                        doc.setFontStyle('bold');

                                        doc.text('Trabajo',cell.textPos.x, cell.textPos.y + 2);

                                        doc.setDrawColor(0, 0, 0);
                                        doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                    }

                                    if (data.row.index === 1) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('¿Trabaja?' ,cell.textPos.x, cell.textPos.y);
                                    }

                                    if (data.row.index === 2) {
                                        var trabaja = doc.splitTextToSize(!!trabajo[0].Trabaja ? trabajo[0].Trabaja : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        if (trabajo[0].Trabaja === 'NO'){
                                            doc.setTextColor(255,0,0);
                                        } 
                                        doc.text(trabaja,cell.textPos.x, cell.y - 2);
                                    }

                                    if (data.row.index === 3) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Periodicidad',cell.textPos.x, cell.y);
                                    }

                                    if (data.row.index === 4) {
                                        var periodicidad = doc.splitTextToSize(!!trabajo[0].Periodicidad ? trabajo[0].Periodicidad : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        doc.text(periodicidad,cell.textPos.x, cell.y - 4);
                                    }

                                    if (data.row.index === 5) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Aportes Jubilatorios',cell.textPos.x, cell.y);
                                    }

                                    if (data.row.index === 6) {
                                        var aportes = doc.splitTextToSize(!!trabajo[0].Aportes_jubilatorios ? trabajo[0].Aportes_jubilatorios : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        doc.text(aportes,cell.textPos.x, cell.y - 4);
                                    }
                                    return false;
                                }

                                if (data.column.dataKey === 'column2') {

                                    if (data.row.index === 1) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Situación Laboral',cell.textPos.x, cell.textPos.y);
                                    }

                                    if (data.row.index === 2) {
                                        var sitLaboral = doc.splitTextToSize(!!trabajo[0].SituacionLaboral ? trabajo[0].SituacionLaboral : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        doc.text(sitLaboral,cell.textPos.x, cell.y - 2);
                                    }

                                    if (data.row.index === 3) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Carga Horaria',cell.textPos.x, cell.y);
                                    }

                                    if (data.row.index === 4) {
                                        var cargaHoraria = doc.splitTextToSize(!!trabajo[0].HorasTrabajadasXDia ? trabajo[0].HorasTrabajadasXDia : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        doc.text(cargaHoraria,cell.textPos.x, cell.y - 4);
                                    }

                                    return false;
                                }

                            } else {
                                // Rowspan
                                if (data.column.dataKey === 'column1') {

                                    if (data.row.index === 0) {
                                        doc.setFontStyle('bold');

                                        doc.text('Trabajo',cell.textPos.x, cell.textPos.y + 2);

                                        doc.setDrawColor(0, 0, 0);
                                        doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                    }

                                    if (data.row.index === 1) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('¿Trabaja?' ,cell.textPos.x, cell.textPos.y);
                                    }

                                    if (data.row.index === 2) {
                                        var trabaja = doc.splitTextToSize(!!trabajo[0].Trabaja ? trabajo[0].Trabaja : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        if (trabajo[0].Trabaja === 'NO'){
                                            doc.setTextColor(255,0,0);
                                        } 
                                        doc.text(trabaja,cell.textPos.x, cell.y - 2);
                                    }

                                    if (data.row.index === 3) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('¿Buscó trabajo en los últimos 30 días?',cell.textPos.x, cell.y);
                                    }

                                    if (data.row.index === 4) {
                                        var busco30 = doc.splitTextToSize(!!trabajo[0].BuscoTrabajoU30D ? trabajo[0].BuscoTrabajoU30D : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        doc.text(busco30,cell.textPos.x, cell.y - 4);
                                    }

                                    if (trabajo[0].BuscoTrabajoU30D === 'NO'){
                                        if (data.row.index === 5) {
                                            doc.setFontStyle('bold');
                                            doc.setFontSize(8);
                                            doc.text('¿Buscó trabajo en los últimos 12 meses?',cell.textPos.x, cell.y);
                                        }

                                        if (data.row.index === 6) {
                                            var busco12 = doc.splitTextToSize(!!trabajo[0].BuscoTrabajoU12M ? trabajo[0].BuscoTrabajoU12M : '-', (data.table.width / 2) + 4);
                                            doc.setFontSize(8);
                                            doc.text(busco12,cell.textPos.x, cell.y - 4);
                                        }
                                    }
                                    return false;
                                }

                                if (data.column.dataKey === 'column2') {

                                    if (data.row.index === 1) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Motivo',cell.textPos.x, cell.textPos.y);
                                    }

                                    if (data.row.index === 2) {
                                        var motivoNoTrabaja = doc.splitTextToSize(!!trabajo[0].MotivoNoTrabaja ? trabajo[0].MotivoNoTrabaja : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        doc.text(motivoNoTrabaja,cell.textPos.x, cell.y - 2);
                                    }

                                    return false;
                                }
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    //resolve(doc);
                }

             //ingresos
                rows = 5;
                //console.log('servicios '+ index,servicios);
                let oficios = this.fichaPService.oficios !== undefined ? this.fichaPService.oficios.filter((of: any) => of.Fecha === Fecha && of.Origen === Origen && of.NroRelevamiento === NroRelevamiento) : [];
                if (ingresos.length > 0 && this.fichaPService.listTarjetas[11].Checked) {

                    rows = ingresos.length + 4;

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        let rowsAux = !!listTarj && listTarj.length > 0 ? listTarj[0].rows : rows;

                        if (oficios.length > 0){
                            rowsAux = rows;
                        }
                        
                        rows = rowsAux > rows ? rowsAux : rows;
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }

                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {

                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Ingresos',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Fuentes de Ingreso' ,cell.textPos.x, cell.textPos.y);
                                }

                                // if (data.row.index === 2) {
                                //     var fuenteIngreso = doc.splitTextToSize(!!ingresos[0].FuenteIngreso ? ingresos[0].FuenteIngreso : '-', (data.table.width / 2) + 4);
                                //     doc.setFontSize(8);
                                //     doc.text(fuenteIngreso,cell.textPos.x, cell.y);
                                // }

                                if (data.row.index >= 2 && data.row.index < ingresos.length + 2) {
                                    let row = data.row.index - 2;
                                    let y = row === 0 ? cell.y : cell.y - 2;
                                    var fuenteIngreso = doc.splitTextToSize(!!ingresos[row] ? ingresos[row].FuenteIngreso : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(fuenteIngreso,cell.textPos.x, y);

                                    // if (!!grupoFamiliar[row] && data.row.index < rows - 1){
                                    //     //doc.setDrawColor(0, 0, 0);
                                    //     doc.rect(cell.x, cell.y - 5, data.table.width, cell.height );
                                    // }
                                }

                                if (data.row.index === ingresos.length + 2) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Rango de Ingresos',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === ingresos.length + 3) {
                                    var rangoIngreso = doc.splitTextToSize(!!ingresos[0].RangoIngreso ? ingresos[0].RangoIngreso : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(rangoIngreso,cell.textPos.x, cell.y - 4);
                                }
                                
                                return false;
                            }

                            if (data.column.dataKey === 'column2') {

                                if (data.row.index === 1) {
                                    let titleFuente = doc.splitTextToSize('¿Es la Fuente de Ingreso Principal?', (data.table.width / 2) + 4);
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text(titleFuente,cell.textPos.x, cell.textPos.y);
                                }

                                // if (data.row.index === 2) {
                                //     var esPrincipal = doc.splitTextToSize(!!ingresos[0].EsPrincipal ? ingresos[0].EsPrincipal : '-', (data.table.width / 2) + 4);
                                //     doc.setFontSize(8);
                                //     doc.text(esPrincipal,cell.textPos.x, cell.y);
                                // }

                                if (data.row.index >= 2 && data.row.index < ingresos.length + 2) {
                                    let row = data.row.index - 2
                                    let y = row === 0 ? cell.y : cell.y - 2;
                                    var esPrincipal = doc.splitTextToSize(!!ingresos[row] ? ingresos[row].EsPrincipal : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(esPrincipal,cell.textPos.x, y);
                                }

                                return false;
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                }

                 //Oficios
                rows = 4;
                //console.log('servicios '+ index,servicios);
                if (oficios.length > 0 && this.fichaPService.listTarjetas[11].Checked) {
                    
                    rows = oficios.length +2;
                    
                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }

                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {

                        // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Oficios',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Oficio' ,cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index >= 2 && data.row.index < oficios.length + 2) {
                                    let row = data.row.index - 2
                                    let y = row === 0 ? cell.y : cell.y - 2;
                                    var oficio = doc.splitTextToSize(!!oficios[row].Oficio ? oficios[row].Oficio : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(oficio,cell.textPos.x, y);
                                }
                                return false;
                            }

                            if (data.column.dataKey === 'column2') {

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('¿Ejerce el Oficio?',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index >= 2 && data.row.index < oficios.length + 2) {
                                    let row = data.row.index - 2
                                    let y = row === 0 ? cell.y : cell.y - 2;
                                    var ejerce = doc.splitTextToSize(!!oficios[row].Ejerce ? oficios[row].Ejerce : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(ejerce,cell.textPos.x, y);
                                }

                                return false;
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                }
                resolve(doc);
        });
    }

    private generaSanitarios(doc:any, Fecha:string, Origen:string, NroRelevamiento:string){
        return new Promise((resolve, reject) => {
	        let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = this.fichaPService.listTarjetas[12].rows;
            //Sanitario

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};

                let sanitario = this.fichaPService.sanitarios !== undefined ? this.fichaPService.sanitarios.filter((san: any) => san.Fecha === Fecha && san.Origen === Origen && san.NroRelevamiento === NroRelevamiento) : [];
                let servicios = this.fichaPService.servicios !== undefined ? this.fichaPService.servicios.filter((serv: any) => serv.Fecha === Fecha && serv.Origen === Origen && serv.NroRelevamiento === NroRelevamiento) : [];
                
                if (sanitario.length > 0 && this.fichaPService.listTarjetas[12].Checked) {

                    rows = 5;

                    let listTarj = this.fichaPService.listTarjetas.slice(13);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha && list.Origen === Origen && list.NroRelevamiento === NroRelevamiento);

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        rows = servicios.length > 0 ? 7 : !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;//ToDo validar cuando no haya servicios
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }


                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Sanitario',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text(!!!sanitario[0].TieneBanio || sanitario[0].TieneBanio === 'NO' ? 'No Tiene Baño' : 'Tiene Baño' ,cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var usoBanio = doc.splitTextToSize(!!sanitario[0].UsoBanio ? sanitario[0].UsoBanio : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(usoBanio,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    var tieneAguaCaliente = doc.splitTextToSize('¿Tiene Agua Caliente en el Baño?', (data.table.width / 2) + 4);
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text(tieneAguaCaliente,cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var aguaCaliente = doc.splitTextToSize(!!sanitario[0].TieneAguaCaliente_txt ? sanitario[0].TieneAguaCaliente_txt : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(aguaCaliente,cell.textPos.x, cell.y - 1);
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Ubicación del Baño',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var ubicacion = doc.splitTextToSize(!!sanitario[0].UbicacionBanio ? sanitario[0].UbicacionBanio : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(ubicacion,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Tipo de Desagüe',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var desague = doc.splitTextToSize(!!sanitario[0].TipoDesague ? sanitario[0].TipoDesague : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(desague,cell.textPos.x, cell.y - 1);
                                }

                                return false;
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                    resolve(doc);
                }
        });
    }

    private generaServicios(doc:any, Fecha:string, Origen:string, NroRelevamiento:string){
        return new Promise((resolve, reject) => {
	        let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = this.fichaPService.listTarjetas[12].rows;

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};

            mThis.first = doc.autoTable.previous;
            
            //Servicios
                let servicios = this.fichaPService.servicios !== undefined ? this.fichaPService.servicios.filter((serv: any) => serv.Fecha === Fecha && serv.Origen === Origen && serv.NroRelevamiento === NroRelevamiento) : [];
                //console.log('servicios '+ index,servicios);
                if (servicios.length > 0 && this.fichaPService.listTarjetas[12].Checked) {

                    let listTarj = this.fichaPService.listTarjetas.slice(13);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha && list.Origen === Origen && list.NroRelevamiento === NroRelevamiento)
                    
                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }

                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Servicios',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text(servicios[0].FuenteAguaBeber ? 'Fuente Agua para Beber' : 'Fuente de Agua',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var aguaBeber = doc.splitTextToSize(servicios[0].FuenteAguaBeber ? servicios[0].FuenteAguaBeber : servicios[0].FuenteAgua ? servicios[0].FuenteAgua : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(aguaBeber,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Fuente de Electricidad',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var fuenteElect = doc.splitTextToSize(!!servicios[0].FuenteElectricidad ? servicios[0].FuenteElectricidad : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(fuenteElect,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 5) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Fuente de Gas',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 6) {
                                    var fuenteGas = doc.splitTextToSize(!!servicios[0].FuenteGas ? servicios[0].FuenteGas : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(fuenteGas,cell.textPos.x, cell.y);
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text(servicios[0].FuenteAguaCocinar ? 'Fuente Agua para Cocinar' : 'Ubicación del Agua',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var aguaCocinar = doc.splitTextToSize(servicios[0].FuenteAguaCocinar ? servicios[0].FuenteAguaCocinar : servicios[0].UbicacionAgua ? servicios[0].UbicacionAgua : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(aguaCocinar,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Fuente de Iluminación',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var fuenteIlum = doc.splitTextToSize(!!servicios[0].FuenteIluminacion ? servicios[0].FuenteIluminacion : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(fuenteIlum,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 5) {
                                    var tituloCocina = doc.splitTextToSize('Medio que utiliza para cocinar los alimentos', (data.table.width / 2) + 4);
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text(tituloCocina,cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 6) {
                                    var cocina = doc.splitTextToSize(!!servicios[0].CocinaCon ? servicios[0].CocinaCon : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(cocina,cell.textPos.x, cell.y);
                                }

                                return false;
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                    resolve(doc);
                }
        });
    }

    private generaInfraEstructura(doc:any, Fecha:string, Origen:string, NroRelevamiento:string){
        return new Promise((resolve, reject) => {
	        let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = this.fichaPService.listTarjetas[13].rows;

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};

         //Infraestructura
                let infra = this.fichaPService.infraestructura !== undefined ? this.fichaPService.infraestructura.filter((inf: any) => inf.Fecha === Fecha && inf.Origen === Origen && inf.NroRelevamiento === NroRelevamiento) : [];
                //console.log('infra '+ index,infra);
                if (infra.length > 0 && this.fichaPService.listTarjetas[13].Checked) {

                    let listTarj = this.fichaPService.listTarjetas.slice(14);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha && list.Origen === Origen && list.NroRelevamiento === NroRelevamiento)

                    let width = 88.8908362253333;
                    let cellWidth = 29.63027874177777;
                    let pos = {x:0,textx:0};

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }

                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Infraestructura',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);

                                    pos.textx = cell.textPos.x;
                                    pos.x = cell.x;
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Tipo de Vivienda' ,cell.textPos.x, cell.textPos.y);

                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Cant. de Dormitorios',pos.textx + cellWidth, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var tipoVivienda = doc.splitTextToSize(!!infra[0].TipoVivienda ? infra[0].TipoVivienda : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(tipoVivienda,cell.textPos.x, cell.y - 2);

                                    var cantDorm = doc.splitTextToSize(!!infra[0].CantDormitorios ? infra[0].CantDormitorios : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(cantDorm,pos.textx + cellWidth, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Hacinamiento (MPI) ',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var hacinamiento = doc.splitTextToSize(!!infra[0].Hacinamiento ? infra[0].Hacinamiento : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(hacinamiento,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 5) {
                                    var titleCantMax = doc.splitTextToSize('Cant. Max. Personas en dormitorio', (data.table.width / 2) + 4);
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text(titleCantMax,cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 6) {
                                    var cantMax = doc.splitTextToSize(!!infra[0].CantMaxEnUnDormitorio ? infra[0].CantMaxEnUnDormitorio : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(cantMax,cell.textPos.x, cell.y - 1);
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Pers. en la Vivienda',pos.textx + (cellWidth * 2), cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var persVivienda = doc.splitTextToSize(!!infra[0].CantPersonas ? infra[0].CantPersonas : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(persVivienda,pos.textx + (cellWidth * 2), cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Nivel de Hacinamiento (INDEC)',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var hacIndec = doc.splitTextToSize(!!infra[0].IndiceHacinamiento ? infra[0].IndiceHacinamiento : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(hacIndec,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 5) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('¿Sufrió ilícito?',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 6) {
                                    var licito = doc.splitTextToSize(!!infra[0].SufrioAlgunIlicito ? infra[0].SufrioAlgunIlicito : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(licito,cell.textPos.x, cell.y - 1);
                                }

                                return false;
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                    resolve(doc);
                }
        });
    }

    private generaObservaciones(doc:any, Fecha:string, Origen:string, NroRelevamiento:string){
        return new Promise((resolve, reject) => {
	        let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = 4;

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};

         ///Oberservaciones
                //console.log('array.Fecha',array.Fecha);
                let observaciones = this.fichaPService.observaciones !== undefined ? this.fichaPService.observaciones.filter((obs: any) => obs.Fecha === Fecha && obs.Origen === Origen && obs.NroRelevamiento === NroRelevamiento) : [];
                let urgencias = this.fichaPService.urgencias !== undefined ? this.fichaPService.urgencias.filter((urg: any) => urg.Fecha === Fecha && urg.Origen === Origen && urg.NroRelevamiento === NroRelevamiento) : [];
                //console.log('observaciones '+ index,observaciones);

                let listTarj = this.fichaPService.listTarjetas.slice(15);//.filter((list:any) => list.Checked === true)
                listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha && list.Origen === Origen && list.NroRelevamiento === NroRelevamiento)

                if ((observaciones.length > 0 || urgencias.length > 0) && this.fichaPService.listTarjetas[14].Checked) {

                    if (observaciones.length > 0) {
                        rows = observaciones.length + 1;

                        if (previous.pageStartX !== 107 && this.CountFicha > 0){
                            // Reset page to the same as before previous table
                            doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                            rows = previous.rows.length > rows ? previous.rows.length : rows;
                            startY = previous.pageStartY;
                            margin = {left: 107};
                        } else {
                            if (urgencias.length === 0){
                                rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                            } else{
                                rows = urgencias.length + 1 > rows ? urgencias.length + 1 : rows;
                            }
                            startY = previous.finalY + 2.5;
                            margin = {right: 107};
                        }

                        rows = rows < 4 ? 4 : rows;

                        //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                        columns = [
                            {title: "columnaI", dataKey: "column1"},
                            {title: "columnad", dataKey: "column2"},
                        ];
            
                        data = mThis.generaTemplate(rows);

                        doc.autoTable(columns, data, {
                            startY: startY,
                            showHeader: 'never',
                            pageBreak:'avoid',
                            margin: margin,
                            styles: {
                                halign: 'left',
                            },
                            drawCell: function (cell: any, data: any) {
                                // Rowspan
                                if (data.column.dataKey === 'column1') {

                                    if (data.row.index === 0) {
                                        doc.setFontStyle('bold');

                                        doc.text('Observaciones',cell.textPos.x, cell.textPos.y + 2);

                                        doc.setDrawColor(0, 0, 0);
                                        doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                    }

                                    if (data.row.index >= 1 && data.row.index < observaciones.length + 1) {
                                        var observacion = doc.splitTextToSize(!!observaciones[data.row.index - 1].Observacion ? observaciones[data.row.index - 1].Observacion : '-', data.table.width + 10);
                                        doc.setFontSize(8);
                                        doc.text(observacion,cell.textPos.x, cell.y);
                                    }

                                    return false;
                                }

                                if (data.column.dataKey === 'column2') {

                                    return false;
                                }
                            },
                        });

                        previous = doc.autoTable.previous;
                        this.CountFicha++;
                    }

                    if (urgencias.length > 0) {

                        rows = urgencias.length + 1;

                        if (previous.pageStartX !== 107 && this.CountFicha > 0){
                            // Reset page to the same as before previous table
                            doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                            rows = previous.rows.length > rows ? previous.rows.length : rows;
                            startY = previous.pageStartY;
                            margin = {left: 107};
                        } else {
                            rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                            startY = previous.finalY + 2.5;
                            margin = {right: 107};
                        }

                        rows = rows < 4 ? 4 : rows;

                        //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                        columns = [
                            {title: "columnaI", dataKey: "column1"},
                            {title: "columnad", dataKey: "column2"},
                        ];
            
                        data = mThis.generaTemplate(rows);

                        doc.autoTable(columns, data, {
                            startY: startY,
                            showHeader: 'never',
                            pageBreak:'avoid',
                            margin: margin,
                            styles: {
                                halign: 'left',
                            },
                            drawCell: function (cell: any, data: any) {
                                // Rowspan
                                if (data.column.dataKey === 'column1') {

                                    if (data.row.index === 0) {
                                        doc.setFontStyle('bold');

                                        doc.text('Urgencias',cell.textPos.x, cell.textPos.y + 2);

                                        doc.setDrawColor(0, 0, 0);
                                        doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                    }

                                    if (data.row.index >= 1 && data.row.index < urgencias.length + 1) {
                                        var urgencia = doc.splitTextToSize(!!urgencias[data.row.index - 1].Descripcion ? urgencias[data.row.index - 1].Descripcion : '-', data.table.width + 10);
                                        doc.setFontSize(8);
                                        doc.text(urgencia,cell.textPos.x, cell.y);
                                    }

                                    return false;
                                }

                                if (data.column.dataKey === 'column2') {

                                    return false;
                                }
                            },
                        });

                        previous = doc.autoTable.previous;
                        this.CountFicha++;
                    }

                    resolve(doc)
                } else {
                    resolve(doc);
                }
        });
    }

    private generaAntNutricionales(doc:any, Fecha:string, Origen:string, NroRelevamiento:string){
        return new Promise((resolve, reject) => {
	        let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = this.fichaPService.listTarjetas[6].rows;
            //AntNutricionales

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};

                let antNutricionales = this.fichaPService.antecedentesNutricionales !== undefined ? this.fichaPService.antecedentesNutricionales.filter((antN: any) => antN.Fecha === Fecha && antN.Origen === Origen && antN.NroRelevamiento === NroRelevamiento) : [];
                let antPatologicos = this.fichaPService.antecedentesPatologicos !== undefined ? this.fichaPService.antecedentesPatologicos.filter((antP: any) => antP.Fecha === Fecha && antP.Origen === Origen && antP.NroRelevamiento === NroRelevamiento) : [];

                if (antNutricionales.length > 0 && this.fichaPService.listTarjetas[6].Checked) {

                    let listTarj = this.fichaPService.listTarjetas.slice(7);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha && list.Origen === Origen && list.NroRelevamiento === NroRelevamiento);

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        if (antPatologicos.length === 0){
                            rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                        }
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }


                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Antecedentes Nutricionales',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Asiste comedor',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var asisteComedor = doc.splitTextToSize(!!antNutricionales[0].AsisteComedor ? antNutricionales[0].AsisteComedor : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(asisteComedor,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Motivo Principal',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var motivoAsiste = doc.splitTextToSize(!!antNutricionales[0].AsisteComedorMotivo ? antNutricionales[0].AsisteComedorMotivo : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(motivoAsiste,cell.textPos.x, cell.y - 1);
                                }

                                if (data.row.index === 5) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Consume los Alimentos',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 6) {
                                    var consumeAlimentos = doc.splitTextToSize(!!antNutricionales[0].ConsumeComedor ? antNutricionales[0].ConsumeComedor : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(consumeAlimentos,cell.textPos.x, cell.y - 1);
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Desde cuando asiste',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var tiempoAsiste = doc.splitTextToSize(!!antNutricionales[0].AsisteComedorDesde ? antNutricionales[0].AsisteComedorDesde : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(tiempoAsiste,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Asiste',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var diasAsiste = doc.splitTextToSize(!!antNutricionales[0].AsisteDias ? antNutricionales[0].AsisteDias : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(diasAsiste,cell.textPos.x, cell.y - 1);
                                }

                                return false;
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                    resolve(doc);
                }
        });
    }

    private generaAntPatologicos(doc:any, Fecha:string, Origen:string, NroRelevamiento:string){
        return new Promise((resolve, reject) => {
	        let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = this.fichaPService.listTarjetas[6].rows;
            //AntPatologicos

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};

                let antPatologicos = this.fichaPService.antecedentesPatologicos !== undefined ? this.fichaPService.antecedentesPatologicos.filter((antP: any) => antP.Fecha === Fecha && antP.Origen === Origen && antP.NroRelevamiento === NroRelevamiento) : [];
                
                if (antPatologicos.length > 0 && this.fichaPService.listTarjetas[6].Checked) {

                    let listTarj = this.fichaPService.listTarjetas.slice(7);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha && list.Origen === Origen && list.NroRelevamiento === NroRelevamiento);

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }


                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Antecedentes Patológicos',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (!antPatologicos[0].Cirugia_txt){
                                    if (data.row.index === 1) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Cantidad de Internaciones',cell.textPos.x, cell.textPos.y);
                                    }

                                    if (data.row.index === 2) {
                                        var cantInternaciones = doc.splitTextToSize(!!antPatologicos[0].CantInternaciones ? antPatologicos[0].CantInternaciones : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        doc.text(cantInternaciones,cell.textPos.x, cell.y - 2);
                                    }

                                    if (data.row.index === 3) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Cantidad de Cirugías',cell.textPos.x, cell.y);
                                    }

                                    if (data.row.index === 4) {
                                        var cantCirugias = doc.splitTextToSize(!!antPatologicos[0].CantCirugias ? antPatologicos[0].CantCirugias : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        doc.text(cantCirugias,cell.textPos.x, cell.y - 1);
                                    }

                                    if (data.row.index === 5) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Cantidad de Fracturas',cell.textPos.x, cell.y);
                                    }

                                    if (data.row.index === 6) {
                                        var cantFracturas = doc.splitTextToSize(!!antPatologicos[0].CantFracturas ? antPatologicos[0].CantFracturas : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        doc.text(cantFracturas,cell.textPos.x, cell.y - 1);
                                    }
                                } else{
                                    if (data.row.index === 1) {
                                        var tilteCirugias = doc.splitTextToSize(antPatologicos[0].Cirugia_txt ? antPatologicos[0].Cirugia_txt : '-', data.table.width  + 4);
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text(tilteCirugias,cell.textPos.x, cell.textPos.y);
                                    }

                                    if (data.row.index === 2) {
                                        var descripCirugia = doc.splitTextToSize(antPatologicos[0].DescripcionCirugia ? antPatologicos[0].DescripcionCirugia : '-', data.table.width  + 4);
                                        doc.setFontSize(8);
                                        doc.text(descripCirugia,cell.textPos.x, cell.y - 2);
                                    }
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {
                                if (!antPatologicos[0].Cirugia_txt){
                                    if (data.row.index === 1) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Motivos',cell.textPos.x, cell.textPos.y);
                                    }

                                    if (data.row.index === 2) {
                                        var motivoInter = doc.splitTextToSize(!!antPatologicos[0].MotivoInternaciones ? antPatologicos[0].MotivoInternaciones : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        doc.text(motivoInter,cell.textPos.x, cell.y - 2);
                                    }

                                    if (data.row.index === 3) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Motivos',cell.textPos.x, cell.y);
                                    }

                                    if (data.row.index === 4) {
                                        var motivoCirugias = doc.splitTextToSize(!!antPatologicos[0].MotivoCirugias ? antPatologicos[0].MotivoCirugias : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        doc.text(motivoCirugias,cell.textPos.x, cell.y - 1);
                                    }
                                }
                                return false;
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                    resolve(doc);
                }
        });
    }

    private generaGrupoFamiliar(doc:any){
        return new Promise((resolve, reject) => {

            let mThis = this;
            let previous = doc.autoTable.previous;

            if (mThis.first.autoTable !== undefined) {
                this.CountFicha = 0;
                mThis.first = mThis.first.autoTable.previous;
            }

            let startY = mThis.first.finalY + 5;
            let margin: any = {right: 107};

            let rows = 4;

            var columns = [];
            var data: any;

            var grupoFamiliar = this.fichaPService.grupoFamiliar;

            if (grupoFamiliar && this.fichaPService.listTarjetas[15].Checked) {

                let headGrupoFam: any=[];
                headGrupoFam.push({
                    Fecha: 'Grupo Familiar'
                });

                doc.autoTable(this.getColumnsHVisitas(), headGrupoFam, {
                    startY: previous.finalY + 5,
                    // tableLineColor: [189, 195, 199],
                    // tableLineWidth: 0.75,
                    showHeader: 'never',
                    halign: 'center',
                    styles: {
                        // font: 'courier',
                        // lineColor: [44, 62, 80],
                        // lineWidth: 0.75
                        halign: 'left'
                    },     
                    bodyStyles: {
                        fillColor: [255, 255, 255],
                    },
                    alternateRowStyles: {
                        fillColor: [255, 255, 255]
                    },       
                });

                previous = doc.autoTable.previous;
                this.first = doc.autoTable.previous;

                let startY = mThis.first.finalY + 2.5;
                let margin: any = {right: 107};

                this.CountFicha = 0;

                rows = this.fichaPService.grupoFamiliar.length + 2;

                let cellWidthSexo = 5;
                let cellWidth4 = 20.222709056333326;
                let cellWidth2 = 40.44541811266665;
                let cellWidthImg = 3;
                let pos = {x:0,textx:0,y:0,heigth:0};

                let imgJefeHogar = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAMCAIAAAAyIHCzAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACRSURBVChTY/h+ZaVHaMvEK7/+EwGQVH95tnfJjLiUBrHQGrGolriWjXuffAIp+fnqzJY5AVErLwNV//9/bWLokr0n5xgDFeFDS87AVa/bOQFDGg0NctVRDQFZHQFZDdoIdRDxtYgw2bZzinHJknVXXn0HhysIfLm3bcoUkM6SGTN3XnsJFoOqBtpCDKCd6v//AaTQX9WAAEqdAAAAAElFTkSuQmCC";

                let images: any = [];
                //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                columns = [
                    {title: "columnaI", dataKey: "column1"},
                    {title: "columnad", dataKey: "column2"},
                ];
    
                data = mThis.generaTemplate(rows);

                doc.autoTable(columns, data, {
                    startY: startY,
                    showHeader: 'never',
                    pageBreak:'avoid',
                    margin: margin,
                    styles: {
                        halign: 'left',
                    },
                    drawCell: function (cell: any, data: any) {
                        // Rowspan
                        if (data.column.dataKey === 'column1') {

                            if (data.row.index === 0) {
                                doc.setFontStyle('bold');

                                doc.text('Composición del Hogar',cell.textPos.x, cell.textPos.y + 2);

                                doc.setDrawColor(0, 0, 0);
                                doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);

                                pos.textx = cell.textPos.x;
                                pos.x = cell.x;
                                pos.y = cell.y;
                                pos.heigth = cell.heigth;
                            }

                            // if (data.row.index === 1) {
                            //     //doc.setDrawColor(0, 0, 0);
                            //     doc.rect(cell.x, cell.y - 5, data.table.width, cell.height );
                            // }

                            if (data.row.index >= 2) {
                                let row = data.row.index - 2;
                                //let datoSexo = !!grupoFamiliar[row] ? grupoFamiliar[row].Sexo === 'Femenino' ? 'F' : 'M' : '';
                                var sexo = doc.splitTextToSize(!!grupoFamiliar[row] ? grupoFamiliar[row].Sexo === 'Femenino' ? 'F' : 'M' : '', cellWidthSexo + 4);
                                doc.setFontSize(8);
                                doc.text(sexo,cell.textPos.x, cell.y - 2);

                                var nombres = doc.splitTextToSize(!!grupoFamiliar[row] ? grupoFamiliar[row].ApellidosNombres : '', cellWidth2);
                                doc.setFontSize(8);
                                doc.text(nombres,pos.textx + cellWidthSexo, cell.y - 2);

                                if (!!grupoFamiliar[row] && data.row.index < rows - 1){
                                    //doc.setDrawColor(0, 0, 0);
                                    doc.rect(cell.x, cell.y - 5, data.table.width, cell.height );
                                }
                            }

                            return false;
                        }

                        if (data.column.dataKey === 'column2') {

                            if (data.row.index === 1) {
                                doc.setFontStyle('bold');
                                doc.setFontSize(8);
                                doc.text('Edad',pos.textx + cellWidthSexo + cellWidth2 + cellWidthImg + 2, cell.y);

                                doc.setFontStyle('bold');
                                doc.setFontSize(8);
                                doc.text('Es mi',pos.textx + cellWidthSexo + cellWidth2 + cellWidthImg + cellWidth4 - 2, cell.y);
                            }

                            if (data.row.index >= 2) {
                                let row = data.row.index - 2;
                                var edad = doc.splitTextToSize(!!grupoFamiliar[row] ? grupoFamiliar[row].Edad : '', cellWidth4 + 2);
                                doc.setFontSize(8);
                                doc.text(edad,pos.textx + cellWidthSexo + cellWidth2 + cellWidthImg + 2, cell.y - 2);

                                if (!!grupoFamiliar[row] && grupoFamiliar[row].CodJefeHogar === grupoFamiliar[row].CodPersona){
                                    images.push({
                                        url: imgJefeHogar,
                                        x: pos.textx + cellWidthSexo + cellWidth2 ,
                                        y: cell.textPos.y,
                                        cellx: cell.x,
                                        celly: cell.y - 4,
                                    });
                                }

                                var parentesco = doc.splitTextToSize(!!grupoFamiliar[row] ? grupoFamiliar[row].ParentescoInverso.replace('<em>','').replace('</em>','') : '', cellWidth4);
                                doc.setFontSize(8);
                                doc.text(parentesco,pos.textx + cellWidthSexo + cellWidth2 + cellWidthImg + cellWidth4 - 2, cell.y - 2);
                            }

                            return false;
                        }
                    },
                    addPageContent: function() {
                        for (var i = 0; i < images.length; i++) {
                            if (images[i].url !== undefined){
                                doc.addImage(images[i].url,'JPG', images[i].x + 1, images[i].celly, 3, 3);
                            }
                        }
                    }
                });

                previous = doc.autoTable.previous;
                this.CountFicha++;
                resolve(doc);
                    
            } else {
                resolve(doc);
            }
        });
    }

    private generaAntropometriaTodas(doc: any){
        return new Promise((resolve, reject) => {
            if (!!this.fichaPService.antropometria && this.fichaPService.antropometria.length > 0 && this.fichaPService.listTarjetas[16].Checked){
                let mThis = this;
                let previous = doc.autoTable.previous;

                let listV = this.fichaPService.listTarjetas.filter((v:any) => v.Checked === true);

                if (mThis.first.autoTable !== undefined) {
                    this.CountFicha = 0;
                    mThis.first = mThis.first.autoTable.previous;
                }

                let startY = mThis.first.finalY + 5;
                let margin: any = {right: 107};

                let headAntrop: any=[];
                headAntrop.push({
                    Fecha: 'Antropometría'
                });

                doc.autoTable(this.getColumnsHVisitas(), headAntrop, {
                    startY: previous.finalY + 5,
                    // tableLineColor: [189, 195, 199],
                    // tableLineWidth: 0.75,
                    showHeader: 'never',
                    halign: 'center',
                    styles: {
                        // font: 'courier',
                        // lineColor: [44, 62, 80],
                        // lineWidth: 0.75
                        halign: 'left'
                    },     
                    bodyStyles: {
                        fillColor: [255, 255, 255],
                    },
                    alternateRowStyles: {
                        fillColor: [255, 255, 255]
                    },       
                });

                previous = doc.autoTable.previous;
                this.first = doc.autoTable.previous;

                startY = mThis.first.finalY + 2.5;
                margin = {right: 107};

                this.CountFicha = 0;

                let checkBlack = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAA4UlEQVRIS+2W0Q2CMBCG7ygDMEpNy7tM4Ag6gk5gnEBHcAUn0HdK7CYwAHDmiCASX7BIfGgfm/b/8v+55H4EAJBSRmEY7oloAwAR301wCkQ8l2V5sNYWyBAhxJV5E4h/krBVVSWotT4S0ZZfIOIJAC7GmJsLVGu9BIBVXxeVUjnHxRBjzM4FMPzbM1EwiJ5uElcnQ1Acx7Ku63uj34KyLMMp3bRanREPGhuvj25sYt17H52Pzg/D1zPwB9EFQbBI09S6e3gp8EonIu4jzeKbZ5X/opzwCiei9Vs5ma1uzVUgH8q9xnET1/juAAAAAElFTkSuQmCC';
                let checkRed = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAA40lEQVRIie2UMQrCMBSGP0Xo6uCgo5NHcHfSI4i7B/AYPYC7JxCcRBSv4Cnagji6+ruk8ITGJuKkfRBo0y//92jTQFP/WYJEkApygSrG2bBTwUkwixGknuAXgWAouLu5Y4yg7HzyhmkJ9ka6ihFIoBpmYcIvgs7HAkFfsBQM3H1PcHXcQzAODvcItm4uE4wEG9P9OircI7Af/WauC0H3G4JEsKvYTXP3/Gy3brTAIzkIWj4+WmAkqet4WMdHCz7h2541hVvo/dFMeMnkoQ2FHBVVI40RlO86CwjOHJsEC5r6rXoCCNLsTuA7niUAAAAASUVORK5CYII=';
                
                this.fichaPService.antropometria.forEach((a:any,i: any) => {

                    let rows = 7;
                    let first = doc.autoTable.previous;

                    let previous = doc.autoTable.previous;  

                    let columns: any;
                    let data: any;

                    let width = 88.8908362253333;
                    let cellWidth = 60.593232297333316;//29.63027874177777;
                    let pos = {x:0,textx:0};

                    //Antropometria todas

                    startY = mThis.first.finalY + 5;
                    let margin: any = {right: 107};

                    if (!!a) {

                        rows = 9;

                        // if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        //     // Reset page to the same as before previous table
                        //     doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        //     rows = previous.rows.length > rows ? previous.rows.length : rows;
                        //     startY = previous.pageStartY;
                        //     margin = {left: 107};
                        // } else if (this.CountFicha > 0){
                        //     startY = previous.finalY + 2.5;
                        //     margin = {right: 107};
                        // }

                        startY = previous.finalY + 2.5;

                        //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                        columns = [
                            {title: "columna1", dataKey: "column1"},
                            {title: "columna2", dataKey: "column2"},
                            {title: "columna3", dataKey: "column3"},
                            {title: "columna4", dataKey: "column4"},
                            {title: "columna5", dataKey: "column5"},
                            {title: "columna6", dataKey: "column6"},
                            {title: "columna7", dataKey: "column7"},
                        ];
            
                        data = mThis.generaTemplate7(rows);

                        doc.autoTable(columns, data, {
                            startY: startY,
                            showHeader: 'never',
                            pageBreak:'avoid',
                            //margin: margin,
                            styles: {
                                halign: 'left',
                            },
                            drawCell: function (cell: any, data: any) {
                                // Rowspan
                                if (data.column.dataKey === 'column1') {
                                    if (data.row.index === 0) {
                                        doc.setFontStyle('bold');

                                        doc.text('Antropometría ' + a.Fecha,cell.textPos.x, cell.textPos.y + 2);

                                        doc.setDrawColor(0, 0, 0);
                                        doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);

                                        pos.textx = cell.textPos.x;
                                        pos.x = cell.x;
                                        //cellWidth = data.table.width / 3;
                                    }

                                    if (data.row.index === 1) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Fecha',cell.textPos.x, cell.textPos.y);
                                    }

                                    if (data.row.index === 2) {
                                        var fecha = doc.splitTextToSize(!!a.Fecha ? a.Fecha : '-', (data.table.width / 7) + 4);
                                        doc.setFontSize(8);
                                        doc.text(fecha,cell.textPos.x, cell.y - 2);
                                    }

                                    if (data.row.index === 3) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Peso por edad',cell.textPos.x, cell.y);
                                    }

                                    if (data.row.index === 4) {
                                        var pesoEdad = doc.splitTextToSize(!!a.ZPxE ? a.ZPxE : '-', (data.table.width / 3) + 4);
                                        doc.setFontSize(8);
                                        doc.text(pesoEdad,cell.textPos.x, cell.y - 4);
                                    }

                                    if (data.row.index === 5) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Score Z-2 ',cell.textPos.x, cell.y);
                                    }

                                    if (data.row.index === 6) {
                                        var scoreZ2 = doc.splitTextToSize(!!a.EstadoSALUDx5 ? a.EstadoSALUDx5 : '-', (data.table.width / 3) + 4);
                                        doc.setFontSize(8);
                                        doc.text(scoreZ2,cell.textPos.x, cell.y - 4);
                                    }

                                    if (data.row.index === 7) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Forma Medición ',cell.textPos.x, cell.y);
                                    }

                                    if (data.row.index === 8) {
                                        var formaM = doc.splitTextToSize(!!a.FormaMedicion ? a.FormaMedicion : '-', (data.table.width / 3) + 4);
                                        doc.setFontSize(8);
                                        doc.text(formaM,cell.textPos.x, cell.y - 4);
                                    }

                                    return false;
                                }

                                if (data.column.dataKey === 'column2') {

                                    if (data.row.index === 1) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Edad',cell.textPos.x, cell.textPos.y);
                                    }

                                    if (data.row.index === 2) {
                                        var edad = doc.splitTextToSize(!!a.Edad ? a.Edad : '-', (data.table.width / 7) + 4);
                                        doc.setFontSize(8);
                                        doc.text(edad,cell.textPos.x, cell.y - 2);
                                    }

                                    if (data.row.index === 3) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Talla por Edad',pos.textx + cellWidth, cell.y);
                                    }

                                    if (data.row.index === 4) {
                                        var talla = doc.splitTextToSize(!!a.ZTxE ? a.ZTxE : '-', (data.table.width / 3) + 4);
                                        doc.setFontSize(8);
                                        doc.text(talla,pos.textx + cellWidth, cell.y - 4);
                                    }

                                    if (data.row.index === 5) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Score Z-1.5',pos.textx + cellWidth, cell.y);
                                    }

                                    if (data.row.index === 6) {
                                        var scoreZ1 = doc.splitTextToSize(!!a.EstadoUNICEF ? a.EstadoUNICEF : '-', (data.table.width / 3) + 4);
                                        doc.setFontSize(8);
                                        doc.text(scoreZ1,pos.textx + cellWidth, cell.y - 4);
                                    }
                                    
                                    if (data.row.index === 7) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Posible Edema',pos.textx + cellWidth, cell.y);
                                    }

                                    if (data.row.index === 8) {
                                        var endema = doc.splitTextToSize(!!a.PosibleEdema ? a.PosibleEdema : '-', (data.table.width / 3) + 4);
                                        doc.setFontSize(8);
                                        doc.text(endema,pos.textx + cellWidth, cell.y - 4);
                                    }

                                    return false;
                                }

                                if (data.column.dataKey === 'column3') {

                                    if (data.row.index === 1) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Peso KG',cell.textPos.x - 1, cell.textPos.y);
                                    }

                                    if (data.row.index === 2) {
                                        var peso = doc.splitTextToSize(!!a.Peso ? a.Peso : '-', (data.table.width / 7) + 4);
                                        doc.setFontSize(8);
                                        doc.text(peso,cell.textPos.x, cell.y - 2);
                                    }

                                    if (data.row.index === 3) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('IMC por Edad',pos.textx + (cellWidth * 2), cell.y);
                                    }

                                    if (data.row.index === 4) {
                                        var imce = doc.splitTextToSize(!!a.ZIMCxE ? a.ZIMCxE : '-', (data.table.width / 3) + 4);
                                        doc.setFontSize(8);
                                        doc.text(imce,pos.textx + (cellWidth * 2), cell.y - 4);
                                    }

                                    if (data.row.index === 5) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Score Z-1',pos.textx + (cellWidth * 2), cell.y);
                                    }

                                    if (data.row.index === 6) {
                                        var conin = doc.splitTextToSize(!!a.EstadoCONIN ? a.EstadoCONIN : '-', (data.table.width / 3) + 4);
                                        doc.setFontSize(8);
                                        doc.text(conin,pos.textx + (cellWidth * 2), cell.y - 4);
                                    }
                                    
                                    if (data.row.index === 7) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Prematuro',pos.textx + (cellWidth * 2), cell.y);
                                    }

                                    if (data.row.index === 8) {
                                        var prematuro = doc.splitTextToSize(!!a.Prematuro ? a.Prematuro : '-', (data.table.width / 3) + 4);
                                        doc.setFontSize(8);
                                        doc.text(prematuro,pos.textx + (cellWidth * 2), cell.y - 4);
                                    }

                                    return false;
                                }
                                if (data.column.dataKey === 'column4') {

                                    if (data.row.index === 1) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Talla cm',cell.textPos.x, cell.textPos.y);
                                    }

                                    if (data.row.index === 2) {
                                        var talla = doc.splitTextToSize(!!a.Talla ? a.Talla : '-', (data.table.width / 7) + 4);
                                        doc.setFontSize(8);
                                        doc.text(talla,cell.textPos.x, cell.y - 2);
                                    }

                                    return false;
                                }

                                if (data.column.dataKey === 'column5') {

                                    if (data.row.index === 1) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('P.C. cm',cell.textPos.x + 1, cell.textPos.y);
                                    }

                                    if (data.row.index === 2) {
                                        var pc = doc.splitTextToSize(!!a.PC ? a.PC : '-', (data.table.width / 7) + 4);
                                        doc.setFontSize(8);
                                        doc.text(pc,cell.textPos.x + 1, cell.y - 2);
                                    }

                                    return false;
                                }

                                if (data.column.dataKey === 'column6') {

                                    if (data.row.index === 1) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('IMC',cell.textPos.x + 2, cell.textPos.y);
                                    }

                                    if (data.row.index === 2) {
                                        var imc = doc.splitTextToSize(!!a.IMC ? a.IMC : '-', (data.table.width / 7) + 4);
                                        doc.setFontSize(8);
                                        doc.text(imc,cell.textPos.x + 2, cell.y - 2);
                                    }

                                    return false;
                                }

                                if (data.column.dataKey === 'column7') {

                                    if (data.row.index === 1) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Origen',cell.textPos.x, cell.textPos.y);
                                    }

                                    if (data.row.index === 2) {
                                        var origen = doc.splitTextToSize(!!a.Origen ? a.Origen : '-', (data.table.width / 7) + 4);
                                        doc.setFontSize(8);
                                        doc.text(origen,cell.textPos.x, cell.y - 2);
                                    }

                                    return false;
                                }
                            },
                        });

                        previous = doc.autoTable.previous;
                        this.CountFicha++;
                    }

                    if (i === this.fichaPService.antropometria.length - 1){
                        resolve(doc);
                    }
                    
                });
            } else{
                resolve(doc);
            }
        });
    }

    private generaGraficoPE(doc:any){
        return new Promise((resolve, reject) => {
            let mThis = this;
            let rows = 8;
            let first = doc.autoTable.previous;
            let previous = doc.autoTable.previous;
            let columns;
            let data;

            if (mThis.first.autoTable !== undefined) {
                this.CountFicha = 0;
                mThis.first = mThis.first.autoTable.previous;
            }
            let startY = mThis.first.finalY + 5;
            let margin: any = {right: 107};

            //Grafico PE
            if (!!graficoPECanvas && this.fichaPService.listTarjetas[17].Checked) {
                var img: any = [];

                rows = 8;

                // if (previous.pageStartX !== 107 && this.CountFicha > 0){
                //     // Reset page to the same as before previous table
                //     doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                //     rows = previous.rows.length > rows ? previous.rows.length : rows;
                //     startY = previous.pageStartY;
                //     margin = {left: 107};
                // } else if (this.CountFicha > 0){
                    startY = previous.finalY + 2.5;
                    margin = {right: 107};
                //}

                columns = [
                    {title: "columnaI", dataKey: "column1"},
                    {title: "columnad", dataKey: "column2"},
                ];
                data = mThis.generaTemplate(rows);

                doc.autoTable(columns, data, {
                    startY: startY,
                    showHeader: 'never',
                    pageBreak:'avoid',
                    margin: margin,//en caso de que quiera q empiece en la siguiente fila margin: this.CountFicha > 0 ? previous.pageStartX === 107 ? {right: 107} : {left: 107} : {right: 107},
                    styles: {
                        halign: 'left',
                    },
                    drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {
                                if (data.row.index === 0) {
                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                    // doc.autoTableText(data.row.index / 5 + 1 + '-', cell.x + cell.width / 2, cell.y + cell.height * 5 / 2, {
                                    //     halign: 'center',
                                    //     valign: 'middle'
                                    // });
                                    img.push({
                                        url: graficoPECanvas.canvas.toDataURL("image/jpg"),
                                        x: cell.textPos.x,
                                        y: cell.textPos.y,
                                        width: data.table.width,
                                        height: data.table.height
                                    });
                                }
                            }
                            return false;
                    },
                    addPageContent: function() {
                        if (img[0].url !== undefined){
                            doc.addImage(img[0].url,'JPG', img[0].x, img[0].y - 1, img[0].width - 3, img[0].height - 12);//Width, heith
                        }
                    }
                });

                previous = doc.autoTable.previous;
                this.CountFicha++;
                resolve(doc);
            } else{
                resolve(doc);
            }
        });
    }

    private generaGraficoTE(doc:any){
        return new Promise((resolve, reject) => {
            let mThis = this;
            let rows = 8;
            let first = doc.autoTable.previous;
            let previous = doc.autoTable.previous;
            let columns;
            let data;

            if (mThis.first.autoTable !== undefined) {
                this.CountFicha = 0;
                mThis.first = mThis.first.autoTable.previous;
            }

            let startY = mThis.first.finalY + 5;
            let margin: any = {right: 107};

            //Grafico TE
            if (!!graficoTECanvas && this.fichaPService.listTarjetas[17].Checked) {
                var img: any = [];

                rows = 8;

                if (previous.pageStartX !== 107 && this.CountFicha > 0){
                    // Reset page to the same as before previous table
                    doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                    rows = previous.rows.length > rows ? previous.rows.length : rows;
                    startY = previous.pageStartY;
                    margin = {left: 107};
                } else if (this.CountFicha > 0){
                    startY = previous.finalY + 2.5;
                    margin = {right: 107};
                }

                columns = [
                    {title: "columnaI", dataKey: "column1"},
                    {title: "columnad", dataKey: "column2"},
                ];
                data = mThis.generaTemplate(rows);

                doc.autoTable(columns, data, {
                    startY: startY,
                    showHeader: 'never',
                    pageBreak:'avoid',
                    margin: margin,//en caso de que quiera q empiece en la siguiente fila margin: this.CountFicha > 0 ? previous.pageStartX === 107 ? {right: 107} : {left: 107} : {right: 107},
                    styles: {
                        halign: 'left',
                    },
                    drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {
                                if (data.row.index === 0) {
                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                    // doc.autoTableText(data.row.index / 5 + 1 + '-', cell.x + cell.width / 2, cell.y + cell.height * 5 / 2, {
                                    //     halign: 'center',
                                    //     valign: 'middle'
                                    // });
                                    img.push({
                                        url: graficoTECanvas.canvas.toDataURL("image/jpg"),
                                        x: cell.textPos.x,
                                        y: cell.textPos.y,
                                        width: data.table.width,
                                        height: data.table.height
                                    });
                                }
                            }
                            return false;
                    },
                    addPageContent: function() {
                        if (img[0].url !== undefined){
                            doc.addImage(img[0].url,'JPG', img[0].x, img[0].y - 1, img[0].width - 3, img[0].height - 12);//Width, heith
                        }
                    }
                });

                previous = doc.autoTable.previous;
                this.CountFicha++;
                resolve(doc);
            } else{
                resolve(doc);
            }
        });
    }

    private generaGraficoIMC(doc:any){
        return new Promise((resolve, reject) => {
            let mThis = this;
            let rows = 8;
            let first = doc.autoTable.previous;
            let previous = doc.autoTable.previous;
            let columns;
            let data;

            if (mThis.first.autoTable !== undefined) {
                this.CountFicha = 0;
                mThis.first = mThis.first.autoTable.previous;
            }

            let startY = mThis.first.finalY + 5;
            let margin: any = {right: 107};

            //Grafico IMC
            if (!!graficoIMCCanvas && this.fichaPService.listTarjetas[17].Checked) {
                var img: any = [];

                rows = 8;

                if (previous.pageStartX !== 107 && this.CountFicha > 0){
                    // Reset page to the same as before previous table
                    doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                    rows = previous.rows.length > rows ? previous.rows.length : rows;
                    startY = previous.pageStartY;
                    margin = {left: 107};
                } else if (this.CountFicha > 0){
                    startY = previous.finalY + 2.5;
                    margin = {right: 107};
                }

                columns = [
                    {title: "columnaI", dataKey: "column1"},
                    {title: "columnad", dataKey: "column2"},
                ];
                data = mThis.generaTemplate(rows);

                doc.autoTable(columns, data, {
                    startY: startY,
                    showHeader: 'never',
                    pageBreak:'avoid',
                    margin: margin,//en caso de que quiera q empiece en la siguiente fila margin: this.CountFicha > 0 ? previous.pageStartX === 107 ? {right: 107} : {left: 107} : {right: 107},
                    styles: {
                        halign: 'left',
                    },
                    drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {
                                if (data.row.index === 0) {
                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                    // doc.autoTableText(data.row.index / 5 + 1 + '-', cell.x + cell.width / 2, cell.y + cell.height * 5 / 2, {
                                    //     halign: 'center',
                                    //     valign: 'middle'
                                    // });
                                    img.push({
                                        url: graficoIMCCanvas.canvas.toDataURL("image/jpg"),
                                        x: cell.textPos.x,
                                        y: cell.textPos.y,
                                        width: data.table.width,
                                        height: data.table.height
                                    });
                                }
                            }
                            return false;
                    },
                    addPageContent: function() {
                        if (img[0].url !== undefined){
                            doc.addImage(img[0].url,'JPG', img[0].x, img[0].y - 1, img[0].width - 3, img[0].height - 12);//Width, heith
                        }
                    }
                });

                previous = doc.autoTable.previous;
                this.CountFicha++;
                 resolve(doc);
            } else{
                 resolve(doc);
            }
        });
    }

    private generaHeaderVisita(doc:any, Fecha:string, Origen:any, NroRelevamiento:any){
        return new Promise((resolve, reject) => {
            let mThis = this;
            Origen = Origen ? Origen : '';
            NroRelevamiento = NroRelevamiento ? NroRelevamiento : 'ND';
            let headVisita: any=[];
                headVisita.push({
                    Fecha: 'Fecha de Relevamiento: ' + Fecha,
                    NroRelevamiento: 'Nro. Encuesta: ' + NroRelevamiento,
                    Origen: 'Origen: ' + Origen
                });

                doc.autoTable(this.getColumnsHVisitas(), headVisita, {
                    startY: this.first.finalY + 5,
                    // tableLineColor: [189, 195, 199],
                    // tableLineWidth: 0.75,
                    showHeader: 'never',
                    styles: {
                        // font: 'courier',
                        // lineColor: [44, 62, 80],
                        // lineWidth: 0.75
                        halign: 'center',
                        fontSize: 9,
                    },
                    // headerStyles: {
                    //     fillColor: [44, 62, 80],
                    //     fontSize: 15
                    // },
                    bodyStyles: {
                        fillColor: [52, 73, 94],
                        textColor: 240
                    },
                    alternateRowStyles: {
                        fillColor: [0, 0, 0]
                    },       
                });
                mThis.first = doc.autoTable.previous;
                resolve(doc);
        });
    }

    private generaFotos(doc:any){
        return new Promise((resolve, reject) => {
            let mThis = this;
            let previous = doc.autoTable.previous;

            if (mThis.first.autoTable !== undefined) {
                this.CountFicha = 0;
                mThis.first = mThis.first.autoTable.previous;
            }

            let startY = mThis.first.finalY + 5;
            let margin: any = {right: 107};

            if (this.fichaPService.fotos && this.fichaPService.listTarjetas[18].Checked) {

                let headFotos: any=[];
                headFotos.push({
                    Fecha: 'Galeria de imágenes'
                });

                doc.autoTable(this.getColumnsHVisitas(), headFotos, {
                    startY: previous.finalY + 5,
                    // tableLineColor: [189, 195, 199],
                    // tableLineWidth: 0.75,
                    showHeader: 'never',
                    halign: 'center',
                    styles: {
                        // font: 'courier',
                        // lineColor: [44, 62, 80],
                        // lineWidth: 0.75
                        halign: 'left'
                    },     
                    bodyStyles: {
                        fillColor: [255, 255, 255],
                    },
                    alternateRowStyles: {
                        fillColor: [255, 255, 255]
                    },       
                });

                previous = doc.autoTable.previous;
                this.first = doc.autoTable.previous;

                this.CountFicha = 0;

                this.generaFotosColumn3(doc).then((doc:any) => this.generaFotosColumn2(doc))
                .then((doc:any) => {
                    resolve(doc);
                })

                    
            } else {
                resolve(doc);
            }
        });
    }

    private generaFotosColumn3(doc:any){
        return new Promise((resolve, reject) => {
            let previous = doc.autoTable.previous;
            let mThis = this;
             var columns = [];
            var data: any;

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};

            let rows = 10;
    
            //this.fichaPService.fotos.forEach((array:any, i:any) => {
            let fotosArray = this.fichaPService.fotos.filter((foto:any) => foto.base64 !== undefined && foto.columns === 3 && foto.fotoNoDisp !== true);
            if (fotosArray.length > 0) {
                // Observable.interval(100).take(fotosArray.length).subscribe((i) => {
                //         let array = fotosArray[i];
                var promises :any = [];

                fotosArray.forEach((array:any,i:any) => {


                    promises.push(

                        new Promise((resolve, reject) => { 

                        let fotos: any = [];
                        if (i === 0){
                            startY = previous.finalY + 2.5;
                            margin = {right: 139};
                        } else if (((i + 2) % 3) === 0 && this.CountFicha > 0){//0,3,
                            doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                            rows = previous.rows.length > rows ? previous.rows.length : rows;
                            startY = previous.pageStartY;
                            margin = {left: 76, right: 76};
                        } else if ((i + 1) % 3 === 0 && this.CountFicha > 0){
                            // Reset page to the same as before previous table
                            doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                            rows = previous.rows.length > rows ? previous.rows.length : rows;
                            startY = previous.pageStartY;
                            margin = {left: 139};
                        } else if (i % 3 === 0 && this.CountFicha > 0){
                            startY = previous.finalY + 2.5;
                            margin = {right: 139};
                        }

                            //previous = this.this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                        columns = [
                            {title: "columnaI", dataKey: "column1"},
                            {title: "columnad", dataKey: "column2"},
                        ];
                        data = this.generaTemplate(rows);
                

                        doc.autoTable(columns, data, {
                            startY: startY,
                            showHeader: 'never',
                            pageBreak:'avoid',
                            margin: margin,
                            styles: {
                                halign: 'left',
                            },
                            drawCell: function (cell: any, data: any) {
                                    // Rowspan
                                    if (data.column.dataKey === 'column1') {
                                        if (data.row.index === 0) {
                                            doc.setFontStyle('bold');
                                            doc.setDrawColor(0, 0, 0);
                                            doc.setFontSize(8);
                                            //doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                            doc.autoTableText(array.FotoTitulo, cell.textPos.x + data.table.width / 2, cell.y + data.table.height - 15, {
                                                halign: 'center',
                                                valign: 'middle'
                                            });
                                            // doc.setFontSize(8);
                                            // doc.text(array.FotoTitulo,cell.textPos.x, data.table.height);

                                            fotos.push({
                                                url: array.base64,
                                                x: cell.textPos.x,
                                                y: cell.textPos.y,
                                                width: data.table.width,
                                                height: data.table.height
                                            });
                                        }
                                    }
                                    return false;
                            },
                            addPageContent: function() {
                                try
                                {
                                    if (fotos[0].url !== undefined){
                                        doc.addImage(fotos[0].url,'JPG', fotos[0].x, fotos[0].y - 1, fotos[0].width - 3, fotos[0].height - 20);//Width, heith
                                        mThis.first = doc;
                                        resolve();
                                    }
                                }
                                catch (err)
                                {
                                    doc.addImage(mThis.fotoNoDisp,'JPG', fotos[0].x, fotos[0].y - 1, fotos[0].width - 3, fotos[0].height - 20);//Width, heith
                                    mThis.first = doc;
                                    resolve(); 
                                }
                                // for (var i = 0; i < fotos.length; i++) {
                                //     if (fotos[i].url !== undefined){
                                //         doc.addImage(fotos[i].url,'JPG', fotos[i].x, fotos[i].y - 1,fotos[i].width - 3, fotos[i].height - 12);
                                //     }
                                // }
                            }
                        })

                        previous = doc.autoTable.previous;
                        mThis.CountFicha++;

                        })

                    )
                
                });

                Promise.all(promises).then(() => 
                    resolve(doc)
                );
                } else {
                    resolve(doc)
                }
        });
    }

    private generaFotosColumn2(doc:any){
        return new Promise((resolve, reject) => {
            let previous = doc.autoTable.previous;
            console.log('previous.pageStartX',previous.pageStartX );
            //this.CountFicha = 0;
            let mThis = this;
             var columns = [];
            var data: any;

            let startY = previous.finalY + 2.5;
            let margin: any = {right: 107};

            let rows = 10;
    
            //this.fichaPService.fotos.forEach((array:any, i:any) => {
            let fotosArray = this.fichaPService.fotos.filter((foto:any) => foto.base64 !== undefined && foto.columns === 2 && foto.fotoNoDisp !== true);

            if (fotosArray.length > 0) {
                    // Observable.interval(100).take(fotosArray.length).subscribe((i) => {
                    //     let array = fotosArray[i];
                    var promises :any = [];

                    fotosArray.forEach((array:any,i:any) => {

                        promises.push(

                        new Promise((resolve, reject) => { 

                        var fotos: any = [];
                        if (previous.pageStartX === 14.111139333333332 && this.CountFicha > 0 && i === 0) {
                            doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                            rows = previous.rows.length > rows ? previous.rows.length : rows;
                            startY = previous.pageStartY;
                            margin = {left: 107};
                        } else if (previous.pageStartX !== 107 && this.CountFicha > 0 && i > 0){
                            // Reset page to the same as before previous table
                            doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                            rows = previous.rows.length > rows ? previous.rows.length : rows;
                            startY = previous.pageStartY;
                            margin = {left: 107};
                        } else if (this.CountFicha > 0){
                            startY = previous.finalY + 2.5;
                            margin = {right: 107};
                        }

                        //previous = this.this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                        columns = [
                            {title: "columnaI", dataKey: "column1"},
                            {title: "columnad", dataKey: "column2"},
                        ];
                        data = this.generaTemplate(rows);

                        doc.autoTable(columns, data, {
                            startY: startY,
                            showHeader: 'never',
                            pageBreak:'avoid',
                            margin: margin,
                            styles: {
                                halign: 'left',
                            },
                            drawCell: function (cell: any, data: any) {
                                    // Rowspan
                                    if (data.column.dataKey === 'column1') {
                                        if (data.row.index === 0) {
                                            doc.setFontStyle('bold');
                                            doc.setDrawColor(0, 0, 0);
                                            doc.setFontSize(8);
                                            //doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                            doc.autoTableText(array.FotoTitulo, cell.textPos.x + data.table.width / 2, cell.y + data.table.height - 15, {
                                                halign: 'center',
                                                valign: 'middle'
                                            });
                                            // doc.setFontSize(8);
                                            // doc.text(array.FotoTitulo,cell.textPos.x, data.table.height);

                                            fotos.push({
                                                url: array.base64,
                                                x: cell.textPos.x,
                                                y: cell.textPos.y,
                                                width: data.table.width,
                                                height: data.table.height
                                            });
                                        }
                                    }
                                    return false;
                            },
                            addPageContent: function() {
                                try
                                {
                                if (!!fotos[0] && fotos[0].url !== undefined){
                                    doc.addImage(fotos[0].url,'JPG', fotos[0].x, fotos[0].y - 1, fotos[0].width - 3, fotos[0].height - 20);//Width, heith
                                    mThis.first = doc;
                                    //mThis.CountFicha++;
                                    resolve();
                                }
                                }
                                catch (err)
                                {
                                    doc.addImage(mThis.fotoNoDisp,'JPG', fotos[0].x, fotos[0].y - 1, fotos[0].width - 3, fotos[0].height - 20);//Width, heith
                                    mThis.first = doc;
                                    resolve(); 
                                }
                                // for (var i = 0; i < fotos.length; i++) {
                                //     if (fotos[i].url !== undefined){
                                //         doc.addImage(fotos[i].url,'JPG', fotos[i].x, fotos[i].y - 1,fotos[i].width - 3, fotos[i].height - 12);
                                //     }
                                // }
                            }
                        })

                        previous = doc.autoTable.previous;
                        mThis.CountFicha++;
                        // if (i === fotosArray.length - 1){
                        //     resolve(doc); 
                        // }
                    })
                    );
                });

                Promise.all(promises).then(() => 
                    resolve(doc)
                );

                } else {
                    resolve(doc)
                }
        });
    }

    private getColumnsHVisitas() {
        return [
            {title: "Fecha", dataKey: "Fecha"},
            {title: "NroRelevamiento", dataKey: "NroRelevamiento"},
            {title: "Origen", dataKey: "Origen"},
        ];
    };

    private generaTemplate(rows: number) {
        var data: any = [];
        for (var j = 0; j < rows; j++) {
            data.push({
                column1: 'test',
                column2: 'test',
            });
        }
        return data;
    };

    private generaTemplate3(rows: number) {
        var data: any = [];
        for (var j = 0; j < rows; j++) {
            data.push({
                column1: 'test',
                column2: 'test',
                column3: 'test',
            });
        }
        return data;
    };

    private generaTemplate7(rows: number) {
        var data: any = [];
        for (var j = 0; j < rows; j++) {
            data.push({
                column1: 'test',
                column2: 'test',
                column3: 'test',
                column4: 'test',
                column5: 'test',
                column6: 'test',
                column7: 'test',
            });
        }
        return data;
    };

    private generaDatosPersonales(){
        return new Promise((resolve, reject) => {
            let mThis = this;
            var images: any = [];
            var i = 0;
            
            this.tablaDP().then((doc: any)=>{
                resolve(doc);
            })
        });
    }

    public generaHeaderFooter(doc:any){
        return new Promise((resolve, reject) => {
            var pageCount = doc.internal.getNumberOfPages();
            for(let i = 0; i < pageCount; i++) { 
                doc.setPage(i); 
                var imgData = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASEAAABHCAYAAABIx/HoAAAACXBIWXMAABcSAAAXEgFnn9JSAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAF9FJREFUeNrsXW+ILNlVv08EI0i6owYXw6b7CbJZiZl+HyLxgzv1CEr8oFOPR9Qg0jUGZEFw+km++GGZGvwgfpoaJSTqmqkxmg0sj+lJEBZFplr9oAlheoJkE1Z9NSuRNf7ZnqCJirDW6fmd6dN3blXd6n/TPXMPFP2v6tbtW/f+7u+ce865dz7xdPMt5cQoz7/+5M513v8jH/nFOHtpZ8dZdiTZEb700p+k4vcwe/GzYy07zrOjmf0+cE/OySrJd7omWC7JgMUH2LSy1w5ApgEw8rPfg+y1nx1dgA9LQACU/R5l7wmIIgdITlZBvsM1wbUCTl37TKBzCBBRABEfLIekht/7GgDtZed28Z5+2ybmJMvX7+XEiWNCDoBI1SLVKsTnZvayi58TMBpmQbrUtM9bYEhdqG3nAKkOl59JC2V6jiE5cSDkAMiHenUkvvbF+20ASQxgSfE+BcikOL+F74n9eKSSoVwWT7xvAphCgJMTJw6EbrFEeK0LVallOI+AI4Ux2tN+SwzAduX67HtiPgnKYtYUSQO3EyfXKc4mtHgWVBcqVp9AAkyGQORMO71fASy6DGoQYlKkdh1jFS3JYUhOrj4jUl1JJU5cazgmdNNUMMlImAkdAzA6ULMkAFnbbrJz+8x88NUA5YdQ7zztvsM6CYP2PP97OMXlCZicbEc6unOsO7XPuuu1DoRuEgAR8MTqYhm9m30+U6NldxIyNO9nx1H2u2/DpvIAigcsBuuh+GldY00kh9l5j7Jrojk3wfaU1yeaKtsAELkVPwdCTiwlBNC0AACxYWD21IVh2cSgfDCZhvheXkcsaMzOA7DbBLhJIQAcCMYUqpGNat5yprE9G0kNnxtgek5ugNxxHtP5MiuP6WzADwBCOxhEBAwHYiYn1SLWrgkAEI0KtyJACjX1pQlVrwWwCsAsIqiCJPflNXNggtzHdrL7hDMoz5tzfT1um+w+d9xIcEzoJgj79bTAgHpgQ0ofTGQUxW9rE9yHVC4yRO8BjAZgRx2AURMguLvKqsw8AcjJ4sWtji1G2ON5Q4KFGjkSSvZzMiEASdlSFw6Pctm/hXvuijo4ceKY0E0UeENHWLFSUIf2DcAUaAC0P8NqrAGISHXpw0ZEKmBbO+9IU99oNcq7Qc+CGWALLHAwKZMS/lx0kIqdTlIW1D0uK7nt7M6B0HzEF51VqavG1aERmg3JcwAgqQYOGRHdKzuC7H0KUGQVcSAGBzGz9UUt3U8IBAlYXK8ILDHQQxPjy35jb/TQxg0C4BNi0qgZyorKbF0oo6O1vYJ6bl2OU8ec2M681MnWAC4KHZ4GzR0YOn0BQN6cAEgCUZcDWNHJm6gHrZ61qQ5ikChl9t5epWcQQPVcF6BPdjIKkzlDm7DKWrcAjwTnc1l8cPtuIy4vrwxmPNs4n+pArHQHdToX5fQdE3IyrTQlI8JSOq1wdURQaiAGwCIYhx4zloJR0L1P8VuorgbGriIA+QLUaYB3dK9zgFTEKmsJ6CY4b09nTgKg6HdjOIx4xg2AjW9SvcBCtzF5hbeJETkmNGPROhjTeOp8h5hN6Tue7aIFDvwt4RtEg2IDg5W9g2WnH6woANXBOocARI6fprAXuEOwU6hkrHkATg6dHV11w2dPjRYeTOUEauRmketaANA5wMdbFWDsQGi2api0+SgM7oYGNBGSj1HnbU9wK6LyDyasJg9QCTgNUVclQGoVcxBJm03HYrI4KACPy2dZ5FEOIGI2a7JReQIUy1Qtvk9NW9l06pgTKwlJn0eH7YCm6yznQNDsSel2BytdPVV9mb1Bsz4xgRxvapJNYg8YBF1NvZxWyOZhHcIxgaOgL9o5tTi/i4mgaMDbPKe05PpIWXiKE0gJT/hbE5LimNBsbUG7vCQuBu8RDhrcARiGpybz0+mJVatgUrAUKsl9sAE2ju4AoFitaQgVbhVkXdhxbISe044qCFuZdukc7hGJS53imNAiB0AXqhnPypz7mZbHYwDRpADSEZ2b2AoNoKrBoQ0Yb1uYbUOUxXaSEMfaHNpoZ0EG19QSINIpGOm0qnuzQIVzIORkIuHI+BoGsw+G4WFVjH7rgWVMYgs6MNgUWPWratz2oYoMjeUI8xiqJhggwao1/rIzNrRriLavueHiQGhes29DzG4bUHUONRuEP0HZ58pgaAXDMnlj24CQLG9LzMyhNkicGjE9AAVqfCX0FKqgqW23b1v7OBCancRCJWtrrwwksZosbUZUkD8oBhBVUZ84rYgM49hAHSVInq6QLWMp66k5oxL4dIrsTFUM9zdFnGF6RgJD72nBKR3hV1JJzbOwo0ziV+KpkQ+TBKfalOVeV/svK1hGAtBbLgPAnJjQdz/9dvU9T33/8P3//te31PlX3pjqune+/4dyr/nPN/5Nffufvnn1j7z9u9Q7nnnX8P2bX/u6+r9v/s8iZzrW9T10Oqn3nwOAYi2/NNlhbJwCZcjFAPfssO8KsiwmWHJvWoIPMTZaxQtR/0iNr9YdgX0lUCU8XtlbcmG7HP2nxOLZUXsFaNtwTnVaE2quk3mB0N0P3VfPvXAB+P/9H/+i/vi5dSsQ+MCv/5p65uFHL7j0nz9Wr3z0Y8P3Dx//ReF1dI+vHn5anX7q05eARADE1z1++EH1r1/8x0W24zo6vYcgUXqfYJDXRQdsabN3WAHoaLAE8N8ZAoeIxr6SFC2nnLoaGZ25Lk2AZB9lUyqREJ9jqGu9FenPCeobWA56T422V5o5SGjGcptA2dZtBKGZq2Nv+94fUO9aL29LYkEMQJPco5UB1s9//s9U7UeeWpa2XBOz7zDVA5bCSU3YMoDQVlmnE+EEw904YPvxhEoXqVHS+qbmtW2SUDAxmWjtBHFPvPpWx73atgNoyVSfRkkoBgufM6/4Pbma2bSYIGIHQjOSe8HzVuypTP7yNzvqk+++O3a8+N73DL9nMPrp3/29625DaYvgOKQaOt2hGkVNK3XVC7as09URVd3EANuFnaaGZf81ARjdIrsIAG+rYObdAvOR9iLTYFpmu1BfjTaUjIpAHv91XQOvWdeHwJvthGFeGAwmj0TNxzfrdqhjujz1fm/IUIpsQ+/7hV+eqGxS877yBxf9jFTA+g+/r9CGtICOT45+p6IDsTF3S1MTjOxJ2ncMZbPKdSIGV0MrPwTQlaG6CfDqGsC0BUOoVQDLZZJADOgTJHKL8T+bYH6BAKBNi5iuaSTEZETPLcXkkYj290W7y37UvC0gNHMmRPYakmc//DD3nB+8/94heJB87fEfTnSfJ68cX75/Z+tHr7sdO5palgcAppk51NUoeFf7UOdYFdrIuTeBxQE6OBm/U33GLVrCRxiIvumiTK5/tEohB2IFsieAlTrLmwDzfQDQOQAonnN9qH03xbPaRn2OAU4MQHu0eiaeRcuB0ITyhY//1vD1PQ9+abhiZZJnfvbCFeWNLybq24M3J7qPaYVs0YKdOiMsu27mnHYglmXrOSCiM6EuOmGk7Lyr6ZwnKL+l5bxpqnKja54D5TADJJKeBVM01Q6OZAbNHqOsuAiIkHWRY+N62vFIXSR2iwtUbK6zjSRFdcJ97qqLFVFTXe5SqhDBnHZWRQVeSnWMGMpzL4wM1Gef/9ux36VB+iT+pHrW//BE9yE2xTL4+3+4rvarq1GengAdzYft5nJHDQICsIlUmQNXN2RKVYBICOreV3bb/tAMGhicGm1yFlHd72EAeADBCCtkEVS/nUkbaZbL31WYC8A/meAeqaqwWmZzH971ZJb/zzGhAobCKpbJQM0GaVLbvt6bDOzJ3vRTuy9elvONL712Xe03ECpYotlYuMPtC6ZRpNZc+v2wWgUVwnbfMTrvTeyh7qEsv0CNk0Ig08HurxxOkMIovqWcOFklJjS083yuO2Q7JgM1G6TJz6fMl4jOffcHfmLsu7fVv29YLsvxC1sLc0w0zFoy/0sNQOQJnT4qUMN06QsWxP5AvrKPJdrD/RPBhhI1yl9sVDuEL0sbwCfVnjW9fk6cLD0TIvnn479Tg9e+PHwvDdTSIP3qy4/LdZ3s3OZPPhw7GICo/M+1f+aKuncNcqTZd6RuXzPYGkxiClBNVbVodgKspm4bKVIrDAbnDsrwDAwqccPFycqAEMmXP/up4as0UEuDtE1oB3lRk0+QPAh4XvrQj6vPfnBjCHZLILpRuS3UI11tywOhUAIC2AnbGTYt6nCOsqmBE7nahuX/0xwbkp60q4Z663arA5utcZw4WRp1jEQ3UH+j/+qYQdpGXv+bv7r0CVpWQXzVUYntpVug0pwa/IRowPvwQYoEaDQM4MNBp0HBUjoxqhOT+gcpShU7l5AGJ07mzoR0A7U0SC+BCjVrCVR+BP2eph6dGq7Vga0v8jzT7w+EfUf36bkLEClKUdqX9ZAgBNbUUePR9BKAPJea1MlKghAJGahJyI7zY7/6GxfT/h99/Ca2Yx8gQKrTAYDmHJ9DNb4vvFR/9kq8demaJpbum7hHjN8egAF58InplsSOhRqAdUV96DoPYCY35hume8WeWE6crB4ISQM1qWUkX3356Ca2I61+7YNRRGpklO7ic0MwnkSoV2GJqhcLWwwHONL1PeGJ64tz04KyBmpk/D4XUfMNlJtCDezjPLrfMdTMxA0VJysJQiRfevF3Lt+ToXkZPJ3nIDxI12B7aQpVqy1YDbvxc46hKsbeCPvJJ2q0nB+rCnFduPeRuIbrWVMjR8W+Gt/2+FJ1c+JkVpJNgG/xMff0rk/+9K+V+u2L9692X76pbco7mkrVh4BmN+d8AoFD4WNUJMMdKjRP2kgDFgUfH1vHxnsaCDFgsouBdC3oTbMyhnAPvk9axSNYUwOTeWYlFMnpBgB8x/4WJDMBIVrBylvFIkdCSsGRJxeJzD429l3R+XlCScwmuW4WgqyJobqaq0fKQAMRW0/kbWwVlIr7pYbBagtAPWGHkgynllP3cMrmIRDilbeeqhaRv53DOOchsWjDprpFAaQ3Xh27RRKUsSVOPAYQOaiiihXM4HVVLRd0KGb+vjKvirEc3FJGUHfd2YHQyklJJH1P2G/iCRjGRsG+WjYBqrIeCWLKjjHj5wHYkVqhRPczEPqvtFhwqlZw37Vbr445GVPLEgBMEzMqGat92EbWwUKaE+ygGistLAPA1K440CRrI9uHh3I4eJXUxu5ti+aGba3rerEDoZvQmVMe5FCVUgx+yWTofYxE+L6yS+vZMGRhrMKmdoQtiI3o61imJyDbvI1pJJw4ELoxAsCRM+lAjfyEOmo8bkwyGgKsxFKlCmGkHkhmZSFHBTl9OLMi70cv6xYvKzChvVuaOsy/+WrcsJyC3Q0KymupkS1owICt7RuvbGxkmuqc5vlv4T/ogcdDF4mqdTWUFRfcV28fhfbpV3wGvPNLvUr9HQjNjwENsOQugWEDA7ymDGkxsKoVqdE+WWVAVFMjD2ZbFjS0cSDFKzs/yvQeDdTxWLvuXC23k2JLq/Md/Mcwpx0jMMk8UJV7r/UEc23K+2RlPGC3iAIAkvV6oE1ASmwDlbdCeo6MnaFNXTF5mGyDoXbfUF11wWChVVjqB52i/ydAjJ1w887ZE9kiC8UZpmcrgbq62qQ/KApY5RALsgeFmIFM15pkq6wDaADkYcDuSltQSR2V0iL7V4CJJviPtQIA38cAqjK5JGo83KXsevn7mT6gAUCJKnbRqAEUbFgoPdv9sgkMZW2XnEf94BBgXlTOoUX/20JSPAdC12AP8grA5FzMsL54WHV0Vk9dDVA1iY0xmla3PE6SJu+JGbYofmYvbweQJRZmBtR+O2AgO+rqxo2T/K/uhCBkYhSJxoqpfrSqShHej7Tn37aI25OgcqZGuatTARwdrc+c4V730U66u8iuaTUWAN7W+jOXY6r/mk3coVPHZg9EHJMVCr2bZoQYNJ1Vhro2myXiWtsE93lAF2og4suBinuQqsjZGDmZ2TAQd4V9g3oI5h0Dgez/So92MvC3Kto/pHNpTeYDN9hIJEOIDeqQBCB9MSAB05BA1YFqNih55n7Bc+to57a08rpgLbvaNYmB6Y+xMI0tc/1lXvSgzHTgQGh+jCgwdNJHmGXSnA7MEfMBHmZH2eWI5s4VAUQG7MQI1sOz5anY6fNU6Ow3YWn6tICl6O3oqQrxcIa95fycNgs0tbuvqWESDA5M9ik8Oyr/iWA6QQmD80pAdQwYTYCGPe464lzPoEbKNtwxqeuofwgVkUG/XgSiDoTma6fw1SginZfrz/GAHolTea8sAqh7yCeUqFGWRF+NViHWBej01Wg1omug/QMxQNfwmWfZM9hRWiiDVnGCFW7uMK+jA0TkV5N4RMeCKfgWqlhs+E2qTmEJ6MlEeV4BCPUsWJ1ciCgKRwl08JHgkr3csWyr1GC3ShwIXQ8j6mobDzYMnfAAapgvqL+nsaqoii0Dy/drGhPoqvGVu4aoT0utvnf0vNPPShC6opIZVDF9UvA0lpSW3K8vQKg5Zd27Qr1f11ZKZX9N1DWsiDoQmr/4OTaemlChpKyX0VfLe5KciQ6dKrNT5BnsCS5dR/GEMtDYia6SBRo70UFGAkkTLLRI5PnT7lEfa0xsF6z7CH2jko+Q8Eliv6Q1B0JL3nk1G48n1CV6mCewFVGH3tbpK9SxBLMsr3qkoqM2mckIGwOrG5zRMcZ5e+g4XFY3z0bgJJdRbJhsJtrnuKScmrJ3NJ1FH+S96LoaW9vAQe4A56h3VMTSwNhjZR+v6EBoicAoEcAivVvZh+cIrKSurXLURcdZL+i8dU0nXxc2oMv96pXLGT0tCLFT4OUqGyaKtQJVbBn6X1/kLA8M7IX+0xYmTKOhG2r+vvb1qWDaksW1HQgtqYh4Mn0m4Rn2QKpj6Dy8G8a5ULU8wZzOtNmXbQDSIa6NY7fM69dJoUom7SsB2K00SB9ZMEtrb+I5sPKhfRHAyWEXvhrPhXWi9xH020gDnyAHrLwqIOScFa+nI3gqfy8w6hAptoJuCfvDKToIgU2oxg3JNBho++gOlv8PldnpcZh8f8EAJFmddaIw8d9ZlkVlNDkuBhVUsUrtMMd+SKuhZAsiNZ4A6ZF2SqB91lf3ZmZHdCB0TdQ4O6gjbkIN60FdaqnRrhoeZqQBOvYr2fHvarQ5IXeIV3H+E6h1NOjvo2NtqpEHLXWy1jUEpErwqBnAxXag9pfk2XUFwDe0LAjnBQAvwXgdzGIRzJvUqxBHUPC/IjXuXa77pzXlZDlLld6pY9fboeOcmZOYTij2ow9UvtPiswAwumYserlqIvwFMCH+b76FyhpKBrdkXtxdoerGlrYgufCgDGzW1A5D14opB7xUjc5K+sOYG4fIAqpLmdd54JjQ6tuNmpi1OjgagvVI+Zaw9ww7dQWmsTDWp8+wRfFEwvYgV3GWLY5NDuSajSpmaIetImaC1VRitv2CrJpVJ4FGXnCqAfj1XOa6OtwpqHelkCPHhJYLfJglrGl2HArE/EJ2fAafY8zEr2XH72fHz2EGo+t4uZVmtWWJhOc4JB6w2xiAkaZmse+JBKDTgpQW1wasWhgHqyhJxXbY19qhrkYpd9cFyHlqQidCwyYMHJwaG+4pAfXAwJJkbFkbXuhdlMFe/ZWX7h0TWh4AikHXuWMPo6uzTsSpHz6DB9zBykoP5/4KOsA90XFqmI36JbuyLpINeWo8u0ADnfpYHFs6AKmcMIIlY0NlqlhROxDY0EIC2fRO8F66YRzMAIR97Z4bJfc81ZkOJjPdeN3GtfsosyYmTiktB0LLD0AtQWEJSO4hGpyjko/xgI+EYTnAwyYgog7iIfbrHWqUUqGmpt+yZ5ZARP/TZgteZn/eEjtS6qATVWgHT11NMWJqg0eziOcTbd+zOP0gr91hvN5UxXmvemBW55pdyqljSy5NAAenbvUNOWDGdoFAkKMn6P0uxwShnCgnjed1AhGBpS+CcpuifrwFtW16UDrvvva5SOS5aY6qVM+xfxj/CwUb8zVV1F4GIrHwINO1Jqo8HW2luor6eaLteSWW24PDN1IL9a6LvijLSJQI/0DfLKrj5fO484mnm285DDDL868/ubPI+xW4xB+onG2jRViG7gF7uuRMwokTB0LLBkIAFZlAi1SXyDK5eoAZcg1UOHBe0U5WQf5fgAEAGOAI5D32VpIAAAAASUVORK5CYII=';
            

                doc.addImage(imgData, 'PNG', 14, 3.5, 38, 8);

                doc.setFontSize(8);
                // doc.setTextColor(40);
                // doc.setFontStyle('normal');
                doc.text('Ficha de Persona de: ', doc.internal.pageSize.width - 14, 7, 'right');
                doc.setFontSize(8);
                doc.text(this.datosPersonales[0].Apellidos + ', ' + this.datosPersonales[0].Nombres, doc.internal.pageSize.width - 14, 11, 'right');

                // FOOTER
                var str = "Página " + doc.internal.getCurrentPageInfo().pageNumber + " de " + pageCount;
                // Total page number plugin only available in jspdf v1.0+
                // if (typeof doc.putTotalPages === 'function') {
                //     str = str + " of " + totalPagesExp;
                // }
                doc.setFontSize(8);
                doc.text(str, doc.internal.pageSize.width - 10, doc.internal.pageSize.height - 5, 'right');

                if (i === pageCount - 1){
                    resolve(doc);
                }
            }
         });
    }

    private tablaDP(){
        return new Promise((resolve, reject) => {
            let mThis = this;
            var images: any = [];                                                              
            var i = 0;
            let widhtImag = 49.77969689199995;
            let restWidth = 132;
            let cellWidth = 22;
            let widthfoto = mThis.datosPersonales[0].columns === 3 ? 30 : 46;

            if (widthfoto === 30){
                widhtImag = 31.77969689199995;
                restWidth = 150;
                cellWidth = 25;
            }

            var doc = new jsPDF();
                doc.autoTable(this.getColumnsDP(), this.datosPersonales, {
                    // columnStyles: { 
                    //     0: {columnWidth: 49.77969689199995}, 
                    //     1: {columnWidth: 22}, 
                    //     2: {columnWidth: 22} , 
                    //     3: {columnWidth: 22} , 
                    //     4: {columnWidth: 22} , 
                    //     5: {columnWidth: 22}, 
                    //     6: {columnWidth: 22} , 
                    //     7: {columnWidth: 22}
                    // }, // etc }
                    theme: 'plain',
                    startY: 20,
                    showHeader: 'never',
                    styles: {
                        //fontSize: 8,
                        overflow: 'linebreak',
                        //columnWidth: 'number'
                    },
                    // drawHeaderRow: function(row: any, data: any){
                    //     return false
                    // },
                    // drawHeaderCell: function (cell: any, data: any) {
                    //     cell.styles.fontSize= 2;
                    // },
                    drawRow: function (row: any, data: any) {
                        // Colspan
                        /*doc.setFontStyle('bold');
                        doc.setFontSize(10);
                        if (row.index === 0) {
                            doc.setTextColor(200, 0, 0);
                            doc.rect(data.settings.margin.left, row.y, data.table.width, 20, 'S');
                            doc.autoTableText("Priority Group", data.settings.margin.left + data.table.width / 2, row.y + row.height / 2, {
                                halign: 'center',
                                valign: 'middle'
                            });
                            data.cursor.y += 20;
                        } else if (row.index === 5) {
                            doc.rect(data.settings.margin.left, row.y, data.table.width, 20, 'S');
                            doc.autoTableText("Other Groups", data.settings.margin.left + data.table.width / 2, row.y + row.height / 2, {
                                halign: 'center',
                                valign: 'middle'
                            });
                            data.cursor.y += 20;
                        }

                        if (row.index % 5 === 0) {
                            var posY = row.y + row.height * 6 + data.settings.margin.bottom;
                            if (posY > doc.internal.pageSize.height) {
                                data.addPage();
                            }
                        }*/
                    },
                    drawCell: function (cell: any, data: any) {
                        // Rowspan
                        if (data.column.dataKey === 'Fot') {
                            if (data.row.index === 0) {
                                doc.setDrawColor(0, 0, 0);
                                doc.roundedRect(data.table.cursor.x, data.table.cursor.x, data.table.width, data.table.height,2,2);
                                // doc.autoTableText(data.row.index / 5 + 1 + '-', cell.x + cell.width / 2, cell.y + cell.height * 5 / 2, {
                                //     halign: 'center',
                                //     valign: 'middle'
                                // });
                                images.push({
                                    url: mThis.datosPersonales[0].base64,
                                    x: cell.textPos.x,
                                    y: cell.textPos.y,
                                    cellx: cell.x,
                                    celly: cell.y
                                });
                                i++;
                            }
                            return false;
                        }
                        if (data.column.dataKey === 'FNac') {
                            if (data.row.index === 0) {

                                //doc.rect(images[0].cellx + widhtImag, cell.y, restWidth, cell.height);
                                // doc.autoTableText(mThis.datosPersonales[0].Apellidos+' '+mThis.datosPersonales[0].Nombres, images[0].x + widhtImag, cell.textPos.y + 2, {
                                //     halign: 'left',
                                //     valign: 'middle'
                                // });
                                doc.setFontStyle('bold');

                                doc.text(mThis.datosPersonales[0].Apellidos+' '+mThis.datosPersonales[0].Nombres,images[0].x + widhtImag, cell.textPos.y);
                                return false;
                            }
                            if (data.row.index === 1) {
                                //doc.rect(images[0].cellx + widhtImag, cell.y, restWidth, cell.height);
                                doc.autoTableText(mThis.datosPersonales[0].Edad+' (edad actual)', images[0].x + widhtImag, cell.textPos.y, {
                                    halign: 'left',
                                    valign: 'middle'
                                });

                                return false
                            }
                            if (data.row.index === 2) {
                                //doc.rect(images[0].cellx + widhtImag, cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.setFontStyle('bold');
                                doc.text('F. Nacimiento',images[0].x + widhtImag, cell.textPos.y);

                                return false
                            }
                            if (data.row.index === 3) {
                                //doc.rect(images[0].cellx + widhtImag, cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.text(mThis.datosPersonales[0].FechaNacimiento ? mThis.datosPersonales[0].FechaNacimiento : '',images[0].x + widhtImag, cell.textPos.y);

                                return false
                            }
                        }
                        if (data.column.dataKey === 'Doc') {
                            if (data.row.index < 2) {
                                return false;
                            }
                            if (data.row.index === 2) {
                                //doc.rect(images[0].cellx + widhtImag + cellWidth, cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.setFontStyle('bold');
                                doc.text('Documento',images[0].x + widhtImag + cellWidth, cell.textPos.y);

                                return false
                            }
                            if (data.row.index === 3) {

                                let documento = doc.splitTextToSize(mThis.datosPersonales[0].Documento ? mThis.datosPersonales[0].Documento : '', cellWidth);
                                doc.setFontSize(8);
                                doc.text(documento,images[0].x + widhtImag + cellWidth, cell.textPos.y);


                                return false
                            }
                        }
                        if (data.column.dataKey === 'Sex') {
                            if (data.row.index < 2) {
                                return false;
                            }
                            if (data.row.index === 2) {
                                //doc.rect(images[0].cellx + widhtImag + (cellWidth * 2), cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.setFontStyle('bold');
                                doc.text('Sexo',images[0].x + widhtImag + (cellWidth * 2), cell.textPos.y);

                                return false
                            }
                            if (data.row.index === 3) {
                                //doc.rect(images[0].cellx + widhtImag + (cellWidth * 2), cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.text(mThis.datosPersonales[0].Sexo ? mThis.datosPersonales[0].Sexo : '',images[0].x + widhtImag + (cellWidth * 2), cell.textPos.y);

                                return false
                            }
                        }
                        if (data.column.dataKey === 'Nac') {
                            if (data.row.index < 2) {
                                return false;
                            }
                            if (data.row.index === 2) {
                                //doc.rect(images[0].cellx + widhtImag + (cellWidth * 3), cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.setFontStyle('bold');
                                doc.text('Nacionalidad',images[0].x + widhtImag + (cellWidth * 3) - 2, cell.textPos.y);

                                return false
                            }
                            if (data.row.index === 3) {

                                let nacionalidad = doc.splitTextToSize(mThis.datosPersonales[0].Nacionalidad ? mThis.datosPersonales[0].Nacionalidad : '', cellWidth);
                                doc.setFontSize(8);
                                doc.text(nacionalidad,images[0].x + widhtImag + (cellWidth * 3) - 2, cell.textPos.y);

                                return false
                            }
                        }
                        if (data.column.dataKey === 'Pais') {
                            if (data.row.index < 2) {
                                return false;
                            }
                            if (data.row.index === 2) {
                                //doc.rect(images[0].cellx + widhtImag + (cellWidth * 4), cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.setFontStyle('bold');
                                doc.text('País de Origen',images[0].x + widhtImag + (cellWidth * 4), cell.textPos.y);

                                return false
                            }
                            if (data.row.index === 3) {
                                //doc.rect(images[0].cellx + widhtImag + (cellWidth * 4), cell.y, cellWidth, cell.height);

                                // doc.setFontSize(8);
                                // doc.text(mThis.datosPersonales[0].PaisOrigen,images[0].x + widhtImag + (cellWidth * 4), cell.textPos.y);

                                let pais = doc.splitTextToSize(mThis.datosPersonales[0].PaisOrigen ? mThis.datosPersonales[0].PaisOrigen : '', cellWidth);
                                doc.setFontSize(8);
                                doc.text(pais,images[0].x + widhtImag + (cellWidth * 4), cell.textPos.y);

                                return false
                            }
                        }
                        if (data.column.dataKey === 'Etnia') {
                            if (data.row.index < 2) {
                                return false;
                            }
                            if (data.row.index === 2) {
                                //doc.rect(images[0].cellx + widhtImag + (cellWidth * 5), cell.y, cellWidth, cell.height,'DF');

                                doc.setFontSize(8);
                                doc.setFontStyle('bold');
                                doc.text('Etnia',images[0].x + widhtImag + (cellWidth * 5), cell.textPos.y);

                                return false
                            }
                            if (data.row.index === 3) {
                                //doc.rect(images[0].cellx + widhtImag + (cellWidth * 5), cell.y, cellWidth, cell.height,'DF');

                                var splitTitle = doc.splitTextToSize(mThis.datosPersonales[0].EtniaCod ? mThis.datosPersonales[0].EtniaCod : '-', cellWidth);
                                doc.setFontSize(8);
                                doc.text(splitTitle,images[0].x + widhtImag + (cellWidth * 5), cell.textPos.y);

                                return false
                            }
                        }
                    },
                //    createdCell: function (cell: any, data: any) {
                //         if (cell.dataKey === 'Nacionalidad') {
                //             cell.styles.fontSize= 5;
                //             cell.styles.textColor = [255,0,0];
                //         } 
                //     },
                    addPageContent: function() {
                        // for (var i = 0; i < images.length; i++) {
                        //     if (images[i].url !== undefined){
                        //         doc.addImage(images[i].url,'JPG', images[i].x, images[i].y, 20, 20);
                        //     }
                        // }
                        try
                        {
                            if (images[0].url !== undefined){
                                doc.addImage(images[0].url,'JPG', images[0].x - 1, images[0].celly - 5, widthfoto, 36);
                                mThis.first = doc;
                                resolve(doc);  
                            }
                        }
                        catch (err)
                        {
                            doc.addImage(mThis.fotoDefault,'JPG', images[0].x - 1, images[0].celly - 5, widthfoto, 36);
                            mThis.first = doc;
                            resolve(doc); 
                        }
                    }
                });
                //resolve(doc);  
        });
    }

    private getDatosPersonales(){
        return new Promise((resolve, reject) => {
            if (this.datosPersonales.length === 0) {
                //this.datosPersonales = [];
                this.datosPersonales.push(this.fichaPService.datosPersonales[0]);//Foto
                //this.apiBackEndService.getImagenBase64(this.fichaPService.datosPersonales[0].Foto.replace('https://sprodresources.blob.core.windows.net/multimedia/','')).subscribe((result:any) => {
                    this.loadAllImages(this.datosPersonales[0],'Foto').then(()=>{
                    //this.datosPersonales[0].base64 = !!result.FileBase64 ? 'data:image/jpeg;base64,' + result.FileBase64 : this.fotoDefault;   
                    for (var j = 1; j < 4; j++) {
                        this.datosPersonales.push({
                            Foto: '',
                            FechaNacimiento: '',
                            Documento: '',
                            Sexo: '',
                            Nacionalidad: '',
                            PaisOrigen: '',
                            EtniaCod: ''
                        });
                    }
                        resolve();
                    });
                //})
            } else {
                resolve();
            }
        });
    }

    private getDatosFotos(){
        return new Promise((resolve, reject) => {
            if (!!this.fichaPService.fotos && this.fichaPService.fotos.length > 0){
                //this.fichaPService.fotos.forEach((array:any,i:any) => {
                    //this.apiBackEndService.getImagenBase64(array.FotoMedium.replace('https://sprodresources.blob.core.windows.net/multimedia/','')).subscribe((result:any) => {
                    console.log('this.fichaPService.fotos',this.fichaPService.fotos);
                    this.loadAllImagesFotos(this.fichaPService.fotos,'FotoMedium').then(()=>{
                        // array.base64 = 'data:image/jpeg;base64,' + result.FileBase64;
                        // if (i === this.fichaPService.fotos.length - 1){
                        //     console.log('fotos',this.fichaPService.fotos);
                        //     resolve()
                        // }
                        resolve();
                    })                
                //});
            } else{
                resolve();
            }
        });
    }

    private calculaWidthColumn(array:any, key:string){
        return new Promise((resolve, reject) => {
            let mThis = this;
            var img = new Image();
            if (!!array){
                //array.forEach((a:any,i:any) => {
                Observable.interval(300).take(array.length).subscribe((i) => {
                    let a = array[i];
                    // img.onload = function(){
                    //     console.log('img',img);
                    //     console.log( img.width + ", "+ img.height );
                    //     a.columns = img.width > img.height ? 2 : 3;
                        if (i === array.length - 1){
                            console.log('fotos',mThis.fichaPService.fotos);
                            resolve()
                        }
                        resolve();
                    // };
                    // img.src = a[key]; 
                });
            } else{
                resolve();
            }
        });
    }

    private getColumnsDP() {
        return [
            {title: "Foto", dataKey: "Fot"},
            {title: "FechaNacimiento", dataKey: "FNac"},
            {title: "Documento", dataKey: "Doc"},
            {title: "Sexo", dataKey: "Sex"},
            {title: "Nacionalidad", dataKey: "Nac"},
            {title: "PaisOrigen", dataKey: "Pais"},
            {title: "EtniaCod", dataKey: "Etnia"}
        ];
    };

    // Datos personales
    private getDataDP(datosPersonales: any) {
        var data: any = [];
        data.push(datosPersonales);
        this.cargarImagen(datosPersonales.Foto).then((base64)=>{
            data[0].base64 = base64;
            for (var j = 1; j < 4; j++) {
                data.push({
                    Foto: '',
                    FechaNacimiento: '',
                    Documento: '',
                    Sexo: '',
                    Nacionalidad: '',
                    PaisOrigen: '',
                    EtniaCod: ''
                });
            }
            return data;
        });
    }    
    
}
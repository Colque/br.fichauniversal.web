import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import { ApiBackEndService } from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {
    Component,
    EventEmitter,
    OnInit,
    Output,
    ViewChild
    } from '@angular/core';
import { Md2Autocomplete } from 'Md2';
import { Md2Dialog } from 'Md2';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
//import { BlockTemplateComponent } from './../../../components/block-template/block-template.component';

//import * as jsPDF from 'jspdf';
//require('jspdf-autotable');
//declare var jsPDF: any; // Important 
//import 'jspdf-autotable';
import * as $ from 'jquery';
import * as moment from 'moment';

declare var html2canvas: any;
declare var jsPDF: any;
declare var ByOsoAntro: any;
declare var graficoPECanvas: any;
declare var graficoTECanvas: any;
declare var graficoIMCCanvas: any;

@Component({
    selector: 'descargar-ficha-pColombia',
    templateUrl: './descargarFichaPColombia.component.html',
    styleUrls: ['./descargarFichaPColombia.component.scss']
})
export class DescargarFichaPColombiaComponent implements OnInit {
    @ViewChild('modal', undefined) modal: Md2Dialog;
    @BlockUI() blockUI: NgBlockUI;
    //blockTemplate: BlockTemplateComponent = BlockTemplateComponent;
    public disable: boolean;
    public rubros: any;
    public unidadesMedida: any;
    public item: any;
    public codigoPadre: string;
    aframe: any;
    public url: string;
    public error: boolean = false;

    datosPersonales: any = [];
    visitas: any = [];//{Fecha:'09-04-18'},{Fecha:'18-02-16'},{Fecha:'09-04-18'}];
    tokenMultimedia = '?st=2018-06-21T12%3A58%3A00Z&se=2019-06-22T12%3A58%3A00Z&sp=rl&sv=2017-07-29&sr=c&sig=Mz8f3PBwo1LS0s%2Bs2UEDVM7yoW1FxEaouW1mRXp%2FTPU%3D';
    base64Characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
    private CountFicha : number = 0;
    private first: any;

    //img de checkbox
    private checkBlack = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAA4UlEQVRIS+2W0Q2CMBCG7ygDMEpNy7tM4Ag6gk5gnEBHcAUn0HdK7CYwAHDmiCASX7BIfGgfm/b/8v+55H4EAJBSRmEY7oloAwAR301wCkQ8l2V5sNYWyBAhxJV5E4h/krBVVSWotT4S0ZZfIOIJAC7GmJsLVGu9BIBVXxeVUjnHxRBjzM4FMPzbM1EwiJ5uElcnQ1Acx7Ku63uj34KyLMMp3bRanREPGhuvj25sYt17H52Pzg/D1zPwB9EFQbBI09S6e3gp8EonIu4jzeKbZ5X/opzwCiei9Vs5ma1uzVUgH8q9xnET1/juAAAAAElFTkSuQmCC';
    private checkRed = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAA40lEQVRIie2UMQrCMBSGP0Xo6uCgo5NHcHfSI4i7B/AYPYC7JxCcRBSv4Cnagji6+ruk8ITGJuKkfRBo0y//92jTQFP/WYJEkApygSrG2bBTwUkwixGknuAXgWAouLu5Y4yg7HzyhmkJ9ka6ihFIoBpmYcIvgs7HAkFfsBQM3H1PcHXcQzAODvcItm4uE4wEG9P9OircI7Af/WauC0H3G4JEsKvYTXP3/Gy3brTAIzkIWj4+WmAkqet4WMdHCz7h2541hVvo/dFMeMnkoQ2FHBVVI40RlO86CwjOHJsEC5r6rXoCCNLsTuA7niUAAAAASUVORK5CYII=';

    //foto default
    private fotoDefault = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKcAAACnCAYAAAB0FkzsAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAABSjSURBVHhe7Z3pVxtnlsavtirtCDBeweANO3HcnqSnz5wz58yH+cvnw3zok0y3gx1jbIMNmEUsQkJ7aZ/73FKlaU8mToKAqrfuk5S1IFAtv/cu73IrsrtfHJFK5UNFx48qle+kcKp8K4VT5VspnCrfSuFU+VYKp8q3UjhVvpXCqfKtFE6Vb6VwqnwrhVPlWymcKt9K4VT5VgqnyrdSOFW+lcKp8q0UTpVvpXCqfCuFU+VbKZwq30rhVPlWCqfKt1I4Vb6VwqnyrRROlW+lcKp8K4VT5VspnCrfSuFU+VYKp8q3UjjPqeFoRMQbHke/sKn+uBTO3yHANhwOf36EYlE+hbzFYzGKxaLuxq+j441/iz/rgur9ruq3SYvHfkGASTZ+DgCxjSjCoPWp3+tRvz8Q+HqDPn+Y4eOfRSMxisYjFI/GKR6PkZWI8/uANUKDPv/eYCB/OxKJyKb6ZSmcvyJYOsaH7KQtgNVrDarW63RSqVK706Jeb8BwAtA+uOSzyRtABpzRISVigDNBlp2gXDpLuVyGCoU85bI5cpwOdbtd+Y4YA6/6v1I4P5PrfkcMTIRSySRbugEdHB/TxuYWdRkowRUWT9w2fsO1fIB4/BSeXP7xXPiALSWeeZ/MZdN0f2mR5mav8esRdcaQqiX9ZymcY3nuG3CkUkkGpkcft7bo094+Q9mlTDrlAskxpQfZb5ZYU/68fAexxe1S23Hkb96/u0ALCwvyvXgfDcONVVUKJ8uDMh6Ps6vt0MdPO7T9aZfPToQyDCpcs5cAnVeeNQWAaAAdhjTOIcODxSW6ffs6hw8W9ThU8EytWOSQKtRwetYywVDCR+/t7dH2TpFaTouSti2wep+5KHcL+ACjw6CKu1+8S7ev36AuvzcYDuTnYXX1oYYTmXM6k6E6JzlvNz5SpVLhWDNOlpUYf+Jy1el0GMghzc3M0PNnXwuYDsej0l0VQoUWTrjpbDZLx8cl+uHHFekisizryq0U9qvNiVfStugv3/4LZTkubbHrD2McGjo4vZgvyZk4Hzu9fL1K6XSa4rj4PnGf2Atk8F3O8v/1+XOaK+Sp3evJ+2Fy8aFqjh6YiCXX3q/Ti5cvKZvJitX0C5gQ9hJW3Ob9/Ov3/0MftnbEgrpDpO5nwqDQwZnLZ+n9+oZk41P5KYr62BAlEgmamsrR2vo6J2p7lMukf25gYVBo4MRFzQNMTnzWP25RJpty4zifu0nsYzaboVdr7zgMOaB0yqbBIByAhgJOJBlwkx8+bHOM+YZmr83ym8GJ3TC8mU7a9Gr1DR2USpS04pLVmy7j4QSY6MesVKu0vrlFU7kcDTHxImB5BQCFm3/1eo0G7AUS/Np0F288nLh8GCaExeR0QpKhoGa8gBPj9K/4WNDbgOMwGVCj4RwOB1TI5+j7FyvUcTpks2sPutKpFB0cHUkGb8fdwQJT+TQWTliURMKivf0ibweSVJggWEsMHrz/sEHVZp09AS6hmXQaCycuIiznx+1dsu3gW8yzQgaPxvdpZ4+PMxbYMOVLMhJO2BEkEIdHJU6ETiVTN00Y3tw/OKJTTvRMHdo08qhgRzCjB8OTFicRJlqWaBTZ+lCsZ3ycKJkm4+BE1xGGI49LZaqcViU7N1WZTIY+7e9To86xp4FdS0ZaTliV3b39QHcb/SYxjMjeX6+9c/tvDeuYNwpOWA4sza036lSqlGW5hdHihpeIJ+i0VqNqo2Gc9TQKTsRduWyeNja3pRspDEJjRJRdPDyUTnop8mCIjIETlwRZa7/XpePyiQzvhUVYtoyOeTh1kzJ3c+Bki2FxjHl4UmZAB0YnQp8Lx1pvtKjBrh1LOkxx7eY0M74g8USMTspljsPCVaTAHXAYUa1WdxfrGSJz4OS4qz8YUq1ekw54o7P0zyTHOuJj50QQk1tMkRFwull6hDqdLrXq7NpCFG96wtr609M69foDgdUERI2xnNFIlBynSz22nqjMETahSFin36XBoD+2pMHH0xw4Y1FqtVtiMcLj0M8I1nI4pE7bceHEFnAZAyeyVFRuGw7DWSHDPeIItTsdOX6AGnQZAyfqYjocc6L0YBgFIIeDkcAZNaRxGgMn1gUNpUhm+OJNCHBGIijfyOfBkIzdIMuJ4UskA+7rUAruHMuGhwqnbyQxFmenJsRZ51PEXTJsSAM1xnLiQAK0FP1CZNrhmwEnrCZbz6g5be0PitNCGVsfvwy4jLiauBiINWU+4/i9MArHjs54bCbIDFPD1wIjRDFZVxNWPN3jlop5hjh4Y/wgkiLLYjgNyVR/rzAriWMbd5K1IQ3UGDixfiaVcivHmTQz5/coGhlRJmlLf68J8wuMgRNdKIAT83HC6NkRzsB72HwOpG0acBKMspxJthoRJEUh7O+EV7dsi1CeRtYRIUMMuIyAExYDi9tsy6Z8NmtkgYEvCWun8rk8WXHLtaLj94MsYywnhEwV95fELQHDmLVP8bEj3jblyI2BE9YTd+Odm7lG/WG44ERIE+WGmc/n5SaxZnQkmQYnX5jZ6WmxoKZVv/g1IYxJp1OUzWSkSwnnwgQZ5dZRvCuVTtJ0YYp6vf74XfPV6fXoxrXrnAyZ1SiNghMjRFgeu/zgnky6DYPk3kQcY8/fvk5duZGWKU7dMDjduLNPU5yxz81Oy7IN09XtcpbOnqKQm+KMneNNQ8bVIaPg9IQb9t+dv0M9jkFNT4y6jkNfP35E9WZDRsdMknFwwnoCytnpGU4Q0kb3eWK16ezsHM0UpqmLLN2QRMiTkXAiKbCtOC0u3JG17CYKjQ5e4dH9Reqzazex9LaRbh0XCtn6zevXKZ/PyX3MTVO3yxn63HUqFPLSr2uWzXRlJJwQOlTQ3/lwaUG6WkySdBdFRrS0uCD9mqbKWDixdhv3LF/gxGg6P8XxWXv8kwALHPIVazYatLSwQAVvRMiwWNOTsXBCKOhVrdXpP//j3/kCsisMugXlY2g3HSrMTNPTx8vSjWRirOnJaDg9OWw1v33+jEaDobt0NqCeUDrZoxH69k/PZJDB9CFa4+GEZen0ujQ7XaDFxXmqN5pS9Csoc3e8/UR2jiToq+WHUsEZSZ7JVhMKheVETNZxuvT0yRN6yElEpVJhD+n/Gp4AE8OR2P8mW/+nT5Zp/tYtcedhqEEaCjglWOMjrVar4t5v3rxBjUbN96NHABML9k7KFfp6+RHd54bljC2mqUnQWYUEThyoe0FPqzX6M8dsd+/coWarJYD6FVHMT222W+zKH9Dy/SVuUE24gfFPzVdo4IQ8a9PjxOKrx4/o4fiCo0Kd3wTX3eNY+ZvlZXr84AFV6w0BMzxohgxOCIBimhnG37969Iief/M1tZpNSTDEil6xq8f3O44j5XW+e/YNzd+949bcDIkrP6vI7n4xGGnrBQgZcD6XpdJJhdbWN+T20Klk8h/JBkC9RCDQYBBTzk1P0/PnTykZt6nltHgXQmdDRKGGE1YKfYWWlRAON7c/0ae9fXHzlm27N5ziz10cnu5fHzCU3X5Pbk19/+5dmr99U/at1xtwQwknmFCo4fQEQGEtbYaj0W7TxsePdHBUYss6pGw2SxFUTJ6wBYWLxoBAvdGgpGUxkLckScukUtTud2XAwPR+zC9J4RxLrChDiLvwAtRGsyGW9P3Gpiw3TtpJAeq8wMhUN/6v2WzJuPjj5Ye0ePs2JRnKASdqfQYWbjxk4eUvSuE8I0DjFQLDeiQslsPt+lZW39Bh8UjAGY2tKNjBclw8AlrA7SUseO7Je467y6FUDuC2uAHML9yiJ5yQdTnGbDsdgTYMHeu/RwrnrwjAME+ymhPZPLpzcPvCGj822450SeEz2KQEzBhEz8IC8GgsQja77Vw2R5l0StaWFwo5irN1RJ8rCm7JJr+pOiuF85cEyIQW/DOS2DAO0ODSGTy43y6/J4lMty+L6tz6TG5XlNRYjkcpEYtTgpMt3F4byQ6SG1hmdK57llasrXyf4vm5FE7WWZeM5EjKB/JZwft47f4c95eMcWZvUSqVHgMXd0foZcXjZ3Dx7+DEYkZ+f9Alp9OX/kuAid4Az7pCeMT3eK+hs/sUVoUOTlz0s49nIYFi/Bqzy0dRWEF+YxQhNoBiITFhGfc0PzmpUa/rMHA96gx6TDQgBoyIK8FqnAEfSVcUKt9ZKDCWy1Ihn6N0Jk0p2+JPsQXmL2B7LPcNwj4gnj07s10aymeAhgnYUMDpgQjhOUp0wwpi6hz6ElGhDUkJOsCdjsMQcpLCjx28x9au2XLEhcfYVcc4mbHZVQvUY7Dd7Brf4YGDzB9wwX3zK4YM39NjkFFkDONySQYWFjjNoCZTSekNSCX5eZIf+bWVsOW+8Zi/6ca2sOCwsv+A03RQjYXTBRJWEC7UhRFriuK4WT6DVDw8pnKlQq1mWyZXDPnie+vcx78qlk9qrDMQiDnlROHneJSnv+565ed4Mv4MPotniE6H/F3I/pFIwc171lsaDocLSduWBCrHFvf2jRuU5ee4fSKsKbqgEAe7DeP///6gy0g4vfgtzpYRMOIe5FjjjSy7WDyi7Z1P0rmO+FE+ywACRO9Ce48efAI63vsCjF8S/gz/K38rcuZvuo/4Hw3D3bz+ULj5ZrMh2f69xQVZTVrIF8jixga40aA+j2FNkRFwCjxjwdUiMwZ0lWqVSuUyVcoVqaHU46xa3CdKU7PO/t5VCXvwJdyjbLU7HGag+woQZtntF2YKUlFvtjDNFtamLideA8QS42M6TyPyiwIPp5tAjMQN4lmbk5bdYpHK5So1nTaNcD9MvrhxzmpgSeXTAbuA0vHPCMMyem4dWT/2Ho1til3/nTu3aIqtapxjYiznwH1Ag25JAwsnYjW4RpuhRJKAsXA+Fqo26tTnzDo2jjEFP0lY5NeMEkDFBteOsCTNHuH63DVavHNbzkuLre2wjxAHY1PBaYyeAgUnLgSEk4xRF0zYKZ1U6e8/vZJsVhIYjjGxwTqa4Np+q8SisjVFbwD6Vh8/ukf3l+5Jtt/nbB8ZP85HkKxpYOB0XXGELM5k4db2Odve3N6mk9NTmpuZCbwLm6TQJsvVOkU45FlaWpCaUflMVpYWn23gfpev4QSQ2AAeNpzP45Myvd/4wLGlI9m23LFMnJbKk5wzPlmwpEikMOJ1b/4uzc/fkrmr/TM3dPAzpL6GE90pCbaUiB33i4e0e3BAJ+VTTgIAZWL8Kdeiqv5ZgM8DD32i7VabUimbbt28KaVsACmSR7fR+/P8+RJOnFiAOTU1RQ5byLW1t7RfKklMCWsZBJfkN3nntNPtcHY/RcsPl+jOrZt0yu5fLC2fW7/JV3B6rgYuCZnn1s4OrayuyQx1ZJ8QwMTHlM8/IoRJxG6dLanTols3btB3z59JDO/VXfJTw/cNnAATG7JwjG+vvl2nvcNDms7nfdmqTVC90ZDw6E9Pn9Dc7DV3+JZDAL8A6gs4BUx+xMrHg6MjevN+nbqdnlhPtZAXK6yNR6f94t15WR+P+BSW1A8G4crhBJhYnoCFXavv1mn9wyYlU5zwxL2ER3XRQvcSkqMp9lL/9udvJWbCDC3E+FepK4UTJwUd5hjtefXTazoqnUgS5MWeqsuVg+FeQjGHp3RteprabFHhua7KzV9Z0wCYWBKLOZM//P1HKldrsr7G6yRWXb6SyZRYy7+9/Il2igeUySTl/asyFlcCJw4W8SVmCf2w8lIA9WYKaTfR1QpddegZefPuPe3sFmU+6VXpUuEElLCMyBBR+uW//vuv8trGTfzHn1FdvTDoAUBXXr+hN2/fi4fDdbpsC3rpcAJMFBT44cVLmfGNriOV/4QkNZfNSA2pje1tyqTTuICXCuilwYmWB5eBUYrv//aC/Te7kJ+HIFV+FEKsfDZHa2/XaXtnn9Ick0KXBeilwIkJwQARi7wAJsJKxJwq/wtr7bE0ZPXtGu0fHYmBuSxdCpyxmFsTc+3dW6k1ibpAquAIHfKAEi4ey10uq2zOhcMJdx6NxunD5rZMd4PFvOzAWnV+IVfAROaXq6vw95cSf14onNh5ZH2V2qmUFcRz7SoKrmzbolqjRatr76XIGa7vRQJ6YXBip6UgQKdLP/74WvoxdQJH8DU9lefkaIe2Pu1IAYhAwgmhFMvrd+9oyP95U95UwRZgxCrPN+8+yK20sar1onQhcEq3UcKiw8MT2j84lpvyX2QLU12ukBBhzu3K61V277Z0D16ELgROuO/haEArqz9RPgswxz9QGaN0OsUJboXd++6FJbkThxN9mhj12dzakVqUKGigMk+A0eIEaWevKJOUUePJnZU7OU0UTjQe9Gm2nDYVj46k+0GTc3OFkuSNZpOOsb4rHmFjNNmLPWE43fmZe/tFuRGqThg2WwjfEHt+YNcOj4nOmEm694nBiZ1CH2av26P9wyOKJtSdh0EJDuFa9TqVypWfK61MShOF07YTVD6tULvpyLQrlfmCI0dVv73igYzDT3KQZXJuHTvFjWa3eMg763Y1qMIh5Bbl0yo1G22ZSY+uxEloInDCasJSokRMhXcSZfhU4RFiz16nT4cnR2JFJ2U9J2Y50WJK5VOpz4Na66pwKZm06OCgxB5z/MYENDGKcDOoo+OSDGepQw+fUNOqWqtRi72nO4fi/InRROCEGUeSVj51i2xNMmNTBUNgAAV7S5WK9H9OIuw8N5wAES0FBaFwY1MXVIUzbPI4qFVrkgxPIuycCJy4X061yonQuPvINeuqMAlGCWXQkRRjOc4kYrtzU+RlZg2nhRdSCEoVTsEoOd0uDYZ9ZvP8dJ7fcvI+yA2mpHQJ75JazdAK136ImvT9/vid8+nctZLg1jG/7/sXK1Q9rVKMs7YJT05RBUEwUhhfj4zoL989p2z6/JVCzg+n/DOkw9IxDXqcEKGjS+EMn3DZGU5c+mszMzKd7ryjhBOpMgfriUF/7IxyGW4BRylCy0x4+cgf1cRKIE5qPFUVfEnucU4woYnBiZaiUkGTABOaGJwq1aSl/T4q30rhVPlWCqfKt1I4Vb6VwqnyrRROlW+lcKp8K4VT5VspnCrfSuFU+VYKp8qnIvpf3DYieukz3doAAAAASUVORK5CYII=';
    
    //foto no disponible
    private fotoNoDisp = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARcAAAEjCAYAAAAYOEVvAAAAA3NCSVQICAjb4U/gAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAABH6SURBVHhe7d3Zb1R1H8fxXwtUVquIJmrQuBHUJ0YvjHqtMdFLFbzxxsilif4R9cILY0hMuFAhQNw3VFwRFde6iwtoRXBXcAGVglCBx/fPHp4+0JZpO9+ZM2fer+SknTPtdGY653N+++no6+s7kCSpzjoHv0pSXRkukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkIYLpJCGC6SQhgukkJ09PX1HRj8vtI6OjpSZ6dZqvI5cOBA2r9//+Ct6miLcCFYdu/enXbs2JEDhttSGezbty9NmTIlHX/88ZULmLYIl8mTJ6cNGzakp59+OnV1dVmCUWns2bMnzZkzJy1atCifAKukLcKFQOnt7U3Lly9P06ZNG9wrNR8ll+7u7nTrrbemP//8c3BvNbTNKZzSCiUYvrq5lWmjWlRFbVk/oM3Fza0MW5XZ+CAphOEiKYThIimE4SIphOEiKYThIimE4SIphOEiKYThIimE4SIpRNtMXHz77bfTypUr01FHHTWmYdfFWht///13/l4aDp8pPmdjHdLP52r27Nmpp6enchMXDZdRECZsxx57bJo/f36eYMYsVmmoSZMmpV27dqX169fnsBhLwBguLW4i4UKp5ZxzzkkLFy7Mj1PFFcM0MYTLH3/8ke64444cENyuleHS4iYaLmeffXYOl5kzZxouOgxhsn379nT77bfnEozh8i8bdGtQVI/c3IbbOOF40jmc4SIphOEiKYThIimE4SIphOEiKYThIimE4aKWxXilQzeVh+GilkKAMEiNr0zFGBgYOLhxe+j9ai7DRS2D0CBEfvrpp/T++++nxx9/PK1YsSItXbo0f33sscfy/m3btuWfG8tIWdWf4aLSoxTCpNFvvvkmX+972bJl6f77709vvPFG2rhxY9q0aVP++uabb+b9d911V1qzZk3+eX7XUkxzGC4qtaKa884776T77rsvrVu3Lv366695H4HDvLHiKxv7f/vtt/Tyyy/noHnrrbcMmCYxXFRqXN+boFi1alXaunVrnnjKvuECo9jH/YQM1afVq1entWvXpqlTpw7+lBrFcFFpURLp7e3N4bB79+5cQqm1BMLP8ft79uxJzz77bHr33XcNmAYzXFRKlD6+//779Oqrr6b+/v5cEqk1WIbicUAVaceOHamz0498o/hOq3QIEdY5oYH2hx9+OFgNGi+C6a+//solGEozagzDRaVD6eLHH39MW7ZsybcnEizg93nMTz75JLfDEDaKZ7iodAiDn3/++WCvUD0QLox9+eqrr6waNYjvskpn7969eSAcjbETLbUUeByqWpSIWD1O8QwXlQohQAmDBa/rHQIsRcnjEjKKZ7iodAiViEu4FI9LyNSrRKSRGS4qHdpE6NWpdwDweMUoXqtG8QyXkuOA4GBga4ezLQc9o3CPPvrowT31w3vY3d1dt0Zijc5wKTHChGL8888/n1588cX8fTv0dDCu5eSTT04zZsyo2yU7CC0e95RTTmmLkC4Dw6VGzShGEySMUmXoOssLMBSe51H1g4MQPeGEE9JJJ52Uw2Wi7z2/z+Mcc8wx6bTTTrNBt0EMlxrw4Wx0uBAgdMl+9NFHebwH82qeeuqpPMu36lUkguC4445L5557bq4i1aP0wmNccsklubrV6P9luzJcatCscKHU8sEHH+QSDEV6zrhPPvlk+vDDDys/jJ33+7zzzktnnXXWhN9/xsuceeaZ6cILL8zfqzEMlxIiWDjTcn3r33///WA7CwHDwcGKa+vXr0/Tpk3L+6uI10/j66WXXpqrSIx9GU/A8H5xLeZrrrkml4IafZJoZ4ZLCVHt+fbbb/OSjYdO2uM2FyynDebjjz+u9DIClNRogL322mtzNYnJh7WGAz/HMg0E03XXXZdOPPHEHFBqHMOlhKjyPPfcc/lgOLRthdsEDKutPfroo6mvr6/SJRgad0899dR000035aoNwUpbFCUbAuTQjf3cT4mFatUNN9yQzjjjjLxPjWW4lAzBQiMu7SojLY7EPko3TO574IEH0hdffFHpNhgCZubMmenmm29ON954Y5o/f35+/ZRsuK/YuE0VkvsXLVqUrr/++lxysXeoOTr+OfNVvhLKgUf7xcqVK3O9e7gDdjjFmXDevHlpwYIFuQ2A21F4XvzN2267LU/co4QyGn6WM/TcuXPTwoULcxWi6gcS/0teN+8Pa73Qk1aEypw5c/L4GL4SPuNtpxkr/vb27dvT4sWL065du/LfrhXPnTahnp6eXCqrEksuJUJJhRCsdc0Rwog2F1a5p5G3WFipyqjeEBoEyPnnn58uu+yydMUVV6TLL788XXDBBXk/pRh+rhHBopEZLiVBmNCOwuUyxnLmAwHz5Zdf5oD55ZdfckhVHQFCyBTtK0XosF/lYLiUBCUOBshR3KdEUmvVDUUJhraXBx98MO3cubPyJRiVn+FSAgTB119/nRtyOfOOJVgK/A7tEZ9//nlejJqz+VhLQFI9GS4lQKC89957udRCIIwnXMDv0WD96aef5guIUU1oVsAQmIZbezNcmoz2ERpkuRxpvXqiOKhZjJo2GHojGn2Q8zoYf0PDNGGn9mS4NBElDbouuVRp0UM03lJLgd8vNnqennjiiVxFKqYQROM10C1Lt//dd9+dQ4ZSzERfl1qP4dJEHPDfffddrsbU8wDkcXjsImBeeumlXEWKDhj+HqUWrjfEmA16ru69995ciuK+er0+tQbDpUk40Jn7wsEfdSVAShG053CtZa5cON7G4lrx95jJzbozhCVVIrrXGUXMPkQHnMrD/3STcJBzDR1mN9PLE3XQFwHzwgsvpNdee60uVa/h8JhsLAnBBEP+DnhtVP24kDzPgTYggkfVZ7g0AQch1ZR169blwV/RZ3MOdP4ekyEZpBcx0ZEQYT4UXeF8PxRhQnWJ5TqZzU2VqR0G+rU7w6UJOPhYLmG4AzEKBzgBQwMvVbHp06cP3jNxhCWlE1bKG2myJQFHiDJQ8OGHH85zghr12tUchksTcKBxUXQOtuEOxChFCYaDmxXu6hUwPA5VriNdfpXXykaw0tDL1Q/tqq4uw6XBGKa/du3apkwy5MDm4KchmVG8tPdM9ODmNdB29MorrxwxKItw4Xc2bdqUli1bludEUYI50u+q9RguDcSBzbKVa9asye0ezTig+Js8D+YfPfTQQ2nDhg35YB/Pc+F3aCwmLPv7+2sqiXE/G4HC2B4ChlJUcZ+qw3BpIA4+gqU4EJulOLjpJmYULxMex/N8CCmqOPw+xhIO/CylJhp3qSIxNoYlEkarVqm1GC4NQkMn6+IyObEsZ+ii9EAjL1WbsVTTCAHG5zC6mFLQeMOS94XSD0t2cuE3urEbXV1UDMOlAQgTxnfQU8KZupbqQyPwHAgYQo8uYtqBau0ipmuZkcVbtmyZ8OshqHgMusoZJ0PV0a7q1me4NAAHCgfhZ599VrrV0YqAYckH2mA4sI9UcuB+qlRcCZKSRj2CsggYAphSDDPE7UlqbYZLMA48xoCwpAJdtWUptQzF8yEAqRqtWLEid1eP1PbBz1IKY8BcUZWq1+vhvWFjLhKLXlGiIvjUmgyXYMVBS69MGYOlUAQMXcNLly4dsXGV10CphflK3F/v18Pj87g8j+XLl6fNmzfnEkxZ3zeNzHAJxIFCo+frr7+ev45UGiiLImCovt155525xDX0OXM/ofPMM8/ktqOo18PfoURE1WjJkiW50Zj3kk2tw/9WMM7AjRzmP1HFgU33Mo2rQ3uCCB5eC9MHoq/0yPPgPaMKxtowzMOyq7q1GC5BOCA58zNRkDaMVjrr8lw5uLmcLAPkiu5huoxXr17d0GoKf5eNRl4mPjK6mNsqP8MlCAcfS1dScuGM32oImGLhJ4b2Eyz05NCr1OiDm+dCoBEujMmhzacV39N2Y7gE4GBgaUnGbaBRZ/l6owpCqLDQFFUkGnGjq0Mj4T0l1Gh/KS4AZ09SuRkuATgAqQ7RINnqRXgChqCkBNPsrnT+NhslQrqqKRVaRSovw6XO+PAzEI0eFc6srVpqGYqAoTGV19Ls11MEDFdMYE4So4Sr8j5XjeFSZ8x2pm2g1Rpxj6QMwVLgeRB4tL0w6I9SogFTPoZLHVFEZ5g/VQi+98MepwgY2oTuueeevAoeYc4+lYPhUidFkNDoydiMKpVayqoIGNq4aHCmoddZ1eXhEVAnhAn1f0a3wlJL4/Des9QmvVrF+rwGTPMZLnXEqFYGeVlqaTzecwKFCZUEDPO5KNUY8s3jUVAn9KawqXkIGDZCnioSXdbcNmCaw3CpIz/IzVcEDJfJJWCYwuBo3uYwXOrMcGk+/gdUibggPlc5oJF91qxZ/m8azHBRJREklGBAVzUjepmfVOxTPN9pVRYBw9bd3Z0HNrJ0w9DrWCuW4aJKKwKGkdOsQ1Nc6dH2sXiGi9oCQUJXNeOQWBuGKz4WwaMYhovaRlFaYQzMqlWr8jWkLMHEMVzqyA9p+REmbFu3bk2PPPJIXgDLBcBjdPT19VV+5BczZqlv06A3lg8Sg+JYjW3evHlpwYIFuWGQ28OhkZBeid7eXkeGtgD+t0x6pKp00UUXpauuuip/P56BkIQV3d6LFy8+bFHzI2Ee2uzZs1NPT09e9LxKDJdRjCVceEyGnluXby38PynFXHnllen0008f8f87GsNleIbLKMYSLmAkqBPmWgufBf6n/f394+6iNlyGZ7iMYqzhovZkuAzPBl1JIQwXSSEMF0khDBdJIQwXSSEMlxrQG0APQDF+xc1t6FZ8Pviq/7ErehRFV/T8+fPT1VdfnVeZtytahyJU6EZesmRJ/mpX9L8Ml1EUQ8FZWX7u3Ll5gBxDxqWhCBMuebt58+Z88qn18wXDpcWNN1xAwLARKkXYSIfiM8XJZyyfLRguLW4i4SJFcoSuJI2R4SIphOEiKYThIimE4SIphOEiKYThIimE4SIphOEiKYThIilEW4VLMU+IyWVubmXY+DwyBaCK2mZuEVfW46JlXJDcuUUqC4Jl1qxZ6ZZbbkk7d+4c3FsNbREurLexbdu2tHHjxjxz1UV9VBYDAwP5hHfxxRfn76ukLcIFBAoXLZPKhupR1YIFbRMukhrL+oGkEIaLpBCGi6QQhoukEIaLpBCGi6QQhoukEIaLpBCGi6QQhoukEIaLpBCGi6QQhoukEIaLpBCGi6QQhoukEIaLpBCGi6QQhoukEIaLpBCGi6QQhoukEIaLpBCGi6QQhoukEIaLpBCGi6QQhoukEIaLpBCGi6QQhoukEIaLpBCGi6QQ5Q2XzklpypSu1NXVNbhDOhyfj66uKamzY3AHOobeULOUN1z270sDA3vT3r17B3dIh+PzsXfvQNp/YHAHwTKwa/CGIkyaNCnVEt9Wi1QtB/5JmcnTBm8owvrf9qXO/ysqDs9wkTQm/+lOad/BouLIDBdJIQwXSSEMF0khDBdJIQwXSSEMF0khDBdJIQwXSSFqD5eOztQ1bVaaNWtGmtr1/8N/OyZP/Wf/P/fNmJ6OmnzIQ3ZOSdO5b3CbPrUrTXLqh1R5HX19fUceaidJY2S1SFIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVIIw0VSCMNFUgjDRVKAlP4L5HBNrN5ASvkAAAAASUVORK5CYII=';

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService,
    ) {
        this.item = {};
        this.disable = false;
    }


    ngOnInit() {
    }


    open() {
        this.blockUI.stop();
        this.error = false;
        this.modal.open();
    }


    close(flag: boolean = false) {
        if (!flag) {
            this.modal.close();
        }
    }
    
    fetchBlob(uri:any) {
        return new Promise((resolve, reject) => {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', uri, true);
        xhr.responseType = 'arraybuffer';

        xhr.onload = function(e) {
            if (xhr.status == 200) {
                var blob = xhr.response;
                resolve( blob);
                // if (callback) {
                //     callback(blob);
                // }
            }
        };
        xhr.send();
        });
    };

    _arrayBufferToBase64(buffer:any) {
        var binary = '';
        var bytes = new Uint8Array(buffer);
        var len = bytes.byteLength;
        for (var i = 0; i < len; i++) {
            binary += String.fromCharCode(bytes[i]);
        }
        return window.btoa(binary);
    };

    private getListVisitas(){
        return new Promise((resolve, reject) => {
            this.visitas = [];
            let listV = this.fichaPService.listTarjetasColombia.filter((v:any) => v.Checked === true);
            listV.forEach((list:any,i:any) => {
                if (list.Array.length > 0){
                    this.readArray(list.Array[0]).then(() => {
                        if (i === listV.length - 1) {
                            resolve();
                        }
                    })
                } else{
                    if (i === listV.length - 1) {
                            resolve();
                        }
                }
            });
        });
    }
    private readArray(array: any){
        return new Promise((resolve, reject) => {
            if (!!array){
                array.forEach((a:any, i:any) => {
                    let fecha = a.Fecha.indexOf(':') > 0 ? moment(a.Fecha).format('DD-MM-YY') : a.Fecha;
                    if (this.visitas.filter((v: any) => v.Fecha === fecha).length === 0){
                        let parts = fecha.split('-');
                        let Fechadate = new Date(20 + parts[2], parts[1] - 1, parts[0]);
                        this.visitas.push({Fecha : fecha, Date: Fechadate});//, Date: Fechadate
                    }
                    if (i === array.length - 1){
                        resolve();
                    }
                });
            } else{
                resolve();
            }
        });
    }

    private orderListVisitas(){
        return new Promise((resolve, reject) => {
            this.visitas.sort((a:any, b:any) => {
                if (a.Date.getTime() < b.Date.getTime()) {
                    return 1;
                }
                if (a.Date.getTime() > b.Date.getTime()) {
                    return -1;
                }
                return 0;
            });
            resolve();
        });
    }

    public loadAllImagesViviendas(array: any){
        return new Promise((resolve, reject) => {
            array.forEach((a:any,i: any) => {
                let url = 'https://maps.googleapis.com/maps/api/staticmap?center='+ String(a.Latitud)+',' + String(a.Longitud) + '&zoom=16&scale=false&size=500x500&maptype=hybrid&key=' + this.apiBackEndService.apiKeyGoogleMap + '&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0x91b8fd%7Clabel:A%7C'+ String(a.Latitud)+',' + String(a.Longitud);
                this.cargarImagen(url).then((base64)=>{
                    a.base64 = base64;
                    if (i === array.length - 1){
                        console.log('loadAllImagesViviendas',array);
                        resolve()
                    }
                });
            });
        });
    }

    public loadAllImages(array: any,key: string){
        return new Promise((resolve, reject) => {
            let mThis = this;
            //array.forEach((a:any,i: any) => {
                array[key] = !!array[key] && array[key] !== 'about:Blank' && array[key].substring(array[key].length - 14) !== 'img_negada.jpg' ? array[key]: '/Content/Images/sin_imagen.png';
                let tipo = array[key].substring(array[key].length - 3) === 'peg' ? array[key].substring(array[key].length - 4) : array[key].substring(array[key].length - 3) ;
                this.cargarImagenFotos(array[key],array,tipo,true).then((base64:any)=>{
                    array.base64 = base64.replace('multipart/form-data','image/jpeg');
                    // if (i === array.length - 1){
                    //     console.log(array);
                    //     resolve()
                    // }
                    console.log(array);
                    resolve();
                });
            //});
        });
    }

    public loadAllImagesFotos(array: any,key: string){
        return new Promise((resolve, reject) => {
            let mThis = this;
            var promises :any = [];
            array.forEach((a:any,i: any) => {
            // Observable.interval(250).take(array.length).subscribe((i) => {
            //     let a = array[i];
                promises.push(
                    new Promise((resolve, reject) => { 
                        if (a.base64 === undefined){
                            let tipo = a[key].substring(a[key].length - 3) === 'peg' ? a[key].substring(a[key].length - 4) : a[key].substring(a[key].length - 3);
                            this.cargarImagenFotos(a[key],a,tipo).then((base64:any)=>{
                                a.base64 = base64.replace('multipart/form-data','image/jpeg');
                                resolve()
                                // if (i === array.length - 1){
                                //     console.log(array);
                                //     resolve()
                                // }
                            });
                        } else{
                            resolve();
                        }
                    })
                )
            });
            
            Promise.all(promises).then(() => 
                resolve()
            );

        });
    }

    private cargarImagen(url:string){
        return new Promise((resolve, reject) => {
            //var mThis = this;
            //var url = 'http://upload.wikimedia.org/wikipedia/commons/4/4a/Logo_2013_Google.png';  
            var xhr = new XMLHttpRequest();
            xhr.onload = function() {
                var reader = new FileReader();
                reader.onloadend = function() {
                    resolve(reader.result);

                }
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();
        });
    }

    private cargarImagenFotos(url:string, array:any, tipo?:any, datosP?: boolean){
        return new Promise((resolve, reject) => {
            var mThis = this;
            //var url = 'http://upload.wikimedia.org/wikipedia/commons/4/4a/Logo_2013_Google.png';  
            // var xhr = new XMLHttpRequest();
            // xhr.onload = function() {
            //     var reader = new FileReader();
            //     reader.onloadend = function() {
            //         var img = new Image;
            //         img.onload = function() {
            //             array.columns = img.width > img.height ? 2 : 3;
            //             resolve(reader.result);
            //         };
            //         img.src = reader.result;
            //     }
            //     reader.readAsDataURL(xhr.response);
            // };
            // xhr.open('GET', url);
            // xhr.responseType = 'blob';
            // xhr.send();

            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.responseType = 'arraybuffer';

            xhr.onload = function(e) {
                if (xhr.status == 200) {
                    var blob = xhr.response;
                    let base64 = 'data:image/'+ tipo + ';base64,' + mThis._arrayBufferToBase64(blob);
                    var img = new Image;
                    img.onload = function() {
                        array.columns = img.width > img.height ? 2 : 3;
                        resolve(base64);
                    };
                    img.src = base64;
                    // if (callback) {
                    //     callback(blob);
                    // }
                }
            };
            xhr.onerror= function(e) {
                var img = new Image;
                img.onload = function() {
                    array.columns = img.width > img.height ? 2 : 3;
                    resolve(mThis.fotoNoDisp);
                };
                img.src = mThis.fotoNoDisp;
            };
            xhr.send();
        });
    }

    public imprimir(){
        // let blob = 'https://cresourcestorage.blob.core.windows.net/multimedia/img_negada.jpg?st=2018-06-21T12%3A58%3A00Z&se=2019-06-22T12%3A58%3A00Z&sp=rl&sv=2017-07-29&sr=c&sig=Mz8f3PBwo1LS0s%2Bs2UEDVM7yoW1FxEaouW1mRXp%2FTPU%3D';
        // this.fetchBlob(blob).then((buffer) =>{
        //         console.log('buffer',buffer);
        //         let base64 = 'data:image/jpeg;base64,' + this._arrayBufferToBase64(buffer);
        //         console.log('base64',base64);
        //     })
        //let buffer = this.fetchBlob(blob);
        // console.log('buffer',buffer);
        // console.log(this._arrayBufferToBase64(buffer));
        let mThis = this;
        this.error = false;
        this.aframe = undefined;
        var images: any = [];
        this.blockUI.start('Generando PDF ...');
        if (!!this.fichaPService.datosPersonales){
            this.getListVisitas().then(()=> this.getDatosPersonales())
            //.then(() => this.cargarImagen('https://sprodresources.blob.core.windows.net/multimedia/relevamiento/cod_form_133_re_102940917/cod_ver_308_re102941117/ce_38_3c07488a7b76a091_20180421180334467/fotos/c3f21be4-af47-0b59-8a4a-ab70c8012d7f.jpg?st=2018-05-22T13%3A12%3A42Z&se=2018-05-23T13%3A12%3A42Z&sp=rl&sv=2017-07-29&sr=b&sig=73w%2B5Kz6dHz4TGtkpeegOCkH%2FD73blW4mkvlJ0LpO6E%3D'))
            .then(() => this.orderListVisitas())
            .then(() => this.getDatosFotos())
            .then(() => this.getDatosViviendas().then(()=> {
                    console.log('visitas',this.visitas);
                    this.tablaDP().then((doc: any) => {
                        this.generaVisitas(doc)
                         .then((doc:any) => this.generaFotos(doc))
                         .then((doc:any) => this.generaHeaderFooter(doc))
                         .then((doc:any)=>{
                            doc.setProperties({
                                title: 'Ficha de Persona de: ' + this.fichaPService.nombreCompleto,
                                subject: this.fichaPService.datosPersonales[0].CodPersona
                            });
                            this.blockUI.stop();
                            //mThis.aframe = doc.output('datauristring');
                            doc.save(this.fichaPService.datosPersonales[0].CodPersona);
                        });
                    });
                })
            );
        } else{
            this.error = true;
            this.blockUI.stop();
        }
    }

    private getDatosViviendas(){
        return new Promise((resolve, reject) => {
            this.loadAllImagesViviendas(this.fichaPService.vivienda).then(()=>{
                resolve();
            });
        });
    }

    private generaVisitas(doc: any){
        return new Promise((resolve, reject) => {
            let mThis = this;

            console.log('this.visitas',this.visitas);
            if (this.visitas.length > 0) {
                //var promises :any = [];
                Observable.interval(300).take(this.visitas.length).subscribe((index) => {
                    //this.visitas.forEach((array:any, index:any) => {
                    // promises.push(

                    // new Promise((resolve, reject) => { 
                    let array = this.visitas[index];
                    this.CountFicha = 0;
                    let rows = 7;
                    this.first = doc.autoTable.previous;
                    let mThis = this;

                    let previous: any =[];
                    let columns: any[];
                    let data: any[];
                        

                    let vivienda = this.fichaPService.vivienda !== undefined ? this.fichaPService.vivienda.filter((viv: any) => viv.Fecha === array.Fecha) : []; 
                    this.getRowsList(array.Fecha).then(() => this.generaHeaderVisita(doc,array.Fecha))
                    //this.generaHeaderVisita(doc,array.Fecha).then((doc:any) => this.generaVivienda(doc,vivienda))
                    .then((doc:any) => this.generaVivienda(doc,vivienda,array.Fecha))
                    .then((doc: any) => this.generaMapa(doc, vivienda,array.Fecha))
                    .then((doc:any) =>  this.generaSalud(doc,array.Fecha))
                    .then((doc:any) =>  this.generaEmbarazadas(doc,array.Fecha))
                    .then((doc:any) =>  this.generaLactantes(doc,array.Fecha))
                    .then((doc:any) =>  this.generaAntropometria(doc,array.Fecha))
                    .then((doc:any) =>  this.generaEducacion(doc,array.Fecha))
                    .then((doc:any) =>  this.generaTrabajo(doc,array.Fecha))
                    .then((doc:any) =>  this.generaGrupoFamiliar(doc,array.Fecha))
                    .then((doc:any) =>{
                    previous = doc.autoTable.previous;

                    if (index === mThis.visitas.length - 1){
                        console.log(mThis.visitas);
                        resolve(doc)
                    }
                     //resolve()
                });
                
                //  })
                // )
            });

            // Promise.all(promises).then(() => 
            //         resolve(doc)
            //     );
            
            } else {
                resolve(doc);
            }
        });
    }

    /**
     * 
     * Proceso para cargar en el listado de tarjetas la cantidad de filas correspondiente a cada tarjeta,
     * también según la fecha valida si la tarjeta deberia estar visible
     * @private
     * @param {*} Fecha 
     * @returns 
     * @memberof DescargarFichaPColombiaComponent
     */
    private getRowsList(Fecha:any){
        return new Promise((resolve, reject) => {
            this.fichaPService.listTarjetasColombia.forEach((tarj:any, i:any) => {
                tarj.Fecha = null;
                let rows = 4;
                if (tarj.Checked) {

                    if (i === 2) {
                        let discapacidades = this.fichaPService.discapacidades !== undefined ? this.fichaPService.discapacidades.filter((d: any) => d.Fecha === Fecha) : [];
                        let riesgosSalud = this.fichaPService.riesgosSalud !== undefined ? this.fichaPService.riesgosSalud.filter((r: any) => r.Fecha === Fecha) : [];

                        if (discapacidades.length > 0 || riesgosSalud.length > 0) {
                            tarj.rows = discapacidades.length > riesgosSalud.length ? discapacidades.length + 2 : riesgosSalud.length + 2;
                            tarj.Fecha = Fecha;
                        }
                    }

                    if (i === 3) {
                        let embarazo = this.fichaPService.embarazo !== undefined ? this.fichaPService.embarazo.filter((e: any) => e.Fecha === Fecha) : [];
                        if (embarazo.length > 0) {
                            tarj.Fecha = Fecha;
                        }
                    }

                    if (i === 4) {
                        let lactantes = this.fichaPService.lactantes !== undefined ? this.fichaPService.lactantes.filter((e: any) => e.Fecha === Fecha) : [];
                        if (lactantes.length > 0) {
                            tarj.Fecha = Fecha;
                        }
                    }

                    if (i === 5) {
                        let antropometria = this.fichaPService.antropometria !== undefined ? this.fichaPService.antropometria.filter((ant: any) => ant.Fecha === Fecha) : [];
                        if (antropometria.length > 0) {
                            tarj.Fecha = Fecha;
                        }
                    }

                    if (i === 6){
                        let educacion = this.fichaPService.educacion !== undefined ? this.fichaPService.educacion.filter((ed: any) => ed.Fecha === Fecha) : [];
                        
                        if (educacion.length > 0){
                            tarj.rows = educacion[0].Concurre ? educacion[0].Concurre == 'SÍ' ? 9 : 7 : 7;
                            tarj.Fecha = Fecha;
                        }
                    }

                    if (i === 7){
                        let trabajo = this.fichaPService.trabajo !== undefined ? this.fichaPService.trabajo.filter((tr: any) => tr.Fecha === Fecha) : [];
                        
                        if (trabajo.length > 0){
                            tarj.Fecha = Fecha;
                        }
                    }

                    if (i === 8){
                        let grupoFamiliar = this.fichaPService.grupoFamiliar !== undefined ? this.fichaPService.grupoFamiliar.filter((gf: any) => gf.Fecha === Fecha) : [];
                        
                        if (grupoFamiliar.length > 0){
                            tarj.rows = rows + grupoFamiliar.length;
                            tarj.Fecha = Fecha;
                        }
                    }
                }
                if (i === this.fichaPService.listTarjetasColombia.length - 1){
                    console.log('tarjetas',this.fichaPService.listTarjetasColombia);
                    resolve()
                }
            });
        });
    }

    private generaVivienda(doc: any, vivienda: any, Fecha:any){
        return new Promise((resolve, reject) => {
            let previous = doc.autoTable.previous;
                    
                console.log('vivienda',vivienda);
                
                //Vivienda
                if (vivienda.length > 0 && this.fichaPService.listTarjetasColombia[0].Checked) {
                    console.log('entre vivienda');

                    var columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];

                    let rows = this.fichaPService.listTarjetasColombia[0].rows;

                    let listTarj = this.fichaPService.listTarjetasColombia.slice(1);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha);

                    rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;

                    var data = this.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: previous.finalY + 2.5,
                        showHeader: 'never',
                        theme: 'plain',
                        pageBreak:'avoid',
                        margin: {right: 107},
                        styles: {
                            halign: 'left'
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {
                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Vivienda',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                    // doc.autoTableText(data.row.index / 5 + 1 + '-', cell.x + cell.width / 2, cell.y + cell.height * 5 / 2, {
                                    //     halign: 'center',
                                    //     valign: 'middle'
                                    // });
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Departamento',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var departamento = doc.splitTextToSize(!!vivienda[0].Departamento ? vivienda[0].Departamento :  '-', data.table.width + 10);
                                    doc.setFontSize(8);
                                    doc.text(departamento,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Municipio',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var municipio = doc.splitTextToSize(String(!!vivienda[0].Municipio ? vivienda[0].Municipio :  '-'), (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(municipio,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 5) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Tipo de Vivienda',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 6) {
                                    var tipoVivienda = doc.splitTextToSize(!!vivienda[0].TipoVivienda ? vivienda[0].TipoVivienda : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(tipoVivienda,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 7) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Autoridad',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 8) {
                                    var autoridad = doc.splitTextToSize(!!vivienda[0].Autoridad ? vivienda[0].Autoridad : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(autoridad,cell.textPos.x, cell.y - 4);
                                }
                                return false;
                            }

                            if (data.column.dataKey === 'column2') {
                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('País',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 2) {
                                    var pais = doc.splitTextToSize(!!vivienda[0].Pais ? vivienda[0].Pais : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(pais,cell.textPos.x, cell.y - 4);
                                }
                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Resguardo Indígena',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var resguardo = doc.splitTextToSize(!!vivienda[0].ResguardoIndigena ? vivienda[0].ResguardoIndigena : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(resguardo,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 5) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Nombre de la Comunidad',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 6) {
                                    var comunidad = doc.splitTextToSize(!!vivienda[0].NombreDeComunidad ? vivienda[0].NombreDeComunidad : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(comunidad,cell.textPos.x, cell.y - 4);
                                }
                                return false;
                            }
                        },
                    });

                    this.first = doc;//.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                    resolve(doc);
                }
        });
    }

    private generaMapa(doc:any, vivienda: any, Fecha:any){
        return new Promise((resolve, reject) => {
            //Tarjeta Mapa
            let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};

            let rows = this.fichaPService.listTarjetasColombia[1].rows;

            if (vivienda.length > 0 && this.fichaPService.listTarjetasColombia[1].Checked) {
                var map: any = [];

                let listTarj = this.fichaPService.listTarjetasColombia.slice(2);//.filter((list:any) => list.Checked === true)
                listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha);

                if (previous.pageStartX !== 107 && this.CountFicha > 0){
                    // Reset page to the same as before previous table
                    doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                    rows = previous.rows.length > rows ? previous.rows.length : rows;
                    startY = previous.pageStartY;
                    margin = {left: 107};
                } else {
                    rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                    startY = previous.finalY + 2.5;
                    margin = {right: 107};
                }

                //previous = this.this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                columns = [
                    {title: "columnaI", dataKey: "column1"},
                    {title: "columnad", dataKey: "column2"},
                ];
                data = this.generaTemplate(rows);

                doc.autoTable(columns, data, {
                    startY: startY,
                    showHeader: 'never',
                    pageBreak:'avoid',
                    margin: margin,
                    styles: {
                        halign: 'left',
                    },
                    drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {
                                if (data.row.index === 0) {
                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                    // doc.autoTableText(data.row.index / 5 + 1 + '-', cell.x + cell.width / 2, cell.y + cell.height * 5 / 2, {
                                    //     halign: 'center',
                                    //     valign: 'middle'
                                    // });
                                    map.push({
                                        url: vivienda[0].base64,
                                        x: cell.textPos.x,
                                        y: cell.textPos.y,
                                        width: data.table.width,
                                        height: data.table.height
                                    });
                                }
                            }
                            return false;
                    },
                    addPageContent: function() {
                        if (map[0].url !== undefined){
                            doc.addImage(map[0].url,'JPG', map[0].x, map[0].y - 1, map[0].width - 3, map[0].height - 12);//Width, heith
                            mThis.first = doc;
                            mThis.CountFicha++;
                            resolve(doc); 
                        }
                    }
                })

            } else {
                resolve(doc);
            }
        });
    }

    private generaSalud(doc:any, Fecha: string){
        return new Promise((resolve, reject) => {
            //Tarjeta Salud
            let previous = doc.autoTable.previous;

            let mThis = this;
            var columns = [];
            var data: any;
            let rows = 4;

                let discapacidades = this.fichaPService.discapacidades !== undefined ? this.fichaPService.discapacidades.filter((d: any) => d.Fecha === Fecha) : [];
                let riesgosSalud = this.fichaPService.riesgosSalud !== undefined ? this.fichaPService.riesgosSalud.filter((r: any) => r.Fecha === Fecha) : [];

                console.log('discapacidades',discapacidades);
                console.log('riesgosSalud',riesgosSalud);

                let startY = mThis.first.finalY + 2.5;
                let margin: any = {right: 107};

                if ((discapacidades.length > 0 || riesgosSalud.length > 0 ) && this.fichaPService.listTarjetasColombia[2].Checked) {

                    rows = discapacidades.length > riesgosSalud.length ? discapacidades.length + 2 : riesgosSalud.length + 2;
                    
                    let listTarj = this.fichaPService.listTarjetasColombia.slice(3);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha)

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }

                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Salud',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Discapacidad',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index > 1 && data.row.index < discapacidades.length + 2) {
                                    var discapacidad = doc.splitTextToSize(discapacidades[data.row.index - 2].Discapacidad ? discapacidades[data.row.index - 2].Discapacidad : 'No posee discapacidades', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(discapacidad,cell.textPos.x, cell.y - 2);
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Riesgos',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index > 1 && data.row.index < riesgosSalud.length + 2) {
                                    var riesgo = doc.splitTextToSize(riesgosSalud[data.row.index - 2].Riesgo ? riesgosSalud[data.row.index - 2].Riesgo : 'No tiene riesgos', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(riesgo,cell.textPos.x, cell.y - 2);
                                }
                                return false;
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                    resolve(doc);
                }
        });
    }

    private generaEmbarazadas(doc:any, Fecha:string){
        return new Promise((resolve, reject) => {
	        let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = this.fichaPService.listTarjetasColombia[3].rows;

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};
            
            //Servicios
                let embarazo = this.fichaPService.embarazo !== undefined ? this.fichaPService.embarazo.filter((e: any) => e.Fecha === Fecha) : [];
                //console.log('servicios '+ index,servicios);
                if (embarazo.length > 0 && this.fichaPService.listTarjetasColombia[3].Checked) {

                    let listTarj = this.fichaPService.listTarjetasColombia.slice(4);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha);

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }

                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Gestantes',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Edad gestacional (Semanas)' ,cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var edadG = doc.splitTextToSize(!!embarazo[0].EG ? embarazo[0].EG : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(edadG,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Recibió Suplementos',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var suplementos = doc.splitTextToSize(!!embarazo[0].RecibioSuplemento ? embarazo[0].RecibioSuplemento : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(suplementos,cell.textPos.x, cell.y - 4);
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Edad gestacional Actual',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var embarazoActual = doc.splitTextToSize(!!embarazo[0].EmbarazoEnMesActual ? embarazo[0].EmbarazoEnMesActual : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(embarazoActual,cell.textPos.x, cell.y - 2);
                                }

                                return false;
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                    resolve(doc);
                }
        });
    }

    private generaLactantes(doc:any, Fecha:string){
        return new Promise((resolve, reject) => {
	        let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = this.fichaPService.listTarjetasColombia[4].rows;

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};
            
            //Lactantes
                let lactantes = this.fichaPService.lactantes !== undefined ? this.fichaPService.lactantes.filter((e: any) => e.Fecha === Fecha) : [];
                if (lactantes.length > 0 && this.fichaPService.listTarjetasColombia[4].Checked) {

                    let listTarj = this.fichaPService.listTarjetas.slice(5);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha)

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }

                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Lactantes',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Leche Materna' ,cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var recibeLactancia = doc.splitTextToSize(lactantes[0].RecibeLactancia || lactantes[0].RecibioLactancia ? lactantes[0].RecibeLactancia || lactantes[0].RecibioLactancia : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(recibeLactancia,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Meses exclusivos LM',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var mesesLM = doc.splitTextToSize(lactantes[0].HastaCuandoLactMatExclus ? lactantes[0].HastaCuandoLactMatExclus : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(mesesLM,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 5) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Deja el pecho',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 6) {
                                    var dejaPecho = doc.splitTextToSize(lactantes[0].EdadMesesDejoPecho ? lactantes[0].EdadMesesDejoPecho : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(dejaPecho,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 7) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Primer alimento semi-sólido',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 8) {
                                    var alimSemSol = doc.splitTextToSize(lactantes[0].EdadMesesComioAlimSemiSolido ||lactantes[0].SabeEdadComioAlimSemiSol ? lactantes[0].EdadMesesComioAlimSemiSolido ||lactantes[0].SabeEdadComioAlimSemiSol : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(alimSemSol,cell.textPos.x, cell.y - 4);
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Frecuencia',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 2) {
                                    var frecuencia = doc.splitTextToSize(lactantes[0].CadaCuantoPecho24hs ? lactantes[0].CadaCuantoPecho24hs : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(frecuencia,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 5) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Primera ingesta no materna',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 6) {
                                    var talla = doc.splitTextToSize(lactantes[0].EdadMesesTomoLiquidos || lactantes[0].d.SabeEdadTomoLiquidos  ? lactantes[0].EdadMesesTomoLiquidos || lactantes[0].d.SabeEdadTomoLiquidos : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(talla,cell.textPos.x, cell.y - 4);
                                }

                                if (data.row.index === 7) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Primer alimento sólido',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 8) {
                                    var alimSolido = doc.splitTextToSize(lactantes[0].EdadMesesComioAlimSolido || lactantes[0].SabeEdadComioAlimSolido ? lactantes[0].EdadMesesComioAlimSolido || lactantes[0].SabeEdadComioAlimSolido : '-', (data.table.width / 3) + 4);
                                    doc.setFontSize(8);
                                    doc.text(alimSolido,cell.textPos.x, cell.y - 4);
                                }

                                return false;
                            }
                        },
                    });
                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                    resolve(doc);
                }
        });
    }

    private generaAntropometria(doc:any, Fecha: string){
        return new Promise((resolve, reject) => {
            let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = this.fichaPService.listTarjetasColombia[5].rows;
            //Antropometria

                let antropometria = this.fichaPService.antropometria !== undefined ? this.fichaPService.antropometria.filter((ant: any) => ant.Fecha === Fecha) : [];
                
                let startY = mThis.first.finalY + 2.5;
                let margin: any = {right: 107};

                if (antropometria.length > 0 && this.fichaPService.listTarjetasColombia[5].Checked) {

                    let listTarj = this.fichaPService.listTarjetasColombia.slice(6);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha)

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }

                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Antropometria',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Circunferencia del brazo',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var cirBrazo = doc.splitTextToSize(antropometria[0].CircunferenciaBrazo ? antropometria[0].CircunferenciaBrazo : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(cirBrazo,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('¿Edema (hinchazón) en el empeine?',cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 4) {
                                    var edema = doc.splitTextToSize(antropometria[0].Edema ? antropometria[0].Edema : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(edema,cell.textPos.x, cell.y - 4);
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('¿Fue remitido(a) según la ruta?',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var remitido = doc.splitTextToSize(antropometria[0].FueRemitidoRutaSegunTamizaje ? antropometria[0].FueRemitidoRutaSegunTamizaje : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(remitido,cell.textPos.x, cell.y - 2);
                                }

                                return false;
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                resolve(doc);
            }
        });
    }

    private generaEducacion(doc:any, Fecha:string){
        return new Promise((resolve, reject) => {
	        let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = this.fichaPService.listTarjetasColombia[6].rows;

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};
            
            //Educacion
                let educacion = this.fichaPService.educacion !== undefined ? this.fichaPService.educacion.filter((ed: any) => ed.Fecha === Fecha) : [];
                //console.log('servicios '+ index,servicios);
                if (educacion.length > 0 && this.fichaPService.listTarjetasColombia[6].Checked) {

                    rows = educacion[0].Concurre ? educacion[0].Concurre == 'SÍ' ? 9 : 7 : 7;

                    let listTarj = this.fichaPService.listTarjetasColombia.slice(7);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha);

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }

                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {
                            // Rowspan
                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Educación',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('¿Analfabetismo?' ,cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var analfabetismo = doc.splitTextToSize(educacion[0].Analfabeto_txt ? educacion[0].Analfabeto_txt : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    if (educacion[0].Analfabeto !== 'NO'){
                                        doc.setTextColor(255,0,0);
                                    } 
                                    doc.text(analfabetismo,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    let titleLeerCom = doc.splitTextToSize('Sabe leer y escribir en lengua de su comunidad', (data.table.width / 2) + 4);
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text(titleLeerCom ,cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 4) {
                                    let sabeLeerCom = doc.splitTextToSize(educacion[0].SabeLeerEscribirComunidad ? educacion[0].SabeLeerEscribirComunidad : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(sabeLeerCom,cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 5) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Año de estudio alcanzado' ,cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 6) {
                                    let anioAlcanzado = doc.splitTextToSize(educacion[0].AnioAlcanzado ? educacion[0].AnioAlcanzado : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(anioAlcanzado,cell.textPos.x, cell.y - 1);
                                }

                                if (educacion[0].Concurre === 'SÍ'){
                                    if (data.row.index === 7) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Institución Educativa oficial',cell.textPos.x, cell.textPos.y);
                                    }

                                    if (data.row.index === 8) {
                                        var institucion = doc.splitTextToSize(educacion[0].EsInstitucionOficial ? educacion[0].EsInstitucionOficial : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        doc.text(institucion,cell.textPos.x, cell.y - 2);
                                    }
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Sabe leer y escribir en español',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var sabeLeerEspaniol = doc.splitTextToSize(educacion[0].SabeLeerEscribirEspanol ? educacion[0].SabeLeerEscribirEspanol : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(sabeLeerEspaniol,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Nivel Educativo Alcanzado',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 4) {
                                    var mne = doc.splitTextToSize(educacion[0].MNE ? educacion[0].MNE : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(mne,cell.textPos.x, cell.y);
                                }

                                if (data.row.index === 5) {
                                    let titleConcurre = doc.splitTextToSize('¿Concurre a un establecimiento educativo?', (data.table.width / 2) + 4);
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text(titleConcurre,cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 6) {
                                    let concurre = doc.splitTextToSize(educacion[0].Concurre ? educacion[0].Concurre : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    if (educacion[0].Concurre !== 'SÍ'){
                                        doc.setTextColor(255,0,0);
                                    } 
                                    doc.text(concurre,cell.textPos.x, cell.y);
                                }

                                return false;
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                    resolve(doc);
                }
        });
    }

    private generaTrabajo(doc:any, Fecha:string){
        return new Promise((resolve, reject) => {
	        let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = this.fichaPService.listTarjetasColombia[7].rows;

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};
            
            //Trabajo
                let trabajo = this.fichaPService.trabajo !== undefined ? this.fichaPService.trabajo.filter((tr: any) => tr.Fecha === Fecha) : [];

                //console.log('servicios '+ index,servicios);
                if (trabajo.length > 0 && this.fichaPService.listTarjetasColombia[7].Checked) {

                    let listTarj = this.fichaPService.listTarjetasColombia.slice(8);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha);

                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                        // Reset page to the same as before previous table
                        doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                        rows = previous.rows.length > rows ? previous.rows.length : rows;
                        startY = previous.pageStartY;
                        margin = {left: 107};
                    } else {
                        rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                        startY = previous.finalY + 2.5;
                        margin = {right: 107};
                    }

                    //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                    columns = [
                        {title: "columnaI", dataKey: "column1"},
                        {title: "columnad", dataKey: "column2"},
                    ];
        
                    data = mThis.generaTemplate(rows);

                    doc.autoTable(columns, data, {
                        startY: startY,
                        showHeader: 'never',
                        pageBreak:'avoid',
                        margin: margin,
                        styles: {
                            halign: 'left',
                        },
                        drawCell: function (cell: any, data: any) {

                            if (data.column.dataKey === 'column1') {

                                if (data.row.index === 0) {
                                    doc.setFontStyle('bold');

                                    doc.text('Trabajo',cell.textPos.x, cell.textPos.y + 2);

                                    doc.setDrawColor(0, 0, 0);
                                    doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                }

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Actividad Laboral' ,cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var actividadLab = doc.splitTextToSize(trabajo[0].ActividadLaboral ? trabajo[0].ActividadLaboral : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(actividadLab,cell.textPos.x, cell.y - 2);
                                }

                                if (data.row.index === 3) {
                                    let titleIngAct = doc.splitTextToSize('¿Recibió ingresos por esta actividad?', (data.table.width / 2) + 4);
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text(titleIngAct,cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 4) {
                                    var recIngresos = doc.splitTextToSize(trabajo[0].RecibioIngreso ? trabajo[0].RecibioIngreso : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(recIngresos,cell.textPos.x, cell.y);
                                }

                                return false;
                            }

                            if (data.column.dataKey === 'column2') {

                                if (data.row.index === 1) {
                                    doc.setFontStyle('bold');
                                    doc.setFontSize(8);
                                    doc.text('Frecuencia',cell.textPos.x, cell.textPos.y);
                                }

                                if (data.row.index === 2) {
                                    var frecuencia = doc.splitTextToSize(trabajo[0].Frecuencia ? trabajo[0].Frecuencia : '-', (data.table.width / 2) + 4);
                                    doc.setFontSize(8);
                                    doc.text(frecuencia,cell.textPos.x, cell.y - 2);
                                }

                                if (trabajo[0].Trabaja === 'SÍ'){
                                    if (data.row.index === 3) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('¿En Qué trabajó?',cell.textPos.x, cell.textPos.y);
                                    }

                                    if (data.row.index === 4) {
                                        var empleo = doc.splitTextToSize(trabajo[0].TipoEmpleo ? trabajo[0].TipoEmpleo : '-', (data.table.width / 2) + 4);
                                        doc.setFontSize(8);
                                        doc.text(empleo,cell.textPos.x, cell.y);
                                    }
                                }

                                return false;
                            }
                        },
                    });

                    previous = doc.autoTable.previous;
                    this.CountFicha++;
                    resolve(doc);
                } else {
                    resolve(doc);
                }
        });
    }

    private generaGrupoFamiliar(doc:any, Fecha:string){
        return new Promise((resolve, reject) => {
	        let previous = doc.autoTable.previous;

            let mThis = this;

            var columns = [];
            var data: any;

            let rows = this.fichaPService.listTarjetasColombia[8].rows;
            //grupoFamiliar

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};

                let grupoFamiliar = this.fichaPService.grupoFamiliar !== undefined ? this.fichaPService.grupoFamiliar.filter((gf: any) => gf.Fecha === Fecha) : [];
                
                if (grupoFamiliar.length > 0 && this.fichaPService.listTarjetasColombia[8].Checked) {

                    rows = grupoFamiliar.length + 3;

                    let listTarj = this.fichaPService.listTarjetasColombia.slice(9);//.filter((list:any) => list.Checked === true)
                    listTarj = listTarj.filter((list:any) => list.Checked === true && list.Fecha === Fecha)
                    
                    if (previous.pageStartX !== 107 && this.CountFicha > 0){
                            // Reset page to the same as before previous table
                            doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                            rows = previous.rows.length > rows ? previous.rows.length : rows;
                            startY = previous.pageStartY;
                            margin = {left: 107};
                        } else {
                            rows = !!listTarj && listTarj.length > 0 ? listTarj[0].rows > rows ? listTarj[0].rows : rows : rows;
                            startY = previous.finalY + 2.5;
                            margin = {right: 107};
                        }

                        let cellWidthSexo = 5;
                        let cellWidth4 = 20.222709056333326;
                        let cellWidth2 = 40.44541811266665;
                        let cellWidthImg = 3;
                        let pos = {x:0,textx:0,y:0,heigth:0};

                        let imgJefeHogar = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAMCAIAAAAyIHCzAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACRSURBVChTY/h+ZaVHaMvEK7/+EwGQVH95tnfJjLiUBrHQGrGolriWjXuffAIp+fnqzJY5AVErLwNV//9/bWLokr0n5xgDFeFDS87AVa/bOQFDGg0NctVRDQFZHQFZDdoIdRDxtYgw2bZzinHJknVXXn0HhysIfLm3bcoUkM6SGTN3XnsJFoOqBtpCDKCd6v//AaTQX9WAAEqdAAAAAElFTkSuQmCC";

                        let images: any = [];
                        //previous = this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                        columns = [
                            {title: "columnaI", dataKey: "column1"},
                            {title: "columnad", dataKey: "column2"},
                        ];
            
                        data = mThis.generaTemplate(rows);

                        doc.autoTable(columns, data, {
                            startY: startY,
                            showHeader: 'never',
                            pageBreak:'avoid',
                            margin: margin,
                            styles: {
                                halign: 'left',
                            },
                            drawCell: function (cell: any, data: any) {
                                // Rowspan
                                if (data.column.dataKey === 'column1') {

                                    if (data.row.index === 0) {
                                        doc.setFontStyle('bold');

                                        doc.text('Grupo Familiar',cell.textPos.x, cell.textPos.y + 2);

                                        doc.setDrawColor(0, 0, 0);
                                        doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);

                                        pos.textx = cell.textPos.x;
                                        pos.x = cell.x;
                                        pos.y = cell.y;
                                        pos.heigth = cell.heigth;
                                    }

                                    if (data.row.index === 1) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Composición del Hogar',cell.textPos.x, cell.textPos.y);
                                    }

                                    if (data.row.index === 2) {
                                        //doc.setDrawColor(0, 0, 0);
                                        doc.rect(cell.x, cell.y - 5, data.table.width, cell.height );
                                    }

                                    if (data.row.index >= 3) {
                                        let row = data.row.index - 3
                                        //let datoSexo = !!grupoFamiliar[row] ? grupoFamiliar[row].Sexo === 'Femenino' ? 'F' : 'M' : '';
                                        var sexo = doc.splitTextToSize(!!grupoFamiliar[row] ? grupoFamiliar[row].Sexo === 'Femenino' ? 'F' : 'M' : '', cellWidthSexo + 4);
                                        doc.setFontSize(8);
                                        doc.text(sexo,cell.textPos.x, cell.y - 2);

                                        var nombres = doc.splitTextToSize(!!grupoFamiliar[row] ? grupoFamiliar[row].ApellidosNombres : '', cellWidth2);
                                        doc.setFontSize(8);
                                        doc.text(nombres,pos.textx + cellWidthSexo, cell.y - 2);

                                        if (!!grupoFamiliar[row] && data.row.index < rows - 1){
                                            //doc.setDrawColor(0, 0, 0);
                                            doc.rect(cell.x, cell.y - 5, data.table.width, cell.height );
                                        }
                                    }

                                    return false;
                                }

                                if (data.column.dataKey === 'column2') {

                                    if (data.row.index === 2) {
                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Edad',pos.textx + cellWidthSexo + cellWidth2 + cellWidthImg + 2, cell.y);

                                        doc.setFontStyle('bold');
                                        doc.setFontSize(8);
                                        doc.text('Es mi',pos.textx + cellWidthSexo + cellWidth2 + cellWidthImg + cellWidth4 - 2, cell.y);
                                    }

                                    if (data.row.index >= 3) {
                                        let row = data.row.index - 3
                                        var edad = doc.splitTextToSize(!!grupoFamiliar[row] ? grupoFamiliar[row].Edad : '', cellWidth4 + 2);
                                        doc.setFontSize(8);
                                        doc.text(edad,pos.textx + cellWidthSexo + cellWidth2 + cellWidthImg + 2, cell.y - 2);

                                        if (!!grupoFamiliar[row] && grupoFamiliar[row].CodJefeHogar === grupoFamiliar[row].CodPersona){
                                            images.push({
                                                url: imgJefeHogar,
                                                x: pos.textx + cellWidthSexo + cellWidth2 ,
                                                y: cell.textPos.y,
                                                cellx: cell.x,
                                                celly: cell.y - 4,
                                            });
                                        }

                                        var parentesco = doc.splitTextToSize(!!grupoFamiliar[row] ? grupoFamiliar[row].ParentescoInverso.replace('<em>','').replace('</em>','') : '', cellWidth4);
                                        doc.setFontSize(8);
                                        doc.text(parentesco,pos.textx + cellWidthSexo + cellWidth2 + cellWidthImg + cellWidth4 - 2, cell.y - 2);
                                    }

                                    return false;
                                }
                            },
                            addPageContent: function() {
                                for (var i = 0; i < images.length; i++) {
                                    if (images[i].url !== undefined){
                                        doc.addImage(images[i].url,'JPG', images[i].x + 1, images[i].celly, 3, 3);
                                    }
                                }
                            }
                        });

                        previous = doc.autoTable.previous;
                        this.CountFicha++;
                        resolve(doc);
                } else {
                    resolve(doc);
                }
        });
    }

    private generaHeaderVisita(doc:any, Fecha:string){
        return new Promise((resolve, reject) => {
            let mThis = this;
            let headVisita: any=[];
                headVisita.push({
                    Fecha: 'Fecha de Relevamiento: ' + Fecha
                });

                doc.autoTable(this.getColumnsHVisitas(), headVisita, {
                    startY: this.first.finalY + 5,
                    // tableLineColor: [189, 195, 199],
                    // tableLineWidth: 0.75,
                    showHeader: 'never',
                    halign: 'left',
                    styles: {
                        // font: 'courier',
                        // lineColor: [44, 62, 80],
                        // lineWidth: 0.75
                        halign: 'center'
                    },
                    // headerStyles: {
                    //     fillColor: [44, 62, 80],
                    //     fontSize: 15
                    // },
                    bodyStyles: {
                        fillColor: [52, 73, 94],
                        textColor: 240
                    },
                    alternateRowStyles: {
                        fillColor: [0, 0, 0]
                    },
                    // columnStyles: {
                    //     email: {
                    //         fontStyle: 'bold'
                    //     }
                    // },                    
                });
                mThis.first = doc.autoTable.previous;
                resolve(doc);
        });
    }

    private generaFotos(doc:any){
        return new Promise((resolve, reject) => {
            let mThis = this;
            let previous = doc.autoTable.previous;

            if (mThis.first.autoTable !== undefined) {
                this.CountFicha = 0;
                mThis.first = mThis.first.autoTable.previous;
            }

            let startY = mThis.first.finalY + 5;
            let margin: any = {right: 107};

            if (!!this.fichaPService.fotos && this.fichaPService.listTarjetasColombia[9].Checked) {

                let headFotos: any=[];
                headFotos.push({
                    Fecha: 'Galeria de imágenes'
                });

                doc.autoTable(this.getColumnsHVisitas(), headFotos, {
                    startY: previous.finalY + 5,
                    // tableLineColor: [189, 195, 199],
                    // tableLineWidth: 0.75,
                    showHeader: 'never',
                    halign: 'left',
                    styles: {
                        // font: 'courier',
                        // lineColor: [44, 62, 80],
                        // lineWidth: 0.75
                        halign: 'left'
                    },     
                    bodyStyles: {
                        fillColor: [255, 255, 255],
                    },
                    alternateRowStyles: {
                        fillColor: [255, 255, 255]
                    },       
                });

                previous = doc.autoTable.previous;
                this.first = doc.autoTable.previous;

                this.CountFicha = 0;

                this.generaFotosColumn3(doc).then((doc:any) => this.generaFotosColumn2(doc))
                .then((doc:any) => {
                    resolve(doc);
                })

                    
            } else {
                resolve(doc);
            }
        });
    }

    private generaFotosColumn3(doc:any){
        return new Promise((resolve, reject) => {
            let previous = doc.autoTable.previous;
            let mThis = this;
             var columns = [];
            var data: any;

            let startY = mThis.first.finalY + 2.5;
            let margin: any = {right: 107};

            let rows = 10;
    
            //this.fichaPService.fotos.forEach((array:any, i:any) => {
            let fotosArray = this.fichaPService.fotos.filter((foto:any) => foto.base64 !== undefined && foto.columns === 3);

            if (fotosArray.length > 0) {
                // Observable.interval(100).take(fotosArray.length).subscribe((i) => {
                //         let array = fotosArray[i];
                var promises :any = [];

                fotosArray.forEach((array:any,i:any) => {


                    promises.push(

                        new Promise((resolve, reject) => { 

                        let fotos: any = [];
                        if (i === 0){
                            startY = previous.finalY + 2.5;
                            margin = {right: 139};
                        } else if (((i + 2) % 3) === 0 && this.CountFicha > 0){//0,3,
                            doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                            rows = previous.rows.length > rows ? previous.rows.length : rows;
                            startY = previous.pageStartY;
                            margin = {left: 76, right: 76};
                        } else if ((i + 1) % 3 === 0 && this.CountFicha > 0){
                            // Reset page to the same as before previous table
                            doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                            rows = previous.rows.length > rows ? previous.rows.length : rows;
                            startY = previous.pageStartY;
                            margin = {left: 139};
                        } else if (i % 3 === 0 && this.CountFicha > 0){
                            startY = previous.finalY + 2.5;
                            margin = {right: 139};
                        }

                            //previous = this.this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                        columns = [
                            {title: "columnaI", dataKey: "column1"},
                            {title: "columnad", dataKey: "column2"},
                        ];
                        data = this.generaTemplate(rows);
                

                        doc.autoTable(columns, data, {
                            startY: startY,
                            showHeader: 'never',
                            pageBreak:'avoid',
                            margin: margin,
                            styles: {
                                halign: 'left',
                            },
                            drawCell: function (cell: any, data: any) {
                                    // Rowspan
                                    if (data.column.dataKey === 'column1') {
                                        if (data.row.index === 0) {
                                            doc.setFontStyle('bold');
                                            doc.setDrawColor(0, 0, 0);
                                            doc.setFontSize(8);
                                            //doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                            doc.autoTableText(array.FotoTitulo, cell.textPos.x + data.table.width / 2, cell.y + data.table.height - 15, {
                                                halign: 'center',
                                                valign: 'middle'
                                            });
                                            // doc.setFontSize(8);
                                            // doc.text(array.FotoTitulo,cell.textPos.x, data.table.height);

                                            fotos.push({
                                                url: array.base64,
                                                x: cell.textPos.x,
                                                y: cell.textPos.y,
                                                width: data.table.width,
                                                height: data.table.height
                                            });
                                        }
                                    }
                                    return false;
                            },
                            addPageContent: function() {
                                if (fotos[0].url !== undefined){
                                    doc.addImage(fotos[0].url,'JPG', fotos[0].x, fotos[0].y - 1, fotos[0].width - 3, fotos[0].height - 20);//Width, heith
                                    mThis.first = doc;
                                    resolve();
                                }
                                // for (var i = 0; i < fotos.length; i++) {
                                //     if (fotos[i].url !== undefined){
                                //         doc.addImage(fotos[i].url,'JPG', fotos[i].x, fotos[i].y - 1,fotos[i].width - 3, fotos[i].height - 12);
                                //     }
                                // }
                            }
                        })

                        previous = doc.autoTable.previous;
                        mThis.CountFicha++;

                        })

                    )
                
                });

                Promise.all(promises).then(() => 
                    resolve(doc)
                );
                } else {
                    resolve(doc)
                }
        });
    }

    private generaFotosColumn2(doc:any){
        return new Promise((resolve, reject) => {
            let previous = doc.autoTable.previous;
            console.log('previous.pageStartX',previous.pageStartX );
            //this.CountFicha = 0;
            let mThis = this;
             var columns = [];
            var data: any;

            let startY = previous.finalY + 2.5;
            let margin: any = {right: 107};

            let rows = 10;
    
            //this.fichaPService.fotos.forEach((array:any, i:any) => {
            let fotosArray = this.fichaPService.fotos.filter((foto:any) => foto.base64 !== undefined && foto.columns === 2);

            if (fotosArray.length > 0) {
                    // Observable.interval(100).take(fotosArray.length).subscribe((i) => {
                    //     let array = fotosArray[i];
                    var promises :any = [];

                    fotosArray.forEach((array:any,i:any) => {

                        promises.push(

                        new Promise((resolve, reject) => { 

                        var fotos: any = [];
                        if (previous.pageStartX === 14.111139333333332 && this.CountFicha > 0 && i === 0) {
                            doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                            rows = previous.rows.length > rows ? previous.rows.length : rows;
                            startY = previous.pageStartY;
                            margin = {left: 107};
                        } else if (previous.pageStartX !== 107 && this.CountFicha > 0 && i > 0){
                            // Reset page to the same as before previous table
                            doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);
                            rows = previous.rows.length > rows ? previous.rows.length : rows;
                            startY = previous.pageStartY;
                            margin = {left: 107};
                        } else if (this.CountFicha > 0){
                            startY = previous.finalY + 2.5;
                            margin = {right: 107};
                        }

                        //previous = this.this.CountFicha === 1 ? first : previous === undefined ? first : previous;

                        columns = [
                            {title: "columnaI", dataKey: "column1"},
                            {title: "columnad", dataKey: "column2"},
                        ];
                        data = this.generaTemplate(rows);

                        doc.autoTable(columns, data, {
                            startY: startY,
                            showHeader: 'never',
                            pageBreak:'avoid',
                            margin: margin,
                            styles: {
                                halign: 'left',
                            },
                            drawCell: function (cell: any, data: any) {
                                    // Rowspan
                                    if (data.column.dataKey === 'column1') {
                                        if (data.row.index === 0) {
                                            doc.setFontStyle('bold');
                                            doc.setDrawColor(0, 0, 0);
                                            doc.setFontSize(8);
                                            //doc.roundedRect(data.table.cursor.x, data.table.cursor.y, data.table.width, data.table.height - 10,2,2);
                                            doc.autoTableText(array.FotoTitulo, cell.textPos.x + data.table.width / 2, cell.y + data.table.height - 15, {
                                                halign: 'center',
                                                valign: 'middle'
                                            });
                                            // doc.setFontSize(8);
                                            // doc.text(array.FotoTitulo,cell.textPos.x, data.table.height);

                                            fotos.push({
                                                url: array.base64,
                                                x: cell.textPos.x,
                                                y: cell.textPos.y,
                                                width: data.table.width,
                                                height: data.table.height
                                            });
                                        }
                                    }
                                    return false;
                            },
                            addPageContent: function() {
                                if (!!fotos[0] && fotos[0].url !== undefined){
                                    doc.addImage(fotos[0].url,'JPG', fotos[0].x, fotos[0].y - 1, fotos[0].width - 3, fotos[0].height - 20);//Width, heith
                                    mThis.first = doc;
                                    //mThis.CountFicha++;
                                    resolve();
                                }
                                // for (var i = 0; i < fotos.length; i++) {
                                //     if (fotos[i].url !== undefined){
                                //         doc.addImage(fotos[i].url,'JPG', fotos[i].x, fotos[i].y - 1,fotos[i].width - 3, fotos[i].height - 12);
                                //     }
                                // }
                            }
                        })

                        previous = doc.autoTable.previous;
                        mThis.CountFicha++;
                        // if (i === fotosArray.length - 1){
                        //     resolve(doc); 
                        // }
                    })
                    );
                });

                Promise.all(promises).then(() => 
                    resolve(doc)
                );

                } else {
                    resolve(doc)
                }
        });
    }

    private getColumnsHVisitas() {
        return [
            {title: "Fecha", dataKey: "Fecha"},
        ];
    };

    private generaTemplate(rows: number) {
        var data: any = [];
        for (var j = 0; j < rows; j++) {
            data.push({
                column1: 'test',
                column2: 'test',
            });
        }
        return data;
    };

    private generaTemplate3(rows: number) {
        var data: any = [];
        for (var j = 0; j < rows; j++) {
            data.push({
                column1: 'test',
                column2: 'test',
                column3: 'test',
            });
        }
        return data;
    };

    private generaTemplate7(rows: number) {
        var data: any = [];
        for (var j = 0; j < rows; j++) {
            data.push({
                column1: 'test',
                column2: 'test',
                column3: 'test',
                column4: 'test',
                column5: 'test',
                column6: 'test',
                column7: 'test',
            });
        }
        return data;
    };

    private generaDatosPersonales(){
        return new Promise((resolve, reject) => {
            let mThis = this;
            var images: any = [];
            var i = 0;
            
            this.tablaDP().then((doc: any)=>{
                resolve(doc);
            })
        });
    }

    public generaHeaderFooter(doc:any){
        return new Promise((resolve, reject) => {
            var pageCount = doc.internal.getNumberOfPages();
            for(let i = 0; i < pageCount; i++) { 
                doc.setPage(i); 
                var imgData = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG8AAAAzCAYAAACZiVbsAAAABHNCSVQICAgIfAhkiAAAAF96VFh0UmF3IHByb2ZpbGUgdHlwZSBBUFAxAAAImeNKT81LLcpMVigoyk/LzEnlUgADYxMuE0sTS6NEAwMDCwMIMDQwMDYEkkZAtjlUKNEABZiYm6UBoblZspkpiM8FAE+6FWgbLdiMAAAOg0lEQVR4nO2ca4wb13XHfzNDcne5D1IP65UqSzmupbittI4tpIGlioLQyEGRaBdNYFtNq1WQxq3RRuvAaRv0IaqxP7hInRWQtkErWCvYcFXYhVdNW7RJiuU6iaA0sk1FtiTHVsWValnGSha5Dy6fM/0w93Iuh0NyuSsphc0/QMzc1zl37plz7rnn3qHGLYJ1gTBF+kyLCBYRAB0S2npGb1Uf3m/w3SzC1s/oMy2iaESx6DPz9FZV0tl+s/h/EHBDhWe9Qb+F/TMtQnamd10Njmi/SPxG8v+gQVssgcJJtmid/Jam8xAIgTVGWg8Q0daRWiz/DzIWrHm5HzNkZtlfnCUc6G66+XBLcItH05pXOMmW4izfsQqEAfwrQG9risSEsd52WFpYHJrSvLnjxAsptsm03tG04NB1Bptr0UItzEt4+Z/wQGmWw1aGjnKmDr5wk9w0jrWclBuHhsLLneBQcZovYFaaWKMLtOZmzLReYqjJ/rVQB3WHP3ec/yjNsNOdr/nAN1+/0sGw9lGSTbdqoSZqOizZH/GqOUefV1nLSfn/Ad0rM3eCQ7UEtyAnhZa5vBmo0rz8T3igmOZorQaB1U3PdePGeqLNd62FRqgQnjVGOAuTVsl7LjR6Gs915iyUMmDmoDQFXRtY15rrbg4qhJRtY9TKeAtO84HPI5IihVR8z75ikDfaOK/5SPiCHGsJ7uahrHnWK/TOpbjgXhJI+Jfb8x2AVYTidci/A1YW0+jigubn3y2TQ12f4ae3pustlLWskGW4luD0Nltw5izkr0BhEnw9nDeCjHR+jsdvXXdbUFEW1lycjFVUIigKjG7IXwZzBtMI8T2jk4c7djBxE/oTr5E/BESAGLAJSIu6Q0CyRrthKG/09om2u0R6XKTjgu6wUnZKtIspdNyed1wpjwGDQK/o16jS32FXu5TIk/0dFD/5jAmlbETw7VPqhhWatgdvjRHOfB/L8/efWOlnMWf/jRe5+bBq/P6sRn6yQTs5iKka5RFBw52v7u7H69CO1SiLA9EaZSnBF1f7qOtZYi7eIy6aUR0gH/QOFpfSYOZIBFazrvM3GPCq0xS2/ukDDWpsBx5V0o+KPNkuLdLHRLoX6FfqHwEGRD2wB2AIZ5/xURf9bwgaYGvcAHAQah7NOKjc9+OsXydEv8ZFehtwh+s5Ton7ECwoYLHH3c6e84rcX1HNhGKKki/EY8GdVapfH1sf24jR8RCa7xNo+no0rR1fux3CLsxdAf6pTuu4Ky3NyC+70uCYOdWkJbEHfhT7YUPAryrl8lm+Ka4RpWwTtmkaxX7LvTAK7BP3XTgvxajoVxzKuy5dNWgsBjE14QOwStwlM6w8mBlS/mX0zWte2/rYRvTgl9H9UQxfL4bfewlvmhal7OcW1fX5I6nct9epN4OtrXtEepf4RcHTGo24eNwj7lNKnoT6Un1TuT9F7bm9ESrOAUnhrQRbcFaO892frVD5amzaF8afH6TnQ1/H3z6/N6w49xLjT/xwYX1uGhHlfqZB3UGcOUoKcQ+2SXTv9qsm9iDwmyItN8dUvgmFnormLJmNcUG7UnjWGOG5EgGrYAuua6CO4DY/0odlDaEX9hDoAX+9l1pBqVBkbH90AZ2WmBAd76NaKxLKfQR7LupX2r0F3CfSajvZdgRbSCOCdvUpNwfytFvcld+PbTpVbVNfmgEoO3wxHA85qdSJ4rwEXogBh9UMXz7IINNg5ij6etjh2eyeh6PoegyNbWgaaDoEl9Xh40Ix8435V/bECLAfe44ZU/InqHQu9lD5tkvtkXkVDw88jeNI7FPyT1GtdVAtNGlye139Gsd+aSRS2Jq6T9SN4syREvs9eEWV9Ai2AMsvl06WT5WmwOhGq5rjNu0Lc+/vj2AYY2ha+fgDHUtsAc4HxWyK+Ne/Nr/KgP2g4+InBzAGHMDxIsH2OPupRlq0HcBxJAZwhASOZzkl6MyHrheGsAWotj9So/0Qttep1k169G0C2Iv3vBhTE1rmu1xBYyUa+EI8GNgsvMHNj/SBFUfTKkPRRgB6fqHhU5WRvb71Fs51HyjopWmWyTiLleMJADY/MugpOIDg8vlTz8+OtwTXFCLYpjJKdVSnCjpKfLOU5/a/OPCxvWANewou0A2+Ok5KKQ+5KUhdynP1Z2myeS9vq4XaGMWeO8eYx0Lep6vRTBPt+ZN3fhtNC1TV9HJSChmYS5lkr09RzP2UYv5FMpMjTIwv9EBtVLlP4ISSIiIvCT+XLaaoch+/STwiOAGGBJWOWB+OJyrHpXLv7plXNnD23aXVggNoD9kCnHsPZiZzlDLj5PNPc/b5ehGTZqF6bEdwArfSEzuAPZDblHRM3KtfRWiutBtpnOjIXpzFd0rJH8AJBIdcbYeojsLU4ndQ9DGF/Sxuj/dubIFEqPQ4NWzHZ5jq5cspoM9nztk+CMB3zqyrZm2ZkEuDaZpcO38Os/QwZ56/FfPYHlze1Q1EnOrwWoRKIXVSPdCIOoexhTGfz9PkEmQIby80SuVaVSICNTcDkgA+TWca6AaYuO7aKs9cg/x0HqP9GdJnH1uEOVwoYtwYM3nAlU7hCC/quoK91PiqkpbbRP3YMVDZt1rCU0NuYGvckIuHRBTvqMugK30Q2xL0IUy3TzP4V+AhgNPvCE+ymIPZK9x3Z9r8+F2p7U89+drxGp282diDPRCLRcyVjuDEG6UwVO8uTqUJGxJ5CRxt2ERtjGC/dJJGSNCXmj2OY/qjdehIpHHmwbIC6XonXyvNKPa6mEOfeZu//fzrfG/fD/UnPvXa9/Mn2DgPBmWM+dc+MBZYOzQWWHsjjvzNd8FcD+p+Wgx7YNWARJTKQfzfGnTclidSh6ca6kpT7YxJ/lKwbiSVe2mqr2O/GGEAvWMHE7rO0XS2rSy4kd99ncFP2G2tIh3FWV7N/ahiH6wMIajnjhsffutlrTfzOuusFQXf0WV54ylMTtZ5uHoYx4lENH82e36IK/dRHE1KUxnaqodIjfwxKsNt0uSqvNV5LupBYwRvq7MHaTYBOj/D7n956radTF9e2r5kKZsjrhfMQi/N8VT2JfrPHGn/nddeCG3vnQ0MLSkaG42C5nnuJW2YL20vXlqMYzNMdbyvFiINytU5Ly6ucs8PKg8Fx/GOa3oh3rCGPV8OYWuNhNsRieLttAxij8MgtvCl17kJ6CsHKP/wxV/5FoaPDGG+9EJ1fHry9TZ+/FT4107/89JkV8l/eHnRt8nAW3B5zSxuLV2MNn6uuhimMg4o4fXGNopGxJRfXOTFlXJVu+NUD2TExY8afZM4gv3C7KXyHEotRGvkDwveQ1Q7MOHyOm/iXb2XQBCAH1xYw5de2MHff/a/SCX9JJ4OMXnGWf6tyRp1e5Lym18l36C7jZHCW/vUgd1G9frqGNVw19EE/VNUOx7SgzyG45Eexh68bR71vDBC5cuhmsxxpUx1aNxbcf3Ypncf3kg4i3SjI4FZKLu3z76yHt4o8uuvvlnRYnneoLtoK+y0ZnFZN7lmmBQ0cxpLu17AfHNf/p2FbDh6YZjKMyjgnM7y8vbSNLc2jLvoTOA4CjFsjZC8VcHJhfp8EVXuR3GWBurSw62dUWrjAJByhHf2hWHu2PnXWKaOphM0C2w5laxoUTRhNg/xjjxtS0osW59n3bY57t8yB9CtB0jh48yXc4S17Qv65lxudqaUaxTHc0sqee51UxJ7UKRmjtMYI1QvESQSomzIo84w1fOiys9dllLKVR6q67/ERWMY+5miOM8vrVEc3B+abOh/Dl/bQwSXsfP6eXZPngbgWgmuohFabfLJ3SnW28KqC83HDAFeNoI85w/y39qdnhNyC4tAtcOx/tNv0R76yMDMZXZMnuNiEd5Y2suxNXcDsHXdZf7gvlN8+q4Ljanr0LYa9UMyxyxpdnjJuLPmSa0WGsD748r1u048OHXx472pK7y94naeXVk9vXw4NM1v33uOz3/sHL1Lpj3J+ML2aes6aH3+NT/04bGU8HYbr71x6JN628BKzVy1sZDio5mrXPUHueoPlqukc2384MKH+Jvjm3jm5AYmUj28OxMk3J4j3JFH84O/8TGXib/8VoXmxcU1gW3nj4pfBHt+GBRlVzxoDeIcTT8BfFvknRDt5dlU9/Wo0iaLE9q6Iuj9nsg/V+MZJF8Jld6DLl6rRHlYlA9iz2knRBtZlhVtkiJvUFw3CJpZYFXNz5r/Krj6i9cs/uGuNqfKVX+Q74Y/wstdqysE6YYeXku3P0NPMMvtS6+yJjzN5t5LAOy6+00iS9+TVd2aF1cGJIrt8fXjCDMu8r2coRhOpD+KLTDpdMh2ta6S3qhoM4yzJuwX11prNVkvibMTLumpfY5jOz9DOB5tnMpgcz+OQxZV6qWUOiOCRqpiP0/FH2XeOfRk56o73sxZX+kN6P6ABssLGXZPnmb35GmuGe3m2c4V+sW2EBNtIc7J4xEdSzA1P+liiPRUiEtTKwH4x4R9cPkryirMz+xm1zJOendJ8SBJnIh8udO1+oxjXsI4nqlEmNpH6xI4A5YQvGOiLNWAJzjCkUjiLOxVvrJf8l7WkVc3nwjK5qvACCLqUvcD5T+evfInj7ev+rvLefPP0bRd3Ya2PKyDocGyUlbfMnWxXNewIDIbsPJGQDM1nSvtXVz32wv7ic5lXOjsqaD98pK1JNu63Ev5BM4mZwRHGDJKEaXxbnoE58h6CmfBPIizp6ZqURRn6RHDMYMRUS5d9nqICl4J0T4prtISJEUfpGbHcTQ8haPdCaUeou/yXq4r44Jfc39ftd+/YqPl17/YrrGhXdPWoGm/FNCgW4c1WV/DyIsb7/m0/Nbi/zT59wTzRoTKefJ9h0X/69/j7bdtQfPdGy5pX7jN1M2MZt3WbemhNgtrSrfaOkzd3215sylp1vT9pYs9noUtNMSihbcQPN22Zm/J0u7xwaW9+bef/Hn04f2A/wMP4dlPkNTOwAAAAABJRU5ErkJggg==';
            

                doc.addImage(imgData, 'PNG', 14, 3.5, 28, 8);

                doc.setFontSize(8);
                // doc.setTextColor(40);
                // doc.setFontStyle('normal');
                doc.text('Ficha de Persona de: ', doc.internal.pageSize.width - 14, 7, 'right');
                doc.setFontSize(8);
                doc.text(this.datosPersonales[0].Apellidos + ', ' + this.datosPersonales[0].Nombres, doc.internal.pageSize.width - 14, 11, 'right');

                // FOOTER
                var str = "Página " + doc.internal.getCurrentPageInfo().pageNumber + " de " + pageCount;
                // Total page number plugin only available in jspdf v1.0+
                // if (typeof doc.putTotalPages === 'function') {
                //     str = str + " of " + totalPagesExp;
                // }
                doc.setFontSize(8);
                doc.text(str, doc.internal.pageSize.width - 10, doc.internal.pageSize.height - 5, 'right');

                if (i === pageCount - 1){
                    resolve(doc);
                }
            }
         });
    }

    private tablaDP(){
        return new Promise((resolve, reject) => {
            let mThis = this;
            var images: any = [];                                                              
            var i = 0;
            let widhtImag = 46.77969689199995;
            let restWidth = 135;
            let cellWidth = 27;
            let widthfoto = mThis.datosPersonales[0].columns === 3 ? 30 : 46;

            if (widthfoto === 30){
                widhtImag = 31.77969689199995;
                restWidth = 150;
                cellWidth = 30;
            }

            var doc = new jsPDF();
                doc.autoTable(this.getColumnsDP(), this.datosPersonales, {
                    // columnStyles: { 
                    //     0: {columnWidth: 49.77969689199995}, 
                    //     1: {columnWidth: 22}, 
                    //     2: {columnWidth: 22} , 
                    //     3: {columnWidth: 22} , 
                    //     4: {columnWidth: 22} , 
                    //     5: {columnWidth: 22}, 
                    //     6: {columnWidth: 22} , 
                    //     7: {columnWidth: 22}
                    // }, // etc }
                    theme: 'plain',
                    startY: 20,
                    showHeader: 'never',
                    styles: {
                        //fontSize: 8,
                        overflow: 'linebreak',
                        //columnWidth: 'number'
                    },
                    // drawHeaderRow: function(row: any, data: any){
                    //     return false
                    // },
                    // drawHeaderCell: function (cell: any, data: any) {
                    //     cell.styles.fontSize= 2;
                    // },
                    drawRow: function (row: any, data: any) {
                        // Colspan
                        /*doc.setFontStyle('bold');
                        doc.setFontSize(10);
                        if (row.index === 0) {
                            doc.setTextColor(200, 0, 0);
                            doc.rect(data.settings.margin.left, row.y, data.table.width, 20, 'S');
                            doc.autoTableText("Priority Group", data.settings.margin.left + data.table.width / 2, row.y + row.height / 2, {
                                halign: 'center',
                                valign: 'middle'
                            });
                            data.cursor.y += 20;
                        } else if (row.index === 5) {
                            doc.rect(data.settings.margin.left, row.y, data.table.width, 20, 'S');
                            doc.autoTableText("Other Groups", data.settings.margin.left + data.table.width / 2, row.y + row.height / 2, {
                                halign: 'center',
                                valign: 'middle'
                            });
                            data.cursor.y += 20;
                        }

                        if (row.index % 5 === 0) {
                            var posY = row.y + row.height * 6 + data.settings.margin.bottom;
                            if (posY > doc.internal.pageSize.height) {
                                data.addPage();
                            }
                        }*/
                    },
                    drawCell: function (cell: any, data: any) {
                        // Rowspan
                        if (data.column.dataKey === 'Fot') {
                            if (data.row.index === 0) {
                                doc.setDrawColor(0, 0, 0);
                                doc.roundedRect(data.table.cursor.x, data.table.cursor.x, data.table.width, data.table.height - 5,2,2);
                                // doc.autoTableText(data.row.index / 5 + 1 + '-', cell.x + cell.width / 2, cell.y + cell.height * 5 / 2, {
                                //     halign: 'center',
                                //     valign: 'middle'
                                // });
                                images.push({
                                    url: mThis.datosPersonales[0].base64,
                                    x: cell.textPos.x,
                                    y: cell.textPos.y,
                                    cellx: cell.x,
                                    celly: cell.y
                                });
                                i++;
                            }
                            return false;
                        }
                        if (data.column.dataKey === 'FNac') {
                            if (data.row.index === 0) {

                                //doc.rect(images[0].cellx + widhtImag, cell.y, restWidth, cell.height);
                                // doc.autoTableText(mThis.datosPersonales[0].Apellidos+' '+mThis.datosPersonales[0].Nombres, images[0].x + widhtImag, cell.textPos.y + 2, {
                                //     halign: 'left',
                                //     valign: 'middle'
                                // });
                                doc.setFontStyle('bold');

                                doc.text(mThis.datosPersonales[0].Apellidos+' '+mThis.datosPersonales[0].Nombres,images[0].x + widhtImag, cell.textPos.y);
                                return false;
                            }
                            if (data.row.index === 1) {
                                //doc.rect(images[0].cellx + widhtImag, cell.y, restWidth, cell.height);
                                doc.autoTableText(mThis.datosPersonales[0].Edad+' (edad actual)', images[0].x + widhtImag, cell.textPos.y, {
                                    halign: 'left',
                                    valign: 'middle'
                                });

                                return false
                            }
                            if (data.row.index === 2) {
                                //doc.rect(images[0].cellx + widhtImag, cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.setFontStyle('bold');
                                doc.text('F. Nacimiento',images[0].x + widhtImag, cell.textPos.y);

                                return false
                            }
                            if (data.row.index === 3) {
                                //doc.rect(images[0].cellx + widhtImag, cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.text(mThis.datosPersonales[0].FechaNacimiento ? mThis.datosPersonales[0].FechaNacimiento : '',images[0].x + widhtImag, cell.textPos.y - 2);

                                return false
                            }
                            if (data.row.index === 4) {
                                //doc.rect(images[0].cellx + widhtImag, cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.setFontStyle('bold');
                                doc.text('Sexo',images[0].x + widhtImag, cell.textPos.y);

                                return false
                            }
                            if (data.row.index === 5) {
                                //doc.rect(images[0].cellx + widhtImag, cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.text(mThis.datosPersonales[0].Sexo ? mThis.datosPersonales[0].Sexo : '',images[0].x + widhtImag, cell.textPos.y - 1);

                                return false
                            }
                        }
                        if (data.column.dataKey === 'DepNac') {
                            if (data.row.index < 2) {
                                return false;
                            }
                            if (data.row.index === 2) {
                                //doc.rect(images[0].cellx + widhtImag + cellWidth, cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.setFontStyle('bold');
                                doc.text('Dep. Nacimiento',images[0].x + widhtImag + cellWidth, cell.textPos.y);

                                return false
                            }
                            if (data.row.index === 3) {

                                let depNacimiento = doc.splitTextToSize(mThis.datosPersonales[0].DepartamentoNacimiento ? mThis.datosPersonales[0].DepartamentoNacimiento : '', cellWidth);
                                doc.setFontSize(8);
                                doc.text(depNacimiento,images[0].x + widhtImag + cellWidth, cell.textPos.y - 2);


                                return false
                            }
                            if (data.row.index === 4) {
                                //doc.rect(images[0].cellx + widhtImag, cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.setFontStyle('bold');
                                doc.text('País de origen',images[0].x + widhtImag + cellWidth, cell.textPos.y);

                                return false
                            }
                            if (data.row.index === 5) {
                                //doc.rect(images[0].cellx + widhtImag, cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.text(mThis.datosPersonales[0].PaisOrigen ? mThis.datosPersonales[0].PaisOrigen : '',images[0].x + widhtImag + cellWidth, cell.textPos.y - 1);

                                return false
                            }
                        }
                        if (data.column.dataKey === 'MunNac') {
                            if (data.row.index < 2) {
                                return false;
                            }
                            if (data.row.index === 2) {
                                //doc.rect(images[0].cellx + widhtImag + (cellWidth * 2), cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.setFontStyle('bold');
                                doc.text('Mun. Nacimiento',images[0].x + widhtImag + (cellWidth * 2), cell.textPos.y);

                                return false
                            }
                            if (data.row.index === 3) {
                                //doc.rect(images[0].cellx + widhtImag + (cellWidth * 2), cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.text(mThis.datosPersonales[0].MunicipioNacimiento ? mThis.datosPersonales[0].MunicipioNacimiento : '',images[0].x + widhtImag + (cellWidth * 2), cell.textPos.y - 2);

                                return false
                            }
                            if (data.row.index === 4) {
                                //doc.rect(images[0].cellx + widhtImag, cell.y, cellWidth, cell.height);
                                let titleCultura = doc.splitTextToSize('Cultura, Pueblo o Rasgos Físicos', cellWidth);
                                doc.setFontSize(8);
                                doc.setFontStyle('bold');
                                doc.text(titleCultura,images[0].x + widhtImag + (cellWidth * 2), cell.textPos.y);

                                return false
                            }
                            if (data.row.index === 5) {
                                //doc.rect(images[0].cellx + widhtImag, cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.text(mThis.datosPersonales[0].Cultura ? mThis.datosPersonales[0].Cultura : '',images[0].x + widhtImag + (cellWidth * 2), cell.textPos.y - 1);

                                return false
                            }
                        }
                        if (data.column.dataKey === 'TipoDoc') {
                            if (data.row.index < 2) {
                                return false;
                            }
                            if (data.row.index === 2) {
                                //doc.rect(images[0].cellx + widhtImag + (cellWidth * 3), cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.setFontStyle('bold');
                                doc.text('Tipo Documento',images[0].x + widhtImag + (cellWidth * 3) - 2, cell.textPos.y);

                                return false
                            }
                            if (data.row.index === 3) {

                                let tipoDoc = doc.splitTextToSize(mThis.datosPersonales[0].TipoDocumento ? mThis.datosPersonales[0].TipoDocumento : '', cellWidth);
                                doc.setFontSize(8);
                                doc.text(tipoDoc,images[0].x + widhtImag + (cellWidth * 3) - 2, cell.textPos.y - 2);

                                return false
                            }
                            if (data.row.index === 4) {
                                //doc.rect(images[0].cellx + widhtImag, cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.setFontStyle('bold');
                                doc.text('Clan',images[0].x + widhtImag + (cellWidth * 3) - 2, cell.textPos.y);

                                return false
                            }
                            if (data.row.index === 5) {
                                //doc.rect(images[0].cellx + widhtImag, cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.text(mThis.datosPersonales[0].Clan ? mThis.datosPersonales[0].Clan : '',images[0].x + widhtImag + (cellWidth * 3) - 2, cell.textPos.y - 1);

                                return false
                            }
                        }
                        if (data.column.dataKey === 'NroDoc') {
                            if (data.row.index < 2) {
                                return false;
                            }
                            if (data.row.index === 2) {
                                //doc.rect(images[0].cellx + widhtImag + (cellWidth * 4), cell.y, cellWidth, cell.height);

                                doc.setFontSize(8);
                                doc.setFontStyle('bold');
                                doc.text('Nro. Documento',images[0].x + widhtImag + (cellWidth * 4), cell.textPos.y);

                                return false
                            }
                            if (data.row.index === 3) {
                                //doc.rect(images[0].cellx + widhtImag + (cellWidth * 4), cell.y, cellWidth, cell.height);

                                // doc.setFontSize(8);
                                // doc.text(mThis.datosPersonales[0].PaisOrigen,images[0].x + widhtImag + (cellWidth * 4), cell.textPos.y);

                                let nroDoc = doc.splitTextToSize(mThis.datosPersonales[0].NroDocumento ? mThis.datosPersonales[0].NroDocumento : '', cellWidth);
                                doc.setFontSize(8);
                                doc.text(nroDoc,images[0].x + widhtImag + (cellWidth * 4), cell.textPos.y - 2);

                                return false
                            }
                        }
                        if (data.column.dataKey === 'Cultura') {
                            return false;
                        }
                        if (data.column.dataKey === 'Clan') {
                            return false;
                        }
                    },
                //    createdCell: function (cell: any, data: any) {
                //         if (cell.dataKey === 'Nacionalidad') {
                //             cell.styles.fontSize= 5;
                //             cell.styles.textColor = [255,0,0];
                //         } 
                //     },
                    addPageContent: function() {
                        // for (var i = 0; i < images.length; i++) {
                        //     if (images[i].url !== undefined){
                        //         doc.addImage(images[i].url,'JPG', images[i].x, images[i].y, 20, 20);
                        //     }
                        // }
                        if (images[0].url !== undefined){
                            doc.addImage(images[0].url,'JPG', images[0].x - 1, images[0].celly - 5, widthfoto, 36);
                            mThis.first = doc;
                            resolve(doc);  
                        }
                    }
                });
                //resolve(doc);  
        });
    }

    private getDatosPersonales(){
        return new Promise((resolve, reject) => {
            if (this.datosPersonales.length === 0) {
                //this.datosPersonales = [];
                this.datosPersonales.push(this.fichaPService.datosPersonales[0]);//Foto
                //this.apiBackEndService.getImagenBase64(this.fichaPService.datosPersonales[0].Foto.replace('https://sprodresources.blob.core.windows.net/multimedia/','')).subscribe((result:any) => {
                    this.loadAllImages(this.datosPersonales[0],'Foto').then(()=>{
                    //this.datosPersonales[0].base64 = !!result.FileBase64 ? 'data:image/jpeg;base64,' + result.FileBase64 : this.fotoDefault;   
                    for (var j = 1; j < 6; j++) {
                        this.datosPersonales.push({
                            Foto: '',
                            FechaNacimiento: '',
                            Documento: '',
                            Sexo: '',
                            Nacionalidad: '',
                            PaisOrigen: '',
                            EtniaCod: ''
                        });
                    }
                        resolve();
                    });
                //})
            } else {
                resolve();
            }
        });
    }

    private getDatosFotos(){
        return new Promise((resolve, reject) => {
            if (!!this.fichaPService.fotos && this.fichaPService.fotos.length > 0){
                //this.fichaPService.fotos.forEach((array:any,i:any) => {
                    //this.apiBackEndService.getImagenBase64(array.FotoMedium.replace('https://sprodresources.blob.core.windows.net/multimedia/','')).subscribe((result:any) => {
                    this.loadAllImagesFotos(this.fichaPService.fotos,'FotoMedium').then(()=>{
                        // array.base64 = 'data:image/jpeg;base64,' + result.FileBase64;
                        // if (i === this.fichaPService.fotos.length - 1){
                        //     console.log('fotos',this.fichaPService.fotos);
                        //     resolve()
                        // }
                        resolve();
                    })                
                //});
            } else{
                resolve();
            }
        });
    }

    private calculaWidthColumn(array:any, key:string){
        return new Promise((resolve, reject) => {
            let mThis = this;
            var img = new Image();
            if (!!array){
                //array.forEach((a:any,i:any) => {
                Observable.interval(300).take(array.length).subscribe((i) => {
                    let a = array[i];
                    // img.onload = function(){
                    //     console.log('img',img);
                    //     console.log( img.width + ", "+ img.height );
                    //     a.columns = img.width > img.height ? 2 : 3;
                        if (i === array.length - 1){
                            console.log('fotos',mThis.fichaPService.fotos);
                            resolve()
                        }
                        resolve();
                    // };
                    // img.src = a[key]; 
                });
            } else{
                resolve();
            }
        });
    }

    private getColumnsDP() {
        return [
            {title: "Foto", dataKey: "Fot"},
            {title: "FechaNacimiento", dataKey: "FNac"},
            {title: "DepartamentoNacimiento", dataKey: "DepNac"},
            {title: "MunicipioNacimiento", dataKey: "MunNac"},
            {title: "TipoDocumento", dataKey: "TipoDoc"},
            {title: "NroDocumento", dataKey: "NroDoc"},
            {title: "Sexo", dataKey: "Sex"},
            {title: "PaisOrigen", dataKey: "Pais"},
            {title: "Cultura", dataKey: "Cultura"},
            {title: "Clan", dataKey: "Clan"}
        ];
    };

    // Datos personales
    private getDataDP(datosPersonales: any) {
        var data: any = [];
        data.push(datosPersonales);
        this.cargarImagen(datosPersonales.Foto).then((base64)=>{
            data[0].base64 = base64;
            for (var j = 1; j < 4; j++) {
                data.push({
                    Foto: '',
                    FechaNacimiento: '',
                    DepartamentoNacimiento: '',
                    MunicipioNacimiento: '',
                    TipoDocumento: '',
                    NroDocumento: '',
                    Sexo: '',
                    PaisOrigen: '',
                    Cultura: '',
                    Clan: ''
                });
            }
            return data;
        });
    }    
    
}
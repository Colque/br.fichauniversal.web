import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaEducacion',
    templateUrl: 'educacion.component.html',
    styleUrls: ['educacion.component.scss']
})

export class EducacionComponent implements OnInit {
    educacion: any;
    prefijo = 'EDU';
    educacionLineTime: any = [];

    constructor(
        public fichaPService: FichaPersonaDataService,
        public apiBackEndService: ApiBackEndService
    ) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fichaPService.getPersonaEducacion().then(() =>{
            this.educacion = this.fichaPService.educacion;
            this.educacionLineTime = this.educacion.filter((ec:any) => !ec.isTimeline);
            if (this.educacion && this.educacion.length > 0){
                this.educacionLineTime = this.apiBackEndService.ordernarArrayPorFecha(this.educacionLineTime,'FechaRegistro');
                this.educacionLineTime.forEach((ed:any) => {
                    ed.edadText = this.fichaPService.calculaEdadFunc(new Date(this.fichaPService.datosPersonales[0].FNac), ed.FechaRegistro);
                });
                console.log('educacion', this.educacion);
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaPService.tieneEducacionP = false;
            }
        });
    }
    
}
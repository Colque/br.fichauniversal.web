import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaEducacionColombia',
    templateUrl: 'educacionColombia.component.html',
    styleUrls: ['educacionColombia.component.scss']
})

export class EducacionColombiaComponent implements OnInit {
    educacion: any;
    prefijo = 'EDUColombia';

    constructor(
        public fichaPService: FichaPersonaDataService,
        public apiBackEndService: ApiBackEndService
    ) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.educacion = this.fichaPService.educacion;
    }
}
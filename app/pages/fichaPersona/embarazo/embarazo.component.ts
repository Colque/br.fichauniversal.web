import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var ByOso: any;


@Component({
    selector: 'tarjetaEmbarazo',
    templateUrl: 'embarazo.component.html',
    styleUrls: ['embarazo.component.scss']
})

export class EmbarazoComponent implements OnInit {
    embarazo: any;
    embarazoFC: Observable<Array<any>>;
    prefijo = 'EMB';

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fichaPService.getEmbarazo().then(() => {
            this.embarazo = this.fichaPService.embarazo;
            this.embarazoFC = this.fichaPService.embarazoFC;
            if (this.fichaPService.estaEmbarazada){
                myLoading.Ocultar('Loading'+ this.prefijo);
            }
        });
    }
    
    public openModal(index: any, modal: string){
        ByOso.openModalVisita(index,modal);
    }
}
import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var ByOso: any;


@Component({
    selector: 'tarjetaEmbarazoColombia',
    templateUrl: 'embarazoColombia.component.html',
    styleUrls: ['embarazoColombia.component.scss']
})

export class EmbarazoColombiaComponent implements OnInit {
    embarazo: any;
    embarazoFC: Observable<Array<any>>;
    prefijo = 'EMBColombia';

    constructor(
        public fichaPService: FichaPersonaDataService,
        public apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.embarazo = this.fichaPService.embarazo;
    }
}
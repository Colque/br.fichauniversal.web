import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var myStringFunctions: any;


@Component({
    selector: 'tarjetaFamilia',
    templateUrl: 'familia.component.html',
    styleUrls: ['familia.component.scss']
})

export class FamiliaComponent implements OnInit{
    fechasAgrupadas: any;
     grupoFamiliar: any;
    grupoFamiliar1: Observable<Array<any>>;
    integrantesVivienda: Observable<Array<any>>;
    padre: any = [];
    madre: any = [];
    prefijo = 'FAM';
    stringFunctions = myStringFunctions;

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService
    ) {

    }

    ngOnInit(){
        this.fichaPService.getGrupoFamiliar().then(() => {
            this.grupoFamiliar = this.fichaPService.grupoFamiliar;
            this.padre = this.fichaPService.padre;
            this.madre = this.fichaPService.madre;
            console.log('this.padre',this.padre);
            if (this.grupoFamiliar && this.grupoFamiliar.length > 0){
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaPService.tieneFamiliaP = false;
            }
        })
    }


    loadData() {
        this.fichaPService.getPersonaFamilia().then(() =>{
            this.fechasAgrupadas = this.fichaPService.fechasFamilia;
            console.log('fechasAgrupadas',this.fechasAgrupadas);
            //    this.grupoFamiliar = this.fichaPService.grupoFamiliar;
            this.integrantesVivienda = this.fichaPService.integrantesVivienda;
            this.padre = this.fichaPService.padre;
            this.madre = this.fichaPService.madre;
            if (this.fechasAgrupadas && this.fechasAgrupadas.length > 0){
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaPService.tieneFamiliaP = false;
            }
        });
    }

    tieneVisita(Opcion: string): boolean{
        var EsIgual = false;
        switch(Opcion){
            case "GrupoFamiliar":
                        EsIgual = true;
            break;
            case "Padre":
                this.padre.forEach((element: any) => {
                    EsIgual = true;
                });
            break;
            case "Madre":
                this.madre.forEach((element: any)  => {
                    EsIgual = true;
                });
            break;
        }

        return EsIgual;
    }

    tabActiva(Opcion: string): string{
        var EsActiva = false;

        switch(Opcion){
            case "GrupoFamiliar":
                if(this.tieneVisita("GrupoFamiliar")){
                    EsActiva = true;
                }
            break;
            case "IntegrantesVivienda":
                if(!this.tieneVisita("GrupoFamiliar")){
                    if(this.tieneVisita("IntegrantesVivienda")){
                        EsActiva = true;
                    }
                }
            break;
        }

        return (EsActiva ? "active show" : "");
    }
    
}
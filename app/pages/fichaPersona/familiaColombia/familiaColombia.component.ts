import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var myStringFunctions: any;


@Component({
    selector: 'tarjetaFamiliaColombia',
    templateUrl: 'familiaColombia.component.html',
    styleUrls: ['familiaColombia.component.scss']
})

export class FamiliaColombiaComponent implements OnInit{
    fechasAgrupadas: any;
    grupoFamiliar: Observable<Array<any>>;
    integrantesVivienda: Observable<Array<any>>;
    padre: Observable<Array<any>>;
    madre: Observable<Array<any>>;
    prefijo = 'FAMCol';
    stringFunctions = myStringFunctions;

    constructor(
        public fichaPService: FichaPersonaDataService,
        public apiBackEndService: ApiBackEndService
    ) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fechasAgrupadas = this.fichaPService.fechasFamilia;
        this.grupoFamiliar = this.fichaPService.grupoFamiliar;
        this.integrantesVivienda = this.fichaPService.integrantesVivienda;
        this.padre = this.fichaPService.padre;
        this.madre = this.fichaPService.madre;
    }

    tieneVisita(Opcion: string, Fecha: string, Origen: string, NroRelevamiento: string): boolean{
        var EsIgual = false;
        switch(Opcion){
            case "GrupoFamiliar":
                this.grupoFamiliar.forEach(element => {
                    if(element['Fecha'] == Fecha && element['Origen'] == Origen && element['NroRelevamiento'] == NroRelevamiento){
                        EsIgual = true;
                    }
                });
            break;
            case "IntegrantesVivienda":
                this.integrantesVivienda.forEach(element => {
                    if(element['Fecha'] == Fecha && element['Origen'] == Origen && element['NroRelevamiento'] == NroRelevamiento){
                        EsIgual = true;
                    }
                });
            break;
            case "Padre":
                this.padre.forEach(element => {
                    if(element['Fecha'] == Fecha && element['Origen'] == Origen && element['NroRelevamiento'] == NroRelevamiento){
                        EsIgual = true;
                    }
                });
            break;
            case "Madre":
                this.madre.forEach(element => {
                    if(element['Fecha'] == Fecha && element['Origen'] == Origen && element['NroRelevamiento'] == NroRelevamiento){
                        EsIgual = true;
                    }
                });
            break;
        }

        return EsIgual;
    }

    tabActiva(Opcion: string, Fecha: string, Origen: string, NroRelevamiento: string): string{
        var EsActiva = false;

        switch(Opcion){
            case "GrupoFamiliar":
                if(this.tieneVisita("GrupoFamiliar", Fecha, Origen, NroRelevamiento)){
                    EsActiva = true;
                }
            break;
            case "IntegrantesVivienda":
                if(!this.tieneVisita("GrupoFamiliar", Fecha, Origen, NroRelevamiento)){
                    if(this.tieneVisita("IntegrantesVivienda", Fecha, Origen, NroRelevamiento)){
                        EsActiva = true;
                    }
                }
            break;
        }

        return (EsActiva ? "active show" : "");
    }
    
}
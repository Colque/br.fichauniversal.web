import { FusionComponent } from './fusion/fusion.component';
import { CPIComponent } from './CPI/CPI.component';
import { Component, OnInit, ViewChild, Inject, NgZone, Output, EventEmitter, ChangeDetectorRef, AfterViewChecked } from '@angular/core';

import { ApiBackEndService } from './../../_services/index';

import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { FichaPersonaDataService } from './fichaPersonaData.service';
import { DescargarFichaPComponent } from './descargarFichaP/descargarFichaP.component';
import { DescargarFichaPColombiaComponent } from './descargarFichaPColombia/descargarFichaPColombia.component';
//tarjetas
import { ActivatedRoute } from "@angular/router";
import { Title }     from '@angular/platform-browser';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

//import {Observable} from 'rxjs/Rx';
//import 'rxjs/Rx';

import * as $ from 'jquery';
import { TLItem } from '../../components/timeline/model/tl-item';

declare var html2canvas: any;
declare var jsPDF: any;
declare var ByOso: any;

//paraloading
import { Observable } from 'rxjs';
import { Md2Dialog } from 'Md2';
declare var myLoading: any;
declare var myModalCPIActividades: any;
declare var myModalTestResultados: any;
declare var myStringFunctions: any;

@Component({
    moduleId: module.id,
    templateUrl: './fichaPersona.component.html'
})

export class FichaPersonaComponent implements AfterViewChecked, OnInit {
    //declarar variable de datos de tarjeta aquí
    //datosPersonales: any[];
    @ViewChild('modalImprimir', undefined) modalImprimir: DescargarFichaPComponent;

    @ViewChild('modalCPI', undefined) modalCpi: CPIComponent;

    @ViewChild('modalFUSION', undefined) modalFUSION: FusionComponent;

    @ViewChild('modalImprimirColombia', undefined) modalImprimirColombia: DescargarFichaPColombiaComponent;

    @ViewChild('ModalTest', undefined) ModalTest: Md2Dialog;
    
    
    @BlockUI() blockUI: NgBlockUI;
    prefijoDatosPersonales = "PER";
    public datosPersonales: any;
    public nombreCompleto: string;
    public modalFicha: boolean = false;
    public divCpi: boolean = false;
    public divFicha: boolean = true;
    public divFusion: boolean= false;
    testPersona: any;
    detalleTest: any = [];
    prefijo = 'CPI';
    admisiones: any;
    grupoFamiliar: Observable<Array<any>>;
    tieneItemsTimeLine = false;
    public ActividadesFotos: any;
    private ActividadesFotosLinea: any;
    filterTest: any;
    loading: boolean = false;

    constructor ( 
        private cdRef:ChangeDetectorRef,
        @Inject(NgZone) private zone: NgZone,
        public apiBackEndService: ApiBackEndService, 
        private route: ActivatedRoute,
        public fichaPService: FichaPersonaDataService,
        private title:Title,
    ) {}

    ngOnInit() {
        // this.loadData();
        // this.loadtest()
        this.route.params.subscribe(params => {
            console.log(params);
            this.apiBackEndService.CodPersona = params['p']; 
        });

        if (this.apiBackEndService.enviroment !== 'colombia'){
            // if (this.apiBackEndService.currentUser.CodSistemaPermisoList.indexOf("COD_FUSION") != -1){
            //     // this.fichaPService.getPersonaFusionCasos().then(()=>{
            //     //     console.log('cargo fusion')
            //     //         this.fichaPService.finishFusionLoad.emit();
            //     // });
            // }
        } else{
            this.blockUI.start('Cargando Ficha ...');
            this.fichaPService.getVivienda();
            this.fichaPService.getPersonaFotos();
            this.fichaPService.getPersonaDiscapacidadesCol();
            this.fichaPService.getPersonaRiesgosSalud();
            this.fichaPService.getEmbarazo();
            this.fichaPService.getPersonaLactantes();

            this.fichaPService.getPersonaAntropometria();

            this.fichaPService.getPersonaEducacion();
            this.fichaPService.getPersonaTrabajo();

            this.fichaPService.getPersonaFamilia();

            this.fichaPService.getDatosPersonales().then(()=>{
                let titulo = this.fichaPService.nombreCompleto !== null &&  this.fichaPService.edad !== null ? this.fichaPService.nombreCompleto+ ' - ' + this.fichaPService.edad + ' - ' + 'Ficha Universal - ' : 'Ficha Universal - ';
                this.title.setTitle(this.apiBackEndService.enviroment === 'colombia' ? titulo + 'Colombia' : titulo + 'MPI');
                this.blockUI.stop();
            });
        }
        //this.lodDatos();
    }

    ngAfterViewChecked(){
        this.cdRef.detectChanges();
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('CodUsuario');
        location.reload();
    }

    showDivCpi(){
        this.divCpi = true;
        this.divFicha = false;
        this.divFusion = false;
    }

    showDivFusion(){
        this.divFusion = true;
        this.divFicha = false;
        this.divCpi = false;
    }

    showDivFicha(){
        this.divFicha = true;
        this.divFusion = false;
        this.divCpi= false;
    }

    //obtener valores de tarjetas aquí
    private lodDatos() {
        this.apiBackEndService.getPersonaDatosPersonales().subscribe(resultado => {
            this.datosPersonales = JSON.parse(resultado.toString());
            this.nombreCompleto = this.datosPersonales[0] !== undefined ? this.datosPersonales[0].Apellidos + ', ' + this.datosPersonales[0].Nombres : null;

            let edad = this.datosPersonales[0] !== undefined ?  this.datosPersonales[0].Edad : null;
            this.title.setTitle(this.nombreCompleto !== null &&  edad !== null ? this.nombreCompleto+ ' - ' + edad + ' - ' + 'Ficha Universal - MPI': 'Ficha Universal - MPI');
        });
    }

    public openModal(index: any, modal: string){
        ByOso.openModalVisita(index,modal);
    }

    public abrirModal(){
        console.log('openModal');
        //this.modalFicha = !this.modalFicha;
        if (this.apiBackEndService.enviroment !== 'colombia'){
            this.modalImprimir.open();
        } else {
            this.modalImprimirColombia.open();
        }
        //this.printDiv();
    }

    public abrirModal2(){
        if (this.modalCpi !== undefined){
            this.modalCpi.open();
        }
    }

    public abrirModalFusion(){
        if (this.modalFUSION !== undefined){
            this.modalFUSION.open();
        }
    }
    // open() {
    //     this.fichaPService.esVisible = true
    //     this.blockUI.stop();
    //     this.error = "";
    //     this.ModalCPI2.open();
    // }

}
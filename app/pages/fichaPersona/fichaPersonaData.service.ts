import { Observable } from 'rxjs/Observable';
import { ApplicationRef, Injectable, EventEmitter } from '@angular/core';
import { ApiBackEndService } from './../../_services/index';
import * as moment from 'moment';
declare var ByOso: any;
declare var  _AntroOrigenes: any;
declare var  _AntroColorOrigen: any;
declare var myStringFunctions: any;
@Injectable()
export class FichaPersonaDataService {

    finishFusionLoad: EventEmitter<any> = new EventEmitter()
    public haveChanges: boolean;
    maxIntentos = 10;
    public openModalFotPersona: boolean = false;
    //Datos personales
    public tieneDatosP: boolean = true;
    public datosPersonales: any;
    public nombreCompleto: string;
    public edad: string;
    public anio: number;

    //RiesgoEmbarazo
    public tieneRiesgoEmbarazo: boolean = true;
    public riesgoEmbarazo: any;

    //RiesgoNinio
    public tieneRiesgoNinio: boolean = true;
    public riesgoNinio: any;

    //RiesgoAdolescente
    public tieneRiesgoAdolescente: boolean = true;
    public riesgoAdolescente: any;

    //Vvienda
    public tieneViviendaP: boolean = true;
    public vivienda: any;
    public mapa: any;

    public embarazo: any;
    public embarazoFC: any;
    public estaEmbarazada: boolean = true;

    //Salud
    public tieneSaludP: boolean = true;
    public salud: any;
    public riesgosSalud: any = [];
    public discapacidades: any = [];
    public discapacidadesCognitivas: any;
    public enfermedadesAgudas: any;
    public enfermedadesCronicas: any;

    //GrupoFamiliar
    public tieneFamiliaP1: boolean = true;
    public grupoFamiliar1: any[] = [];

    //Vacunas
    public tieneVacunasP: boolean = true;
    public fechasVacunas: any;
    public vacunas: any;
    public calendarioVacunas: any;
    public edadesCalendario: any;
    public vacunasDosis: any;

    //HistoriaClinia
    public tieneHistCli: boolean = true;
    public historiaClinica: any;

    //Graficos
    _AntroOrigenes: any = [];
    _AntroColorOrigen: any = [];
    coordenadasPE: Observable<Array<any>>;
    historialPE: any;
    TieneGraficoPE: boolean = true;

    coordenadasTE: Observable<Array<any>>;
    historialTE: any;
    TieneGraficoTE: boolean = true;

    coordenadasIMC: Observable<Array<any>>;
    historialIMC: any;
    TieneGraficoIMC: boolean = true;

    //antropometria
    public tieneAntAntroP: boolean = true;
    public antecedentes: any;
    public antecedentesFC: any;
    //
    public tieneAntroP: boolean = true;
    public antropometria: any;
    public origenes: any;

    //Servicios
    public tieneServiciosP: boolean = true;
    public fechasServSan: any;
    public sanitarios: any;
    public servicios: any;

    //Infraestructura
    public tieneInfraestructuraP: boolean = true;
    public infraestructura: any;

    //observaciones
    public tieneObsUrgP: boolean = true;
    public fechasObs: any;
    public observaciones: any;
    public urgencias: any;

    //Antecedentes Nutricionales Patalógicos
    public tieneAntNutPat: boolean = true;
    public fechasNutr: any;
    public antecedentesNutricionales: any;
    public antecedentesPatologicos: any;

    public lactantes:any = [];

    //Familia
    public tieneFamiliaP: boolean = true;
    public fechasFamilia: any;
    public grupoFamiliar: any;
    public integrantesVivienda: any;
    public padre: any;
    public madre: any;

    //
    public tieneFotosP: boolean = true;
    public fotos: any;
    public tieneVideosP: boolean = true;
    public videos2D: any;
    
    //
    public tieneEducacionP: boolean = true;
    public educacion:any;

    //Trabajo
    public tieneTrabajoP: boolean = true;
    public fechasTrabajo:any;
    public trabajo:any;
    public ingresos:any;
    public oficios:any;

    //
    public tieneInteligenciaFam: boolean = true;
    public inteligenciaFamilia:any = [];

    //Fusion
    public casosFusion: any = [];
    public datosFusion: any = [];
    public contador: any;
    public tieneDatosFusion: boolean = false;

    //Conin
    public tieneDatosConin: boolean = true;
    public admisionesConin: any = [];

    //CPI
    public tieneDatosCPI: boolean = false;
    public isVisible: boolean = false
    public esVisible: boolean = false
    public admisionesCPI: any = [];

    //TestPersona
    public tieneDatosTest: boolean = true;
    public testPersona: any = [];
    public detalleTest: any = [];

    public listTarjetas: any = [
        {
            Nombre:"Vivienda",
            Checked: false,
            Visible: false,
            Array:[],
            rows: 13
        },
        {
            Nombre:"Mapa",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 10
        },
        {
            Nombre:"Salud",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 7
        },
        {
            Nombre:"Vacunas",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
        {
            Nombre:"Embarazo",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 9
        },
        {
            Nombre:"Antecedentes Antropométricos",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 9
        },
        {
            Nombre:"Antecedentes Nutricionales Patológicos",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 7
        },
        {
            Nombre:"Riesgo del Niño",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
        {
            Nombre:"Riesgo del Adolescente",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
        {
            Nombre:"Riesgo del Embarazo",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
        {
            Nombre:"Educación",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 5
        },
        {
            Nombre:"Trabajo e Ingresos",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 7
        },
        {
            Nombre:"Servicios",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 7
        },
        {
            Nombre:"Infraestructura",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 7
        },
        {
            Nombre:"Observaciones y Urgencias",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
        {
            Nombre:"Grupo Familiar",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
        {
            Nombre:"Antropometría (Todas)",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 9
        },
        {
            Nombre:"Gráficos",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
        {
            Nombre:"Fotos",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
    ];

    public listTarjetasColombia: any = [
        {
            Nombre:"Vivienda",
            Checked: false,
            Visible: false,
            Array:[],
            rows: 9
        },
        {
            Nombre:"Mapa",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 9
        },
        {
            Nombre:"Salud",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
        {
            Nombre:"Gestantes",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 5
        },
        {
            Nombre:"Lactantes",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 9
        },
        {
            Nombre:"Antropometria",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 5
        },
        {
            Nombre:"Educación",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 7
        },
        {
            Nombre:"Trabajo",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 5
        },
        {
            Nombre:"Familia",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 5
        },
        {
            Nombre:"Fotos",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
    ];

    constructor(
        private applicationRef: ApplicationRef,
         private apiBackEndService: ApiBackEndService, 
    ) {
    }


    /**
     * @param {any} data
     */
    public getDatosPersonales() {
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaDatosPersonales().subscribe(resultado => {
                this.datosPersonales = JSON.parse(resultado.toString());
                if (this.datosPersonales && this.datosPersonales[0] !== undefined){
                    this.nombreCompleto = this.datosPersonales[0].Apellidos + ', ' + this.datosPersonales[0].Nombres;
                    this.calculaEdad(this.datosPersonales[0], new Date(this.datosPersonales[0].FNac)).then(() => {
                        this.edad = this.datosPersonales[0].Edad;
                        this.anio = this.datosPersonales[0].anio;
                        resolve();
                    });
                } else{
                    resolve();
                }
            });
        });
    }



    public getRiesgoEmbarazo(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaRiesgosEmbarazo().subscribe((resultado: any) => {
                this.riesgoEmbarazo = !!resultado && resultado !== 'null' ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : [];
                if (!!this.riesgoEmbarazo && this.riesgoEmbarazo.length > 0 ){
                    this.listTarjetas[9].Visible = true;
                    this.listTarjetas[9].Checked = true;
                    this.listTarjetas[9].Array.push(this.riesgoEmbarazo);
                }
                resolve();
            });
        });
    }

    public getRiesgoNinio(){
         return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaRiesgosNinio().subscribe((resultado: any) => {
                this.riesgoNinio = !!resultado && resultado !== 'null'  ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : [];
                if (!!this.riesgoNinio && this.riesgoNinio.length > 0 ){
                    this.listTarjetas[7].Visible = true;
                    this.listTarjetas[7].Checked = true;
                    this.listTarjetas[7].Array.push(this.riesgoNinio);
                }
                resolve();
            });
        });
    }

    public getRiesgoEscolarAdolescente(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaRiesgosEscolarAdolescente().subscribe((resultado: any) => {
                this.riesgoAdolescente = !!resultado && resultado !== 'null' ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1)  : [];
                if (!!this.riesgoAdolescente && this.riesgoAdolescente.length > 0 ){
                    this.listTarjetas[8].Visible = true;
                    this.listTarjetas[8].Checked = true;
                    this.listTarjetas[8].Array.push(this.riesgoAdolescente);
                }
                resolve();
            });
        });
    }

    public getVivienda(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaVivienda().subscribe(resultado => {
                this.vivienda = resultado ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                this.mapa = this.vivienda;
                if (this.vivienda && this.vivienda.length > 0){
                    //Vivienda
                    this.listTarjetas[0].Visible = true;
                    this.listTarjetas[0].Checked = true;
                    this.listTarjetas[0].Array.push(this.vivienda);
                    //Mapa
                    this.listTarjetas[1].Visible = true;
                    this.listTarjetas[1].Checked = true;
                    this.listTarjetas[1].Array.push(this.vivienda);
                    /*******************COLOMBIA******************/
                    //Vivienda
                    this.listTarjetasColombia[0].Visible = true;
                    this.listTarjetasColombia[0].Checked = true;
                    this.listTarjetasColombia[0].Array.push(this.vivienda);
                    //Mapa
                    this.listTarjetasColombia[1].Visible = true;
                    this.listTarjetasColombia[1].Checked = true;
                    this.listTarjetasColombia[1].Array.push(this.vivienda);
                }
                resolve();
            });
        });
    }

    public getEmbarazo(){
        return new Promise((resolve, reject) => {
            Observable.combineLatest(
                this.apiBackEndService.getPersonaEmbarazo(),
                this.apiBackEndService.getPersonaEmbarazoFC()
                ).subscribe(([
                _embarazo,
                _embarazoFC
            ]) =>{
                this.embarazo = _embarazo ? JSON.parse(_embarazo.toString()).filter((e:any) => this.apiBackEndService.organizacionesUsuario.indexOf(e.OrganizacionCod) !== -1) : null;
                this.embarazoFC = _embarazoFC ? JSON.parse(_embarazoFC.toString()).filter((e:any) => this.apiBackEndService.organizacionesUsuario.indexOf(e.OrganizacionCod) !== -1) : null;
                if (this.embarazo && this.embarazo[0] !== undefined){
                    this.estaEmbarazada = this.embarazo[0].EstaEmbarazada;
                    if (this.estaEmbarazada){
                        this.listTarjetas[4].Visible = true;
                        this.listTarjetas[4].Checked = true;
                        this.listTarjetas[4].Array.push(this.embarazo);

                        /*******************COLOMBIA******************/
                        this.listTarjetasColombia[3].Visible = true;
                        this.listTarjetasColombia[3].Checked = true;
                        this.listTarjetasColombia[3].Array.push(this.embarazo);
                    }
                } else{
                    this.estaEmbarazada = false;
                }
                resolve();
            })
        });
    }

    //Salud
    public getPersonaSalud(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaSalud().subscribe(resultado => {
                this.salud = resultado ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                if (!!this.salud && this.salud.length > 0){
                    this.listTarjetas[2].Visible = true;
                    this.listTarjetas[2].Checked = true;
                    this.listTarjetas[2].Array.push(this.salud);
                }
                resolve();
            });
        });
    }

    public getPersonaDiscapacidades(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaDiscapacidades().subscribe(resultado => {
                this.discapacidades = resultado ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                if (this.discapacidades){
                    this.discapacidades.forEach((d:any) =>{
                        if (d.DiscapacidadCognitiva !== '[]'){
                            this.discapacidadesCognitivas = JSON.parse(d.DiscapacidadCognitiva);
                        }
                        d.DiscapacidadCognitiva = JSON.parse(d.DiscapacidadCognitiva);//ToDO mejorar proceso
                    });
                    resolve();
                } else{
                    resolve();
                }
            });
        });
    }

    public getPersonaDiscapacidadesCol(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaDiscapacidades().subscribe(resultado => {
                this.discapacidades = JSON.parse(resultado.toString());
                if (!!this.discapacidades && this.discapacidades.length > 0){
                    this.listTarjetasColombia[2].Visible = true;
                    this.listTarjetasColombia[2].Checked = true;
                    this.listTarjetasColombia[2].Array.push(this.discapacidades);
                }
                resolve();
            });
        });
    }

    public getPersonaRiesgosSalud(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaRiesgosSalud().subscribe(resultado => {
                this.riesgosSalud = JSON.parse(resultado.toString());
                if (!!this.riesgosSalud && this.riesgosSalud.length > 0){
                    this.listTarjetas[2].Visible = true;
                    this.listTarjetas[2].Checked = true;
                    this.listTarjetas[2].Array.push(this.riesgosSalud);
                }
                resolve();
            });
        });
    }

    public getPersonaEnfermedadesAgudas(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaEnfermedadesAgudas().subscribe(resultado => {
                this.enfermedadesAgudas = resultado ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                resolve();
            });
        });
    }

    public getPersonaEnfermedadesCronicas(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaEnfermedadesCronicas().subscribe(resultado => {
                this.enfermedadesCronicas = resultado ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                resolve()
            });
        });
    }

    //Vacunas
    public getPersonaVacunas(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaVacunas().subscribe(resultado => {
                resultado = JSON.parse(resultado.toString());
                this.vacunas = resultado[0].Resultado ? resultado[0].Resultado.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                this.fechasVacunas = resultado[0].FechasAgrupadas;
                if (this.vacunas.length > 0){
                    this.listTarjetas[3].Visible = true;
                    this.listTarjetas[3].Checked = true;
                    this.listTarjetas[3].Array.push(this.fechasVacunas);
                }           
                resolve();
            });
        });
    }

    public getCalendarioVacunas(anio: string){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getCalendarioVacunas(anio).subscribe(resultado => {
                this.calendarioVacunas = JSON.parse(resultado.toString());
                resolve();
            });
        });
    }

    public getEdadesCalendario(anio: string){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getEdadesCalendario(anio).subscribe(resultado => {
                this.edadesCalendario = JSON.parse(resultado.toString());
                resolve();
            });
        });
    }

    public getVacunasDosis(anio: string, codPersona: string){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getVacunasDosis(anio,codPersona).subscribe(resultado => {
                this.vacunasDosis = JSON.parse(resultado.toString());
                resolve();
            });
        });
    }

    //HistoriaClinica
    public getPersonaHistoriaClinica(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaHistoriaClinica().subscribe(resultado => {
                this.historiaClinica = resultado && JSON.parse(resultado.toString()) !== null ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                
                // if (this.vacunas.length > 0){
                //     this.listTarjetas[3].Visible = true;
                //     this.listTarjetas[3].Checked = true;
                //     this.listTarjetas[3].Array.push(this.fechasVacunas);
                // }           
                resolve();
            });
        });
    }

    //Graficos
    public getPersonaGraficoPE(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaGraficoPE().subscribe(resultado => {
                var resultadoCompleto = JSON.parse(resultado.toString());
                if(typeof resultadoCompleto.Historial !== "undefined"){
                    this.historialPE = resultadoCompleto.Historial ? resultadoCompleto.Historial.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                    if(this.historialPE.length > 0){
                        this.coordenadasPE = resultadoCompleto.Coordenadas;
                        //this.historialPE = resultadoCompleto.Historial;
                        this.TieneGraficoPE = true;

                        this.listTarjetas[17].Visible = true;
                        this.listTarjetas[17].Checked = true;
                        resolve();
                    } else{
                        this.TieneGraficoPE = false;
                        resolve();
                    }
                } else{
                    this.TieneGraficoPE = false;
                    resolve();
                }
            });
        });
    }

    public getPersonaGraficoTE(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaGraficoTE().subscribe(resultado => {
                var resultadoCompleto = JSON.parse(resultado.toString());
                
                if(typeof resultadoCompleto.Historial !== "undefined"){
                    this.historialTE = resultadoCompleto.Historial ? resultadoCompleto.Historial.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                    if(this.historialTE.length > 0){
                        this.coordenadasTE = resultadoCompleto.Coordenadas;
                        //this.historialTE = resultadoCompleto.Historial;
                        this.TieneGraficoTE = true;

                        this.listTarjetas[17].Visible = true;
                        this.listTarjetas[17].Checked = true;
                        resolve();
                    } else{
                        this.TieneGraficoTE = false;
                        resolve();
                    }
                } else{
                    this.TieneGraficoTE = false;
                    resolve();
                }
            });
        });
    }

    public getPersonaGraficoIMC(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaGraficoIMC().subscribe(resultado => {
                var resultadoCompleto = JSON.parse(resultado.toString());
                
                if(typeof resultadoCompleto.Historial !== "undefined"){
                    this.historialIMC = resultadoCompleto.Historial ? resultadoCompleto.Historial.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                    if(this.historialIMC.length > 0){
                        this.coordenadasIMC = resultadoCompleto.Coordenadas;
                        //this.historialIMC = resultadoCompleto.Historial;
                        this.TieneGraficoIMC = true;

                        this.listTarjetas[17].Visible = true;
                        this.listTarjetas[17].Checked = true;
                        resolve();
                    } else{
                        this.TieneGraficoIMC = false;
                        resolve();
                    }
                } else{
                    this.TieneGraficoIMC = false;
                    resolve();
                }
            });
        });
    }

    //Antropometria
    public getPersonaAntecedentesAntropometricos(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaAntecedentesAntropometricos().subscribe(resultado => {
                this.antecedentes = resultado ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                if (!!this.antecedentes && this.antecedentes.length > 0){
                    this.listTarjetas[5].Visible = true;
                    this.listTarjetas[5].Checked = true;
                    this.listTarjetas[5].Array.push(this.antecedentes);
                }
                resolve();
            });
        });
    }

    public getPersonaAntecedentesAntropometricosFC(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaAntecedentesAntropometricosFC().subscribe(resultado => {
                this.antecedentesFC = resultado ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                resolve();
            });
        });
    }

    public getPersonaAntropometria(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaAntropometria().subscribe(resultado => {
                this.antropometria = resultado ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                if (!!this.antropometria && this.antropometria.length > 0){
                    this.antropometria.forEach((a:any, i:any) => {
                        this._AntroOrigenes.push(a.Origen);
                        if (i === this.antropometria.length - 1){
                            _AntroOrigenes = this._AntroOrigenes;
                            resolve();
                        }
                    });
                    this.listTarjetas[16].Visible = true;
                    this.listTarjetas[16].Checked = true;

                    /***********COLOMBIA***********/
                    this.listTarjetasColombia[5].Visible = true;
                    this.listTarjetasColombia[5].Checked = true;
                    this.listTarjetasColombia[5].Array.push(this.antropometria);
                } else {
                    resolve();
                }
            });
        });
    }

    public getPersonaOrigenesAntropometria(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaOrigenesAntropometria().subscribe(resultado => {
                this.origenes = JSON.parse(resultado.toString());
                let arrayOrigenes = this.origenes.split(";");
                console.log('arrayOrigenes',arrayOrigenes);
                arrayOrigenes.forEach((a:any, i: any) => {
                    var Origen = a.split(",")[0].replace('"', '');
                    var Color = a.split(",")[1].replace('"', '');

                    if (Origen != "") {
                        this._AntroColorOrigen.push({ Origen, Color});
                    }
                    if (i === arrayOrigenes.length - 1){
                        _AntroColorOrigen = this._AntroColorOrigen;
                        resolve();
                    }
                });
            });
        });
    }

    public getPersonaLactantes(){
        return new Promise((resolve, reject) => {
             this.apiBackEndService.getPersonaLactantes().subscribe(resultado => {
                this.lactantes = JSON.parse(resultado.toString());
                if (this.lactantes.length > 0){
                    this.listTarjetasColombia[4].Visible = true;
                    this.listTarjetasColombia[4].Checked = true;
                }
                resolve();
            });
        });
    }

    //Servicios
    public getPersonaSanitariosServicios(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaSanitariosServicios().subscribe(resultado => {
                var resultadoCompleto = JSON.parse(resultado.toString());
                if (resultadoCompleto[0].FechasAgrupadas && resultadoCompleto[0].Resultado){
                    this.fechasServSan = resultadoCompleto[0].FechasAgrupadas ? resultadoCompleto[0].FechasAgrupadas.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoCompleto[0].FechasAgrupadas;
                    this.sanitarios = resultadoCompleto[0].Resultado.ResultadoSanitarios ? resultadoCompleto[0].Resultado.ResultadoSanitarios.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1 && r.TieneBanio !== 'NO') : null;//resultadoCompleto[0].Resultado.ResultadoSanitarios;
                    this.servicios = resultadoCompleto[0].Resultado.ResultadoServicios ? resultadoCompleto[0].Resultado.ResultadoServicios.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoCompleto[0].Resultado.ResultadoServicios;
                    //Lista para imprimir PDF
                    if (!!this.sanitarios && this.sanitarios.length > 0 || !!this.servicios && this.servicios.length > 0 ){
                        this.listTarjetas[12].Visible = true;
                        this.listTarjetas[12].Checked = true;
                        this.listTarjetas[12].Array.push(this.fechasServSan);
                    }
                    resolve();
                } else {
                    resolve();
                }
            });
        });
    }

    public getPersonaServicios(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaSanitariosServicios().subscribe(resultado => {
                console.log('resultadoServicios',JSON.parse(resultado.toString()));
                var resultadoCompleto = JSON.parse(resultado.toString());
                this.fechasServSan = resultadoCompleto[0].FechasAgrupadas;
                this.servicios = resultadoCompleto[0].Resultado.ResultadoServicios;

                this.servicios.forEach((serv: any) => {
                    serv.Servicios = JSON.parse(serv.Servicios);
                });
                console.log('servicios',this.servicios);
                
                //Lista para imprimir PDF
                if (!!this.servicios && this.servicios.length > 0 ){
                    this.listTarjetas[12].Visible = true;
                    this.listTarjetas[12].Checked = true;
                    this.listTarjetas[12].Array.push(this.fechasServSan);
                }
                resolve();
            });
        });
    }

    //Infraestructura
    public getPersonaInfraestructura(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaInfraestructura().subscribe(resultado => {
                this.infraestructura = resultado ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                //Lista para imprimir PDF
                if (!!this.infraestructura && this.infraestructura.length > 0 ){
                    this.listTarjetas[13].Visible = true;
                    this.listTarjetas[13].Checked = true;
                    this.listTarjetas[13].Array.push(this.infraestructura);
                }
                resolve();
            });
        });
    }

    //Observaciones
    public getPersonaObservacionesUrgencias(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaObservacionesUrgencias().subscribe(resultado => {
                var resultadoCompleto = JSON.parse(resultado.toString());
                this.fechasObs = resultadoCompleto[0].FechasAgrupadas ? resultadoCompleto[0].FechasAgrupadas.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoCompleto[0].FechasAgrupadas;
                this.observaciones = resultadoCompleto[0].Resultado.ResultadoObservaciones ? resultadoCompleto[0].Resultado.ResultadoObservaciones.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoCompleto[0].Resultado.ResultadoObservaciones;
                this.urgencias = resultadoCompleto[0].Resultado.ResultadoUrgencias ? resultadoCompleto[0].Resultado.ResultadoUrgencias.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoCompleto[0].Resultado.ResultadoUrgencias;

                //Lista para imprimir PDF
                if (!!this.observaciones && this.observaciones.length > 0 || !!this.urgencias && this.urgencias.length > 0){
                    this.listTarjetas[14].Visible = true;
                    this.listTarjetas[14].Checked = true;
                    this.listTarjetas[14].Array.push(this.fechasObs);
                }
                resolve();
            });
        });
    }

    //Antecedentes Nutricionales Patalógicos
    public getPersonaAntecedentesNutricionales(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaAntecedentesNutricionales().subscribe(resultado => {
                var resultadoCompleto = JSON.parse(resultado.toString());
                this.fechasNutr = resultadoCompleto[0].FechasAgrupadas ? resultadoCompleto[0].FechasAgrupadas.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoCompleto[0].FechasAgrupadas;
                this.antecedentesNutricionales = resultadoCompleto[0].Resultado.ResultadoNutricionales ? resultadoCompleto[0].Resultado.ResultadoNutricionales.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : [];//resultadoCompleto[0].Resultado.ResultadoNutricionales;
                this.antecedentesPatologicos = resultadoCompleto[0].Resultado.ResultadoPatologicos ? resultadoCompleto[0].Resultado.ResultadoPatologicos.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : [];//resultadoCompleto[0].Resultado.ResultadoPatologicos;
                if (!!this.antecedentesNutricionales && this.antecedentesNutricionales.length > 0 || !!this.antecedentesPatologicos && this.antecedentesPatologicos.length > 0){
                    this.listTarjetas[6].Visible = true;
                    this.listTarjetas[6].Checked = true;
                    this.listTarjetas[6].Array.push(this.fechasNutr);
                }
                resolve();
            });
        });
    }

    //Familia
    public getPersonaFamilia(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaFamilia().subscribe(resultado => {
                var resultadoCompleto = JSON.parse(resultado.toString());
                if (resultadoCompleto[0]){
                    this.fechasFamilia = resultadoCompleto[0].FechasAgrupadas;
                    this.grupoFamiliar = resultadoCompleto[0].Resultado.ResultadoGrupoFamiliar;
                    this.integrantesVivienda = resultadoCompleto[0].Resultado.ResultadoIntegrantesVivienda;
                    this.padre = resultadoCompleto[0].Resultado.ResultadoPadre;
                    this.madre = resultadoCompleto[0].Resultado.ResultadoMadre;

                    //Lista para imprimir PDF
                    if (!!this.grupoFamiliar && this.grupoFamiliar.length > 0 ){
                        this.listTarjetas[15].Visible = true;
                        this.listTarjetas[15].Checked = true;
                        this.listTarjetas[15].Array.push(this.fechasFamilia);

                        /***********COLOMBIA***********/
                        this.listTarjetasColombia[8].Visible = true;
                        this.listTarjetasColombia[8].Checked = true;
                        this.listTarjetasColombia[8].Array.push(this.fechasFamilia);
                    }
                }                
                resolve();
            });
        });
    }

    public getGrupoFamiliar(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getGrupoFamiliar().subscribe(resultado => {

                var resultadoCompleto = JSON.parse(resultado.toString());
                if (resultadoCompleto[0]){
                    this.grupoFamiliar = resultadoCompleto[0].Resultado.ResultadoGrupoFamiliar;
                    this.padre = resultadoCompleto[0].Resultado.ResultadoPadre ? resultadoCompleto[0].Resultado.ResultadoPadre : [];
                    this.madre = resultadoCompleto[0].Resultado.ResultadoMadre ? resultadoCompleto[0].Resultado.ResultadoMadre : [];

                    //Lista para imprimir PDF
                    if (!!this.grupoFamiliar && this.grupoFamiliar.length > 0 ){
                        this.listTarjetas[15].Visible = true;
                        this.listTarjetas[15].Checked = true;
                        //this.listTarjetas[15].Array.push(this.fechasFamilia);

                        /***********COLOMBIA***********/
                        this.listTarjetasColombia[8].Visible = true;
                        this.listTarjetasColombia[8].Checked = true;
                        //this.listTarjetasColombia[8].Array.push(this.fechasFamilia);
                    }
                }
                resolve();
            });
        });
    }

    //Fotos
    public getPersonaFotos(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaFotos().subscribe(resultado => {
                let fotosAux: any = [];
                let fotoResult: any = [];
                //this.fotos = [];
                fotosAux = resultado ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                //Lista para imprimir PDF
                if (!!fotosAux && fotosAux.length > 0 ){
                    this.listTarjetas[18].Visible = true;
                    this.listTarjetas[18].Checked = true;
                    fotosAux.forEach((foto:any, i:any) => {
                      if(foto.FotoMedium){
                        this.apiBackEndService.getBase64(foto.FotoMedium).then((base:any) => {
                            if (base.base64){        
                                let indexF = fotosAux.map(function(e:any) { return e.base64; }).indexOf(base.base64);                        
                                if (indexF === -1){
                                    fotoResult.push(foto);
                                } 
                                foto.base64 = base.base64;
                                foto.columns = base.columns;
                            }
                            if (i === fotosAux.length - 1){
                                resolve(fotoResult);
                            }
                        }); 
                      } else {
                          if (i === fotosAux.length - 1){
                                resolve(fotoResult);
                            }
                      }
                    });

                    /***********COLOMBIA***********/
                    this.listTarjetasColombia[9].Visible = true;
                    this.listTarjetasColombia[9].Checked = true;
                } else {
                    resolve(fotoResult);
                }
            });
        });
    }

    //Videos 2D
    public getPersonaVideos2D(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaVideos2D().subscribe(resultado => {
                this.videos2D = resultado ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                resolve();
            });
         });
    }

    //Educacion
    public getPersonaEducacion(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaEducacion().subscribe(resultado => {
                this.educacion = resultado ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                if (!!this.educacion && this.educacion.length > 0 ){
                    this.educacion.forEach((ed:any) => {
                        ed.Analfabeto_txt = this.anio < 10 ? 'No, es menor de 10 años' : ed.Analfabeto_txt;
                        ed.Analfabeto = this.anio < 10 ? 'NO' : ed.Analfabeto;
                        ed.MNE = this.anio < 4 ? 'No esta en edad escolar obligatoria' : ed.MNE;
                    });
                    this.listTarjetas[10].Visible = true;
                    this.listTarjetas[10].Checked = true;
                    this.listTarjetas[10].Array.push(this.educacion);

                    /***********COLOMBIA***********/
                    this.listTarjetasColombia[6].Visible = true;
                    this.listTarjetasColombia[6].Checked = true;
                    this.listTarjetasColombia[6].Array.push(this.educacion);
                }
                resolve()
            });
        });
    }

    //Trabajo
    public getPersonaTrabajo(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaTrabajo().subscribe(resultado => {
                var resultadoCompleto = JSON.parse(resultado.toString());
                if (resultadoCompleto[0].FechasAgrupadas && resultadoCompleto[0].Resultado){
                    this.fechasTrabajo = resultadoCompleto[0].FechasAgrupadas ? resultadoCompleto[0].FechasAgrupadas.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoCompleto[0].FechasAgrupadas;
                    this.trabajo = resultadoCompleto[0].Resultado.ResultadoTrabajo ? resultadoCompleto[0].Resultado.ResultadoTrabajo.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoCompleto[0].Resultado.ResultadoTrabajo;
                    this.ingresos = resultadoCompleto[0].Resultado.ResultadoIngresos ? resultadoCompleto[0].Resultado.ResultadoIngresos.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoCompleto[0].Resultado.ResultadoIngresos;
                    this.oficios = resultadoCompleto[0].Resultado.ResultadoOficios ? resultadoCompleto[0].Resultado.ResultadoOficios.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoCompleto[0].Resultado.ResultadoOficios;
                    //Lista para PDF
                    if (!!this.trabajo && this.trabajo.length > 0 || !!this.ingresos && this.ingresos.length > 0 || !!this.oficios && this.oficios.length > 0){
                        this.listTarjetas[11].Visible = true;
                        this.listTarjetas[11].Checked = true;
                        this.listTarjetas[11].Array.push(this.fechasTrabajo);

                        /***********COLOMBIA***********/
                        this.listTarjetasColombia[7].Visible = true;
                        this.listTarjetasColombia[7].Checked = true;
                        this.listTarjetasColombia[7].Array.push(this.fechasTrabajo);
                    }
                    resolve();
                } else {
                    resolve();
                }
            });
        });
    }

    //Test
    public getPersonaTest(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaTest().subscribe((resultado: any) => {
                var resultadoCompleto : any[] = resultado && resultado !== "null" ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : [];
                if (resultadoCompleto){
                    resultadoCompleto.forEach((a:any, i:any) => {
                        a.Resultados = JSON.parse(a.Resultados);
                        a.Entidades = JSON.parse(a.Entidades);
                        if (this.testPersona.filter((t: any) => t.CodCabeceraTest === a.CodCabeceraTest && t.CodTest === a.CodTest && t.FechaCargaDate === a.FechaCargaDate).length === 0){
                            this.testPersona.push({CodCabeceraTest: a.CodCabeceraTest, CodTest : a.CodTest, Test: a.Test, cabeceraCPI: a.cabeceraCPI, Resultados: a.Resultados, 
                            Fecha: a.FechaCarga, FechaCargaDate: a.FechaCargaDate, Origen: a.Origen, Entidades:a.Entidades});//, Date: Fechadate
                        }
                    });
                    
                    this.detalleTest = resultadoCompleto;
                }
                resolve();
            });
        });
    }

    //Inteligencia Artificial
    // public getPersonaInteligenciaArtificial(){
    //     return new Promise((resolve, reject) => {
    //         this.apiBackEndService.getPersonaTematicasIA().subscribe((tematicas: any)=> {
    //             resolve(tematicas["Result"]);
    //         },
    //         (err: any) =>{
    //             resolve(null);
    //         }
    //         );
    // });
    // }

    //Fusion
    async getPersonaFusionCasos(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaFusionCasos().subscribe(
                resultado => {
                    if(resultado && resultado["Result"].length > 0){

                        //var resultadoJSON = JSON.parse(resultado.toString());
                        var resultadoJSON = resultado;
                        if(Number(resultadoJSON["ResultCode"]) == 1){
                            this.casosFusion = resultadoJSON["Result"];
                            var promises :any = [];
                            promises.push(
                                new Promise((resolve, reject) => { 
                                    this.casosFusion.forEach((element: any, i:any) => {
                                        Promise.all([
                                            this.getIntervencionesAll(element,element["IdCaso"]),
                                            this.getPersonaFusionArchivos(element,element["IdCaso"]),
                                            this.getPersonaFusionTareas(element,element["IdCaso"])
                                        ]).then((resultado: any) => 
                                            {
                                                if (i === this.casosFusion.length - 1){
                                                    resolve();
                                                }
                                            }
                                        );
                                    });
                                })
                            )
                            
                            promises.push(
                                new Promise((resolve, reject) => { 
                                    if(resultadoJSON["Result"].length > 0) {
                                        //var algo = this.GetItemsLinea(resultadoJSON["Result"][0].IdCaso);
                                        //traer chat
                                        this.getChatAll(resultadoJSON["Result"][0].CodCaso).then(() =>{
                                            resolve();
                                        });
                                    } else{
                                        resolve();
                                    }
                                })
                            )

                            Promise.all(promises).then(() => {
                                    this.tieneDatosFusion = this.casosFusion && this.casosFusion.length > 0 ? true : false;
                                    resolve();
                                }
                            );
                            //resolve();
                        }else{
                            resolve();
                            console.log(resultadoJSON["ResultMsg"]);
                        }
                    } else{
                        resolve();
                    }

            }, (error:any) => {
                resolve()
            });
        });
    }

    async getIntervencionesAll(element: any,caso: any){
        let getDatosInter = false;
        let countInt = 1;
        while (!getDatosInter && countInt <= this.maxIntentos)
        {
            var result: any = [];
            await this.getDatosMetodo('intervencion', caso)
            .then((resultadoI: any) => {
                if (resultadoI.name !== "TimeoutError"){
                    getDatosInter = true;
                    if(typeof resultadoI !== "undefined"){
                        var resultadoJSONI = resultadoI;
                        if(Number(resultadoJSONI["ResultCode"]) == 1){
                            resultadoJSONI["Result"].forEach((inter: any)=> {
                                var secciones: any = [];
                                
                                inter.Detalles.forEach((det: any) => {
                                    if (secciones.indexOf(det.CodSeccion) == -1){
                                        secciones.push(det.CodSeccion);
                                    }
                                });

                                if (secciones.length >= 2){
                                    result.push(inter);
                                }
                            });
                            //result = resultadoJSONI["Result"];
                            element["Intervenciones"] = result;//resultadoJSONI["Result"];
                        }else{
                            console.log(resultadoJSONI["ResultMsg"]);
                        }
                    }
                } 
            })

            if (countInt === this.maxIntentos && !result){
                element["Intervenciones"] = [];
            }

            countInt++;
        }
    }

    async getPersonaFusionArchivos(element: any,caso: any){
            let getDatosArchivos = false;
            let countArch = 1;
            while (!getDatosArchivos && countArch <= this.maxIntentos)
            {
                var resultadoJSONA: any;
                countArch++;

                //trae archivos
                await this.apiBackEndService.getPersonaFusionArchivos(caso).subscribe(
                resultadoA => {
                    getDatosArchivos = true;
                    if(typeof resultadoA !== "undefined"){
                        resultadoJSONA = resultadoA;
                        if(Number(resultadoJSONA["ResultCode"]) == 1){
                            element["Archivos"] = resultadoJSONA["Result"];
                        }else{
                            console.log(resultadoJSONA["ResultMsg"]);
                        }
                    }

                }, (error:any) => {
                        if (error.name !== "TimeoutError"){
                            getDatosArchivos = true;
                        }
                    }
                );
            }
    }

    async getPersonaFusionTareas(element: any,caso: any){
        let getDatosTareas = false;
        let countTareas = 1;
        while (!getDatosTareas && countTareas <= this.maxIntentos)
        {
            var resultadoJSONA: any;
            countTareas++;

            this.apiBackEndService.getPersonaFusionTareas(element["CodCaso"]).subscribe(
                resultadoT => {
                    getDatosTareas = true;
                    if(typeof resultadoT !== "undefined"){
                        var resultadoJSONT = resultadoT;
                        if(Number(resultadoJSONT["ResultCode"]) == 1){
                            element["Tareas"] = resultadoJSONT["Result"];
                        }else{
                            console.log(resultadoJSONT["ResultMsg"]);
                        }
                    }
                }, (error:any) => {
                    if (error.name !== "TimeoutError"){
                        getDatosTareas = true;
                    }
                });
        }
    }

    async getChatAll(codCaso: any){
        return new Promise((resolve, reject) => {
            let getDatosChat = false;
            let countChat = 1;
            while (!getDatosChat && countChat <= this.maxIntentos)
            {
                countChat++;
                this.getDatosMetodo('chat', codCaso).then((chat: any) => {
                    if (chat.name !== "TimeoutError"){
                        getDatosChat = true;
                        if (typeof chat !== "undefined" && typeof chat["resultCode"] === "undefined") {
                            if (chat["ResultCode"] == "1") {
                                this.datosFusion = chat["Result"];
                            } else {
                                console.log(chat["ResultMsg"]);
                            }
                        }
                        resolve();
                    } else if (countChat === this.maxIntentos){
                        resolve();
                    }  
                })
            }//);
        });
    }

    getDatosMetodo(metodo: any, caso: any){
        return new Promise((resolve, reject) => {
            if (metodo === 'chat'){
                this.apiBackEndService.getchat(caso).subscribe(
                    (result:any) => {
                        resolve(result);

                    }, (error:any) => {
                        console.log('error',error);
                        resolve(error);
                    }
                );   
            } else if (metodo === 'intervencion'){
                this.apiBackEndService.getPersonaFusionIntervenciones(caso).subscribe(
                    (result:any) => {
                        resolve(result);

                    }, (error:any) => {
                        console.log('error',error);
                        resolve(error);
                    }
                );   
            }
        });
    }

    //Conin
    public getPersonaConin(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaConin().subscribe(
                resultado => {
                this.admisionesConin = resultado["Result"].Admisiones;
                //this.admisionesConin = resultado["Admisiones"];
                resolve();
            });
        });
    }

    getPersonaCPIAUX(){
        return new Promise((resolve, reject) => {
            this.getPersonaCPI();
        });
    }

    //CPI
    public getPersonaCPI(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaCPI().subscribe(
            resultado => {
                this.admisionesCPI = resultado["Result"].Admisiones;
                //this.admisiones = resultado["Admisiones"]
                resolve();
            });
        });
    }

    public getDatosCPI(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getPersonaCPI().subscribe(
                (result:any) => {
                    console.log('result>>>>', result)
                    resolve(result);
                

                }, (error:any) => {
                    console.log('error',error);
                    resolve(error);
                }
            );   
        });
    }

    //

    public calculaEdadFunc(fechaNac: Date, fechaCompara?:Date):string{
            let date = fechaCompara ? fechaCompara : new Date();
            let EdadText;

            var fecha1 = moment(fechaNac);
            var fecha2 = moment(date);

            let anio = fecha2.diff(fecha1, 'year');

            if (anio > 0){
                fechaNac = new Date(fechaNac.getFullYear() + anio, fechaNac.getMonth(), fechaNac.getDate());
            }

            fecha1 = moment(fechaNac);

            let mes =  fecha2.diff(fecha1, 'month');

            if (anio === 0 && mes === 0){
                let dia = fecha2.diff(fecha1, 'day');
                EdadText = dia === 1 ? dia + ' dia' : dia + ' dias';

            } else if (anio === 0 && mes > 0) {
                EdadText = mes === 1 ? mes + ' mes' : mes + ' meses';
            } else if (anio < 6){
                EdadText = (anio === 1 ? anio + ' año ' : anio + ' años ') + (mes === 1 ? mes + ' mes' : mes + ' meses');
            } else if (anio > 5){
                EdadText = anio === 1 ? anio + ' año' : anio + ' años';
            }
            return EdadText;
    }

    public calculaEdad(array: any,fecha: Date){
        return new Promise((resolve, reject) => {
            let date = new Date();
            let EdadText;

            var fecha1 = moment(fecha);
            var fecha2 = moment(date);

            let anio = fecha2.diff(fecha1, 'year');

            if (anio > 0){
                fecha = new Date(fecha.getFullYear() + anio, fecha.getMonth(), fecha.getDate());
            }

            fecha1 = moment(fecha);

            let mes =  fecha2.diff(fecha1, 'month');

            array.anio = anio;
            array.mes = mes;

            if (anio === 0 && mes === 0){
                let dia = fecha2.diff(fecha1, 'day');
                EdadText = dia === 1 ? dia + ' dia' : dia + ' dias';

            } else if (anio === 0 && mes > 0) {
                EdadText = mes === 1 ? mes + ' mes' : mes + ' meses';
            } else if (anio < 6){
                EdadText = (anio === 1 ? anio + ' año ' : anio + ' años ') + (mes === 1 ? mes + ' mes' : mes + ' meses');
            } else if (anio > 5){
                EdadText = anio === 1 ? anio + ' año' : anio + ' años';
            }
            array.Edad = EdadText;
            
            resolve();
        });
    }

    public reset() {
        this.datosPersonales = undefined;
        this.nombreCompleto = undefined;
        this.edad = undefined;
    }

    public listarTarjetas(nombre:string)
    {
        console.log('nombre',nombre);
        console.log('listTarjetas',this.listTarjetas);
        let tarjeta = this.listTarjetas.filter((list:any) => list.Nombre === nombre);
        console.log('tarjeta',tarjeta);
        tarjeta[0].Visible = true;
        tarjeta[0].Checked = true;
        tarjeta[0].Array[0].forEach((a:any) => {
            console.log('datos',a);
        });
    }
}

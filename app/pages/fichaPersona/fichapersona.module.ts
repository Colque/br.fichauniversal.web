import { ApiBackEndService } from './../../_services/apiBackEnd.service';
import { NgModule }      from '@angular/core';
import { BrowserModule, Title  } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';

// used to create fake backend
import { fakeBackendProvider } from './../../_helpers/index';

// import { routing }        from './../app.routing';

import { AuthGuard } from './../../_guards/index';
import { JwtInterceptor } from './../../_helpers/index';
// import {
//     AlertService,
//     AuthenticationService,
//     UserService,
//     ApiBackEndService
// } from './../../_services/index';

import { ServicesModule } from './../../_services/services.module';

//persona
import { FichaPersonaComponent } from './fichaPersona.component';
import { DatosPersonalesComponent } from './datosPersonales/datosPersonales.component';
import { ViviendaComponent } from './vivienda/vivienda.component';
import { MapaComponent } from './mapa/mapa.component';
import { FotosComponent } from './fotos/fotos.component';
import { VideosComponent } from './videos/videos.component';
import { SaludComponent } from './salud/salud.component';
import { VacunasComponent } from './vacunas/vacunas.component';
import { EmbarazoComponent } from './embarazo/embarazo.component';
import { AntecedentesAntropometricosComponent } from './antecedentesAntropometricos/antecedentesAntropometricos.component';
import { AntecedentesNutricionalesPatologicosComponent } from './antecedentesNutricionalesPatologicos/antecedentesNutricionalesPatologicos.component';
import { RiesgosNinioComponent } from './riesgosNinio/riesgosNinio.component';
import { RiesgosEscolarAdolescenteComponent } from './riesgosEscolarAdolescente/riesgosEscolarAdolescente.component';
import { RiesgosEmbarazoComponent } from './riesgosEmbarazo/riesgosEmbarazo.component';
import { EducacionComponent } from './educacion/educacion.component';
import { TrabajoComponent } from './trabajo/trabajo.component';
import { SanitariosServiciosComponent } from './sanitariosServicios/sanitariosServicios.component';
import { InfraestructuraComponent } from './infraestructura/infraestructura.component';
import { ObservacionesUrgenciasComponent } from './observacionesUrgencias/observacionesUrgencias.component';
import { FamiliaComponent } from './familia/familia.component';
import { InteligenciaArtificialComponent } from './inteligenciaArtificial/inteligenciaArtificial.component';
import { AntropometriaComponent } from './antropometria/antropometria.component';
import { GraficoPEComponent } from './graficoPE/graficoPE.component';
import { GraficoTEComponent } from './graficoTE/graficoTE.component';
import { GraficoIMCComponent } from './graficoIMC/graficoIMC.component';
import { MasInformacionComponent } from './masInformacion/masInformacion.component';
import { FusionComponent } from './fusion/fusion.component';
import { CPIComponent } from './CPI/CPI.component';
import { ConinComponent } from './Conin/Conin.component';
import { TestPersonaComponent } from './testPersona/testPersona.component';

import { HistoriaClinicaComponent } from './historiaClinica/historiaClinica.component';
import { DescargarFichaPComponent } from './descargarFichaP/descargarFichaP.component';

// Colombia
import { DatosPersonalesColombiaComponent } from './datosPersonalesColombia/datosPersonalesColombia.component';
import { ViviendaColombiaComponent } from './viviendaColombia/viviendaColombia.component';
import { AntropometriaColombiaComponent } from './antropometriaColombia/antropometriaColombia.component';
import { ServiciosColombiaComponent } from './serviciosColombia/serviciosColombia.component';
import { TrabajoColombiaComponent } from './trabajoColombia/trabajoColombia.component';
import { EducacionColombiaComponent } from './educacionColombia/educacionColombia.component';
import { EmbarazoColombiaComponent } from './embarazoColombia/embarazoColombia.component';
import { SaludColombiaComponent } from './saludColombia/saludColombia.component';
import { FamiliaColombiaComponent } from './familiaColombia/familiaColombia.component';
import { LactantesColombiaComponent } from './lactantesColombia/lactantesColombia.component';

import { DescargarFichaPColombiaComponent } from './descargarFichaPColombia/descargarFichaPColombia.component';


import { Md2Module } from 'Md2';
import {BrowserAnimationsModule} from  "@angular/platform-browser/animations";
// import { TimelineComponent } from './../../components/timeline/timeline.component';
import { MaterialModule } from "@angular/material";

import { AgmCoreModule } from '@agm/core';

//import { SafePipe } from './../../myjs/utils/HtmlPipe';
// import { RegisterComponent } from './../../register/register.component'
//import { ChatWindowComponent } from './../../components/chat/chat-window/chat-window.component';

import { PagesRoutes } from './../pages.routes';

import { PipesModule } from './../../pipes/pipes.module';
import { ComponentsModule } from './../../components/components.module';
import { FichaPersonaDataService } from './fichaPersonaData.service';
import { BlockUIModule } from 'ng-block-ui';
import { TimelineComponent } from '../../components/timeline/timeline.component';
import { HttpClientService } from '../../_services/http-client.service';
import { Router, ActivatedRoute } from '@angular/router';

var http: HttpClient;
var https: HttpClientService;
var router: Router;
var route: ActivatedRoute;

var apiBackEndService: ApiBackEndService = new ApiBackEndService(http,https,router,route);

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MaterialModule,
        AgmCoreModule.forRoot({ apiKey: apiBackEndService.apiKeyGoogleMap }),
        Md2Module,
        ComponentsModule,
        PipesModule,
        PagesRoutes,
        BlockUIModule,
    ],
    declarations: [
        //aqui nuestras tarjetas
        //persona
        FichaPersonaComponent,
        DatosPersonalesComponent,
        ViviendaComponent,
        MapaComponent,
        FotosComponent,
        VideosComponent,
        SaludComponent,
        VacunasComponent,
        EmbarazoComponent,
        AntecedentesAntropometricosComponent,
        AntecedentesNutricionalesPatologicosComponent,
        RiesgosNinioComponent,
        RiesgosEscolarAdolescenteComponent,
        RiesgosEmbarazoComponent,
        EducacionComponent,
        TrabajoComponent,
        SanitariosServiciosComponent,
        InfraestructuraComponent,
        ObservacionesUrgenciasComponent,
        FamiliaComponent,
        InteligenciaArtificialComponent,
        AntropometriaComponent,
        GraficoPEComponent,
        GraficoTEComponent,
        GraficoIMCComponent,
        MasInformacionComponent,
        FusionComponent,
        CPIComponent,
        ConinComponent,
        DescargarFichaPComponent,
        TestPersonaComponent,
        HistoriaClinicaComponent,

        DatosPersonalesColombiaComponent,
        ViviendaColombiaComponent,
        ServiciosColombiaComponent,
        TrabajoColombiaComponent,
        AntropometriaColombiaComponent,
        EducacionColombiaComponent,
        EmbarazoColombiaComponent,
        SaludColombiaComponent,
        FamiliaColombiaComponent,
        LactantesColombiaComponent,
        DescargarFichaPColombiaComponent,
        // TimelineComponent,

        //RegisterComponent,
        //SafePipe,
        //ChatWindowComponent,
    ],
    exports: [
        FichaPersonaComponent,
        DatosPersonalesComponent,
        ViviendaComponent,
        MapaComponent,
        FotosComponent,
        VideosComponent,
        SaludComponent,
        VacunasComponent,
        EmbarazoComponent,
        AntecedentesAntropometricosComponent,
        AntecedentesNutricionalesPatologicosComponent,
        RiesgosNinioComponent,
        RiesgosEscolarAdolescenteComponent,
        RiesgosEmbarazoComponent,
        EducacionComponent,
        TrabajoComponent,
        SanitariosServiciosComponent,
        InfraestructuraComponent,
        ObservacionesUrgenciasComponent,
        FamiliaComponent,
        InteligenciaArtificialComponent,
        AntropometriaComponent,
        GraficoPEComponent,
        GraficoTEComponent,
        GraficoIMCComponent,
        MasInformacionComponent,
        FusionComponent,
        CPIComponent,
        ConinComponent,
        DescargarFichaPComponent,
        TestPersonaComponent,
        HistoriaClinicaComponent,

        DatosPersonalesColombiaComponent,
        ViviendaColombiaComponent,
        ServiciosColombiaComponent,
        TrabajoColombiaComponent,
        AntropometriaColombiaComponent,
        EducacionColombiaComponent,
        EmbarazoColombiaComponent,
        SaludColombiaComponent,
        FamiliaColombiaComponent,
        LactantesColombiaComponent,
        DescargarFichaPColombiaComponent,
        // TimelineComponent
    ],
    providers: [
        AuthGuard,
        // AlertService,
        // AuthenticationService,
        
        FichaPersonaDataService,
        Title,
        //UserService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
        //aqui nuestro provider
        //ApiBackEndService,

        // provider used to create fake backend
        fakeBackendProvider,
    ],
    bootstrap: []
})

export class FichaPersonaModule { }
import { CPIComponent } from './../CPI/CPI.component';
import { FusionComponent } from './../fusion/fusion.component';
import { Component, OnInit, HostListener, ChangeDetectorRef, EventEmitter, Input, ɵConsole, NgZone } from '@angular/core';
//import {NgControl} from '@angular/common';
import { ApiBackEndService } from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';
import { DomSanitizer, SafeResourceUrl, SafeUrl, Title } from '@angular/platform-browser';
import { TLItem, MultimediaType } from './../../../components/timeline/model/tl-item';
//paraloading
import './../../../../js/myplugins.js'
import { Sanitizer } from '@angular/core/src/security';
declare var myLoading: any;
declare var galeria: any;
declare var modales: any;

import { NgxGalleryAnimation, NgxGalleryImage, NgxGalleryOptions } from 'ngx-gallery';
import * as moment from 'moment';
import { ViewRef_ } from '@angular/core/src/view';

declare var carouselFotos: any;

@Component({
    selector: 'tarjetaFotos',
    templateUrl: 'fotos.component.html',
    styleUrls: ['fotos.component.css']
})

export class FotosComponent implements OnInit {
    openLinea: boolean = false;
    fusionFoto: boolean = false;
    fotos: any = [];
    prefijo = 'FOT';
    CantidadFotos = 0;
    CodPersona = '';
    galeriaCargada = false;
    viewerActivado = false;
    timeLineItems: any = [];
    tieneItemsTimeLine = false;
    galleryOptions: NgxGalleryOptions[];
    galleryHeaderImageOptions: NgxGalleryOptions[];
    galleryImages: NgxGalleryImage[];
    galleryImagesHeader: NgxGalleryImage[];
    timeLineItemsCardGallery: TLItem[] = [];

    fotos1: TLItem[] = [];

    fotos12: any

    public selectedItemPosition: number;
    public widthmodalGalery = 0;
    public selectedItem: TLItem;
    public widthmodal = 0;
    elementRef: any;
    screenHeight: number;
    public _display_width: number;
    orientation = 'none';

    //Cpi
    admisiones: any;
    fotosAdmision: any = [];
    private ActividadesFotosLinea: any;
    testPersona: any;
    detalleTest: any = [];
    timeLineItemsCpi: any = [];
    timeLineItemsTaller: any= [];

    //Fusion
    casos: any;
    fotosIntervenciones: any;
    InterventionsDetails: any[] = []
    intervencionIndice: number = 0
    timeLineItemsFusion: any = [];
    private fotosCasos: any[] = [];
    private fusionLoadFirstTime = true;
    public fusionComp = new FusionComponent(this.apiBackEndService, this.fichaPService, this.title, this.changeDetectorRef);
    public cpiComp = new CPIComponent(this.fichaPService,this.apiBackEndService, this.title, this.zone);
    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService,
        private changeDetectorRef: ChangeDetectorRef,
        private title:Title,
        private zone: NgZone,
        private sanitizer: DomSanitizer) {
        this.clear()
    }

    public getFotos() {

        Promise.all([
            this.fichaPService.getPersonaFotos(),
            this.cpiComp.loadData(),
            this.fusionComp.loadData()
        ]).then((resultado: any) => {
            let _personasFotos = resultado[0];
            // Personas Fotos
            //this.fotos = this.fichaPService.fotos;
            var promises :any = [];
            promises.push(
            new Promise((resolve, reject) => { 
                    if (_personasFotos && _personasFotos.length > 0) {
                        let fotoAux: any[] = _personasFotos;
                        this.CantidadFotos = _personasFotos.length;
                        _personasFotos.forEach((elementA: any, i: any) => {
                            if (elementA["FotoMedium"] != null) {
                                var fechaArray = elementA["FechaRegistro"].split("-");

                                var itemLinea = {
                                    FotoTitulo : elementA["FotoTitulo"],
                                    url : elementA["FotoMedium"],
                                    FotoDescrip : elementA["FotoDescrip"],
                                    type : 'imagen',
                                    FotoLarge : elementA.FotoFull,// + '?h=600' || elementA.URL,
                                    FotoSmall : elementA.FotoSmall + '?h=100' || elementA.URL,
                                    FotoMedium : elementA.FotoSmall + '?h=300' || elementA.URL,
                                    category : "persona",
                                    FechaRegistro : new Date(fechaArray[0], fechaArray[1]-1, fechaArray[2].substring(0,2)),
                                    Fecha : fechaArray[2].substring(0,2) + "-" + fechaArray[1] + "-" + fechaArray[0].substring(2,4),
                                    base64: elementA.base64,
                                    columns: elementA.columns
                                }
                                this.timeLineItems.push(itemLinea);
                                // this.timeLineItemsCardGallery.push(itemLinea);
                            }
                            if (i === _personasFotos.length - 1){
                                console.log('loadFotos');
                                resolve();
                            }
                        });
                    } else {
                        resolve();
                    }
                })
            )

            //Perssona CPI
            this.admisiones = this.fichaPService.admisionesCPI;
            let mThis = this;
            promises.push(
            new Promise((resolve, reject) => { 
                    if (this.admisiones.length > 0) {
                        //myLoading.Ocultar('Loading' + this.prefijo);
                        this.fichaPService.tieneDatosCPI = true;
                        this.GetItemsLinea(this.admisiones[0].Admision, this.admisiones[0].FechaIngreso).then(() => {
                            console.log('loadFotosCPI');
                            setTimeout( function(){
                                resolve();
                            }, 100);
                        });
                    } else {
                        console.log('loadFotosSinCPI');
                        resolve();
                    }
                })
            )

            //Personas Fusion
            this.casos = this.fichaPService.casosFusion; 
            promises.push(
            new Promise((resolve, reject) => { 
                    if (this.casos.length > 0) {
                        if (this.casos[0].Intervenciones && this.casos[0].Intervenciones.length > 0) {
                            //this.formatDetail(this.casos[0].Intervenciones[0].Detalles, 0);
                            this.GetItemsLineaFusion(this.casos[0].Caso).then(() =>{
                                console.log('loadFotosFusion');
                                resolve();
                            });
                        } else{
                            console.log('loadFotosSinFusion');
                            resolve();
                        }
                    } else{
                        console.log('loadFotosSinFusion');
                        resolve();
                    }
                })
            )

            Promise.all(promises).then(() => 
            {   
                let c = [].concat(this.timeLineItemsCpi).concat(this.timeLineItems).concat(this.timeLineItemsTaller).concat(this.timeLineItemsFusion);
                console.log('c',c);
                if (c.length > 0){
                    c = this.apiBackEndService.ordernarArrayPorFecha(c,"FechaRegistro");
                    c.forEach((f,i) => {
                        if (f.base64){        
                            let indexF = this.fotos.map(function(e:any) { return e.base64; }).indexOf(f.base64);                        
                            f.base64 = f.base64;
                            f.columns = f.columns;
                            if (indexF === -1){
                                this.fotos.push(f);
                            } 
                        }
                        if (i === c.length -1){
                            this.fichaPService.fotos = this.fotos;
                            this.tieneItemsTimeLine = true;
                        }
                    });
                } else{
                    this.fichaPService.tieneFotosP = false;
                }                
            });
        });
    }
    // Fotos Fusion

    GetItemsLineaFusion(_Caso: string): any {
        return new Promise((resolve, reject) => {
        // var resultadoTL: any = [];
        let mThis = this;
        this.casos.forEach((element: any, i: any) => {
            //if (element["Caso"] == _Caso) {
                if (element["Archivos"] && element["Archivos"].length > 0) {
                    var promises :any = [];
                    promises.push(
                    new Promise((resolve, reject) => { 
                        element["Archivos"].forEach((elementA: any, i2: any) => {
                            if (elementA["URL"] != null && elementA["TipoArchivo"] != "AUDIO") {
                                mThis.apiBackEndService.getBase64(elementA["URL"]).then((base:any) =>{
                                    var fechaArray = elementA["FechaRegistro"].split("/");
                                    var itemLinea = {
                                        FotoTitulo : elementA["Titulo"],
                                        url : elementA["URL"],
                                        FotoDescrip : elementA["Descripcion"],
                                        type : this.tipoArchivo(elementA["TipoArchivo"]),
                                        FotoLarge : elementA.FotoFull || elementA.URL,
                                        FotoSmall : elementA["URL"] + '?h=100' || elementA.URL,
                                        FotoMedium : elementA["URL"] + '?h=300' || elementA.URL,
                                        category : "persona",
                                        FechaRegistro : new Date(fechaArray[2], fechaArray[1]-1, fechaArray[0]),
                                        Fecha : fechaArray[0] + "-" + fechaArray[1] + "-" + fechaArray[2].substring(2,4),
                                        base64 : base.base64,
                                        columns : base.columns
                                    }
                                    this.timeLineItemsFusion.push(itemLinea);

                                    if (i2 === element["Archivos"].length - 1){
                                        resolve();
                                    }
                                });
                            } else {
                                if (i2 === element["Archivos"].length - 1){
                                    resolve();
                                }
                            }
                        });
                    })
                  )

                  Promise.all(promises).then(() => 
                        {   
                            if (i ===  this.casos.length - 1){
                                resolve();
                            }
                        }
                    );
                } else{
                    if (i ===  this.casos.length - 1){
                        resolve();
                    }
                }
            //}
        });
        });
    }
    // Fotos CPI

    tipoArchivo(COD: string): any {
        var resultado = "";
        switch (COD) {
            case "COD_IMAGEN":
                resultado = "imagen";
                break;
            case "COD_VIDEO":
                resultado = "video";
                break;
            case "COD_AUDIO":
                resultado = "audio";
                break;
        }

        return resultado;
    }

    async GetFotosActividadesParaLinea(_Taller: any) {
        //return new Promise((resolve, reject) => {
        //this.timeLineItemsTaller = [];
        let mThis = this;
        if (typeof _Taller !== "undefined") {
            if (_Taller["Fotos"] && _Taller["Fotos"].length > 0) {
                let i = 0;
                while (i < _Taller["Fotos"].length)
                {
                    let elementA = _Taller["Fotos"][i]
                    //i++;
                //}
                //_Taller["Fotos"].forEach((elementA: any, i: any) => {
                    if (elementA["URL"] != "" && elementA["URL"] != null) {
                        await mThis.apiBackEndService.getBase64(elementA["URL"]).then((base:any) =>{
                            if (base){
                                var fechaArray = elementA["FechaRegistro"].split("/");
                                var itemLinea = {
                                    FotoTitulo : _Taller["Taller"],
                                    url : elementA["URL"],
                                    FotoDescrip : elementA["Descripcion"],
                                    type : this.tipoArchivo(elementA["TipoArchivo"]),
                                    FotoLarge : elementA.FotoFull || elementA.URL,
                                    FotoSmall : elementA["URL"] + '?h=100' || elementA.URL,
                                    FotoMedium : elementA["URL"] + '?h=300' || elementA.URL,
                                    category : "persona",
                                    FechaRegistro : new Date(fechaArray[2], fechaArray[1]-1, fechaArray[0]),
                                    Fecha : fechaArray[0] + "-" + fechaArray[1] + "-" + fechaArray[2].substring(2,4),
                                    base64 : base.base64,
                                    columns : base.columns
                                } 
                                mThis.timeLineItemsTaller.push(itemLinea);
                            }
                        });

                        if (i ===  _Taller["Fotos"].length - 1){
                            return mThis.timeLineItemsTaller;
                        }
                    } 
                    else{
                        if (i ===  _Taller["Fotos"].length - 1){
                            return mThis.timeLineItemsTaller;
                        }
                   }   

                   i++;
                }


                //});
            } 
            else{
                return mThis.timeLineItemsTaller;
            }
        } 
        else{
            return this.timeLineItemsTaller;
        }
    }

    GetItemsLinea(_Admision: string, _FechaIngreso: string): any {
        return new Promise((resolve, reject) => {
        this.timeLineItemsCpi = [];
        let mThis = this;
        this.admisiones.forEach((element: any, i: any) => {
            //if (element["Admision"] == _Admision && element["FechaIngreso"] == _FechaIngreso) {
                if (element["Archivos"] && element["Archivos"].length > 0) {
                    //agregamos fotos de Archivo
                    var promises :any = [];
                    promises.push(
                        new Promise((resolve, reject) => { 
                            element["Archivos"].forEach((elementA: any,i2: any) => {
                                if (elementA["URL"] != null) {
                                    mThis.apiBackEndService.getBase64(elementA["URL"]).then((base:any) =>{
                                        var fechaArray = elementA["FechaRegistro"].split("/");
                                        var itemLinea = {
                                            FotoTitulo : elementA["Titulo"],
                                            url : elementA["URL"],
                                            FotoDescrip : elementA["Descripcion"],
                                            type : this.tipoArchivo(elementA["TipoArchivo"]),
                                            FotoLarge : elementA["URL"] || elementA.URL,
                                            FotoSmall : elementA["URL"] + '?h=100' || elementA.URL,
                                            FotoMedium : elementA["URL"] + '?h=300' || elementA.URL,
                                            category : "persona",
                                            FechaRegistro : new Date(fechaArray[2], fechaArray[1]-1, fechaArray[0]),
                                            Fecha : fechaArray[0] + "-" + fechaArray[1] + "-" + fechaArray[2].substring(2,4),
                                            base64 : base.base64,
                                            columns : base.columns
                                        }
                                        this.timeLineItemsCpi.push(itemLinea);

                                        if (i2 === element["Archivos"].length - 1){
                                            setTimeout( function(){
                                                resolve();
                                            }, 100);
                                        }
                                    })
                                } else{
                                    if (i2 === element["Archivos"].length - 1){
                                        setTimeout( function(){
                                                resolve();
                                            }, 100);
                                    }
                                }
                            });
                        })
                    )
                    //agregamos fotos de Talleres
                    promises.push(
                        new Promise((resolve, reject) => { 
                            if (element["Talleres"] && element["Talleres"].length > 0){
                                var promisesT :any = [];
                                element["Talleres"].forEach((elementC: any, i3: any) => {
                                    promisesT.push(
                                    new Promise((resolve, reject) => {
                                        this.GetFotosActividadesParaLinea(elementC).then(() => {
                                            resolve();
                                        })
                                    })
                                    );
                                });

                                Promise.all(promisesT).then(() => 
                                        {   
                                            resolve();
                                        }
                                    );
                            } else{
                                resolve();
                            }
                        })
                    )

                    Promise.all(promises).then(() => 
                        {   
                            if(i === this.admisiones.length -1){
                                setTimeout( function(){
                                                resolve();
                                            }, 100);
                                //resolve();
                            }
                        }
                    );

                } else {
                    if(i === this.admisiones.length -1){
                        resolve();
                    }
                }
            //}
        });
        });
    }

    // FIN FOTOS CPI

    ngOnInit() {
        // this.loadData();
        // this.loadDataCpi();
        // this.loadFotosFusion()
        this.getFotos();
    }

    clear() {
        this.galleryImages = [];
        this.galleryImagesHeader = [];
        this.timeLineItems = [];
    }

    loadData() {
        this.timeLineItems = [];
        var a = ''
        var principal = this;
        this.fichaPService.getPersonaFotos().then(() => {
            this.fotos = this.fichaPService.fotos;
            if (this.fotos && this.fotos.length > 0) {
                console.log('fotos', this.fotos);
                principal.CantidadFotos = this.fotos.length;
                this.fotos.forEach((elementA: any, i: any) => {
                    if (elementA["FotoMedium"] != null) {
                        var fechaArray = elementA["Fecha"].split("-");
                        var itemLinea: TLItem = new TLItem();
                        itemLinea.name = elementA["FotoTitulo"];
                        itemLinea.url = elementA["FotoMedium"];
                        itemLinea.description = elementA["FotoDescrip"];
                        itemLinea.type = 'imagen';
                        itemLinea.category = elementA["Categoria"];
                        itemLinea.urlbig = elementA.FotoFull + '?h=600'
                        itemLinea.urlsmall = elementA.FotoSmall + '?h=100'
                        itemLinea.urlmedium = elementA.FotoSmall + '?h=300'
                        itemLinea.date = new Date(Number("20" + fechaArray[2]), fechaArray[1] - 1, fechaArray[0]);
                        this.tieneItemsTimeLine = true;
                        this.timeLineItems.push(itemLinea);
                        this.timeLineItemsCardGallery.push(itemLinea);                        
                    }     

                    if(i===this.fotos.length-1) 
                     {
                        this.timeLineItemsCardGallery.sort(function(a : any, b: any) {
                            var dateA = new Date(a["date"]).getTime(), dateB = new Date(b["date"]).getTime();
                            return  dateA - dateB;
                        });                          
                    }


                });

                let data1 =this.timeLineItemsCardGallery.sort(function(a :any, b: any) {                   
                    var dateA = new Date(a["FechaRegistro"]).getTime(), dateB = new Date(b["FechaRegistro"]).getTime();
                    return dateA - dateB;
                });                 
                //console.log("fotos >>",  data) ;                       
                console.log('this.timeLineItems', this.timeLineItems);

                let data =this.fotos.sort(function(a :any, b: any) {                   
                    var dateA = new Date(a["FechaRegistro"]).getTime(), dateB = new Date(b["FechaRegistro"]).getTime();
                    return dateA - dateB;
                });                 
                //console.log("fotos >>",  data) ;                       
                console.log('this.timeLineItems', this.timeLineItems);
            } else {
                this.fichaPService.tieneFotosP = false;
            }
        });
    }
    AbreModal() {
        modales.Abrir('lineamodal');
    }

    urlSafe(urlUnsafe: string): SafeUrl {
        return this.sanitizer.bypassSecurityTrustResourceUrl(urlUnsafe);
    }

    public activarGaleria(i: any) {
        // if (i === this.timeLineItemsCardGallery.length - 1) {
        //     myLoading.Ocultar('Loading' + this.prefijo);
        // }
        if (!this.galeriaCargada) {
            this.galeriaCargada = true;
            galeria.ActivaGaleriaFotos();
        }
    }

    public activarViewer(i: any) {
        if (!this.viewerActivado) {
            this.viewerActivado = true;
            galeria.ActivarViewerFotos();
            myLoading.Ocultar('Loading' + this.prefijo);
        }
    }
}
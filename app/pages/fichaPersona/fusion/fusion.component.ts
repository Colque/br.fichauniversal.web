import { Component, OnInit, Input, ViewChild, ChangeDetectorRef } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var ByOso: any;

//linea de tiempo
import {TLItem} from './../../../components/timeline/model/tl-item';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import { Md2Dialog } from 'Md2';

import { ViewRef_ } from '@angular/core/src/view';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Title } from '@angular/platform-browser';

declare var carouselFotos: any;

@Component({
    selector: 'tarjetaFusion',
    templateUrl: 'fusion.component.html',
    styleUrls: ['fusion.component.scss']
})

export class FusionComponent implements OnInit {
    @ViewChild('ModalFusion', undefined) ModalFusion: Md2Dialog;
    casos: any;
    @BlockUI() blockUI: NgBlockUI;
    error: string = "";
    intervenciones2: any;
    InterventionsDetails: any[]= []
    intervencionIndice: number = 0
    archivos: Observable<Array<any>>;
    intervenciones: Observable<Array<any>>;
    esPrimeraVez = true
    tabindex:number=0
    prefijo = 'FUS';
    timeLineItems: TLItem[] = [];
    tieneItemsTimeLine = false;
    showChat: boolean = false;
    private datos: any[] = [];
    public nombreCompleto : string;
    public datosPersonales: any;
    public contadorIntervenciones: any;
    tabIndex = 0;
    fotosIntervenciones: any;

    private fotosCasos: any[] = [];
    constructor(
        private apiBackEndService: ApiBackEndService,
        public fichaPService: FichaPersonaDataService,
        private title:Title,
        private changeDetectorRef: ChangeDetectorRef
    ) {

    }

    ngOnInit(){
        //this.loadData();
        //this.DataModal()
        let mThis = this;
        this.nombreCompleto = this.fichaPService.nombreCompleto;
        this.casos = this.fichaPService.casosFusion;
        this.datos = this.fichaPService.datosFusion;
        if (this.casos[0].Intervenciones && this.casos[0].Intervenciones.length > 0){
            this.formatDetail(this.casos[0].Intervenciones[0].Detalles,0);
            this.GetItemsLinea(this.casos[0].Caso);
        }
    }

    open() {
        this.blockUI.stop();
        this.error = "";
        this.ModalFusion.open();
    }

    detectChanges() {
        if (this.changeDetectorRef !== null &&
        this.changeDetectorRef !== undefined &&
        !(this.changeDetectorRef as ViewRef_).destroyed) {
        this.changeDetectorRef.detectChanges();
        }
        }

    formatDetail(detallesIntervencionSeleccionada: any[], index: number) {       
            let sections:any = [];
            this.fotosIntervenciones = [];
            for (const d of detallesIntervencionSeleccionada) {
                let section = sections.find((s:any) => s.SectionCode == d.CodSeccion);
                if (section) {
                    if (d.TipoDatoCodigo == 'COD_IMAGEN'){
                        this.fotosIntervenciones.push({
                            url: d.Respuesta
                        })
                    }

                    if (d.Pregunta === 'Edad'){
                        let question = section.QuestionsAnswers.find((q:any) => q.Question == 'Fecha de Nacimiento');
                        var fechaArray = question.Answer.split("-");
                        let dateFecha = new Date(Number(fechaArray[0]), fechaArray[1] - 1, fechaArray[2].substr(0,2));
                        let edad = this.fichaPService.calculaEdadFunc(dateFecha);
                        
                                section.QuestionsAnswers.push({
                                Question: d.Pregunta,
                                Answer: d.Respuesta,
                                DataTypeCode: d.TipoDatoCodigo,
                                Edad: edad
                            });
                    } else{
                        section.QuestionsAnswers.push({
                            Question: d.Pregunta,
                            Answer: d.Respuesta,
                            DataTypeCode: d.TipoDatoCodigo,
                            Edad: null
                        });
                    }
                } else {
                        if (d.TipoDatoCodigo == 'COD_IMAGEN'){
                            this.fotosIntervenciones.push({
                                url: d.Respuesta
                            })
                        }
                        if (d.Pregunta === 'Edad'){
                            let question = section.QuestionsAnswers.find((q:any) => q.Question == 'Fecha de Nacimiento');
                            var fechaArray = question.Answer.split("-");
                            let dateFecha = new Date(Number(fechaArray[0]), fechaArray[1] - 1, fechaArray[2].substr(0,2));
                            let edad = this.fichaPService.calculaEdadFunc(dateFecha);
                            sections.push({
                                SectionCode: d.CodSeccion,
                                Section: d.Seccion,
                                QuestionsAnswers: [
                                    {
                                        Question: d.Pregunta,
                                        Answer: d.Respuesta,
                                        DataTypeCode: d.TipoDatoCodigo,
                                        Edad: d.Edad
                                    }
                                ]
                            });
                        } else{

                            sections.push({
                                SectionCode: d.CodSeccion,
                                Section: d.Seccion,
                                QuestionsAnswers: [
                                    {
                                        Question: d.Pregunta,
                                        Answer: d.Respuesta,
                                        DataTypeCode: d.TipoDatoCodigo,
                                        Edad: null
                                    }
                                ]
                            });

                        }
                    }
                }
                this.intervencionIndice= index;
                this.InterventionsDetails[index] = sections;
                this.detectChanges()
                carouselFotos.cargar("carouselInterven",this.fotosIntervenciones.length)
                console.log("this.InterventionsDetails["+index+"]", this.InterventionsDetails[index]);
            }

    public openModal(){
        //ByOso.openModalVisita(index,modal);
        this.ModalFusion.open();
    }

    public closeModal(){
        this.ModalFusion.close()
    }

    public loadData() {
        return new Promise((resolve, reject) => {
            if (this.apiBackEndService.currentUser.CodSistemaPermisoList.indexOf("COD_FUSION") != -1){
                this.fichaPService.getPersonaFusionCasos().then(()=>{
                    resolve();
                });
            } else {
                resolve();
            }
        });
    }

    onChangeTab(event:any){
        this.tabIndex = event.index;
        this.GetItemsLinea(this.casos[event.index].Caso);
    }


    GetItemsLinea(_Caso: string): any{
        var resultadoTL: any = [];
        this.casos.forEach((element: any) => {
            if(element["Caso"] == _Caso){
                if(element["Archivos"] && element["Archivos"].length > 0){
                    element["Archivos"].forEach((elementA: any) => {
                        if(elementA["URL"] != null && elementA["TipoArchivo"] != "AUDIO"){
                            var fechaArray = elementA["FechaRegistro"].split("/");
                            var itemLinea = {
                                FotoTitulo : elementA["Titulo"],
                                url : elementA["URL"],
                                FotoDescrip : elementA["Descripcion"],
                                type : this.tipoArchivo(elementA["TipoArchivo"]),
                                FotoLarge : elementA.FotoFull || elementA.URL,
                                FotoSmall : elementA.FotoSmall || elementA.URL,
                                FotoMedium : elementA.FotoSmall || elementA.URL,
                                category : "persona",
                                FechaRegistro : new Date(fechaArray[2], fechaArray[1]-1, fechaArray[0]),
                                Fecha : fechaArray[0] + "-" + fechaArray[1] + "-" + fechaArray[2].substring(2,4),
                            }
                            this.tieneItemsTimeLine = true;
                            resultadoTL.push(itemLinea);
                        }
                    });
                }
            }
        });
        this.fotosCasos = resultadoTL;
    }

    MostrarLinea(_Caso: string): boolean{
        return (this.fotosCasos.length > 0);
    }

    tipoArchivo(COD: string): any{
        var resultado = "";
        switch(COD){
            case "IMAGEN":
                resultado = "imagen";
            break;
            case "VIDEO":
                resultado = "video";
            break;
            case "AUDIO":
                resultado = "audio";
            break;
        }

        return resultado;
    }

    iconoTarea(_Tarea: any): string{
        var Icono = "zmdi-circle";
        
        if(typeof _Tarea["Dependencias"] !== "undefined"){
            if(_Tarea["Dependencias"].length){
                Icono = "zmdi-caret-down-circle";
            }
        }

        return Icono;
    }

}
import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var ByOsoAntro: any;


@Component({
    selector: 'tarjetaGraficoIMC',
    templateUrl: 'graficoIMC.component.html',
    //styleUrls: ['./app/components/todolist/todolist.component.css']
})

export class GraficoIMCComponent implements OnInit{
    coordenadas: Observable<Array<any>>;
    historial: Observable<Array<any>>;
    TieneGraficoIMC: boolean;
    prefijo = 'GIMC';

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        let mThis = this;
        this.fichaPService.getPersonaGraficoIMC().then(() => {
            this.coordenadas = this.fichaPService.coordenadasIMC;
            this.historial = this.fichaPService.historialIMC;
            this.TieneGraficoIMC = this.fichaPService.TieneGraficoIMC;
            setTimeout( function(){
                ByOsoAntro.grafico("IMC");
                if (mThis.TieneGraficoIMC){
                    myLoading.Ocultar('Loading'+ mThis.prefijo);
                }
            }, 100);
        })
    }
}
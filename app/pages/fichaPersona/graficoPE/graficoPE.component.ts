import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var ByOsoAntro: any;


@Component({
    selector: 'tarjetaGraficoPE',
    templateUrl: 'graficoPE.component.html',
    //styleUrls: ['./app/components/todolist/todolist.component.css']
})

export class GraficoPEComponent implements OnInit{
    coordenadas: any;
    historial: Observable<Array<any>>;
    TieneGraficoPE: boolean;
    prefijo = 'GPE';

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService
    ) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        let mThis = this;
        this.fichaPService.getPersonaGraficoPE().then(() => {
            this.coordenadas = this.fichaPService.coordenadasPE;
            this.historial = this.fichaPService.historialPE;
            this.TieneGraficoPE = this.fichaPService.TieneGraficoPE;
            setTimeout( function(){
                ByOsoAntro.grafico("PE");
                if (mThis.TieneGraficoPE){
                    myLoading.Ocultar('Loading'+ mThis.prefijo);
                }
            }, 100);
        })
    }
}
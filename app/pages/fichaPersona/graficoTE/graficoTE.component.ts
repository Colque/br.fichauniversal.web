import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js';

declare var myLoading: any;
declare var ByOsoAntro: any;


@Component({
    selector: 'tarjetaGraficoTE',
    templateUrl: 'graficoTE.component.html',
    //styleUrls: ['./app/components/todolist/todolist.component.css']
})

export class GraficoTEComponent implements OnInit{
    coordenadas: Observable<Array<any>>;
    historial: Observable<Array<any>>;
    TieneGraficoTE: boolean;
    prefijo = 'GTE';
    origenes: any;

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        let mThis = this;
        this.fichaPService.getPersonaGraficoTE().then(() => {
            this.coordenadas = this.fichaPService.coordenadasTE;
            this.historial = this.fichaPService.historialTE;
            this.TieneGraficoTE = this.fichaPService.TieneGraficoTE;
            setTimeout( function(){
                ByOsoAntro.grafico("TE");
                if (mThis.TieneGraficoTE){
                    myLoading.Ocultar('Loading'+ mThis.prefijo);
                }
            }, 100);
        })
    }
}
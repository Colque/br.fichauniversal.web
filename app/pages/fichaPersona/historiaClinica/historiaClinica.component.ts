import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var ByOso: any;

@Component({
    selector: 'tarjetaHistoriaCli',
    templateUrl: 'historiaClinica.component.html',
    styleUrls: ['historiaClinica.component.css']
})

export class HistoriaClinicaComponent implements OnInit {
    historiaClinica: any[] = [];
    origenes: Observable<Array<any>>;
    prefijo = 'HISTCLI';



    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService
    ) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fichaPService.getPersonaHistoriaClinica().then(() =>{
            this.historiaClinica = this.fichaPService.historiaClinica;
            if (this.historiaClinica && this.historiaClinica.length > 0){
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaPService.tieneHistCli = false;
            }
        })
        // var promises :any = [];
        // promises.push(
        //     new Promise((resolve, reject) => { 
        //         this.fichaPService.getPersonaAntropometria().then(() => {
        //             this.antropometria = this.fichaPService.antropometria;
        //             resolve();
        //         });
        //     })
        // )

        // promises.push(
        //     new Promise((resolve, reject) => { 
        //         this.fichaPService.getPersonaOrigenesAntropometria().then(() => {
        //             this.origenes = this.fichaPService.origenes;
        //             resolve();
        //         })
        //     })
        // )

        // Promise.all(promises).then(() => 
        //     {
        //         if (this.antropometria && this.antropometria.length > 0){
        //             myLoading.Ocultar('Loading'+ this.prefijo);
        //         } else{
        //             this.fichaPService.tieneAntroP = false;
        //         }
        //     }
        // );
    }
    
    Color(_Color: string): string {
        _Color = _Color == null ? "" : _Color;
        return _Color;
    }

    EdadSmall(_Edad: string): string{
        if(typeof _Edad !== "undefined"){
            _Edad = _Edad.replace("meses", "m");
            _Edad = _Edad.replace("mes", "m");
            _Edad = _Edad.replace("años", "a");
            _Edad = _Edad.replace("año", "a");
            _Edad = _Edad.replace("dias", "d");
            _Edad = _Edad.replace("dia", "d");
        }

        return _Edad;
        
    }

    public openModal(index: any, modal: string){
        ByOso.openModalVisita(index,modal);
    }
}
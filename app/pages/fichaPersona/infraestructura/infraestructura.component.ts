import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaInfraestructura',
    templateUrl: 'infraestructura.component.html',
    styleUrls: ['infraestructura.component.scss']
})

export class InfraestructuraComponent implements OnInit{
    infraestructura: any;
    prefijo = 'INFR';

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService
    ) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fichaPService.getPersonaInfraestructura().then(() => {
            this.infraestructura = this.fichaPService.infraestructura;
            if (this.infraestructura && this.infraestructura.length > 0){
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaPService.tieneInfraestructuraP = false;
            }
        })
    }
    
}
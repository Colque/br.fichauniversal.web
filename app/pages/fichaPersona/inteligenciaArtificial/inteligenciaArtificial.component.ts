import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var myStringFunctions: any;


@Component({
    selector: 'tarjetaInteligenciaArtificial',
    templateUrl: 'inteligenciaArtificial.component.html',
    styleUrls: ['inteligenciaArtificial.component.css']
})

export class InteligenciaArtificialComponent implements OnInit{
    inteligenciaArtificial: Observable<Array<any>>;
    inteligenciaFamilia: any = [];
    prefijo = 'INTE';
    stringFunctions = myStringFunctions;

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        //var promises :any = [];
        // this.fichaPService.getPersonaInteligenciaArtificial().then((result) => {
        //     let tematicas: any = result ? result : [];
        //     if (tematicas.length > 0){
        //         this.generaDatosInteligencia(tematicas).then((promises: any) => {
        //             Promise.all(promises).then(() => 
        //                 {   
        //                     this.fichaPService.inteligenciaFamilia = this.inteligenciaFamilia;
        //                     if (this.inteligenciaFamilia.length > 0){
        //                         myLoading.Ocultar('Loading'+ this.prefijo);
        //                     } else{
        //                         this.fichaPService.tieneInteligenciaFam = false;
        //                     }
        //                 }
        //             );
        //         })
        //     } else{
        //         this.fichaPService.tieneInteligenciaFam = false;
        //     }
        // })

        this.fichaPService.tieneInteligenciaFam = false;
        /*this.apiBackEndService.getPersonaInteligenciaArtificial().subscribe(resultado => {
            this.inteligenciaArtificial = JSON.parse(resultado.toString());
        });*/
    }

    generaDatosInteligencia(tematicas: any){
        return new Promise((resolve, reject) => {
            var promises :any = [];
            tematicas.forEach((elementA:any, i:any) => {
                //if(elementA["CodTematica"] != ""){
                    this.apiBackEndService.getPersonaResultadosIA(elementA["CodTematica"]).subscribe(resultado => {
                        promises.push(
                        new Promise((resolve, reject) => { 
                            if(resultado["Result"] && resultado["Result"].length > 0){
                                resultado["Result"].forEach((elementB: any, j:any) => {
                                    //console.log(elementB);
                                    if(elementB["Resultado"] != ""){
                                        this.inteligenciaFamilia.push(
                                            {
                                                Apellido: myStringFunctions.Capitalizar(elementB["Apellidos"], true),
                                                Nombre: myStringFunctions.Capitalizar(elementB["Nombres"], true),
                                                NombreCompleto: myStringFunctions.Capitalizar((elementB["Apellidos"] + ", " + elementB["Nombres"]), true),
                                                Porcentaje: Number(elementB["Porcentaje"]),
                                                Resultado: myStringFunctions.Capitalizar(elementB["Resultado"], false),
                                                Modelo: myStringFunctions.Capitalizar(elementA["Tematica"], false)
                                            }
                                        );
                                    }
                                    if (j === resultado["Result"].length - 1){
                                        resolve();
                                    }
                                });
                            } else{
                                resolve();
                            }
                        })
                        );
                        if (i === tematicas.length - 1){
                            resolve(promises);
                        }
                    });
            });
        });
    }

    FactoresLista(Factores: string): string {
        var Lista = Factores.split("#");
        var ListaResultado = "No presentó al momento de realizarse el Modelo.";
        if (Lista.length > 1 || (Lista.length == 1 && Lista[0] != "")) {
            ListaResultado = "";
            for (var i = 0; i < Lista.length; i++) {
                if (Lista[i] != "" && Lista[i] != " ") {
                    ListaResultado += "- " + Lista[i] + "<br />";
                }
            }
        }
        return ListaResultado;
    }

}
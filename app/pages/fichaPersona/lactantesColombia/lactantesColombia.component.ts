import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var ByOso: any;


@Component({
    selector: 'tarjetaLactantesColombia',
    templateUrl: 'lactantesColombia.component.html',
    styleUrls: ['lactantesColombia.component.scss']
})

export class LactantesColombiaComponent implements OnInit{
    lactantes: any;
    prefijo = 'LACCol';

    constructor(
        public apiBackEndService: ApiBackEndService,
        public fichaPService: FichaPersonaDataService
    ) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.lactantes = this.fichaPService.lactantes;
    }

    public openModal(index: any, modal: string){
        ByOso.openModalVisita(index,modal);
    }    
}
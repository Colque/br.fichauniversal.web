import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';
import * as $ from 'jquery';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaMapa',
    templateUrl: 'mapa.component.html',
    styleUrls: ['mapa.component.scss']
})

export class MapaComponent implements OnInit{
    mapa: any;
    prefijo = 'MAP';
    private indexActive = 0;

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        let mThis = this;
        this.mapa = this.fichaPService.mapa;
        if (this.mapa && this.mapa.length > 0){
            this.mapa[0].active = true;
            setTimeout( function(){
                myLoading.Ocultar('Loading'+ mThis.prefijo);
            }, 50);
        }
    }    

    public onchange(index:any){
        if (index !== this.indexActive) {
             this.mapa[this.indexActive].active = false;
            this.mapa[this.indexActive].MostrarOrigen = false;
            this.mapa[index].MostrarOrigen = true;
            this.mapa[index].active = true;
            this.indexActive = index;
        }
    }

    public mostrarOcultarOrigen(index:any){
         this.mapa[index].MostrarOrigen = !this.mapa[index].MostrarOrigen;
    }
}
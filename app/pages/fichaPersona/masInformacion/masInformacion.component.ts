import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaMasInformacion',
    templateUrl: 'masInformacion.component.html',
    //styleUrls: ['./app/components/todolist/todolist.component.css']
})

export class MasInformacionComponent implements OnInit{
    sintys: Observable<Array<any>>;
    informacionAdicional: Observable<Array<any>>;
    prefijo = 'MAS';

    constructor(private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.apiBackEndService.getPersonaInformacionAdicional().subscribe(resultado => {
            this.informacionAdicional = JSON.parse(resultado.toString());
        });

        if (this.apiBackEndService.enviroment !== 'colombia'){
            this.apiBackEndService.getPersonaSintys().subscribe(resultado => {
                this.sintys = JSON.parse(resultado.toString());
            });
        }
    }
    
}
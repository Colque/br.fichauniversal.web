import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var myStringFunctions: any;

@Component({
    selector: 'tarjetaObservacionesUrgencias',
    templateUrl: 'observacionesUrgencias.component.html',
    styleUrls: ['observacionesUrgencias.component.scss']
})

export class ObservacionesUrgenciasComponent implements OnInit {
    fechasAgrupadas: any;
    observaciones: any = [];
    urgencias: any = [];
    prefijo = 'OBUR';
    stringFunctions = myStringFunctions;
    obsActivo: boolean = true;

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService
    ) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fichaPService.getPersonaObservacionesUrgencias().then(() => {
            this.fechasAgrupadas = this.fichaPService.fechasObs;
            this.observaciones = this.fichaPService.observaciones;
            this.urgencias = this.fichaPService.urgencias;
            console.log('observaciones', this.observaciones);
            if (this.fechasAgrupadas && this.fechasAgrupadas.length > 0){
                this.obsActivo = this.observaciones && this.observaciones.length > 0 ? true : false;
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaPService.tieneObsUrgP = false;
            }
        });
    }

    tieneVisita(EsObservacion: boolean, Fecha: string, Origen: string, NroRelevamiento: string): boolean{
        var EsIgual = false;
        if(EsObservacion){
            this.observaciones.forEach((element:any) => {
                if(element['Fecha'] == Fecha && element['Origen'] == Origen && element['NroRelevamiento'] == NroRelevamiento){
                    EsIgual = true;
                }
            });
        }else{
            this.urgencias.forEach((element:any) => {
                if(element['Fecha'] == Fecha && element['Origen'] == Origen && element['NroRelevamiento'] == NroRelevamiento){
                    EsIgual = true;
                }
            });
        }

        return EsIgual;
    }

    tabActiva(EsObservacion: boolean, Fecha: string, Origen: string, NroRelevamiento: string): string{
        var EsActiva = false;

        if(EsObservacion){
            if(this.tieneVisita(true, Fecha, Origen, NroRelevamiento)){
                EsActiva = true;
            }
        }else{
            if(!this.tieneVisita(true, Fecha, Origen, NroRelevamiento)){
                if(this.tieneVisita(false, Fecha, Origen, NroRelevamiento)){
                    EsActiva = true;
                }
            }
        }

        return (EsActiva ? "active show" : "");
    }
}
import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var ByOso: any;

@Component({
    selector: 'tarjetaSalud',
    templateUrl: 'salud.component.html',
    styleUrls: ['./salud.component.scss']
})

export class SaludComponent implements OnInit{
    salud: any = [];
    discapacidades: Observable<Array<any>>;
    discapacidadesCognitivas: Observable<Array<any>>;
    enfermedadesAgudas: Observable<Array<any>>;
    enfermedadesCronicas: Observable<Array<any>>;
    prefijo = 'SAL';

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        var promises :any = [];
        promises.push(
            new Promise((resolve, reject) => { 
                this.fichaPService.getPersonaSalud().then(() => {
                    this.salud = this.fichaPService.salud;
                    console.log('this.salud',this.salud);
                    resolve();
                });
            })
        )

        promises.push(
            new Promise((resolve, reject) => { 
                this.fichaPService.getPersonaDiscapacidades().then(() => {
                    this.discapacidades = this.fichaPService.discapacidades;
                    this.discapacidadesCognitivas = this.fichaPService.discapacidadesCognitivas;
                    console.log('this.discapacidades',this.discapacidades);
                    resolve();
                });
            })
        )

        promises.push(
            new Promise((resolve, reject) => { 
                this.fichaPService.getPersonaEnfermedadesAgudas().then(() => {
                    this.enfermedadesAgudas = this.fichaPService.enfermedadesAgudas;
                    console.log('this.enfermedadesAgudas',this.enfermedadesAgudas);
                    resolve();
                });
            })
        )

        promises.push(
            new Promise((resolve, reject) => { 
                this.fichaPService.getPersonaEnfermedadesCronicas().then(() => {
                    this.enfermedadesCronicas = this.fichaPService.enfermedadesCronicas;
                    console.log('this.enfermedadesCronicas',this.enfermedadesCronicas);
                    resolve();
                });
            })
        )

        Promise.all(promises).then(() => 
            {   
                console.log('promises',this.salud);
                if (this.salud && this.salud.length > 0){
                    myLoading.Ocultar('Loading'+ this.prefijo);
                } else{
                    this.fichaPService.tieneSaludP = false;
                }
            }
        );
    }
    
    public openModal(index: any, modal: string){
        ByOso.openModalVisita(index,modal);
    }
}
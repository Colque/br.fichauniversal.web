import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';
//import * as $ from 'jquery';
//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var ByOso: any;

@Component({
    selector: 'tarjetaSaludColombia',
    templateUrl: 'saludColombia.component.html',
    styleUrls: ['saludColombia.component.scss']
})

export class SaludColombiaComponent implements OnInit{
    salud: any = [];
    fechas: any = [];
    discapacidades: any = [];
    riesgosSalud: any = [];
    discapacidadesCognitivas: Observable<Array<any>>;
    enfermedadesAgudas: Observable<Array<any>>;
    enfermedadesCronicas: Observable<Array<any>>;
    prefijo = 'SALCOLOMBIA';

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.loadDiscapacidades().then(() => this.loadRiesgos())
        .then(() => this.loadSalud())
        .then(() => {
            this.orderListVisitas(this.fechas);
            if (this.salud && this.salud.length > 0){
                this.salud[0].active = true;
                this.salud[0].MostrarOrigen = true;
            }
        })
    }

    private loadSalud(){
        return new Promise((resolve, reject) => {
            this.fechas.forEach((f:any, i:any) => {
                let disc = this.discapacidades.filter((d:any) => d.Fecha === f.Fecha && d.Origen === f.Origen && d.Discapacidad)
                let riesgos = this.riesgosSalud.filter((r:any) => r.Fecha === f.Fecha && r.Origen === f.Origen && r.Riesgo)

                this.salud.push({Fecha : f.Fecha, Date: f.Date, Origen: f.Origen, NroRelevamiento: f.NroRelevamiento, Discapacitado: disc.length > 0 ? 'SÍ' : 'NO', Discapacitado_txt: disc.length > 0 ? 'Persona con discapacidad' : 'Persona sin discapacidad', PoseeRiesgos: riesgos.length > 0 ? 'SÍ' : 'NO', PoseeRiesgos_txt: riesgos.length > 0 ? 'Persona con riesgo' : 'Persona sin riesgo' });//, Date: Fechadate
                
                if (i === this.fechas.length - 1){
                    resolve();
                }
            });
        });
    }

    private loadDiscapacidades(){
        return new Promise((resolve, reject) => {
            //this.fichaPService.getPersonaDiscapacidadesCol().then(() => {
                this.discapacidades = this.fichaPService.discapacidades;
                this.discapacidades.forEach((d: any, i:any) => {
                    if (this.fechas.filter((v: any) => v.Fecha === d.Fecha && v.Origen === d.Origen && v.NroRelevamiento === d.NroRelevamiento).length === 0){
                        let parts = d.Fecha.split('-');
                        let Fechadate = new Date(20 + parts[2], parts[1] - 1, parts[0]);
                        this.fechas.push({ Fecha : d.Fecha, Date: Fechadate, Origen: d.Origen, NroRelevamiento: d.NroRelevamiento });//, Date: Fechadate
                    }

                    if (i === this.discapacidades.length - 1){
                        resolve();
                    }
                });
            //});
        });
    }

    private loadRiesgos(){
        return new Promise((resolve, reject) => {
            //this.fichaPService.getPersonaRiesgosSalud().then(() => {
                this.riesgosSalud = this.fichaPService.riesgosSalud;
                this.riesgosSalud.forEach((r: any, i:any) => {
                    if (this.fechas.filter((v: any) => v.Fecha === r.Fecha  && v.Origen === r.Origen && v.NroRelevamiento === r.NroRelevamiento).length === 0){
                        let parts = r.Fecha.split('-');
                        let Fechadate = new Date(20 + parts[2], parts[1] - 1, parts[0]);
                        this.fechas.push({Fecha : r.Fecha, Date: Fechadate, Origen: r.Origen, NroRelevamiento: r.NroRelevamiento });//, Date: Fechadate
                    }

                    if (i === this.riesgosSalud.length - 1){
                        resolve();
                    }
                });
            //})
        });
    }

    private orderListVisitas(array: any){
        return new Promise((resolve, reject) => {
            array.sort((a:any, b:any) => {
                if (a.Date.getTime() < b.Date.getTime()) {
                    return 1;
                }
                if (a.Date.getTime() > b.Date.getTime()) {
                    return -1;
                }
                return 0;
            });
            resolve();
        });
    }
    
    public openModal(index: any, modal: string){
        ByOso.openModalVisita(index,modal);
    }
}
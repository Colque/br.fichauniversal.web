import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaSanitariosServicios',
    templateUrl: 'sanitariosServicios.component.html',
    styleUrls: ['sanitariosServicios.component.scss']
})

export class SanitariosServiciosComponent implements OnInit{
    fechasAgrupadas: any;
    sanitarios: any = [];
    servicios: any = [];
    serviciosFilter: any = [];
    sanitariosFilter: any = [];
    prefijo = 'SASE';
    servActivo: boolean = true;

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService
    ) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fichaPService.getPersonaSanitariosServicios().then(() => {
            this.fechasAgrupadas = this.fichaPService.fechasServSan;
            this.sanitarios = this.fichaPService.sanitarios;
            this.servicios = this.fichaPService.servicios;

            this.fechasAgrupadas.forEach((fecha:any) => {
                let serv = this.servicios.filter((s:any) => s.Fecha == fecha.Fecha && s.Origen == fecha.Origen && s.NroRelevamiento == fecha.NroRelevamiento);
                fecha.servActivo = serv.length > 0 ? true : false;
            });
            
            console.log('this.sanitarios',this.sanitarios);
            console.log('this.servicios',this.servicios);

            if (this.fechasAgrupadas && this.fechasAgrupadas.length > 0){
                let fecha = this.fechasAgrupadas[0];
                this.serviciosFilter = this.servicios.filter((s:any) => s.Fecha == fecha.Fecha && s.Origen == fecha.Origen && s.NroRelevamiento == fecha.NroRelevamiento);
                this.sanitariosFilter = this.sanitarios.filter((s:any) => s.Fecha == fecha.Fecha && s.Origen == fecha.Origen && s.NroRelevamiento == fecha.NroRelevamiento);
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaPService.tieneServiciosP = false;
            }
        });
    }

    onChangeTab(event:any){
        let fecha = this.fechasAgrupadas[event.index];
        this.serviciosFilter = this.servicios.filter((s:any) => s.Fecha == fecha.Fecha && s.Origen == fecha.Origen && s.NroRelevamiento == fecha.NroRelevamiento);
        this.sanitariosFilter = this.sanitarios.filter((s:any) => s.Fecha == fecha.Fecha && s.Origen == fecha.Origen && s.NroRelevamiento == fecha.NroRelevamiento);
    }

    tieneVisita(EsSanitario: boolean, Fecha: string, Origen: string, NroRelevamiento: string): boolean{
        var EsIgual = false;
        if(EsSanitario){
            this.sanitariosFilter.forEach((element:any) => {
                if(element['Fecha'] == Fecha && element['Origen'] == Origen && element['NroRelevamiento'] == NroRelevamiento){
                    EsIgual = true;
                }
            });
        }else{
            this.serviciosFilter.forEach((element:any) => {
                if(element['Fecha'] == Fecha && element['Origen'] == Origen && element['NroRelevamiento'] == NroRelevamiento){
                    EsIgual = true;
                }
            });
        }

        return EsIgual;
    }

    tabActiva(EsSanitario: boolean, Fecha: string, Origen: string, NroRelevamiento: string): string{
        var EsActiva = false;

        if(!EsSanitario){
            if(this.tieneVisita(false, Fecha, Origen, NroRelevamiento)){
                EsActiva = true;
            }
        }else{
            if(!this.tieneVisita(false, Fecha, Origen, NroRelevamiento)){
                if(this.tieneVisita(true, Fecha, Origen, NroRelevamiento)){
                    EsActiva = true;
                }
            }
        }

        return (EsActiva ? "active" : "");
    }
    
}
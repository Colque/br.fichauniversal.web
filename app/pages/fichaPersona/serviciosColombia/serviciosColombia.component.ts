import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaServiciosColombia',
    templateUrl: 'serviciosColombia.component.html',
    //styleUrls: ['./app/components/todolist/todolist.component.css']
})

export class ServiciosColombiaComponent implements OnInit{
    fechasAgrupadas: Observable<Array<any>>;
    sanitarios: Observable<Array<any>>;
    servicios: Observable<Array<any>>;
    prefijo = 'SERVCOLOMBIA';

    constructor(
        public fichaPService: FichaPersonaDataService,
        public apiBackEndService: ApiBackEndService
    ) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fichaPService.getPersonaServicios().then(() => {
            this.fechasAgrupadas = this.fichaPService.fechasServSan;
            //this.sanitarios = this.fichaPService.sanitarios;
            this.servicios = this.fichaPService.servicios;
        });
    }

    tieneVisita(EsSanitario: boolean, Fecha: string): boolean{
        var EsIgual = false;
        if(EsSanitario){
            this.sanitarios.forEach(element => {
                if(element['Fecha'] == Fecha){
                    EsIgual = true;
                }
            });
        }else{
            this.servicios.forEach(element => {
                if(element['Fecha'] == Fecha){
                    EsIgual = true;
                }
            });
        }

        return EsIgual;
    }

    tabActiva(EsSanitario: boolean, Fecha: string): string{
        var EsActiva = false;

        if(!EsSanitario){
            if(this.tieneVisita(false, Fecha)){
                EsActiva = true;
            }
        }else{
            if(!this.tieneVisita(false, Fecha)){
                if(this.tieneVisita(true, Fecha)){
                    EsActiva = true;
                }
            }
        }

        return (EsActiva ? "active" : "");
    }
    
}
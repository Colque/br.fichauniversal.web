import { Component, OnInit, Input, ViewChild } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var Tests: any;

//linea de tiempo
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TLItem} from './../../../components/timeline/model/tl-item';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import { Md2Dialog } from 'Md2';

@Component({
    selector: 'tarjetaTestPersona',
    templateUrl: 'testPersona.component.html',
    styleUrls: ['testPersona.component.scss']
})

export class TestPersonaComponent implements OnInit {
    @ViewChild('ModalTest', undefined) ModalTest: Md2Dialog;
    detalleTest: any = [];
    testPersona: any;
    prefijo = 'TESTP';
    filterTest: any;

    constructor(
        private apiBackEndService: ApiBackEndService,
        public fichaPService: FichaPersonaDataService,
    ) {

    }

    ngOnInit(){
        this.loadData();
    }

    public openModal(CodCabeceraTest: string, CodTest: string, FechaCargaDate: any){
        this.filterTest = this.detalleTest.filter((d: any) => d.CodCabeceraTest === CodCabeceraTest && d.CodTest === CodTest && d.FechaCargaDate == FechaCargaDate);
        this.sort();
        this.ModalTest.open();
    }

    private sort(){
        var nivelMaximo = this.filterTest.reduce((max: any, p: any) => p.Nivel > max ? p.Nivel : max, this.filterTest[0].Nivel);

        const oldTreeData = JSON.parse(JSON.stringify(this.filterTest));
        //this.filterTest = [];
        let mThis = this;

         this.filterTest.forEach((nodo: any) => {
                var hijos = this.filterTest.filter((r:any) => r.CodItemPadre === nodo.CodItem);
                if (hijos.length > 0){
                        nodo.EsUltimoNivel = false;
                        nodo.hideChildren = false
                } else{
                        nodo.EsUltimoNivel = true;
                        nodo.hideChildren = true;
                }
                nodo.visible = true;
                // }
            });
    }

    loadData() {
       this.fichaPService.getPersonaTest().then(() => {
            this.testPersona = this.fichaPService.testPersona;
            console.log('testPersona', this.testPersona);
            this.detalleTest = this.fichaPService.detalleTest;
            if (this.testPersona && this.testPersona.length > 0){
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaPService.tieneDatosTest = false;
            }
        });
    }

    public toggleChildren(codItem: string) {        
        const item = this.filterTest.find((i:any) => i.CodItem === codItem);
        item.hideChildren = !item.hideChildren;
        this.filterTest.filter((nodo:any) => nodo.CodItemPadre === codItem).forEach((_nodo:any) => {
            if (item.hideChildren) {
                _nodo.visible = false;
                this.toggleChildren(_nodo.CodItem);
            } else {
                _nodo.visible = true;
            }
        });
    }

}
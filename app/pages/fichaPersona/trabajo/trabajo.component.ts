import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaTrabajo',
    templateUrl: 'trabajo.component.html',
    styleUrls: ['trabajo.component.scss']
})

export class TrabajoComponent implements OnInit{
    fechasAgrupadas: any;
    trabajo: Observable<Array<any>>;
    ingresos: Observable<Array<any>>;
    oficios: Observable<Array<any>>;
    prefijo = 'TRA';
    ingresosFilter: any = [];
    oficiosFilter: any = [];
    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fichaPService.getPersonaTrabajo().then(() =>{
            this.fechasAgrupadas = this.fichaPService.fechasTrabajo;
            this.trabajo = this.fichaPService.trabajo;
            this.ingresos = this.fichaPService.ingresos;
            this.oficios = this.fichaPService.oficios;
            console.log('trabajo',this.trabajo);
            console.log('ingresos',this.ingresos);
            console.log('oficios',this.oficios);

            if (this.fechasAgrupadas && this.fechasAgrupadas.length > 0){
                let fecha = this.fechasAgrupadas[0];
                this.ingresosFilter = this.ingresos.filter((s:any) => s.Fecha == fecha.Fecha && s.Origen == fecha.Origen && s.NroRelevamiento == fecha.NroRelevamiento);
                this.oficiosFilter = this.oficios.filter((s:any) => s.Fecha == fecha.Fecha && s.Origen == fecha.Origen && s.NroRelevamiento == fecha.NroRelevamiento);
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaPService.tieneTrabajoP = false;
            }
        });
    }

    onChangeTab(event:any){
        let fecha = this.fechasAgrupadas[event.index];
        this.ingresosFilter = this.ingresos.filter((s:any) => s.Fecha == fecha.Fecha && s.Origen == fecha.Origen && s.NroRelevamiento == fecha.NroRelevamiento);
        this.oficiosFilter = this.oficios.filter((s:any) => s.Fecha == fecha.Fecha && s.Origen == fecha.Origen && s.NroRelevamiento == fecha.NroRelevamiento);
    }

    tabActiva(Opcion: string, Fecha: string, Origen: string, NroRelevamiento: string): string{
        var EsActiva = false;

        switch(Opcion){
            case "Trabajo":
                if(this.tieneVisita("Trabajo", Fecha, Origen, NroRelevamiento)){
                    EsActiva = true;
                }
            break;
            case "Ingresos":
                if(!this.tieneVisita("Trabajo", Fecha, Origen, NroRelevamiento)){
                    if(this.tieneVisita("Ingresos", Fecha, Origen, NroRelevamiento)){
                        EsActiva = true;
                    }
                }
            break;
            case "Oficios":
                if(!this.tieneVisita("Trabajo", Fecha, Origen, NroRelevamiento)){
                    if(!this.tieneVisita("Ingresos", Fecha, Origen, NroRelevamiento)){
                        if(this.tieneVisita("Oficios", Fecha, Origen, NroRelevamiento)){
                            EsActiva = true;
                        }
                    }
                }
            break;
        }

        return (EsActiva ? "active show" : "");
    }

    tieneVisita(Opcion: string, Fecha: string, Origen: string, NroRelevamiento: string): boolean{
        var EsIgual = false;
        switch(Opcion){
            case "Trabajo":
                this.trabajo.forEach(element => {
                    if(element['Fecha'] == Fecha && element['Origen'] == Origen && element['NroRelevamiento'] == NroRelevamiento){
                        EsIgual = true;
                    }
                });
            break;
            case "Ingresos":
                this.ingresos.forEach(element => {
                    if(element['Fecha'] == Fecha && element['Origen'] == Origen && element['NroRelevamiento'] == NroRelevamiento){
                        EsIgual = true;
                    }
                });
            break;
            case "Oficios":
                this.oficios.forEach(element => {
                    if(element['Fecha'] == Fecha && element['Origen'] == Origen && element['NroRelevamiento'] == NroRelevamiento){
                        EsIgual = true;
                    }
                });
            break;
            default:

            break;
        }

        return EsIgual;
    }

    RangoIngreso(Fecha: string, Origen: string, NroRelevamiento: string): string{
        var rango = "";

        this.ingresos.forEach(element => {
            if(element['Fecha'] == Fecha && element['Origen'] == Origen && element['NroRelevamiento'] == NroRelevamiento){
                rango = element['RangoIngreso'];
            }
        });

        return rango;
    }
    
}
import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaTrabajoColombia',
    templateUrl: 'trabajoColombia.component.html',
    styleUrls: ['trabajoColombia.component.scss']
})

export class TrabajoColombiaComponent implements OnInit{
    fechasAgrupadas: Observable<Array<any>>;
    trabajo: any;
    ingresos: Observable<Array<any>>;
    oficios: Observable<Array<any>>;
    prefijo = 'TRACol';

    constructor(
        public fichaPService: FichaPersonaDataService,
        public apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fechasAgrupadas = this.fichaPService.fechasTrabajo;
        this.trabajo = this.fichaPService.trabajo;
    }

    tabActiva(Opcion: string, Fecha: string): string{
        var EsActiva = false;

        switch(Opcion){
            case "Trabajo":
                if(this.tieneVisita("Trabajo", Fecha)){
                    EsActiva = true;
                }
            break;
            case "Ingresos":
                if(!this.tieneVisita("Trabajo", Fecha)){
                    if(this.tieneVisita("Ingresos", Fecha)){
                        EsActiva = true;
                    }
                }
            break;
            case "Oficios":
                if(!this.tieneVisita("Trabajo", Fecha)){
                    if(!this.tieneVisita("Ingresos", Fecha)){
                        if(this.tieneVisita("Oficios", Fecha)){
                            EsActiva = true;
                        }
                    }
                }
            break;
        }

        return (EsActiva ? "active show" : "");
    }

    tieneVisita(Opcion: string, Fecha: string): boolean{
        var EsIgual = false;
        switch(Opcion){
            case "Trabajo":
                this.trabajo.forEach((element:any) => {
                    if(element['Fecha'] == Fecha){
                        EsIgual = true;
                    }
                });
            break;
            case "Ingresos":
                this.ingresos.forEach(element => {
                    if(element['Fecha'] == Fecha){
                        EsIgual = true;
                    }
                });
            break;
            case "Oficios":
                this.oficios.forEach(element => {
                    if(element['Fecha'] == Fecha){
                        EsIgual = true;
                    }
                });
            break;
            default:

            break;
        }

        return EsIgual;
    }

    RangoIngreso(Fecha: string): string{
        var rango = "";

        this.ingresos.forEach(element => {
            if(element['Fecha'] == Fecha){
                rango = element['RangoIngreso'];
            }
        });

        return rango;
    }
    
}
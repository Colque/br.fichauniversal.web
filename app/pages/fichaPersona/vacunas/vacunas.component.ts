import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var ByOso: any;

@Component({
    selector: 'tarjetaVacunas',
    templateUrl: 'vacunas.component.html',
    styleUrls: ['vacunas.component.scss']
})

export class VacunasComponent implements OnInit{
    vacunas: any;
    fechasAgrupadas: any;
    prefijo = 'VAC';
    vacunasFilter: any = [];
    calendarioVacunas: any = [];

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        Observable.combineLatest(
                this.fichaPService.getPersonaVacunas(),
                this.fichaPService.getCalendarioVacunas('COD_CV_2019'),//Parametros de prueba
                this.fichaPService.getEdadesCalendario('COD_CV_2019'),//Parametros de prueba
                this.fichaPService.getVacunasDosis('COD_CV_2019','')//Parametros de prueba
                ).subscribe(([
                _personaVacunas,
                _calendarioVacunas,
                _edadesCalendario,
                _vacunasDosis
            ]) =>{
                let mThis = this;
                this.vacunas = this.fichaPService.vacunas;
                this.fechasAgrupadas = this.fichaPService.fechasVacunas;
                if (this.fichaPService.calendarioVacunas.length > 0){
                    this.calendarioVacunas.push({column0: 'test'});
                    this.fichaPService.calendarioVacunas.forEach((cal:any,i:any) => {
                        this.calendarioVacunas[0]["column"+(i+1)] = cal.CodVacuna;// + '¬'+ cal.Vacuna;
                        if (i === this.fichaPService.calendarioVacunas.length -1){
                            this.fichaPService.edadesCalendario.forEach((edades:any,i:any) => {
                                this.calendarioVacunas.push({column0: edades.EdadVacuna});
                                this.fichaPService.calendarioVacunas.forEach((cal:any,index:any) => {
                                    let indexVC = this.fichaPService.vacunasDosis.findIndex((vc:any) => vc.CodVacuna === cal.CodVacuna && vc.CodEdadVacuna === edades.CodEdadVacuna)                                    
                                    let indexV = -1;
                                    let indexAux = -1;
                                    if (indexVC >= 0){
                                        indexV = this.vacunas.findIndex((vc:any) => vc.CodTipoVacuna === mThis.fichaPService.vacunasDosis[indexVC].CodVacunaDosisEdad);
                                        //para la vacuna antigripal 6 - 24 meses agrego un rowspan
                                        if (this.fichaPService.vacunasDosis[indexVC].rowSpan > 1){
                                            let codVacunaDosisEdad = this.fichaPService.vacunasDosis[indexVC].CodVacunaDosisEdad;
                                            indexAux = this.fichaPService.vacunasDosis.findIndex((vc:any) => vc.CodVacunaDosisEdad === codVacunaDosisEdad && vc.tieneVacuna === true);
                                            //Valido si ya se inserto una fila para vacuna antigripal 6 - 24 meses
                                            if (indexAux < 0){
                                                this.fichaPService.vacunasDosis[indexVC].tieneVacuna = indexV >= 0 ? true : false;
                                            }
                                        } else{
                                            this.fichaPService.vacunasDosis[indexVC].tieneVacuna = indexV >= 0 ? true : false;
                                        }
                                    }
                                    this.calendarioVacunas[i + 1]["column"+(index+1)] = indexAux <= -1 ? indexVC : null;
                                });
                            });
                        }
                    });
                }
                if (this.vacunas && this.vacunas.length > 0){
                    let fecha = this.fechasAgrupadas[0];
                    this.vacunasFilter = this.vacunas.filter((v:any) => v.Fecha == fecha.Fecha && v.Origen == fecha.Origen && v.NroRelevamiento == fecha.NroRelevamiento);
                    myLoading.Ocultar('Loading'+ this.prefijo);
                } else {
                    this.fichaPService.tieneVacunasP = false;
                }
            })


        // this.fichaPService.getPersonaVacunas().then(() => {
        //     this.vacunas = this.fichaPService.vacunas;
        //     this.fechasAgrupadas = this.fichaPService.fechasVacunas;
        //     if (this.vacunas && this.vacunas.length > 0){
        //         let fecha = this.fechasAgrupadas[0];
        //         this.vacunasFilter = this.vacunas.filter((v:any) => v.Fecha == fecha.Fecha && v.Origen == fecha.Origen && v.NroRelevamiento == fecha.NroRelevamiento);
        //         myLoading.Ocultar('Loading'+ this.prefijo);
        //     } else {
        //         this.fichaPService.tieneVacunasP = false;
        //     }
        // })
        //});
    }

    onChangeTab(event:any){
        let fecha = this.fechasAgrupadas[event.index];
        this.vacunasFilter = this.vacunas.filter((v:any) => v.Fecha == fecha.Fecha && v.Origen == fecha.Origen && v.NroRelevamiento == fecha.NroRelevamiento);
    }
    
     public openModal(index: any, modal: string){
        ByOso.openModalVisita(index,modal);
    }
}
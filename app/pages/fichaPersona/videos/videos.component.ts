import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
//paraloading
import './../../../../js/myplugins.js'
import { Sanitizer } from '@angular/core/src/security';
declare var myLoading: any;


@Component({
    selector: 'tarjetaVideos',
    templateUrl: 'videos.component.html',
    styleUrls: ['videos.component.css']
})

export class VideosComponent implements OnInit{
    videos2D: any;
    //videos360: Observable<Array<any>>;
    videos360: any;
    prefijo = 'VID';
    TieneVideos = false;
    CantidadVideos2D = 0;
    CantidadVideos360 = 0;

    constructor(
        public fichaPService: FichaPersonaDataService,
        private apiBackEndService: ApiBackEndService, 
        private sanitizer: DomSanitizer) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        var principal = this;
        //this.apiBackEndService.getPersonaVideos2D().subscribe(resultado => {
        this.fichaPService.getPersonaVideos2D().then(() =>{
            this.videos2D = this.fichaPService.videos2D;//JSON.parse(resultado.toString());

            if (this.videos2D && this.videos2D.length > 0){
                this.videos2D.forEach(function(){
                principal.CantidadVideos2D++;
                principal.TieneVideos = true;
            });
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaPService.tieneVideosP = false;
            }
        })
       // });

        /*this.apiBackEndService.getPersonaVideos360().subscribe(resultado => {
            this.videos360 = JSON.parse(resultado.toString());
            this.videos360.forEach(function(){
                principal.CantidadVideos360++;
                principal.TieneVideos = true;
            });
        });*/

        // var FakeVideo360 = '[' +
        //     '{"CodVisita":""' +
        //     ', "Fecha":"02/03/18"' +
        //     ', "FechaRegistro": "02/03/2018",' +
        //     '"dirArchivo":"../../Content/video/Lote21Ambiental_V2_2.mp4"' +
        //     '"Titulo": "Ambiental Lote 21"' +
        //     '"Descripcion": "Video 360 de visita de caracter ambiental"' +
        //     '"Orden": "0"' +
        //     '"Thumbnail": ""' +
        //     '"Duracion": "00:00:41"' +
        //     '}]';
        // this.videos360 = [
        //     {
        //         CodVisita:"",
        //         Fecha : "02/03/18",
        //         FechaRegistro: "02/03/2018",
        //         dirArchivo:"../../Content/video/Lote21Ambiental_V2_2.mp4",
        //         Titulo: "Ambiental Lote 21",
        //         Descripcion: "Video 360 de visita de caracter ambiental",
        //         Orden: "0",
        //         Thumbnail: "",
        //         Duracion: "00:00:41"
        //     }
        // ];
        // this.TieneVideos = true;
        // this.CantidadVideos360 = 1;
    }

    urlSafe(urlUnsafe: string): SafeUrl{
        return this.sanitizer.bypassSecurityTrustResourceUrl(urlUnsafe);
    }
    
}
import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaVivienda',
    templateUrl: 'vivienda.component.html',
    styleUrls: ['vivienda.component.scss']
})

export class ViviendaComponent implements OnInit{
    vivienda: any;
    prefijo = 'VIV';
    private indexActive = 0;

    constructor(
        public fichaPService: FichaPersonaDataService,
        public apiBackEndService: ApiBackEndService
    ) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fichaPService.getVivienda().then(() =>{
            this.vivienda = this.fichaPService.vivienda;
            if (this.vivienda && this.vivienda.length > 0){
                this.vivienda[0].active = true;
                //this.vivienda[0].MostrarOrigen = true;
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaPService.tieneViviendaP = false;
            }
            console.log('vivienda',this.vivienda);
        });
    }

    public onchange(index:any){
        if (index !== this.indexActive) {
            this.vivienda[this.indexActive].active = false;
            this.vivienda[this.indexActive].MostrarOrigen = false;
            this.vivienda[index].active = true;
            this.vivienda[index].MostrarOrigen = true;
            this.indexActive = index;
        }
    }

    public mostrarOcultarOrigen(index:any){
         this.vivienda[index].MostrarOrigen = !this.vivienda[index].MostrarOrigen;
    }
    
}
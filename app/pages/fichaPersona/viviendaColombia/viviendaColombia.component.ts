import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaPersonaDataService } from './../fichaPersonaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaViviendaColombia',
    templateUrl: 'viviendaColombia.component.html',
    styleUrls: ['viviendaColombia.component.scss']
})

export class ViviendaColombiaComponent implements OnInit{
    vivienda: any;
    comunidad: any;
    prefijo = 'VIVCol';
    tarjetaActiva = 'vivienda';

    constructor(
        public fichaPService: FichaPersonaDataService,
        public apiBackEndService: ApiBackEndService
    ) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.vivienda = this.fichaPService.vivienda;
        //this.comunidad = this.vivienda;
        this.vivienda.forEach((v:any, i:any) => {
            v.paisSRC = v.PAI_CODIGO ? '../../../Content/flag-icon-css/flags/4x3/'+v.PAI_CODIGO +'.svg' : null;
            if (i === 0){
                v.active = true;
                v.MostrarOrigen = true;
            }
        });
        console.log('vivienda',this.vivienda);
    }
}
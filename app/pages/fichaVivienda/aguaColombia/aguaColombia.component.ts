import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaAguaColombia',
    templateUrl: 'aguaColombia.component.html',
    styleUrls: ['aguaColombia.component.scss']
})

export class AguaColombiaComponent implements OnInit{
    agua: any;
    prefijo = 'AGUACOLOMBIA';

    constructor(
        public fichaVService: FichaViviendaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.agua = this.fichaVService.agua;
    }    
}
import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaEquipamientosV',
    templateUrl: 'equipamientos.component.html',
    styleUrls: ['equipamientos.component.scss']
})

export class EquipamientosVComponent implements OnInit{
    equipamientos: any;
    fechasAgrupadas: any;
    eqCocina: any;
    eqBanio: any;
    eqCocinaFilter: any;
    eqBanioFilter: any;
    prefijo = 'EQUV';

    constructor(
        public fichaVService: FichaViviendaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fichaVService.getViviendaEquipamientos().then(() => {
            this.equipamientos = this.fichaVService.equipamientos;
            this.fechasAgrupadas = this.fichaVService.fechasEq;
            if (this.equipamientos && this.equipamientos.length > 0){
                this.eqCocina = this.equipamientos.filter((eq:any) => eq.Ambiente === 'COCINA');
                this.eqBanio = this.equipamientos.filter((eq:any) => eq.Ambiente === 'BAÑO');
                console.log('eqCocina',this.eqCocina);
                console.log('eqBanio',this.eqBanio);
            }

            if (this.fechasAgrupadas && this.fechasAgrupadas.length > 0){
                let fecha = this.fechasAgrupadas[0];
                this.eqCocinaFilter = this.eqCocina.filter((eq:any) => eq.Fecha == fecha.Fecha && eq.Origen == fecha.Origen && eq.NroRelevamiento == fecha.NroRelevamiento);
                this.eqBanioFilter = this.eqBanio.filter((eq:any) => eq.Fecha == fecha.Fecha && eq.Origen == fecha.Origen && eq.NroRelevamiento == fecha.NroRelevamiento);
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaVService.tieneEquipamientosV = false;
            }
        })
    }

    onChangeTab(event:any){
        let fecha = this.fechasAgrupadas[event.index];
        this.eqCocinaFilter = this.eqCocina.filter((eq:any) => eq.Fecha == fecha.Fecha && eq.Origen == fecha.Origen && eq.NroRelevamiento == fecha.NroRelevamiento);
        this.eqBanioFilter = this.eqBanio.filter((eq:any) => eq.Fecha == fecha.Fecha && eq.Origen == fecha.Origen && eq.NroRelevamiento == fecha.NroRelevamiento);
    }
    
}
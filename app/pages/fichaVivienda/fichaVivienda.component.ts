import { Component, OnInit, ViewChild } from '@angular/core';

import { ApiBackEndService } from './../../_services/index';
import { FichaViviendaDataService } from './fichaViviendaData.service';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { DescargarFichaVComponent } from './descargarFichaV/descargarFichaV.component';
//tarjetas
import { ActivatedRoute } from "@angular/router";
import { Title }     from '@angular/platform-browser';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
declare var ByOso: any;


//import {Observable} from 'rxjs/Rx';
//import 'rxjs/Rx';

@Component({
    moduleId: module.id,
    templateUrl: 'fichaVivienda.component.html'
})

export class FichaViviendaComponent implements OnInit {
    //declarar variable de datos de tarjeta aquí
    prefijoDatosPersonales = "VIVV";
    @BlockUI() blockUI: NgBlockUI;
    @ViewChild('modalImprimir', undefined) modalImprimir: DescargarFichaVComponent;
    constructor( 
        public fichaVService: FichaViviendaDataService,
        public apiBackEndService: ApiBackEndService, 
        private title:Title,
        private route: ActivatedRoute 
    ) {}

    ngOnInit() {
        //this.blockUI.start('Cargando Ficha ...');
        this.route.params.subscribe(params => {
            console.log(params);
            this.apiBackEndService.CodVivienda = params['v'];
        });

        if (this.apiBackEndService.enviroment !== 'colombia'){
            this.title.setTitle('Ficha Universal - MPI');
        } else{
            this.blockUI.start('Cargando Ficha ...');
            this.fichaVService.getViviendaUbicacion();
            this.fichaVService.getViviendaPuntoInteres();
            this.fichaVService.getViviendaFotos();
            this.fichaVService.getViviendaAguaColombia();
            this.fichaVService.getViviendaSanitariosServicios();
            this.fichaVService.getViviendaInfraestructura()
            this.fichaVService.getViviendaMateriales();
            //this.fichaVService.getViviendaObservacionesUrgencias();
            this.fichaVService.getViviendaIntegrantes();
            this.fichaVService.getViviendaRiesgos().then(() => {
                this.blockUI.stop();
            });
            this.title.setTitle('Ficha Universal - Colombia');
        }
    }

    //obtener valores de tarjetas aquí
    private getDatos() {
        
    }

    public openModal(index: any, modal: string){
        ByOso.openModalVisita(index,modal);
    }

    public abrirModal(){
        console.log('openModal');
        this.modalImprimir.open();
    }
}
import { NgModule }      from '@angular/core';
import { BrowserModule, Title  } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';

import { AgmCoreModule } from '@agm/core';

// used to create fake backend
import { fakeBackendProvider } from './../../_helpers/index';

// import { routing }        from './../app.routing';

import { AuthGuard } from './../../_guards/index';
import { JwtInterceptor } from './../../_helpers/index';
import {
    AlertService,
    AuthenticationService,
    UserService,
    ApiBackEndService
} from './../../_services/index';

import { ServicesModule } from './../../_services/services.module';

//vivienda
import { FichaViviendaComponent } from './fichaVivienda.component';
import { UbicacionVComponent } from './ubicacionV/ubicacion.component';
import { MapaVComponent } from './mapaV/mapa.component';
import { FotosVComponent } from './fotosV/fotos.component';
import { VideosVComponent } from './videosV/videos.component';
import { SanitariosServiciosVComponent } from './sanitariosServiciosV/sanitariosServicios.component';
import { InfraestructuraVComponent } from './infraestructuraV/infraestructura.component';
import { MaterialesVComponent } from './materialesV/materiales.component';
import { ObservacionesUrgenciasVComponent } from './observacionesUrgenciasV/observacionesUrgencias.component';
import { IntegrantesVComponent } from './integrantesV/integrantes.component';
import { RiesgosVComponent } from './riesgosV/riesgos.component';
import { RiesgosViviendaComponent } from './riesgosVivienda/riesgosVivienda.component';
import { RiesgosFamiliaComponent } from './riesgosFamilia/riesgosFamilia.component';
import { InundacionesVComponent } from './inundacionesV/inundaciones.component';
import { ObservacionesEncuestadorVComponent } from './observacionesEncuestadorV/observacionesEncuestador.component';
import { EquipamientosVComponent } from './equipamientosV/equipamientos.component';
import { MasInformacionVComponent } from './masInformacionV/masInformacion.component';

import { DescargarFichaVComponent } from './descargarFichaV/descargarFichaV.component';

//Colombia
import { UbicacionColombiaComponent } from './ubicacionColombia/ubicacionColombia.component';
import { ServiciosColombiaComponent } from './serviciosColombia/serviciosColombia.component';
import { InfraestructuraColombiaComponent } from './infraestructuraColombia/infraestructuraColombia.component';
import { MaterialesColombiaComponent } from './materialesColombia/materialesColombia.component';
import { AguaColombiaComponent } from './aguaColombia/aguaColombia.component';
import { PuntoInteresColombiaComponent } from './puntoInteresColombia/puntoInteresColombia.component';
import { IntegrantesColombiaComponent } from './integrantesColombia/integrantesColombia.component';
import { RiesgosColombiaComponent } from './riesgosColombia/riesgosColombia.component';

import { Md2Module } from 'Md2';
import {BrowserAnimationsModule} from  "@angular/platform-browser/animations";
//import { TimelineComponent } from './../../components/timeline/timeline.component';
import { MaterialModule } from "@angular/material";

import { PagesRoutes } from './../pages.routes';

//import { SafePipe } from './../../myjs/utils/HtmlPipe';
// import { RegisterComponent } from './../../register/register.component'
//import { ChatWindowComponent } from './../../components/chat/chat-window/chat-window.component';

import { PipesModule } from './../../pipes/pipes.module';
import { ComponentsModule } from './../../components/components.module';
import { FichaViviendaDataService } from './fichaViviendaData.service';
import { BlockUIModule } from 'ng-block-ui';
import { HttpClientService } from '../../_services/http-client.service';
import { Router, ActivatedRoute } from '@angular/router';

var http: HttpClient;
var https: HttpClientService;
var router: Router;
var route: ActivatedRoute;

var apiBackEndService: ApiBackEndService = new ApiBackEndService(http,https,router,route);

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        AgmCoreModule.forRoot({ apiKey: apiBackEndService.apiKeyGoogleMap }),
        MaterialModule,
        Md2Module,
        ComponentsModule,
        PipesModule,
        PagesRoutes,
        ServicesModule,
        BlockUIModule,
    ],
    declarations: [
        //aqui nuestras tarjetas
        //TimelineComponent,

        //RegisterComponent,
        //SafePipe,
        // ChatWindowComponent,
        //vivienda
        FichaViviendaComponent,
        UbicacionVComponent,
        MapaVComponent,
        FotosVComponent,
        VideosVComponent,
        SanitariosServiciosVComponent,
        InfraestructuraVComponent,
        MaterialesVComponent,
        ObservacionesUrgenciasVComponent,
        IntegrantesVComponent,
        RiesgosVComponent,
        RiesgosViviendaComponent,
        RiesgosFamiliaComponent,
        InundacionesVComponent,
        ObservacionesEncuestadorVComponent,
        EquipamientosVComponent,
        MasInformacionVComponent,
        DescargarFichaVComponent,

        UbicacionColombiaComponent,
        ServiciosColombiaComponent,
        InfraestructuraColombiaComponent,
        IntegrantesColombiaComponent,
        MaterialesColombiaComponent,
        AguaColombiaComponent,
        PuntoInteresColombiaComponent,
        RiesgosColombiaComponent
    ],
    exports: [
        FichaViviendaComponent,
        UbicacionVComponent,
        MapaVComponent,
        FotosVComponent,
        VideosVComponent,
        SanitariosServiciosVComponent,
        InfraestructuraVComponent,
        MaterialesVComponent,
        ObservacionesUrgenciasVComponent,
        IntegrantesVComponent,
        RiesgosVComponent,
        RiesgosViviendaComponent,
        RiesgosFamiliaComponent,
        InundacionesVComponent,
        ObservacionesEncuestadorVComponent,
        EquipamientosVComponent,
        MasInformacionVComponent,
        DescargarFichaVComponent,

        UbicacionColombiaComponent,
        ServiciosColombiaComponent,
        InfraestructuraColombiaComponent,
        IntegrantesColombiaComponent,
        MaterialesColombiaComponent,
        AguaColombiaComponent,
        PuntoInteresColombiaComponent,
        RiesgosColombiaComponent
        //TimelineComponent
    ],
    providers: [
        AuthGuard,
        //AlertService,
        //AuthenticationService,
        //UserService,
        FichaViviendaDataService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
        //aqui nuestro provider
        //ApiBackEndService,

        // provider used to create fake backend
        fakeBackendProvider,
        Title
    ],
    bootstrap: []
})

export class FichaViviendaModule { }
import { Observable } from 'rxjs/Observable';
import { ApplicationRef, Injectable } from '@angular/core';
import { ApiBackEndService } from './../../_services/index';

@Injectable()
export class FichaViviendaDataService {
    public haveChanges: boolean;

    //Vivienda
    public tieneUbicacion: boolean = true;
    public ubicacion: any = [];
    public mapa: any = [];

    public puntoInteres: any = [];

    public agua: any = [];
    
    public tieneFotosV: boolean = true;
    public fotos: any = [];

    public tieneVideosV: boolean = true;
    public videos2D: any = [];
    public videos360: any = [];

    //Servicios
    public tieneServiciosV: boolean = true;
    public fechasServSan: any = [];
    public servicios: any = [];
    public sanitarios: any = [];

    //Infraestrucutra
    public tieneInfraestructuraV: boolean = true;
    public infraestructura: any = [];

    public tieneMaterialesV: boolean = true;
    public materiales: any = [];

    //observaciones
    public tieneObsUrgV: boolean = true;
    public fechasObs: any = [];
    public observaciones: any = [];
    public urgencias: any = [];

    //Integrantes
    public tieneIntegrantesV: boolean = true;
    public fechasFamilia: any = [];
    public integrantesVivienda: any = [];

    //Riesgos
    public tieneRiesgosV: boolean = true;
    public fechasRiesgos: any = [];
    public riesgos: any = [];

    //RiesgosVivienda
    public tieneRiesgosViv: boolean = true;
    public riesgosViv: any;

    //RiesgosFamilia
    public tieneRiesgosFam: boolean = true;
    public riesgosFam: any;
    public hogares: any = [];

    //
    public fechasInundacionesV: any = [];
    public tieneInundacionesV: boolean = true;
    public inundaciones: any = [];

    //Obs Encuestador
    public tieneObsEncV: boolean = true;
    public fechasObsEnc: any = [];
    public observacionesEnc: any = [];

    //Equipamientos
    public tieneEquipamientosV: boolean = true;
    public fechasEq: any = [];
    public equipamientos: any = [];

    public listTarjetas: any = [
        {
            Nombre:"Vivienda",
            Checked: false,
            Visible: false,
            Array:[],
            rows: 13
        },
        {
            Nombre:"Mapa",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 10
        },
        {
            Nombre:"Sanitarios",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 5
        },
        {
            Nombre:"Servicios",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 7
        },
        {
            Nombre:"Infraestructura",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 9
        },
        {
            Nombre:"Materiales",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 7
        },
        {
            Nombre:"Observaciones",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
        {
            Nombre:"Urgencias",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
        {
            Nombre:"Integrantes",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
        {
            Nombre:"Riesgos Generales",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
        {
            Nombre:"Riesgos de la Vivienda",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
        {
            Nombre:"Riesgos de la Familia",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
        {
            Nombre:"Inundaciones",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 5
        },
        {
            Nombre:"Observaciones del Encuestador",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
        {
            Nombre:"Equipamientos de la Vivienda",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
        {
            Nombre:"Equipamientos del Baño",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
        {
            Nombre:"Fotos",
            Checked: false,
            Visible: false,
            Array: [],
            rows: 4
        },
    ];

    constructor(
        private applicationRef: ApplicationRef,
         private apiBackEndService: ApiBackEndService, 
    ) {
    }


    //Ubicacion
    public getViviendaUbicacion(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getViviendaUbicacion().subscribe(resultado => {
                this.mapa = resultado ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                this.ubicacion = this.mapa;
                if (this.ubicacion && this.ubicacion.length > 0){
                    this.listTarjetas[0].Visible = true;
                    this.listTarjetas[0].Checked = true;
                    this.listTarjetas[0].Array.push(this.ubicacion);

                    this.listTarjetas[1].Visible = true;
                    this.listTarjetas[1].Checked = true;
                }
                resolve();
            });
        });
    }

    public getViviendaPuntoInteres(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getViviendaPuntoInteres().subscribe(resultado => {
                this.puntoInteres = JSON.parse(resultado.toString());
                resolve();
            });
        });
    }

    public getViviendaAguaColombia(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getViviendaAguaColombia().subscribe(resultado => {
                this.agua = JSON.parse(resultado.toString());   
                resolve();
            });
        });
    }

    // public getViviendaFotos(){
    //     return new Promise((resolve, reject) => {
    //         this.apiBackEndService.getViviendaFotos().subscribe(resultado => {
    //             this.fotos = resultado ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
    //             //Lista para imprimir PDF
    //             if (this.fotos && this.fotos.length > 0 ){
    //                 this.listTarjetas[16].Visible = true;
    //                 this.listTarjetas[16].Checked = true;

    //                 /***********COLOMBIA***********/
    //                 // this.listTarjetasColombia[9].Visible = true;
    //                 // this.listTarjetasColombia[9].Checked = true;
    //             }
    //             resolve();
    //         });
    //     });
    // }

    public getViviendaFotos(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getViviendaFotos().subscribe(resultado => {
                let fotosAux: any = [];
                this.fotos = [];
                fotosAux = resultado ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;

                //Lista para imprimir PDF
                if (fotosAux && fotosAux.length > 0 ){
                    this.listTarjetas[16].Visible = true;
                    this.listTarjetas[16].Checked = true;

                    fotosAux.forEach((foto:any, i:any) => {
                        if(foto.FotoMedium){
                            this.apiBackEndService.getBase64(foto.FotoMedium).then((base:any) => {
                                if (base.base64){        
                                    let indexF = fotosAux.map(function(e:any) { return e.base64; }).indexOf(base.base64);                        
                                    if (indexF === -1){
                                        this.fotos.push(foto);
                                    } 
                                    foto.base64 = base.base64;
                                    foto.columns = base.columns;
                                }
                                if (i === fotosAux.length - 1){
                                    resolve();
                                }
                            }); 
                        } else {
                            if (i === fotosAux.length - 1){
                                    resolve();
                                }
                        }
                    });

                    /***********COLOMBIA***********/
                    // this.listTarjetasColombia[9].Visible = true;
                    // this.listTarjetasColombia[9].Checked = true;
                } else {
                    resolve();
                }
            });
        });
    }

    public getViviendaVideos2D(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getViviendaVideos2D().subscribe(resultado => {
                this.videos2D = resultado ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                console.log('VIDEOS 2D', typeof this.videos2D, this.videos2D);
                if (this.videos2D.length > 0) {
                }
                resolve();
            });
        });
    }

    public getViviendaVideos360(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getViviendaVideos360().subscribe(resultado => {
                console.log('VIDEOS 360', typeof resultado, resultado);
                this.videos360 = resultado ? resultado.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.organizacionCod) !== -1) : null;
                console.log('VIDEOS 360', typeof this.videos360, this.videos360);
                if (this.videos360.length > 0) {
                }
                resolve();
            });
        });
    }

    public getViviendaSanitariosServicios(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getViviendaSanitariosServicios().subscribe(resultado => {
                var resultadoCompleto = JSON.parse(resultado.toString());
                if (resultadoCompleto[0].FechasAgrupadas && resultadoCompleto[0].Resultado){
                    this.fechasServSan = resultadoCompleto[0].FechasAgrupadas ? resultadoCompleto[0].FechasAgrupadas.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoCompleto[0].FechasAgrupadas;
                    this.sanitarios = resultadoCompleto[0].Resultado.ResultadoSanitarios ? resultadoCompleto[0].Resultado.ResultadoSanitarios.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1 && r.TieneBanio !== 'NO') : null;//resultadoCompleto[0].Resultado.ResultadoSanitarios;
                    if (this.sanitarios.length > 0){
                        this.listTarjetas[2].Visible = true;
                        this.listTarjetas[2].Checked = true;
                        this.listTarjetas[2].Array.push(this.sanitarios);
                    }
                    this.servicios = resultadoCompleto[0].Resultado.ResultadoServicios ? resultadoCompleto[0].Resultado.ResultadoServicios.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoCompleto[0].Resultado.ResultadoServicios;
                    if (this.servicios.length > 0){
                        this.listTarjetas[3].Visible = true;
                        this.listTarjetas[3].Checked = true;
                        this.listTarjetas[3].Array.push(this.servicios);
                    }
                    resolve();
                } else{
                    resolve();
                }
            });
        });
    }

    public getViviendaInfraestructura(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getViviendaInfraestructura().subscribe(resultado => {
                this.infraestructura = resultado ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                if (this.infraestructura.length > 0){
                    this.listTarjetas[4].Visible = true;
                    this.listTarjetas[4].Checked = true;
                    this.listTarjetas[4].Array.push(this.infraestructura);
                }
                resolve();
            });
        });
    }

    public getViviendaMateriales(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getViviendaMateriales().subscribe(resultado => {
                this.materiales = resultado ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;
                if (this.materiales.length > 0 && this.materiales[0].TieneDatosValidos){
                    this.listTarjetas[5].Visible = true;
                    this.listTarjetas[5].Checked = true;
                    this.listTarjetas[5].Array.push(this.materiales);
                }
                resolve();
            });
        });
    }

    public getViviendaObservacionesUrgencias(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getViviendaObservacionesUrgencias().subscribe(resultado => {
                var resultadoCompleto = JSON.parse(resultado.toString());

                this.fechasObs = resultadoCompleto[0].FechasAgrupadas ? resultadoCompleto[0].FechasAgrupadas.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoCompleto[0].FechasAgrupadas;
                this.observaciones = resultadoCompleto[0].Resultado.ResultadoObservaciones ? resultadoCompleto[0].Resultado.ResultadoObservaciones.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoCompleto[0].Resultado.ResultadoObservaciones;
                if (this.observaciones.length > 0){
                    this.listTarjetas[6].Visible = true;
                    this.listTarjetas[6].Checked = true;
                    this.listTarjetas[6].Array.push(this.observaciones);
                }
                this.urgencias = resultadoCompleto[0].Resultado.ResultadoUrgencias ? resultadoCompleto[0].Resultado.ResultadoUrgencias.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoCompleto[0].Resultado.ResultadoUrgencias;
                if (this.urgencias.length > 0){
                    this.listTarjetas[7].Visible = true;
                    this.listTarjetas[7].Checked = true;
                    this.listTarjetas[7].Array.push(this.urgencias);
                }
                resolve();
            });
        });
    }

    //Integrantes
    public getViviendaIntegrantes(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getViviendaIntegrantes().subscribe(resultado => {
                var resultadoCompleto = JSON.parse(resultado.toString());
                if (resultadoCompleto[0]){
                    this.fechasFamilia = resultadoCompleto[0].FechasAgrupadas;
                    this.integrantesVivienda = resultadoCompleto[0].Resultado;
                    if (this.integrantesVivienda.length > 0) {
                        this.listTarjetas[8].Visible = true;
                        this.listTarjetas[8].Checked = true;
                        this.listTarjetas[8].Array.push(this.integrantesVivienda);
                    }
                }
                resolve();
            });
        });
    }

    //Riesgos
    public getViviendaRiesgos(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getViviendaRiesgos().subscribe(resultado => {
                var resultadoJSON = JSON.parse(resultado.toString());
                this.fechasRiesgos = resultadoJSON[0].FechasAgrupadas ? resultadoJSON[0].FechasAgrupadas.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoJSON[0].FechasAgrupadas;
                this.riesgos = resultadoJSON[0].Resultado ? resultadoJSON[0].Resultado.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoJSON[0].Resultado;
                if (this.riesgos.length > 0) {
                    this.listTarjetas[9].Visible = true;
                    this.listTarjetas[9].Checked = true;
                    this.listTarjetas[9].Array.push(this.riesgos);
                }
                resolve();
            });
        });
    }

    public getRiesgoVivienda(){
         return new Promise((resolve, reject) => {
            this.apiBackEndService.getRiesgosVivienda().subscribe((resultado: any) => {
                this.riesgosViv = resultado && resultado !== 'null'  ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : [];
                if (this.riesgosViv && this.riesgosViv.length > 0 ){
                    this.listTarjetas[10].Visible = true;
                    this.listTarjetas[10].Checked = true;
                    this.listTarjetas[10].Array.push(this.riesgosViv);
                }
                resolve();
            });
        });
    }

    public getRiesgoFamilia(){
         return new Promise((resolve, reject) => {
            this.apiBackEndService.getRiesgosFamilia().subscribe((resultado: any) => {
                this.riesgosFam = resultado && resultado !== 'null'  ? JSON.parse(resultado.toString()).filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : [];
                if (this.riesgosFam && this.riesgosFam.length > 0 ){
                    this.listTarjetas[11].Visible = true;
                    this.listTarjetas[11].Checked = true;
                    this.listTarjetas[11].Array.push(this.riesgosFam);
                    
                    this.riesgosFam.forEach((r:any) =>{
                        if (this.hogares.filter((h: any) => h.CodHogar === r.CodHogar && h.Fecha === r.Fecha).length === 0){
                            this.hogares.push({CodHogar: r.CodHogar, Fecha: r.Fecha, NroHogar: r.NroHogar, active: r.NroHogar == 1 ? 1 : 0});
                            //numeroFam++
                        }
                    });
                }
                resolve();
            });
        });
    }
    //
    public getViviendaInundaciones(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getViviendaInundaciones().subscribe(resultado => {
                var resultadoCompleto = JSON.parse(resultado.toString());
                this.fechasInundacionesV = resultadoCompleto[0].FechasAgrupadas ? resultadoCompleto[0].FechasAgrupadas.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoCompleto[0].FechasAgrupadas;
                this.inundaciones = resultadoCompleto[0].Resultado ? resultadoCompleto[0].Resultado.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : [];//resultadoCompleto[0].Resultado.ResultadoNutricionales;
                if (this.inundaciones.length > 0) {
                    this.listTarjetas[12].Visible = true;
                    this.listTarjetas[12].Checked = true;
                    this.listTarjetas[12].Array.push(this.inundaciones);
                }
                resolve();
            });
        });
    }

    public getViviendaObservacionesEncuestador(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getViviendaObservacionesEncuestador().subscribe(resultado => {
                var resultadoJSON = JSON.parse(resultado.toString());
                this.fechasObsEnc = resultadoJSON[0].FechasAgrupadas ? resultadoJSON[0].FechasAgrupadas.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoJSON[0].FechasAgrupadas;
                this.observacionesEnc = resultadoJSON[0].Resultado ? resultadoJSON[0].Resultado.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//resultadoJSON[0].Resultado;
                if (this.observacionesEnc.length > 0) {
                    this.listTarjetas[13].Visible = true;
                    this.listTarjetas[13].Checked = true;
                    this.listTarjetas[13].Array.push(this.observacionesEnc);
                }
                resolve();
            });
        });
    }

    public getViviendaEquipamientos(){
        return new Promise((resolve, reject) => {
            this.apiBackEndService.getViviendaEquipamientos().subscribe(resultado => {
                var _Resultado = JSON.parse(resultado.toString());
                this.equipamientos = _Resultado[0].Resultado ? _Resultado[0].Resultado.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//_Resultado[0].Resultado;
                this.fechasEq= _Resultado[0].FechasAgrupadas ? _Resultado[0].FechasAgrupadas.filter((r:any) => this.apiBackEndService.organizacionesUsuario.indexOf(r.OrganizacionCod) !== -1) : null;//_Resultado[0].FechasAgrupadas;
                if (this.equipamientos.length > 0) {
                    let eqCocina = this.equipamientos.filter((eq:any) => eq.Ambiente === 'COCINA');
                    let eqBanio = this.equipamientos.filter((eq:any) => eq.Ambiente === 'BAÑO');
                    this.listTarjetas[14].Array.push(this.observacionesEnc);

                    if (eqCocina.length > 0){
                        this.listTarjetas[14].Visible = true;
                        this.listTarjetas[14].Checked = true;
                    } 
                    if (eqBanio.length > 0){
                        this.listTarjetas[15].Visible = true;
                        this.listTarjetas[15].Checked = true;
                    }
                }
                resolve();
            });
        });
    }
}

import { Component, OnInit, HostListener } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import {TLItem, MultimediaType} from './../../../components/timeline/model/tl-item';

//paraloading
import './../../../../js/myplugins.js'
import { Sanitizer } from '@angular/core/src/security';
declare var myLoading: any;
declare var galeria: any;
declare var modales: any;

import { NgxGalleryAnimation, NgxGalleryImage, NgxGalleryOptions } from 'ngx-gallery';

@Component({
    selector: 'tarjetaFotosV',
    templateUrl: 'fotos.component.html',
    styleUrls: ['fotos.component.css']
})

export class FotosVComponent implements OnInit{
    openLinea: boolean = false;
    fotos: any;
    prefijo = 'FOT';
    CantidadFotos = 0;
    CodPersona = '';
    galeriaCargada = false;
    viewerActivado = false;
    timeLineItems: TLItem[] = [];
    tieneItemsTimeLine = false;
    galleryOptions: NgxGalleryOptions[];
    galleryHeaderImageOptions: NgxGalleryOptions[];
    galleryImages: NgxGalleryImage[];
    galleryImagesHeader: NgxGalleryImage[];
    timeLineItemsCardGallery: TLItem[] = [];
    public selectedItemPosition: number;
    public widthmodalGalery = 0;
    public selectedItem: TLItem;
    public widthmodal = 0;
    elementRef: any;
    screenHeight: number;
    public _display_width: number;
    orientation = 'none';

    constructor(
        public fichaVService: FichaViviendaDataService,
        private apiBackEndService: ApiBackEndService, private sanitizer: DomSanitizer) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.timeLineItems = [];
        var principal = this;
        this.fichaVService.getViviendaFotos().then(() => {
            this.fotos = this.fichaVService.fotos;
            if (this.fotos && this.fotos.length > 0) {
                console.log('fotos', this.fotos);
                principal.CantidadFotos = this.fotos.length;
                this.fotos.forEach((elementA: any, i: any) => {
                    if (elementA["FotoMedium"] != null) {
                        var fechaArray = elementA["Fecha"].split("-");
                        var itemLinea: TLItem = new TLItem();
                        itemLinea.name = elementA["FotoTitulo"];
                        itemLinea.url = elementA["FotoMedium"];
                        itemLinea.description = elementA["FotoDescrip"];
                        itemLinea.type = 'imagen';
                        itemLinea.category = elementA["Categoria"];
                        itemLinea.urlbig = elementA.FotoFull + '?h=600'
                        itemLinea.urlsmall = elementA.FotoSmall + '?h=100'
                        itemLinea.urlmedium = elementA.FotoSmall + '?h=300'
                        itemLinea.date = new Date(Number("20" + fechaArray[2]), fechaArray[1] - 1, fechaArray[0]);
                        this.tieneItemsTimeLine = true;
                        this.timeLineItems.push(itemLinea);
                        this.timeLineItemsCardGallery.push(itemLinea);
                        if(i===this.fotos.length-1) 
                         {
                            this.timeLineItemsCardGallery.sort(function(a : any, b: any) {
                                var dateA = new Date(a["date"]).getTime(), dateB = new Date(b["date"]).getTime();
                                return  dateA - dateB;
                            });                          
                        }
                    }
                    // if (i == this.fotos.length -1){
                    //     myLoading.Ocultar('Loading' + this.prefijo);
                    // }
                });
                
                let data =this.fotos.sort(function(a :any, b: any) {                   
                    var dateA = new Date(a["FechaRegistro"]).getTime(), dateB = new Date(b["FechaRegistro"]).getTime();
                    return dateA - dateB;
                }); 

            } else {
                this.fichaVService.tieneFotosV = false;
            }
        });
    }

    AbreModal(){
        modales.Abrir('lineamodal'); 
    }

    moveSlide(orientation: string) {
        if (orientation === 'left') {
            this.orientation = 'prev';
            this.selectedItemPosition--;
            this.selectedItem = this.timeLineItemsCardGallery[this.selectedItemPosition];
        } else {
            this.orientation = 'next';
            this.selectedItemPosition++;
            this.selectedItem = this.timeLineItemsCardGallery[this.selectedItemPosition];
        }
    }

    abrirLinea(){
        this.openLinea = true;
    }

    fotosFun() {
        this._display_width = window.screen.width;
        this.screenHeight = window.innerHeight;
        this.galleryOptions = [
            {
                width: '100%',
                height: '90%',
                thumbnailsColumns: 4,
                imageAnimation: NgxGalleryAnimation.Slide,
                previewKeyboardNavigation: true,
                previewCloseOnEsc: true
            },
            // max-width 800
            {
                breakpoint: 800,
                width: '100%',
                height: '90%',
                imagePercent: 80,
                thumbnailsPercent: 20,
                thumbnailsMargin: 20,
                thumbnailMargin: 20,
                previewKeyboardNavigation: true,
                previewCloseOnEsc: true
            },
            // max-width 400
            {
                breakpoint: 400,
                preview: false,
                previewKeyboardNavigation: true,
                previewCloseOnEsc: true
            }
        ];
        const body = document.getElementsByTagName('body')[0];
        body.classList.add('body-landing');
        this.fichaVService.getViviendaFotos().then(() => {
            this.fotos = this.fichaVService.fotos;
            console.log('fotos', this.fotos);
            if (this.fotos && this.fotos.length > 0) {
                this.galleryImages = [];
                this.galleryImagesHeader = [];
                this.timeLineItems = [];
                this.timeLineItemsCardGallery = [];
                this.galleryHeaderImageOptions = [
                    {
                        width: '100%',
                        height: '100%',
                        thumbnails: false,
                        imageAnimation: NgxGalleryAnimation.Slide,
                        previewDescription: true,
                        imageDescription: true,
                        startIndex: this.fotos.filter((e:any) => e.IsProjectProfile === 1).length - 1,
                        previewKeyboardNavigation: true,
                        previewCloseOnEsc: true
                    }
                ];
                this.fotos.forEach((img: any) => {
                    if (img["FotoMedium"] != null) {
                        const item = new TLItem();
                        const fechaArray = img["Fecha"].split("-");
                        this.galleryImagesHeader.push({
                            small: img.FotoSmall + '?h=100',
                            big: img.FotoFull + '?h=600',
                            medium: img.FotoMedium + '?h=100',
                            description: img["FotoDescrip"],
                        });
                        this.galleryImages.push({
                            small: img.FotoSmall + '?h=100' ,
                            big: img.FotoFull + '?h=600' ,
                            medium: img.FotoMedium + '?h=100',
                        });
                        item.description = img["FotoDescrip"], 
                        // item.description && item.description.trim().length ? item.description : 'Sin Descripción';
                        item.name = img["FotoTitulo"];
                        item.category = img["Categoria"];
                        item.type = 'imagen';
                        item.urlbig = img.FotoFull + '?h=600'
                        item.urlsmall = img.FotoSmall + '?h=100'
                        item.date = new Date(Number("20" + fechaArray[2]), fechaArray[1] - 1, fechaArray[0]);
                        item.urlmedium = img.FotoSmall + '?h=300';
                        this.tieneItemsTimeLine = true;
                        this.timeLineItems.push(item);
                        this.timeLineItemsCardGallery.push(item);
                    }
                });
            }
        });

    }

    getSelectedItemGaleria(indexItem: number) {
        this.selectedItemPosition = indexItem;
        this.selectedItem = this.timeLineItemsCardGallery[indexItem];
        this.widthmodalGalery = 100;
        $('body').css('overflow', 'hidden');
    }

    openNav() {
        this.widthmodal = 100;
        $('body').css('overflow', 'hidden');
    }

    closeNav() {
        this.widthmodal = 0;
        $('body').css('overflow', '');
    }

    closeNavGallery() {
        this.widthmodalGalery = 0;
        $('body').css('overflow', '');
    }
    
    @HostListener('window:resize', ['$event'])
    onResize(event: any) {
        this._display_width = event.target.innerWidth;
        this.screenHeight = event.target.innerHeight;
    }

    urlSafe(urlUnsafe: string): SafeUrl{
        return this.sanitizer.bypassSecurityTrustResourceUrl(urlUnsafe);
    }
    
    public activarGaleria(i:any){
        if (i === this.timeLineItemsCardGallery.length -1){
            myLoading.Ocultar('Loading' + this.prefijo);
        }
        if (!this.galeriaCargada){
            this.galeriaCargada = true;
            galeria.ActivaGaleriaFotos();
        }
    }

    public activarViewer(i:any){
        if (!this.viewerActivado){
            this.viewerActivado = true;
            galeria.ActivarViewerFotos();
        }
    }
}
import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var ByOso: any;

@Component({
    selector: 'tarjetaInfraestructuraColombia',
    templateUrl: 'infraestructuraColombia.component.html',
    styleUrls: ['infraestructuraColombia.component.scss']
})

export class InfraestructuraColombiaComponent implements OnInit{
    infraestructura: any;
    prefijo = 'INFRVCol';

    constructor(
        public fichaVService: FichaViviendaDataService,
        public apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.infraestructura = this.fichaVService.infraestructura;
    }
    
    public openModal(index: any, modal: string){
        ByOso.openModalVisita(index,modal);
    }
}
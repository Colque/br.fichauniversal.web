import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var ByOso: any;

@Component({
    selector: 'tarjetaInfraestructuraV',
    templateUrl: 'infraestructura.component.html',
    styleUrls: ['infraestructura.component.scss']
})

export class InfraestructuraVComponent implements OnInit{
    infraestructura: any;
    prefijo = 'INFRV';

    constructor(
        public fichaVService: FichaViviendaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fichaVService.getViviendaInfraestructura().then(() => {
            this.infraestructura = this.fichaVService.infraestructura;
            if (this.infraestructura && this.infraestructura.length > 0){
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaVService.tieneInfraestructuraV = false;
            }
        })         
    }
    
    public openModal(index: any, modal: string){
        ByOso.openModalVisita(index,modal);
    }
}
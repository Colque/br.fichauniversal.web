import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var myStringFunctions: any;


@Component({
    selector: 'tarjetaIntegrantesColombia',
    templateUrl: 'integrantesColombia.component.html',
    styleUrls: ['integrantesColombia.component.scss']
})

export class IntegrantesColombiaComponent implements OnInit{
    fechasAgrupadas: any;
    integrantesVivienda: Observable<Array<any>>;
    hogares: any[];
    prefijo = 'INTColombia';
    stringFunctions = myStringFunctions;

    constructor(
        public fichaVService: FichaViviendaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fechasAgrupadas = this.fichaVService.fechasFamilia;
        this.integrantesVivienda = this.fichaVService.integrantesVivienda;
        
        this.armaListaHogares(this.integrantesVivienda);

    }

    armaListaHogares(_Integrantes: any): any{
        this.hogares = [];
        _Integrantes.forEach((elementA: any) => {
            var _ExisteIntegrante = false;
                this.hogares.forEach((elementH: any) => {
                    if(elementH.Hogar == elementA.Hogar && elementH.Fecha == elementA.Fecha){
                        _ExisteIntegrante = true;
                        elementH.CantidadIntegrantes++;
                    }
                });
                if(!_ExisteIntegrante){
                    this.hogares.push({Hogar: elementA.Hogar, CantidadIntegrantes: 1, Fecha: elementA.Fecha});
                }

            });
    }

    tieneVisita(Opcion: string, Fecha: string, Origen: string, NroRelevamiento: string): boolean{
        var EsIgual = false;
        switch(Opcion){
            case "IntegrantesVivienda":
                this.integrantesVivienda.forEach(element => {
                    if(element['Fecha'] == Fecha && element['Origen'] == Origen && element['NroRelevamiento'] == NroRelevamiento){
                        EsIgual = true;
                    }
                });
            break;
        }

        return EsIgual;
    }

    tabActiva(Opcion: string, Fecha: string, Origen: string, NroRelevamiento: string): string{
        var EsActiva = false;

        switch(Opcion){
            case "IntegrantesVivienda":
                if(!this.tieneVisita("GrupoFamiliar", Fecha, Origen, NroRelevamiento)){
                    if(this.tieneVisita("IntegrantesVivienda", Fecha, Origen, NroRelevamiento)){
                        EsActiva = true;
                    }
                }
            break;
        }

        return (EsActiva ? "active show" : "");
    }
    
}
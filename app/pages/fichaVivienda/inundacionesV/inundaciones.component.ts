import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaInundacionesV',
    templateUrl: 'inundaciones.component.html',
    styleUrls: ['inundaciones.component.scss']
})

export class InundacionesVComponent implements OnInit{
    fechasInundaciones: any = [];
    inundaciones: any;
    prefijo = 'INUV';
    inundacionesFilter:any = [];
    constructor(
        public fichaVService: FichaViviendaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fichaVService.getViviendaInundaciones().then(() => {
            this.fechasInundaciones = this.fichaVService.fechasInundacionesV;
            this.inundaciones = this.fichaVService.inundaciones;
            if (this.inundaciones && this.inundaciones.length > 0){
                let fecha = this.fechasInundaciones[0];
                this.inundacionesFilter = this.inundaciones.filter((i:any) => i.Fecha == fecha.Fecha && i.Origen == fecha.Origen && i.NroRelevamiento == fecha.NroRelevamiento);
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaVService.tieneInundacionesV = false;
            }
        })
    }

    onChangeTab(event:any){
        let fecha = this.fechasInundaciones[event.index];
        this.inundacionesFilter = this.inundaciones.filter((i:any) => i.Fecha == fecha.Fecha && i.Origen == fecha.Origen && i.NroRelevamiento == fecha.NroRelevamiento);
    }
    
}
import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaMapaV',
    templateUrl: 'mapa.component.html',
    styleUrls: ['mapa.component.scss']
})

export class MapaVComponent implements OnInit{
    mapa: any;
    prefijo = 'MAP';
    private indexActive = 0;

    constructor(
        public fichaVService: FichaViviendaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        let mThis = this;
        this.mapa = this.fichaVService.mapa;
        if (this.mapa && this.mapa.length > 0){
            this.mapa[0].active = true;
            this.mapa[0].MostrarOrigen = true;
            setTimeout( function(){
                myLoading.Ocultar('Loading'+ mThis.prefijo);
            }, 50);
        }
    }   
}
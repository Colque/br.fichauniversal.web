import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaMasInformacionV',
    templateUrl: 'masInformacion.component.html',
    //styleUrls: ['./app/components/todolist/todolist.component.css']
})

export class MasInformacionVComponent implements OnInit{
    informacionAdicional: Observable<Array<any>>;
    prefijo = 'MAS';

    constructor(private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.apiBackEndService.getViviendaInformacionAdicional().subscribe(resultado => {
            this.informacionAdicional = JSON.parse(resultado.toString());
        });
    }
    
}
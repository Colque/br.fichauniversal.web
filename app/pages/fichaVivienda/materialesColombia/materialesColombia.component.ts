import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaMaterialesColombia',
    templateUrl: 'materialesColombia.component.html',
    styleUrls: ['materialesColombia.component.scss']
})

export class MaterialesColombiaComponent implements OnInit{
    materiales: any;
    prefijo = 'MATVCol';

    constructor(
        public fichaVService: FichaViviendaDataService,
        public apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.materiales = this.fichaVService.materiales;
    }    
}
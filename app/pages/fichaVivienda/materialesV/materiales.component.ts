import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaMaterialesV',
    templateUrl: 'materiales.component.html',
    styleUrls: ['materiales.component.scss']
})

export class MaterialesVComponent implements OnInit{
    materiales: any;
    prefijo = 'MATV';

    constructor(
        public fichaVService: FichaViviendaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fichaVService.getViviendaMateriales().then(() => {
            this.materiales = this.fichaVService.materiales;
            if (this.materiales && this.materiales.length > 0 && this.materiales[0].TieneDatosValidos){
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaVService.tieneMaterialesV = false;
            }
        })        
    }
    
}
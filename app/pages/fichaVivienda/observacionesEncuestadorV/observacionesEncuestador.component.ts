import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaObservacionesEncuestadorV',
    templateUrl: 'observacionesEncuestador.component.html',
    styleUrls: ['observacionesEncuestador.component.scss']
})

export class ObservacionesEncuestadorVComponent implements OnInit{
    observaciones: Observable<Array<any>>;
    fechasAgrupadas: any;

    prefijo = 'OBSEV';

    constructor(
        public fichaVService: FichaViviendaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fichaVService.getViviendaObservacionesEncuestador().then(() => {
            this.fechasAgrupadas = this.fichaVService.fechasObsEnc;
            this.observaciones = this.fichaVService.observacionesEnc;

            if (this.fechasAgrupadas && this.fechasAgrupadas.length > 0){
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaVService.tieneObsEncV = false;
            }
        })        
    }
    
}
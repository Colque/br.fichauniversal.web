import { Component, OnInit, NgZone, Inject, ViewChild } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import { AgmMap, LatLngLiteral, MapsAPILoader } from '@agm/core';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';
import * as $ from 'jquery';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;
declare var myModalFotos: any;


@Component({
    selector: 'tarjetaPuntoInteresColombia',
    templateUrl: 'puntoInteresColombia.component.html',
    styleUrls: ['puntoInteresColombia.component.scss']
})

export class PuntoInteresColombiaComponent implements OnInit{
    @ViewChild('map', undefined) map: AgmMap;
    puntoInteres: Observable<Array<any>>;
    prefijo = 'PUNTOINTERESCOLOMBIA';
    FotosPuntoInteres: any;
    openModal:boolean = false;

    public showMapMarkers: boolean; // map controls
    public showMainPolygon: boolean;
    public marker: any;
    public mapData: any;

    public titlePunto:string = "";
    public direccion:string = "";

    constructor(
        public fichaVService: FichaViviendaDataService,
        public apiBackEndService: ApiBackEndService,
        @Inject(NgZone) private zone: NgZone
    ) {
        this.mapData = {
            latitud: -24.976099493695386,
            longitud: -65.269775390625,
            zoom: 12,
            // styles: MAP_SETTINGS,
            type: 'roadmap'
        };
        this.marker = null;
        this.showMapMarkers = true;

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.puntoInteres = this.fichaVService.puntoInteres;
    }

    GetFotos(index: any){
        this.FotosPuntoInteres = [];
        if (this.puntoInteres[index].Foto){
            this.FotosPuntoInteres.URL = this.puntoInteres[index].Foto;
            this.FotosPuntoInteres.Fecha = this.puntoInteres[index].Fecha;

            this.FotosPuntoInteres.push(
                {
                    URL: this.puntoInteres[index].Foto,
                    Titulo: this.puntoInteres[index].PuntoDeInteres,
                    Fecha: this.puntoInteres[index].Fecha,
                }
            );
            //myModalFotos.Abrir(1,'ModalFotosPuntoInteres');

            if(this.FotosPuntoInteres.length > 0){
                this.zone.runOutsideAngular(()=>{
                    myModalFotos.Abrir(this.FotosPuntoInteres.length,'ModalFotosPuntoInteres');//Título del Modal, CantidadFotos
                });
            }
        }
        //return this.ActividadesFotos;
    }

    openMapa(index:any){
        this.titlePunto = this.puntoInteres[index].PuntoDeInteres;
        this.direccion = this.puntoInteres[index].Direccion ;
        this.mapData.latitud = this.puntoInteres[index].Latitud;
        this.mapData.longitud = this.puntoInteres[index].Longitud;
        this.openModal = true;
        setTimeout( function(){
            $('#Mapa').css('top', '40%');
            $('#fade').css('opacity', '1');
        }, 50);
    
    }

    closeMapa(){
        let mThis = this;
        $('#Mapa').css('top', '100%');
        $('#fade').css('opacity', '0');
        setTimeout( function(){
            mThis.openModal = false;
        }, 200);
    }
    
}
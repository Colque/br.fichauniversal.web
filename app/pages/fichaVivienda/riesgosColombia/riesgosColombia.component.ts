import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaRiesgosColombia',
    templateUrl: 'riesgosColombia.component.html',
    styleUrls: ['riesgosColombia.component.scss']
})

export class RiesgosColombiaComponent implements OnInit{
    riesgos: Observable<Array<any>>;
    fechasAgrupadas: any;

    prefijo = 'RIEColombia';

    constructor(
        public fichaVService: FichaViviendaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fechasAgrupadas = this.fichaVService.fechasRiesgos;
        this.riesgos = this.fichaVService.riesgos;
    }    
}
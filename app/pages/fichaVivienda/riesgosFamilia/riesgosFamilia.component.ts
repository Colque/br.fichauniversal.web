import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaRiesgosFamilia',
    templateUrl: 'riesgosFamilia.component.html',
    styleUrls: ['riesgosFamilia.component.scss']
})

export class RiesgosFamiliaComponent implements OnInit{
    fechasAgrupadas: any= [];//Observable<Array<any>>;
    riesgos: Observable<Array<any>>;
    prefijo = 'RNIN';
    hogares: any = [];
    constructor(public fichaVService: FichaViviendaDataService,) {

    }

    ngOnInit(){
        this.loadData();
        console.log('fichaVService.hogares',this.fichaVService.hogares);
    }


    loadData() {
        this.fichaVService.getRiesgoFamilia().then(() => {
            this.fichaVService.riesgosFam.forEach((result: any) => {
                if (this.fechasAgrupadas.filter((fecha:any) => fecha.Fecha === result.Fecha).length <= 0 ){
                    this.fechasAgrupadas.push(result);
                }
            });
            this.riesgos = this.fichaVService.riesgosFam;
            this.orderFechas();
            if (this.fechasAgrupadas && this.fechasAgrupadas.length > 0){
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else {
                this.fichaVService.tieneRiesgosFam = false;
            }
        })
    }

    private orderFechas(){
        //ordeno el array
        this.fechasAgrupadas.sort((a:any, b:any) => {
            let fecha1 = new Date(a.Fecha);
            let fecha2 = new Date(b.Fecha);
            
            if (fecha1.getTime() < fecha2.getTime()) {
                return 1;
            }
            if (fecha1.getTime() > fecha2.getTime()) {
                return -1;
            }
            return 0;
        });
    }

    filtrarRiesgos(Fecha: string, CodHogar: string){
        return this.riesgos.filter((r: any) => r.CodHogar == CodHogar && r.Fecha == Fecha);
    }

    filtrarHogares(Fecha: string){
        console.log('Fecha', Fecha);
        return this.fichaVService.hogares.filter((h: any) => h.Fecha == Fecha)
    }

    //gauge(TotalPuntos: number, TotalFactoresConRiesgo: number, TotalFactores: number): string {
    gauge(Fecha: string, CodHogar: string): string {
        var TotalPuntos = 0;
        var TotalFactoresConRiesgo = 0;
        var TotalFactores = 0;
        this.riesgos.forEach(element => {
            if(element['Fecha'] == Fecha && element['CodHogar'] == CodHogar){
                if(Number(element['Valor']) > 0){
                    TotalFactoresConRiesgo++;
                    TotalPuntos += Number(element['Valor']);
                }

                TotalFactores++;
            }
        });

        var Colores = [
            "200, 200, 200"//color indice 0 --BASE
            , "10, 200, 53"//color indice 1 --VERDE
            , "230, 255, 80"//color indice 2 --VERDE/AMARILLO
            , "255, 255, 0"//color indice 3 --AMARILLO
            , "252, 180, 52"//color indice 4 --NARANJA
            , "255, 0, 0"//color indice 5 --ROJO
        ];
        
        var _Valor = TotalPuntos;
        var _ValorFixed = _Valor.toFixed(0);
        var _Columnas = TotalFactores;

        var _PorcentajeColumnaPintada = TotalFactoresConRiesgo * 100 / _Columnas;
        var _PorcentajeColumnaGris = 100 - _PorcentajeColumnaPintada;

        var MaximaPonderacion = 20;
        var PorcentajeLeyendaP = Number(_Valor * 100 / MaximaPonderacion).toFixed(2);
        var PorcentajeLeyendaI = Number(TotalFactoresConRiesgo * 100 / _Columnas).toFixed(2);

        var ValorIndice = 0;
        if (Number(PorcentajeLeyendaP) >= 0 && Number(PorcentajeLeyendaP) < 20) ValorIndice = 1;
        if (Number(PorcentajeLeyendaP) >= 20 && Number(PorcentajeLeyendaP) < 40) ValorIndice = 2;
        if (Number(PorcentajeLeyendaP) >= 40 && Number(PorcentajeLeyendaP) < 60) ValorIndice = 3;
        if (Number(PorcentajeLeyendaP) >= 60 && Number(PorcentajeLeyendaP) < 80) ValorIndice = 4;
        if (Number(PorcentajeLeyendaP) >= 80 && Number(PorcentajeLeyendaP) < 100) ValorIndice = 5;

        var Medidor = "";
        Medidor += "    <div class=\"col col-12 \" style=\"height: 20px; border-radius: 10px; background-color: #BBBBBB; float: left; padding: 0px; line-height: 20px;\" > ";
        Medidor += "        <div style=\"width: " + Number(_PorcentajeColumnaPintada).toFixed(2) + "%; background-color: rgb(" + Colores[ValorIndice] + "); float: left; border-radius: 10px;\" > &nbsp;</div>";
        Medidor += "        <div style=\"width: " + Number(_PorcentajeColumnaGris).toFixed(2) + "%; background-color: #BBBBBB; float: left; border-radius: 10px;\" > &nbsp;</div>";
        Medidor += "    </div>";
        Medidor += "    <div class=\"col col-6 \"><div class=\"T_Titulo\">Ponderaci&oacute;n de Riesgo</div><div class=\"T_Descripcion\">" + (TotalPuntos.toFixed(2)) + "</div></div>";
        Medidor += "    <div class=\"col col-6 \"><div class=\"T_Titulo\">Indicadores de Riesgo</div><div class=\"T_Descripcion\">" + PorcentajeLeyendaI + "%</div></div>";

        return Medidor;
    }
    
}
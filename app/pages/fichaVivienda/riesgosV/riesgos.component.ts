import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaRiesgosV',
    templateUrl: 'riesgos.component.html',
    styleUrls: ['riesgos.component.scss']
})

export class RiesgosVComponent implements OnInit{
    riesgos: Observable<Array<any>>;
    fechasAgrupadas: any;

    prefijo = 'RIEV';

    constructor(
        public fichaVService: FichaViviendaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fichaVService.getViviendaRiesgos().then(() => {
            this.fechasAgrupadas = this.fichaVService.fechasRiesgos;
            this.riesgos = this.fichaVService.riesgos;

            if (this.fechasAgrupadas && this.fechasAgrupadas.length > 0){
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaVService.tieneRiesgosV = false;
            }
        })        
    }
    
}
import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaSanitariosServiciosV',
    templateUrl: 'sanitariosServicios.component.html',
    styleUrls: ['sanitariosServicios.component.scss']
})

export class SanitariosServiciosVComponent implements OnInit{
    fechasAgrupadas: any;
    sanitarios: any = [];
    servicios: any = [];
    serviciosFilter: any = [];
    sanitariosFilter: any = [];
    prefijo = 'SASEV';
    servActivo: boolean = true;

    constructor(
        public fichaVService: FichaViviendaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fichaVService.getViviendaSanitariosServicios().then(() => {
            this.fechasAgrupadas = this.fichaVService.fechasServSan;
            this.sanitarios = this.fichaVService.sanitarios;
            this.servicios = this.fichaVService.servicios;

            this.fechasAgrupadas.forEach((fecha:any) => {
                let serv = this.servicios.filter((s:any) => s.Fecha == fecha.Fecha && s.Origen == fecha.Origen && s.NroRelevamiento == fecha.NroRelevamiento);
                fecha.servActivo = serv.length > 0 ? true : false;
            });

            console.log('this.sanitarios',this.sanitarios);
            console.log('this.servicios',this.servicios);
            
            if (this.fechasAgrupadas && this.fechasAgrupadas.length > 0){
                let fecha = this.fechasAgrupadas[0];
                this.serviciosFilter = this.servicios.filter((s:any) => s.Fecha == fecha.Fecha && s.Origen == fecha.Origen && s.NroRelevamiento == fecha.NroRelevamiento);
                this.sanitariosFilter = this.sanitarios.filter((s:any) => s.Fecha == fecha.Fecha && s.Origen == fecha.Origen && s.NroRelevamiento == fecha.NroRelevamiento);
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaVService.tieneServiciosV = false;
            }
        })
    }

    onChangeTab(event:any){
        let fecha = this.fechasAgrupadas[event.index];
        this.serviciosFilter = this.servicios.filter((s:any) => s.Fecha == fecha.Fecha && s.Origen == fecha.Origen && s.NroRelevamiento == fecha.NroRelevamiento);
        this.sanitariosFilter = this.sanitarios.filter((s:any) => s.Fecha == fecha.Fecha && s.Origen == fecha.Origen && s.NroRelevamiento == fecha.NroRelevamiento);
    }

    tieneVisita(EsSanitario: boolean, Fecha: string): boolean{
        var EsIgual = false;
        if(EsSanitario){
            this.sanitarios.forEach((element:any) => {
                if(element['Fecha'] == Fecha){
                    EsIgual = true;
                }
            });
        }else{
            this.servicios.forEach((element:any) => {
                if(element['Fecha'] == Fecha){
                    EsIgual = true;
                }
            });
        }

        return EsIgual;
    }

    tabActiva(EsSanitario: boolean, Fecha: string): string{
        var EsActiva = false;

        if(!EsSanitario){
            if(this.tieneVisita(false, Fecha)){
                EsActiva = true;
            }
        }else{
            if(!this.tieneVisita(false, Fecha)){
                if(this.tieneVisita(true, Fecha)){
                    EsActiva = true;
                }
            }
        }

        return (EsActiva ? "active" : "");
    }
    
}
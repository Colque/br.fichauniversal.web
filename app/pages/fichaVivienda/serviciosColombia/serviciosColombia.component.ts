import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaServiciosColombia',
    templateUrl: 'serviciosColombia.component.html',
    styleUrls: ['serviciosColombia.component.scss']
})

export class ServiciosColombiaComponent implements OnInit{
    fechasAgrupadas: Observable<Array<any>>;
    sanitarios: Observable<Array<any>>;
    servicios: any;
    prefijo = 'SERVCOLOMBIA';

    constructor(
        public fichaVService: FichaViviendaDataService,
        public apiBackEndService: ApiBackEndService
    ) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fechasAgrupadas = this.fichaVService.fechasServSan;
        this.servicios = this.fichaVService.servicios;

        this.servicios.forEach((serv: any, i:any) => {
            serv.Servicios = JSON.parse(serv.Servicios);
        });
    }

    tieneVisita(EsSanitario: boolean, Fecha: string): boolean{
        var EsIgual = false;
        if(EsSanitario){
            this.sanitarios.forEach(element => {
                if(element['Fecha'] == Fecha){
                    EsIgual = true;
                }
            });
        }else{
            this.servicios.forEach((element:any) => {
                if(element['Fecha'] == Fecha){
                    EsIgual = true;
                }
            });
        }

        return EsIgual;
    }

    tabActiva(EsSanitario: boolean, Fecha: string): string{
        var EsActiva = false;

        if(!EsSanitario){
            if(this.tieneVisita(false, Fecha)){
                EsActiva = true;
            }
        }else{
            if(!this.tieneVisita(false, Fecha)){
                if(this.tieneVisita(true, Fecha)){
                    EsActiva = true;
                }
            }
        }

        return (EsActiva ? "active" : "");
    }
    
}
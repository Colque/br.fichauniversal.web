import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaUbicacionColombia',
    templateUrl: 'ubicacionColombia.component.html',
    styleUrls: ['ubicacionColombia.component.scss']
})

export class UbicacionColombiaComponent implements OnInit{
    vivienda: any;
    comunidad: Observable<Array<any>>;
    prefijo = 'UBICol';
    tarjetaActiva = 'vivienda';

    constructor(
        public fichaVService: FichaViviendaDataService,
        public apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        //this.apiBackEndService.getViviendaUbicacion().subscribe(resultado => {
            this.vivienda = this.fichaVService.ubicacion;
                this.vivienda.forEach((v:any, i:any) => {
                v.paisSRC = v.PAI_CODIGO ? '../../../Content/flag-icon-css/flags/4x3/'+v.PAI_CODIGO +'.svg' : null;
                if (i === 0){
                    v.active = true;
                    v.MostrarOrigen = true;
                }
            });
        //});
    }
}
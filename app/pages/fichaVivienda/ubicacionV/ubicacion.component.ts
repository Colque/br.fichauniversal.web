import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';

//paraloading
import './../../../../js/myplugins.js'
declare var myLoading: any;


@Component({
    selector: 'tarjetaUbicacionV',
    templateUrl: 'ubicacion.component.html',
    styleUrls: ['ubicacion.component.scss']
})

export class UbicacionVComponent implements OnInit{
    ubicacion: any;
    prefijo = 'UBIV';

    constructor(
        public fichaVService: FichaViviendaDataService,
        private apiBackEndService: ApiBackEndService) {

    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        this.fichaVService.getViviendaUbicacion().then(() => {
            this.ubicacion = this.fichaVService.ubicacion;
            if (this.ubicacion && this.ubicacion.length > 0){
                myLoading.Ocultar('Loading'+ this.prefijo);
            } else{
                this.fichaVService.tieneUbicacion = false;
            }
        })
    }
    
}
import { Component, OnInit } from '@angular/core';
//import {NgControl} from '@angular/common';
import {ApiBackEndService} from './../../../_services/apiBackEnd.service';
import { FichaViviendaDataService } from './../fichaViviendaData.service';
import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';

//paraloading
import './../../../../js/myplugins.js'
import { Sanitizer } from '@angular/core/src/security';
declare var myLoading: any;


@Component({
    selector: 'tarjetaVideosV',
    templateUrl: 'videos.component.html',
    styleUrls: ['videos.component.css']
})

export class VideosVComponent implements OnInit {
    videos2D: any[];
    //videos360: Observable<Array<any>>;
    videos360: any[];
    prefijo = 'VIDV';
    TieneVideos = false;

    constructor(
        public fichaVService: FichaViviendaDataService,
        private apiBackEndService: ApiBackEndService, private sanitizer: DomSanitizer) {
        this.TieneVideos = false;
        this.videos2D = [];
        this.videos360 = [];
    }

    ngOnInit(){
        this.loadData();
    }


    loadData() {
        var principal = this;
        var promises :any = [];

        promises.push(
            new Promise((resolve, reject) => { 
                this.fichaVService.getViviendaVideos2D().then(() =>{
                    this.videos2D = this.fichaVService.videos2D;
                    resolve();
                });
            })
        )
        promises.push(
            new Promise((resolve, reject) => { 
                this.fichaVService.getViviendaVideos360().then(() =>{
                    this.videos360 = this.fichaVService.videos360;
                    resolve();
                });
            })
        )

        Promise.all(promises).then(() => 
            {   
                if (this.videos2D.length > 0 || this.videos360.length > 0){
                    this.TieneVideos = true;
                    myLoading.Ocultar('Loading'+ this.prefijo);
                } else{
                    this.fichaVService.tieneVideosV = false;
                }
            }
        );

        // this.videos360 = [
        //     {
        //         CodVisita:"",
        //         Fecha : "02/03/18",
        //         FechaRegistro: "02/03/2018",
        //         dirArchivo:"Content/video/Lote21Ambiental_V2_2.mp4",
        //         Titulo: "Ambiental Lote 21",
        //         Descripcion: "Video 360 de visita de caracter ambiental",
        //         Orden: "0",
        //         Thumbnail: "",
        //         Duracion: "00:00:41"
        //     }
        // ];
        // this.TieneVideos = true;
        // this.CantidadVideos360 = 1;
    }

    urlSafe(urlUnsafe: string): SafeUrl{
        return this.sanitizer.bypassSecurityTrustResourceUrl(urlUnsafe);
    }
    
}
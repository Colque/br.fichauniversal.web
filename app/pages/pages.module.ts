import { NgModule }      from '@angular/core';
import { BrowserModule, Title  } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// used to create fake backend
import { fakeBackendProvider } from './../_helpers/index';

// import { routing }        from './../app.routing';

import { AuthGuard } from './../_guards/index';
import { JwtInterceptor } from './../_helpers/index';
import {
    AlertService,
    AuthenticationService,
    UserService,
    ApiBackEndService
} from './../_services/index';
;
//persona
import { FichaPersonaModule } from './fichaPersona/fichaPersona.module';
// //vivienda
import { FichaViviendaModule } from './fichaVivienda/fichaVivienda.module';

import { Md2Module } from 'Md2';
import {BrowserAnimationsModule} from  "@angular/platform-browser/animations";
import { TimelineComponent } from './../components/timeline/timeline.component';
import { MaterialModule } from "@angular/material";

import { PagesRoutes } from './pages.routes';

import { SafePipe } from '../myjs/utils/HtmlPipe';
import { RegisterComponent } from '../register/register.component'
import { ChatWindowComponent } from '../components/chat/chat-window/chat-window.component';

import { ComponentsModule } from '../components/components.module';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MaterialModule,
        Md2Module,
        PagesRoutes,
        ComponentsModule,
        FichaPersonaModule,
        FichaViviendaModule,
    ],
    declarations: [
        // RegisterComponent,
        SafePipe,
    ],
    exports: [
        SafePipe,
        //TimelineComponent
    ],
    providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
        //aqui nuestro provider
        ApiBackEndService,

        // provider used to create fake backend
        fakeBackendProvider,
        Title
    ],
    bootstrap: []
})

export class PagesModule { }

import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './../_guards/index';

import { FichaViviendaComponent } from './fichaVivienda/fichaVivienda.component';
import { FichaPersonaComponent } from './fichaPersona/fichaPersona.component';
import { LoginComponent } from './../login/index';


const _routes: Routes = [
    {
        path: '',
        component: LoginComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'FichaPersona/:p',
        component: FichaPersonaComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'FichaVivienda/:v',
        component: FichaViviendaComponent,
        canActivate: [AuthGuard]
    },
];

export const PagesRoutes = RouterModule.forRoot(_routes, { useHash: true });

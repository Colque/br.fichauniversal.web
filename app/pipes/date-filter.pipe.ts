import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'dateFilter'
})
/**
 * Filtrar datos de un array
 * @author Carlos F. Colque
 */
export class DateFilter implements PipeTransform {
    transform(items: any[], key: string,  filter: string): any {
        if (items === undefined) { return items; }
        if (items.length === 0) { return items; }
        if (key === undefined) { return items; };
        if (items && !filter) { return items; };
        
        return items.filter(
            i =>
            i[key].toLowerCase().indexOf(filter.toLowerCase()) !== -1
        );
    }
}

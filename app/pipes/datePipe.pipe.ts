import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'datePipe'
})
export class datePipe implements PipeTransform {
    transform(fecha: any, format?: any) {
        if (moment(fecha).isValid()){
            return moment(fecha).format(format);
        } else{
            var parts = fecha.split('/');
            // Please pay attention to the month (parts[1]); JavaScript counts months from 0:
            // January - 0, February - 1, etc.
            var mydate = new Date(parts[2], parts[1] - 1, parts[0]);
            return moment(mydate).format(format);
        }
    }
}
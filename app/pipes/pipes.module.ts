import { DateFilter } from './date-filter.pipe';
import { datePipe } from './datePipe.pipe';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { SafePipe } from './safePipe.pipe';
import { ValidaStringPipe } from './validaStringPipe.pipe';

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
    ],
    declarations: [
        SafePipe,
        datePipe,
        ValidaStringPipe,
        DateFilter
    ],
    exports: [
        SafePipe,
        datePipe,
        ValidaStringPipe,
        DateFilter
    ],
    providers: [
    ]
})
export class PipesModule {}

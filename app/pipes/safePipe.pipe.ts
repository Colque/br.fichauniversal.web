import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser';



/**
 * Escapa el contenido HTML de un elementos y los caracteres especiales
 * @author Carlos F. Colque
 */
@Pipe({
    name: 'safe'
})
export class SafePipe implements PipeTransform {
    constructor(private _sanitizer: DomSanitizer) {}
    transform(value: any, type: string) {
        switch (type) {
            case 'html':
                return this._sanitizer.bypassSecurityTrustHtml(value);
            case 'style':
                return this._sanitizer.bypassSecurityTrustStyle(value);
            case 'script':
                return this._sanitizer.bypassSecurityTrustScript(value);
            case 'url':
                return this._sanitizer.bypassSecurityTrustUrl(value);
            case 'resourceUrl':
                return this._sanitizer.bypassSecurityTrustResourceUrl(value);
            default:
                throw new Error(`Unable to bypass security for invalid type: ${type}`);
        }
    }
}

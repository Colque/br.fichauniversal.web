import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
    name: 'validaString'
})
export class ValidaStringPipe implements PipeTransform {
    transform(value: any) {
        let val = value.replace(/\s/g, '');
        return val.length > 0 ? value : '-';
    }
}

import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class VersionCheckService {
    // this will be replaced by actual hash post-build.js
    //private currentHash = 'b7f006dcb2714bc49d23';

    constructor(private http: Http) {}

    /**
     * Checks in every set frequency the version of frontend application
     * @param url
     * @param {number} frequency - in milliseconds, defaults to 30 minutes
     */
    public initVersionCheck(url: any, frequency = 1000 * 60 * 30) {
        //setInterval(() => {
            this.checkVersion(url);
        //}, frequency);
    }

    /**
     * Will do the call and check if the hash has changed or not
     * @param url
     */
    private checkVersion(url: any) {
        // timestamp these requests to invalidate caches
        //let currentHash = "";
        this.http.get(url + '?t=' + new Date().getTime())
            .first()
            .subscribe(
                (response: any) => {
                    let datos = JSON.parse(response._body);
                    let hash = datos.hash;
                    if (localStorage.getItem('version')){
                        let versionActual = JSON.parse(localStorage.getItem('version'));
                        let currentHash = versionActual.hash;
                        let hashChanged = this.hasHashChanged(currentHash, hash);
                        // If new version, do something
                        if (`"${currentHash}"` !== `"${hash}"`) {
                            // ENTER YOUR CODE TO DO SOMETHING UPON VERSION CHANGE
                            // for an example: location.reload();
                            let src = {version: datos.version, hash: datos.hash};//, hashActual: datos.hash};
                            
                            // let src = `{"version": "${datos.version}", "hash": "${datos.hash}", "hashActual": "${datos.hash}"}`;
                            // ArchivosJson.guardarJSON(url, src);
                            localStorage.setItem('version', JSON.stringify(src));
                            location.reload();
                        }
                        // store the new hash so we wouldn't trigger versionChange again
                        // only necessary in case you did not force refresh
                    }else {
                            let src = {version: datos.version, hash: datos.hash};
                            localStorage.setItem('version', JSON.stringify(src));
                            location.reload();
                        }
                },
                (err) => {
                    console.error(err, 'Could not get version');
                }
            );
    }
    /**
     * Checks if hash has changed.
     * This file has the JS hash, if it is a different one than in the version.json
     * we are dealing with version change
     * @param currentHash
     * @param newHash
     * @returns {boolean}
     */
    private hasHashChanged(currentHash: string, newHash: string) {
        if (currentHash === '{{POST_BUILD_ENTERS_HASH_HERE}}') {
            return false;
        }

        return `"${currentHash}"` !== `"${newHash}"`;
    }
}
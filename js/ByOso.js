﻿/*GALERIA*/
function AbreViewer2(index) {
    var _Viewer = $("#ViewerGrid");
    // var _Galeria = $("#FOT");
    // var _FotoActiva = _Galeria.find("div.swiper-slide-active");
     var _IndiceActivo = index.replace("FotoIndex", "");//(_FotoActiva.attr("id")).replace("FotoIndex", "");
     console.log('_IndiceActivo>>>>>>>>>>>>', _IndiceActivo)
    // console.log((_FotoActiva.attr("id")).replace("FotoIndex", ""));

    //agregamos el atributo original
    var _ImagenViewer = _Viewer.find("img[id='ViewerFotoIndex" + _IndiceActivo + "']");
    console.log('_ImagenViewer>>>>>>>>>>>>', _ImagenViewer)
    _ImagenViewer.attr("original", _ImagenViewer.attr("class"));
    //hacemos el clic en la foto correspondiente
    _ImagenViewer.click();
}

function AbreViewer(index) {
    var _Viewer = $("#ViewerGrid");
    var _Galeria = $("#FOT");
    var _FotoActiva = _Galeria.find("div.swiper-slide-active");
    var _IndiceActivo = (_FotoActiva.attr("id")).replace("FotoIndex", "");

    //agregamos el atributo original
    var _ImagenViewer = _Viewer.find("img[id='ViewerFotoIndex" + _IndiceActivo + "']");
    _ImagenViewer.attr("original", _ImagenViewer.attr("class"));
    //hacemos el clic en la foto correspondiente
    _ImagenViewer.click();
}

var galeria = {
    ActivaGaleriaFotos: function() {
        //activa galería de fotos
        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            // slidesPerView: 1,
            paginationClickable: true,
            // spaceBetween: 0,
            // loop: true
        });
    },
    ActivarViewerFotos: function () {
        var _Viewer = $("#ViewerGrid");
        _Viewer.find("img").each(function () {
            $(this).attr("original", $(this).attr("class"));
        });
        $('.imagesViewer').viewer();
    },

    ActivarViewerFotosTime: function () {
        var _Viewer = $("#ViewerGridTime");
        _Viewer.find("img").each(function () {
            $(this).attr("original", $(this).attr("class"));
        });
        $('.imagesViewerTime').viewer('destroy');
        $('.imagesViewerTime').viewer();
    },

    AbreViewerTime: function(index) {
        var _Viewer = $("#ViewerGridTime");
        var _IndiceActivo = index;
        //agregamos el atributo original
        var _ImagenViewer = _Viewer.find("img[id='ViewerFotoIndexTime" + _IndiceActivo + "']");
        _ImagenViewer.attr("original", _ImagenViewer.attr("class"));
        //hacemos el clic en la foto correspondiente
        _ImagenViewer.click();
    },

    ActivarViewerFotosTarjeta: function (tarjeta,append) {
        var _Viewer = $("#ViewerGrid" + tarjeta);
        _Viewer.find("img").each(function () {
            $(this).attr("original", $(this).attr("class"));
        });
        _Viewer.viewer('destroy');
        $('.imagesViewer' + tarjeta).viewer('destroy');
         setTimeout( function(){
            $('.imagesViewer' + tarjeta).viewer();
            if (append)
                $("body").append(_Viewer);
        }, 100);
    },

    AbreViewerTarjeta: function(index,tarjeta) {
        var _Viewer = $("#ViewerGrid" + tarjeta);
        var _IndiceActivo = index;
        //agregamos el atributo original
        var _ImagenViewer = _Viewer.find("img[id='ViewerFotoIndex" + tarjeta + _IndiceActivo + "']");
        _ImagenViewer.attr("original", _ImagenViewer.attr("class"));
        //hacemos el clic en la foto correspondiente
        _ImagenViewer.click();
    }
}

function ActivaGaleriaFotos() {
    //activa galería de fotos
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 1,
        paginationClickable: true,
        spaceBetween: 0,
        loop: true
    });
}

function ActivarViewerFotos() {
    var _Viewer = $("#ViewerGrid");
    _Viewer.find("img").each(function () {
        $(this).attr("original", $(this).attr("class"));
    });
    
    $('.imagesViewer').viewer();
}

function AbreLinea() {
    //$('#lineamodal').modal('show');
    $('#lineamodal').css("display","block");
    $('#lineamodal').appendTo("body");//.modal('show');
}

function CierraLinea() {
    $('#lineamodal').css("display","none");
}


/*MAPA*/
var mapa;

function CargaMapa(CodVisita) {
    if (CodVisita == "") {
        CodVisita = $("#myTabMapContent").find("div.active[type='DivMapa']").attr("id").replace("Mapa", "");
    }

    var lat_ = $("#Mapa" + CodVisita).find("#Latitud").val().replace(",", ".");
    var lng_ = $("#Mapa" + CodVisita).find("#Longitud").val().replace(",", ".");
    var paraje_ = $("#Mapa" + CodVisita).find("#Paraje").val();

    initMap(lat_, lng_, paraje_);
}

function initMap(lat_, lng_, paraje_) {

    var MapType = 'hybrid';//typeof paraje_ === "undefined" ? "roadmap" : (paraje_ != null ? (paraje_ != "" ? "satellite" : "roadmap") : "roadmap");

    if (lat_ === undefined || lng_ === undefined) {
        lat_ = -24.778402;
        lng_ = -65.405971;
    }
    if (mapa == null) {
        if (typeof (window.google) != 'object') {
            setTimeout(function () {
                console.log("retrasando inicio google maps");
                initMap(lat_, lng_, paraje_);
            }, 1000);
        }
        else {
            mapa = new GMaps({
                div: "#Mapa",
                lat: lat_,
                lng: lng_,
                mapType: MapType
            });

            mapa.setCenter(lat_, lng_);
            mapa.addMarker({
                lat: lat_,
                lng: lng_,
            });
        }
    }
    else {
        mapa.removeMarkers();
        if (MapType == "roadmap") {
            mapa.setMapTypeId(google.maps.MapTypeId.ROADMAP);
        } else {
            mapa.setMapTypeId(google.maps.MapTypeId.SATELLITE);
        }
        mapa.setCenter(lat_, lng_);
        mapa.addMarker({
            lat: lat_,
            lng: lng_,
        });
    }
}

/*MODALES*/
var ByOso = {
    openModalVisita: function ModalVisita(_CodVisita, _IdModal) {
        //console.log(_CodVisita);
        //if(_CodVisita != "") {
        _IdModal = "#" + _IdModal;
        $(_IdModal).appendTo("body").modal();

        $(_IdModal).find("a[title='" + _CodVisita + "']").each(function () {
            var _Visita = $(this);

            if (_Visita.attr("title") == _CodVisita) {
                _Visita.click();

                setTimeout(function() {
                    LeerMas();
                }, 500);
            }
        });
        //}
    }
}

var mapaModal;
function MapaModalView(_DIV, _IdModal) {
    var _Latitud = _DIV.find("input[name=_Latitud]").val();
    var _Longitud = _DIV.find("input[name=_Longitud]").val();
    var _Titulo = _DIV.find("input[name=_Titulo]").val();
    
    _IdModalMapa = "#" + _IdModal + "DIV";
    _IdModal = "#" + _IdModal;
    
    $(_IdModal).appendTo("body").modal();
    $(_IdModal).find("[name=ModalTitle]").html(_Titulo);

    var MapType = "satellite";

    if (_Latitud === undefined || _Longitud === undefined) {
        _Latitud = -24.778402;
        _Longitud = -65.405971;
    }
    if (mapaModal == null) {
        if (typeof (window.google) != 'object') {
            setTimeout(function () {
                console.log("retrasando inicio google maps");
                MapaModalView(_DIV, _IdModal);
            }, 1000);
        }
        else {
            mapaModal = new GMaps({
                div: (_IdModalMapa),
                lat: _Latitud,
                lng: _Longitud,
                mapType: MapType
            });

            mapaModal.setCenter(_Latitud, _Longitud);
            mapaModal.addMarker({
                lat: _Latitud,
                lng: _Longitud,
            });
        }
    }
    else {
        mapaModal.removeMarkers();
        if (MapType == "roadmap") {
            mapaModal.setMapTypeId(google.maps.MapTypeId.ROADMAP);
        } else {
            mapaModal.setMapTypeId(google.maps.MapTypeId.SATELLITE);
        }
        mapaModal.setCenter(_Latitud, _Longitud);
        mapaModal.addMarker({
            lat: _Latitud,
            lng: _Longitud,
        });
    }
}

/*ARMA LISTA DE TARJETAS PARA IMPRIMIR EN PDF*/
function ArmaListaPDF() {
    if (typeof $("#DivTarjetasImprimir") !== "undefined") {
        $(".TarjetaContenedor").each(function () {
            var _Seccion = $(this);
            if (_Seccion.css("display") != "none") {
                _Seccion.find("input[name='ParaListaPDF']").each(function () {
                    var _Tarjeta = $(this);
                    if (typeof $("div[id='Div" + _Tarjeta.attr("id") + "']").attr("id") === "undefined") {
                        var _Div = "<div class=\"col col-12\" id=\"Div" + _Tarjeta.attr("id") + "\"><input type=\"checkbox\" id=\"Chk" + _Tarjeta.attr("id") + "\" value=\"" + _Tarjeta.attr("id") + "\" onclick=\"GeneraEnlacePDF();\" checked /><label for=\"Chk" + _Tarjeta.attr("id") + "\">" + _Tarjeta.attr("title") + "</label><br/></div>";
                        $("#DivTarjetasImprimir").append(_Div);
                    }
                });
            } else {
                _Seccion.find("input[name='ParaListaPDF']").each(function () {
                    var _Tarjeta = $(this);
                    //console.log(typeof $("div[id='Div" + _Tarjeta.attr("id") + "']").attr("id"));

                    if (typeof $("div[id='Div" + _Tarjeta.attr("id") + "']").attr("id") !== "undefined") {
                        $("div[id='Div" + _Tarjeta.attr("id")+"']").remove();
                    }
                });
            }
        });
    }    
}

function GeneraEnlacePDF() {
    var Enlace = "PDF/DescargaFichaPersona?codpersona=" + $("#CodPersona").val() + "&Tarjetas=";
    console.log(Enlace);

    $("#DivTarjetasImprimir").find("input[type='checkbox']").each(function () {
        if ($(this).is(":checked")) {
            Enlace += $(this).val() + ";";
        }
        
    });

    $("#BotonDescargaPDF").attr("href", Enlace);
}

/*VIDEOS*/
var myPlayer;
var myPlayer2;
function AbrirVideo(urlvideo) {
    $('#videomodal').appendTo("body").modal('show');

    var videoPlayer = $("video[TypeVideo='2D']");

    videoPlayer.attr('src', urlvideo);

    var myVideo2D = document.getElementById("VideoPlayerNew2D");
    myVideo2D.play();

    $('#videomodal').on('hidden.bs.modal', function () {
        VideoModalCerrar();
    });
}

function AbrirVideo360(urlvideo, titulo) {
    if (urlvideo) {
        //$('#video360modal').modal('show');
        
        $('#video360modal').appendTo("body").modal();
        $("#video360modal").find("span.info-title-cards").html(titulo);
        $("#video-container360").attr("src", urlvideo);
        
        var player = videojs('#video-container360', {
            techOrder: ['html5']
        });
        player.src({src: urlvideo, type: "video/mp4"});
        try {
            player.vr({projection: "360"});    
        } catch (error) {
            
        }
                
        $("#video-container360_html5_api").attr("src", urlvideo);
        
        //myPlayer2 = $("#video-container360");

        $('#video360modal').on('hidden.bs.modal', function () {
            // VideoModalCerrar();
            console.log('Se cerro el modal video 360');
            $("#video-container360").attr("src", "");
        });

        /*if (typeof myPlayer !== "undefined") {
            myPlayer.hide();
            if (!(myPlayer.paused())) {
                myPlayer.pause();
            };
        }*/

        setTimeout(function () {
            $("#video-container360").css("width", $("#contenedor360").width());
            $("#video-container360").css("height", Number($("#contenedor360").width()) * 0.5625);
        }, 500);
    }
}

function VideoModalCerrar() {
    var myVideo2D = document.getElementById("VideoPlayerNew2D");
    myVideo2D.pause();
}

function LeerMas(IdContenedor){
    //activamos readmore
    $('article').readmore({
        speed: 75,
        moreLink: '<a href="#" style="float: right;">Leer más</a>',
        lessLink: '<a href="#" style="float: right;">Minimizar</a>',
        collapsedHeight: 50
    });
}
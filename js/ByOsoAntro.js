﻿﻿
//globales para manipular los gráficos
var graficoPECanvas;
var graficoTECanvas;
var graficoIMCCanvas;

var _AntroOrigenes = [];
var _AntroColorOrigen = [];

// Graficos Antropometrico
var TiposGraficos = [
	{
	    tipo: 'PE',
	    titulo: 'Peso/Edad',
	    axis: {
	        x: 'Edad (meses)',
	        y: 'Peso (kg)'
	    },
	    xk: 'dias',
	},
	{
	    tipo: 'IMC',
	    titulo: 'IMC/Edad',
	    axis: {
	        x: 'Edad (meses)',
	        y: 'Indice de masa corporal'
	    },
	    xk: 'dias',
	},
	{
	    tipo: 'TE',
	    titulo: 'Talla/Edad',
	    axis: {
	        x: 'Edad (meses)',
	        y: 'Longitud/Talla (cm)'
	    },
	    xk: 'dias',
	},
]


var ByOsoAntro = {
grafico: function graficosPtjeZ(_Grafico) {
    $.each(TiposGraficos, function (idx, graph) {
        try {
            if (graph.tipo == _Grafico) {
                crearGrafico(0, 0, graph.tipo, graph.xk, idx, graph.titulo, graph.axis);
            }
            
        } catch (err) {
            //alert(err);
        };
    });
}
};



function crearGrafico(valor, parametro, grafico, xK, i, titulo, eje) {
    console.log("graficando...");
    var data = {};

    yK = ["-1 SD", "-2 SD", "-3 SD", "1 SD", "2 SD", "3 SD", "Median", "dd"];
    yLP = ["3", "15", "50", "85", "97"];
    yL = ["-3", "-2", "-1", "1", "2", "3", "0"];
    lineColors = ['#DAA520', '#FF4500', '#000000', '#DAA520', '#FF4500', '#000000', '#008000'];
    lineColor = ['#78909C', '#78909C', '#78909C', '#78909C', '#78909C', '#78909C', '#78909C', '#333333'];//el ulimo valor es de los points
    var backgroundColor = ['#6A7ED3', '#BFCAEF', '#9DABE0', '#BFCAEF', '#6A7ED3', '#FFFFFF'];

    var ymin = 0;
    var ymax = 0;

    var JsonFromC = "";
    switch (grafico) {
        case "TE":
           JsonFromC = $("#GraficoAntroTE").html();
           break;
        case "PE":
            JsonFromC = $("#GraficoAntroPE").html();
            break;
        case "IMC":
            JsonFromC = $("#GraficoAntroIMC").html();
            break;
    }

    var json = $.parseJSON(JsonFromC);
    
    var ContadorGeneral = 0;

    $.when(getEvolucion(grafico)).done(function (response) {        
        var data_ = $.parseJSON(response);

        var contadorEvo = 0;
        var evolucion = new Array();
        var evolucion2 = new Array();

        $.each(data_, function (index, row) {
            if (row.x !== "" && row.y !== "" && parseFloat(row.x) > 0 && row.y !== "No calculado") {
                evolucion.push([parseFloat(row.x), parseFloat(row.y)]);
                evolucion2.push([parseFloat(row.x), parseFloat(row.y), contadorEvo]);
                contadorEvo++;

                if (row.y > ymax) {
                    ymax = row.y;
                }
                else if (row.y < ymin) {
                    ymin = row.y;
                }
            }
        });
        
        var datos = [];
        var ticks = [];
        var mas1 = [];
        var mas2 = [];
        var mas3 = [];
        var menos1 = [];
        var menos2 = [];
        var menos3 = [];

        var median = [];
        var val = [];
        var ymin = 10000000;
        //0;
        var ymax = 0.00;
        //50;
        //preparamos los datos
        if (true) {
            $.each(json, function (key, value) {
                if (xK == "cm") {
                    this[xK] = parseFloat(this[xK].toFixed(1));
                }
                mas1.push([this[xK], this["1 SD"]]);
                mas2.push([this[xK], this["2 SD"]]);
                mas3.push([this[xK], this["3 SD"]]);
                median.push([this[xK], this["Median"]]);
                menos1.push([this[xK], this["-1 SD"]]);
                menos2.push([this[xK], this["-2 SD"]]);
                menos3.push([this[xK], this["-3 SD"]]);

                ymax = Number(ymax);

                if (ymin > this["1 SD"] && this["1 SD"] != null) {
                    ymin = this["1 SD"];
                }
                if (ymax < this["1 SD"] && this["1 SD"] != null) {
                    ymax = this["1 SD"];
                }
                if (ymin > this["2 SD"] && this["2 SD"] != null) {
                    ymin = this["2 SD"];
                }
                if (ymax < this["2 SD"] && this["2 SD"] != null) {
                    ymax = this["2 SD"];
                }
                if (ymin > this["3 SD"] && this["3 SD"] != null) {
                    ymin = this["3 SD"];
                }
                if (ymax < this["3 SD"] && this["3 SD"] != null) {
                    ymax = this["3 SD"];
                }
                if (ymin > this["Median"] && this["Median"] != null) {
                    ymin = this["Median"];
                }
                if (ymax < this["Median"] && this["Median"] != null) {
                    ymax = this["Median"];
                }
                if (ymin > this["-1 SD"] && this["-1 SD"] != null) {
                    ymin = this["-1 SD"];
                }
                if (ymax < this["-1 SD"] && this["-1 SD"] != null) {
                    ymax = this["-1 SD"];
                }
                if (ymin > this["-2 SD"] && this["-2 SD"] != null) {
                    ymin = this["-2 SD"];
                }
                if (ymax < this["-2 SD"] && this["-2 SD"] != null) {
                    ymax = this["-2 SD"];
                }
                if (ymin > this["-3 SD"] && this["-3 SD"] != null) {
                    ymin = this["-3 SD"];
                }
                if (ymax < this["-3 SD"] && this["-3 SD"] != null) {
                    ymax = this["-3 SD"];
                }

                val.push([this[xK], this["Val"]]);
            });

            //console.log(mas1);

            /*añadir punto a val en caso que no tenga ninguno
             * esto se hace para que siempre muestre las etiquetas
             * del eje y derecho (+3,+2,+1,0,-1,-2,-3)
             */
            var vacio = true;
            $.each(val, function (key, value) {
                if (value[1] !== null) {
                    vacio = false;
                    return false;
                }
            });
            if (vacio) {
                var first = val[0];
                first[1] = 25;
                val[0] = first;
            }

            datos = [
                {
                    data: mas3,
                    lines: {
                        fill: true,
                        fillColor: backgroundColor[0],
                        fillOpacity: 1, lineWidth: 2,
                    }

                },
                {
                    data: mas2,
                    lines: {
                        fill: true,
                        fillColor: backgroundColor[1],
                        fillOpacity: 1, lineWidth: 2,
                    }

                },
                {
                    data: mas1,
                    lines: {
                        fill: true,
                        fillColor: backgroundColor[2],
                        fillOpacity: 1, lineWidth: 2,
                    }

                },
                {
                    data: median,
                    lines: {
                        fill: true,
                        fillColor: backgroundColor[2],
                        fillOpacity: 1, lineWidth: 2,
                    }

                },
                {
                    data: menos1,
                    lines: {
                        fill: true,
                        fillColor: backgroundColor[3],
                        fillOpacity: 1, lineWidth: 2,
                    }

                },
                {
                    data: menos2,
                    lines: {
                        fill: true,
                        fillColor: backgroundColor[4],
                        fillOpacity: 1, lineWidth: 2,
                    }
                },
                {
                    data: menos3,
                    lines: {
                        fill: true,
                        fillColor: backgroundColor[5],
                        fillOpacity: 1, lineWidth: 2
                    }
                },
                {
                    data: evolucion,
                    lines: {
                        show: true,
                        lineWidth: 2
                    },
                    points: {
                        show: true,
                        fillColor: "#FFFFFF",
                        radius: 5,
                    },
                    mouse: {
                        track: true,
                        trackY: false,
                        hitRadius: 5,
                        lineColor: 'red',
                        relative: true,
                        position: 'cc',
                        sensibility: 10,
                        trackDecimals: 0,
                        fillOpacity: 0.7,
                        trackFormatter: function (obj) {
                            var colorOrigen = "";
                            for (var iao = 0; iao < _AntroColorOrigen.length; iao++) {
                                var colorActual = _AntroColorOrigen[iao].Color;
                                var origenActual = _AntroColorOrigen[iao].Origen;
                                if (colorOrigen == "" && origenActual == "OTRO") {
                                    colorOrigen = colorActual;
                                }

                                if (_AntroOrigenes[obj.index] == origenActual) {
                                    colorOrigen = colorActual;
                                }
                            }

                            var origen = '<span style="color:' + colorOrigen + ';">' + _AntroOrigenes[obj.index] + '</span>'

                            switch (grafico) {
                                case "PT":
                                    return ("Talla: " + obj.nearest.x + " cm. <br> Peso: " + obj.nearest.y + " kg." + "<br>Origen: " + origen);
                                case "PL":
                                    return ("Longitud: " + obj.nearest.x + " cm. <br> Peso: " + obj.nearest.y + " kg." + "<br>Origen: " + origen);
                                case "PE":
                                    return ("Edad: " + Math.floor(obj.x / 30.4375) + " (Meses) <br> Peso: " + obj.nearest.y + " kg." + "<br>Origen: " + origen);
                                case "TE":
                                    return ("Edad: " + Math.floor(obj.x / 30.4375) + " (Meses) <br> Talla: " + obj.nearest.y + " cm." + "<br>Origen: " + origen);
                                case "IMC":
                                    return ("Edad: " + Math.floor(obj.x / 30.4375) + " (Meses) <br> IMC: " + obj.nearest.y + "<br>Origen: " + origen);
                                case "PCE":
                                    return ("Edad: " + Math.floor(obj.x / 30.4375) + " (Meses) <br> PC: " + obj.nearest.y + " cm." + "<br>Origen: " + origen);
                            }
                        }
                    }
                },
                {
                    data: val,
                    points: {
                        show: false,
                        fillColor: '#99FF00',
                        radius: 5,
                    },
                    yaxis: 2
                },
            ];

            for (var j = 0; j < evolucion2.length; j++) {
                var colorPunto = "";

                if(typeof _AntroColorOrigen !== "undefined"){
                    for (var iao = 0; iao < _AntroColorOrigen.length; iao++) {
                        var colorActual = _AntroColorOrigen[iao].Color;
                        var origenActual = _AntroColorOrigen[iao].Origen;
                        if (colorPunto == "" && colorActual == "OTRO") {
                            colorPunto = colorActual;
                        }

                        if (_AntroOrigenes[evolucion2[j][2]] == origenActual) {
                            colorPunto = colorActual;
                        }
                    }
                } else {
                    colorPunto = "#66BB6A";
                }
                

                if (ContadorGeneral == 0) {
                    datos.push(
                        {
                            data: [[evolucion2[j][0], evolucion2[j][1]]],
                            points: {
                                show: true,
                                fillColor: colorPunto,
                                color: '#000000',
                                radius: 5,
                            }
                        }
                    );

                }
            }

            //console.log(datos);
            ContadorGeneral++;

            ticks = [

                [mas1[mas1.length - 1][1], "+1"],
                [mas2[mas2.length - 1][1], "+2"],
                [mas3[mas3.length - 1][1], "+3"],
                [median[median.length - 1][1], " 0"],
                [menos1[menos1.length - 1][1], "-1"],
                [menos2[menos2.length - 1][1], "-2"],
                [menos3[menos3.length - 1][1], "-3"]
            ];

        }

        var xmin = median[0][0]; //valor minmo para el eje x
        var xmax = median[median.length - 1][0];//valor maximo para el eje x

        /* Formatear puntos del eje X */
        var tickFormatter;
        var noTicks;
        var zoomMax;
        var zoomMin;
        if (grafico == "PT" || grafico == "PL") {
            zoomMin = 1;
            zoomMax = 5.2;
            tickFormatter = function (n) {
                if (n >= 0) {
                    return parseInt(n);
                } else {
                    return "";
                }
            }
            noTicks = 7;
        } else {
            zoomMin = 30;//40
            zoomMax = 300;//185.6
            tickFormatter = function (n) {
                if ((n >= 0) && (n % 183 == 0)) {
                    return ((n / 183) * 6);
                } else {
                    return "";
                }
            }
            noTicks = 2000;
        }

        var options;

        /*determinar puntos de zoom*/
        var pointXinicial,
            pointYinicial,
            pointXfinal,
            pointYfinal,
            pointYmax,
            pointYmin;

        var aux = [];


        if (evolucion.length) {
            if (evolucion.lenght == 1) {
                pointXinicial = evolucion[0][0];
                pointYinicial = evolucion[0][1];
                pointXfinal = evolucion[0][0];
                pointYfinal = evolucion[0][1];
            } else {
                pointXinicial = evolucion[0][0];
                pointYinicial = evolucion[0][1];
                pointXfinal = evolucion[evolucion.length - 1][0];
                pointYfinal = evolucion[evolucion.length - 1][1];
            }
            $.each(evolucion, function (key, value) {
                aux.push(this[1]);
            });
            aux.sort(function (a, b) { return a - b; });
            pointYmin = aux[0];
            pointYmax = aux[aux.length - 1];
            /*rectificar los ticks de acuerdo al zoom incial*/
            var factor = 8;
            switch (grafico) {
                case "PCE":
                    factor = 6;
                    break;
                case "IMC":
                    factor = 4;
                    break;
                case "PE":
                    factor = 6;
                    break;
            }
            var endMaxEjeX = parseInt(pointXfinal) + ((parseInt(xmax) - parseInt(xmin)) / factor);
            if (grafico == "PT" || grafico == "PL") {
                endMaxEjeX = Math.round(endMaxEjeX * Math.pow(10, 1)) / Math.pow(10, 1);
                if (endMaxEjeX > 120) {
                    endMaxEjeX = 120;
                }
                $.each(json, function (key, value) {
                    if (this[xK] == endMaxEjeX) {
                        ticks = [
                            [this["1 SD"], "+1"],
                            [this["2 SD"], "+2"],
                            [this["3 SD"], "+3"],
                            [this["Median"], " 0"],
                            [this["-1 SD"], "-1"],
                            [this["-2 SD"], "-2"],
                            [this["-3 SD"], "-3"]
                        ];
                        return false;
                    }
                });
            } else {
                endMaxEjeX = Math.round(endMaxEjeX * Math.pow(10, 0)) / Math.pow(10, 0);

                if (grafico == "PE" || grafico == "TE" || grafico == "IMC") {
                    endMaxEjeX = ObtenerRangoEjeX(endMaxEjeX);
                } else {
                    if (endMaxEjeX > 1856) {
                        endMaxEjeX = 1856;
                    }
                }

                var YaEncontroRango = false;
                $.each(json, function (key, value) {
                    if (this[xK] == endMaxEjeX) {
                        ticks = [
                            [this["1 SD"], "+1"],
                            [this["2 SD"], "+2"],
                            [this["3 SD"], "+3"],
                            [this["Median"], " 0"],
                            [this["-1 SD"], "-1"],
                            [this["-2 SD"], "-2"],
                            [this["-3 SD"], "-3"]
                        ];

                        YaEncontroRango = true;
                        return false;
                    }

                    if (this[xK] > endMaxEjeX && !YaEncontroRango ) {
                        ticks = [
                            [this["1 SD"], "+1"],
                            [this["2 SD"], "+2"],
                            [this["3 SD"], "+3"],
                            [this["Median"], " 0"],
                            [this["-1 SD"], "-1"],
                            [this["-2 SD"], "-2"],
                            [this["-3 SD"], "-3"]
                        ];

                        YaEncontroRango = true;
                        return false;
                    }
                });
            }

            /*OPCIONES*/
            var YAxisMax = (parseInt(pointYmax) + ((parseInt(ymax) - parseInt(ymin)) / factor)) * 1.3;

            options = {
                title: titulo,
                xaxis: {
                    noTicks: noTicks,
                    tickFormatter: tickFormatter,
                    min: parseInt(pointXinicial) - ((parseInt(xmax) - parseInt(xmin)) / factor) > 0 ? parseInt(pointXinicial) - ((parseInt(xmax) - parseInt(xmin)) / factor) : 0,
                    max: parseInt(pointXfinal) + ((parseInt(xmax) - parseInt(xmin)) / factor),

                    title: eje["x"]
                },
                yaxis: {
                    min: parseInt(pointYmin) - ((parseInt(ymax) - parseInt(ymin)) / factor),
                    max: parseInt(pointYmax) + ((parseInt(ymax) - parseInt(ymin)) / factor),

                    title: eje["y"]
                },
                y2axis: {
                    ticks: ticks,
                    color: '#545454',
                    min: parseInt(pointYmin) - ((parseInt(ymax) - parseInt(ymin)) / factor),
                    max: parseInt(pointYmax) + ((parseInt(ymax) - parseInt(ymin)) / factor),
                },
                grid: {
                    verticalLines: false,
                    backgroundColor: 'white',
                    outline: 'ns',
                },
                HtmlText: false,
                colors: lineColor,
                shadowSize: 0,
            }
        } else {
            /*OPCIONES*/
            options = {
                title: titulo,
                xaxis: {
                    noTicks: noTicks,
                    tickFormatter: tickFormatter,
                    min: parseInt(xmin),
                    max: parseInt(xmax),
                    title: eje["x"]
                },
                yaxis: {
                    min:
                    ymin - 3,
                    max:
                    ymax + 3,
                    title: eje["y"]
                },
                y2axis: {
                    ticks: ticks,
                    color: '#545454',
                    min: ymin - 3,
                    max: ymax + 3
                },
                grid: {
                    verticalLines: false,
                    backgroundColor: 'white',
                    outline: 'ns',
                },
                HtmlText: false,
                colors: lineColor,
                shadowSize: 0
            }
        }

        //console.log(options);

        var drawGraph;
        var container = document.getElementById(grafico);
        $("#" + grafico.toString() + "Cargado").val(Number($("#" + grafico.toString() + "Cargado").val()) + 1);
        
        drawGraph = function (vOptions) {
            return Flotr.draw(
                container,
                datos,
                Flotr._.extend(Flotr._.clone(options), vOptions || {})
                );
        }
        // var _start;
        var graph = drawGraph();

        switch (grafico) {
            case "TE":
                graficoTECanvas = graph;
                break;
            case "PE":
                graficoPECanvas = graph;
                break;
            case "IMC":
                graficoIMCCanvas = graph;
                break;
        }

        $(".flotr-canvas").css("width", "100%");
        $(".flotr-overlay").css("width", "100%");

        /***********DESCARGAS DE IMAGENES***********/
        var Descargar = function (operation) {
            return graph.download.saveImageSRC("jpeg");
        };
        switch (grafico) {
            case "TE":
                $("#graphTE").val(Descargar("download"));
                break;
            case "PE":
                $("#graphPE").val(Descargar("download"));
                break;
            case "IMC":
                $("#graphIMC").val(Descargar("download"));
                break;
        }
        /********************************************/
        /*Si estamos en la página para PDF generamos la gráfica como PNG*/
        if(typeof $("#AntroParaPDF").val() !== "undefined"){
            graph.download.saveImage("jpeg", null, null, true);
            
            $.post("http://localhost:56580/PDF/CookieGrafico", { Archivo: ($("#" + grafico).find("img").attr("src")), Grafico: grafico }).done(function () {
                var newURL = "http://localhost:56580/PDF/Base64ToJPG?Grafico=" + grafico;

                //$("#" + grafico).find("img").attr("src", newURL);
                $("#" + grafico + "img").attr("src", newURL);
            });

            switch (grafico) {
                case "IMC":
                    setTimeout(
                        function () {
                            $("#ContenedorGraficos").css("display", "none");
                            window.status = 'readytoprint';
                        }, 6000);
                    break;
            }
        }
        
        /****************************************************************/

        /*Eventos sobre el grafico*/
        /* CLICK */
        Flotr.EventAdapter.observe(container, 'flotr:click', function (position) {
            ResaltaFilaAntro(position);
            //$("#fichaAntropometria").find("div[type='fila'][indice='" + indice + "']").css("background-color", "#FF0000");
        });
        
        /* ====================================================================
            The act of zooming - mousewheel
        ==================================================================== */
        Flotr.EventAdapter.observe(graph.overlay, 'mousewheel', function (e) {
            e.preventDefault();

            var
            xaxis = graph.axes.x,
            yaxis = graph.axes.y,
            y2axis = graph.axes.y2,
            wheelData = e.originalEvent.detail
            ? e.originalEvent.detail * -1
            : e.originalEvent.wheelDelta / 40;
            var zoom = Math.abs((xaxis.max - xaxis.min) / 10 * (wheelData / Math.abs(wheelData)));
            if (grafico == "PT" || grafico == "PL") {
                var index = parseFloat((Math.round((xaxis.max - (xaxis.max - xaxis.min) / 10 * (wheelData / Math.abs(wheelData))) * 100) / 100).toFixed(1));
                if (index > 120) {
                    index = 120;
                }
                $.each(json, function (key, value) {
                    if (this[xK] == index) {
                        ticks = [
                            [this["1 SD"], "+1"],
                            [this["2 SD"], "+2"],
                            [this["3 SD"], "+3"],
                            [this["Median"], " 0"],
                            [this["-1 SD"], "-1"],
                            [this["-2 SD"], "-2"],
                            [this["-3 SD"], "-3"]
                        ];
                        return false;
                    }
                });
            } else {
                //var index = parseFloat((Math.round((xaxis.max - (xaxis.max - xaxis.min) / 10 * (wheelData / Math.abs(wheelData))) * 100) / 100).toFixed(1));
                var index = parseFloat((Math.round((xaxis.max - (xaxis.max - xaxis.min) / 10 * (wheelData / Math.abs(wheelData))) * 100) / 100).toFixed(0));
                if (grafico == "PE" || grafico == "TE" || grafico == "IMC") {
                    index = ObtenerRangoEjeX(index);
                } else {
                    if (index > 1856) {
                        index = 1856;
                    }
                }
                $.each(json, function (key, value) {
                    if (this[xK] == index) {
                        ticks = [
                            [this["1 SD"], "+1"],
                            [this["2 SD"], "+2"],
                            [this["3 SD"], "+3"],
                            [this["Median"], " 0"],
                            [this["-1 SD"], "-1"],
                            [this["-2 SD"], "-2"],
                            [this["-3 SD"], "-3"]
                        ];
                        return false;
                    }
                });
                //cambiar tick durante el zoom (solo para grafico donde se muestre meses)
                if (zoom > 180) {
                    noTicks = 2000;
                    tickFormatter = function (n) {
                        if ((n >= 0) && (n % 366 == 0)) {
                            return ((n / 366) * 12);
                        } else {
                            return "";
                        }
                    }
                } else if (zoom > 90 && zoom <= 180) {
                    noTicks = 6000;
                    tickFormatter = function (n) {
                        if ((n >= 0) && (n % 183 == 0)) {
                            return ((n / 183) * 6);
                        } else {
                            return "";
                        }
                    }
                } else if (zoom <= 90) {
                    tickFormatter = function (n) {
                        noTicks = 10000;
                        if ((n >= 0) && ((n % 30.5 == 0))) {
                            return ((n / 30.5));
                        } else {
                            return "";
                        }
                    }
                }
            }
            if (wheelData > 0) //zoom in
            {
                if (zoom >= zoomMin) {
                    // Redrawl the graph with new axis
                    graph = drawGraph({
                        xaxis: {
                            title: eje["x"],
                            min: xaxis.min + (xaxis.max - xaxis.min) / 10 * (wheelData / Math.abs(wheelData)),
                            max: xaxis.max - (xaxis.max - xaxis.min) / 10 * (wheelData / Math.abs(wheelData)),
                            tickFormatter: tickFormatter,
                            noTicks: noTicks
                        },
                        yaxis: {
                            title: eje["y"],
                            min: yaxis.min + (yaxis.max - yaxis.min) / 10 * (wheelData / Math.abs(wheelData)),
                            max: yaxis.max - (yaxis.max - yaxis.min) / 10 * (wheelData / Math.abs(wheelData))
                        },
                        y2axis: {
                            min: y2axis.min + (y2axis.max - y2axis.min) / 10 * (wheelData / Math.abs(wheelData)),
                            max: y2axis.max - (y2axis.max - y2axis.min) / 10 * (wheelData / Math.abs(wheelData)),
                            ticks: ticks,
                        },
                    });
                }
            } else { //zoom out
                if (zoom <= zoomMax) {
                    // Redrawl the graph with new axis
                    graph = drawGraph({
                        xaxis: {
                            title: eje["x"],
                            min: xaxis.min + (xaxis.max - xaxis.min) / 10 * (wheelData / Math.abs(wheelData)),
                            max: xaxis.max - (xaxis.max - xaxis.min) / 10 * (wheelData / Math.abs(wheelData)),
                            tickFormatter: tickFormatter,
                            noTicks: noTicks
                        },
                        yaxis: {
                            title: eje["y"],
                            min: yaxis.min + (yaxis.max - yaxis.min) / 10 * (wheelData / Math.abs(wheelData)),
                            max: yaxis.max - (yaxis.max - yaxis.min) / 10 * (wheelData / Math.abs(wheelData))
                        },
                        y2axis: {
                            min: y2axis.min + (y2axis.max - y2axis.min) / 10 * (wheelData / Math.abs(wheelData)),
                            max: y2axis.max - (y2axis.max - y2axis.min) / 10 * (wheelData / Math.abs(wheelData)),
                            ticks: ticks,
                        },
                    });
                }
            }
        });

        /* ====================================================================
                The act of moving panel
           ==================================================================== */
        Flotr.EventAdapter.observe(graph.overlay, 'mousedown', function (e) {
            _start = graph.getEventPosition(e);
            Flotr.EventAdapter.observe(document, 'mousemove', Panning);
            Flotr.EventAdapter.observe(document, 'mouseup', PanStop);
        });
        /* ====================================================================
            Stop the Dragging Funciton - panning
           ==================================================================== */
        function PanStop() {
            Flotr.EventAdapter.stopObserving(document, 'mousemove', Panning);
        }
        
        /* ====================================================================
            The act of dragging - panning
           ==================================================================== */
        function Panning(e) {
            var end = graph.getEventPosition(e),
            xaxis = graph.axes.x,
            yaxis = graph.axes.y,
            y2axis = graph.axes.y2,
            offsetX = _start.x - end.x,
            offsetY = _start.y - end.y;
            if (grafico == "PT") {
                var index = parseFloat((Math.round((xaxis.max + offsetX) * 100) / 100).toFixed(1));
                if (index > 120) {
                    index = 120;
                }
                $.each(json, function (key, value) {
                    if (this[xK] == index) {
                        ticks = [
                    [this["1 SD"], "+1"],
                    [this["2 SD"], "+2"],
                    [this["3 SD"], "+3"],
                    [this["Median"], " 0"],
                    [this["-1 SD"], "-1"],
                    [this["-2 SD"], "-2"],
                    [this["-3 SD"], "-3"]
                        ];
                        return false;
                    }
                });
            } else {
                //var index = parseFloat((Math.round((xaxis.max + offsetX) * 100) / 100).toFixed(1));
                var index = parseFloat((Math.round((xaxis.max + offsetX) * 100) / 100).toFixed(0));
                if (grafico == "PE" || grafico == "TE" || grafico == "IMC") {
                    index = ObtenerRangoEjeX(index);
                } else {
                    if (index > 1856) {
                        index = 1856;
                    }
                }

                $.each(json, function (key, value) {
                    if (this[xK] == index) {
                        ticks = [
                            [this["1 SD"], "+1"],
                            [this["2 SD"], "+2"],
                            [this["3 SD"], "+3"],
                            [this["Median"], " 0"],
                            [this["-1 SD"], "-1"],
                            [this["-2 SD"], "-2"],
                            [this["-3 SD"], "-3"]
                        ];
                        return false;
                    }
                });

                //console.log(this);
            }

            // Redrawl the graph with new axis
            graph = drawGraph({
                xaxis: {
                    title: eje["x"],
                    min: xaxis.min + offsetX,
                    max: xaxis.max + offsetX,
                    tickFormatter: tickFormatter,
                    noTicks: noTicks
                },
                yaxis: {
                    title: eje["y"],
                    min: yaxis.min + offsetY,
                    max: yaxis.max + offsetY
                },
                y2axis: {
                    min: y2axis.min + offsetY,
                    max: y2axis.max + offsetY,
                    ticks: ticks,
                }
            });
        }
    });
    /*	}
	});*/
}


function getEvolucion(grafico) {
    switch (grafico) {
        case "PE":
            return $("#GraficoAntroHistoPE").html();
            break;
        case "TE":
            return $("#GraficoAntroHistoTE").html();
            break;
        case "IMC":
            return $("#GraficoAntroHistoIMC").html();
            break;
    }
}

function ResaltaFilaAntro(_Position) {
    var hit = _Position.hit;

    //console.log(_Position);

    if (typeof hit !== "undefined") {

        var FichaAntro = $("#fichaAntropometria");
        var FilaSeleccionada = FichaAntro.find("a[id='fila_" + hit.index + "']");

        //quitamos color de cada fila
        FilaSeleccionada.click();

    }
}

function ResaltaFilaAntroByIndex(_Index) {
    if (typeof _Index !== "undefined") {

        var FichaAntro = $("#fichaAntropometria");
        var FilaSeleccionada = FichaAntro.find("div[id='fila_" + _Index + "']");

        FilaSeleccionada.click();
    }
}

function CliclTrAntro(_TrOrigen) {
    var FichaAntro = $("#fichaAntropometria");

    FichaAntro.find("a[type='filtroAntro']").each(function () {
        if ($(this).attr("id") != _TrOrigen.attr("data-target").replace("#", "")) {
            $(this).removeClass("active");
        }
    });
}

function ObtenerRangoEjeX(_ejeX) {
    var listo = false;
    var ejeX = parseFloat(_ejeX);
    var ejexBase = 1886.00
    var ejexBaseAnterior = 0.00
    var mes = 30.4167

    while (!listo) {
        if (ejeX >= 1886.00) {
            //establecemos la posición del ejexBase anterior
            ejexBaseAnterior = ejexBase;
            //establecemos la posición del ejexBase actual
            ejexBase = ejexBase + mes;

            if (ejeX >= ejexBaseAnterior && ejeX <= ejexBase) {
                ejeX = ejexBaseAnterior;
            }

            if (ejeX == ejexBaseAnterior) {
                listo = true;
            }
        } else {
            listo = true;
        }
    }

    return Math.round(ejeX);
}
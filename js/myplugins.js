var myLoading = (function() {
  return {
    Ocultar: function(Tarjeta) {
      $("div.PL_LoadingBox[id='" + Tarjeta + "']").delay(100).fadeOut('slow');
    },
    Mostrar: function(Tarjeta) {
      $("div.PL_LoadingBox[id='" + Tarjeta + "']").fadeIn('slow');
    }
  }
})(myLoading||{});

var modales = (function() {
  return {
    Abrir: function(modalId) {
      $('#' + modalId).appendTo("body").modal('show');
    }
  }
})(modales||{});

var carouselFotos = (function() {
  return {
    cargar: function(_divId,_CantidadFotos) {
      var _IndicadoresFotos = "";
      for(var i = 0; i<_CantidadFotos; i++){
          _IndicadoresFotos += "<li data-target=\"#" + _divId + "\" data-slide-to=\"" + i + "\" class=\"" + (i==0 ? 'active':'') + "\"></li>";
      }
      $('#' + _divId).find("ol.carousel-indicators").html(_IndicadoresFotos);
    }
  }
})(modales||{});

var myModalCPIActividades = (function() {
  return {
    Abrir: function(_CantidadFotos) {
      var _IndicadoresFotos = "";
      for(var i = 0; i<_CantidadFotos; i++){
          _IndicadoresFotos += "<li data-target=\"#carouselActividades\" data-slide-to=\"" + i + "\" class=\"" + (i==0 ? 'active':'') + "\"></li>";
      }

      $('#ModalActividadesCPI').appendTo("body").modal('show');
      $('#ModalActividadesCPI').find("ol.carousel-indicators").html(_IndicadoresFotos);
    }
  }
})(myModalCPIActividades||{});

var myModalConinActividades = (function() {
  return {
    Abrir: function(_CantidadFotos) {
      var _IndicadoresFotos = "";
      for(var i = 0; i<_CantidadFotos; i++){
          _IndicadoresFotos += "<li data-target=\"#carouselActividadesConin\" data-slide-to=\"" + i + "\" class=\"" + (i==0 ? 'active':'') + "\"></li>";
      }

      $('#ModalActividadesConin').appendTo("body").modal('show');
      $('#ModalActividadesConin').find("ol.carousel-indicators").html(_IndicadoresFotos);
    }
  }
})(myModalConinActividades||{});

var myModalTestResultados = (function() {
  return {
    Abrir: function(_Titulo, _Fecha, _URL) {
      $('#ModalTestResultado').appendTo("body").modal('show');
      $('#ModalTestResultado').find("h4[name='ModalTitle']").html((_Titulo + " (" + _Fecha + ")"));
      //$('#ModalTestResultado').find("img[name='FotoTest']").attr("src", _URL);
      $('#ModalTestResultado').find("iframe[name='TestResult']").attr("src", _URL);
    }
  }
})(myModalTestResultados||{});

var myModalTestResultadosConin = (function() {
  return {
    Abrir: function(_Titulo, _Fecha, _URL) {
      $('#ModalTestResultadoConin').appendTo("body").modal('show');
      $('#ModalTestResultadoConin').find("h4[name='ModalTitle']").html((_Titulo + " (" + _Fecha + ")"));
      //$('#ModalTestResultado').find("img[name='FotoTest']").attr("src", _URL);
      $('#ModalTestResultadoConin').find("iframe[name='TestResult']").attr("src", _URL);
    }
  }
})(myModalTestResultados||{});

var LoginActive = (function() {
  return {
    Activar: function() {
      var bodyClass = typeof $("body").attr("class") === "undefined" ? "" : $("body").attr("class");
      if(bodyClass.indexOf("body-login") > 0){

      }else{
        $("body").attr("class", bodyClass + " body-login");
        
        var elems = document.querySelectorAll('.js-switch');
        if(elems.length > 0){
          var switchery = new Switchery(elems[0], { size: 'small', color: '#484785', secondaryColor: '#393869' });	
          switchCargado = true;
        }	
      }
      
    }
  }
})(LoginActive||{});

var myStringFunctions = (function() {
  return {
    Capitalizar: function(_Texto, _CadaPalabra) {
      if(_CadaPalabra){
            var TextoSplit = _Texto.split(" ");
            _Texto = "";
        
            TextoSplit.forEach((palabra) => {
                if (palabra.length > 1) {
                    _Texto += palabra.charAt(0).toUpperCase() + palabra.slice(1).toLowerCase() + " ";
                } else {
                    _Texto += palabra.toUpperCase() + " ";
                }
            });

        }else{
            if (_Texto.length > 1) {
                _Texto = _Texto.charAt(0).toUpperCase() + _Texto.slice(1).toLowerCase();
            } else {
                _Texto = _Texto.toUpperCase();
            }
        }
        return _Texto;
    }
  }
})(myStringFunctions||{});